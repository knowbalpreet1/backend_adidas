from django.conf.urls import url, include
from django.conf import settings

from django.conf.urls.static import static

from django.contrib import admin

#from rest_framework_swagger.views import get_swagger_view

admin.site.site_header ='GoPlayBook Administration'

#schema_view = get_swagger_view(title='GoPlayBook API')
urlpatterns = [
#    url(r'^$', schema_view),
    url(r'^admin/', admin.site.urls),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')), 
    url(r'^api/auth/', include('authentication.urls', namespace = 'authentication_urls')),
    url(r'^api/cricket/scorecard/v1/', include('scorecard.urls', namespace = 'scorecard_urls')),
    url(r'^api/venue/',include('venue.urls', namespace='venue')),
    url(r'^api/event/', include('event.urls',namespace='event_urls')),
    url(r'^api/tournament/v1/', include('tournament.urls',namespace='tournament_urls')),
    url(r'^api/players/v1/', include('players.urls',namespace="all_player")),

    url(r'api/search/v1/', include('search.urls')),

    url(r'^api/push/v1/', include('notification.urls',namespace="notification")),
    url(r'^api/admin/', include('businessapp.urls',namespace="businessapp")),


]#+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
