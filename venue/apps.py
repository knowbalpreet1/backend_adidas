# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class VenueConfig(AppConfig):
    name = 'venue'
    def ready(self):
    	import venue.signals