# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from .models import Slot, Venue, VenueImg , Court_User, Venue_Amenitie,Venue_Review , Court,Holidays, Activities,VenueMapAmenity,SlotCheck,Venue_Followers,Bookmark, Court_Reservation, VenueAdminPermissionMap, VenueManagerPermissionMap

class VenueAdmin(admin.ModelAdmin):
    model = Venue
    list_display = ['name', 'is_active', 'closing', 'created_at', 'average_rating' ]

admin.site.register(Venue, VenueAdmin)
admin.site.register(Venue_Review)
admin.site.register(Venue_Amenitie)
admin.site.register(Court)
admin.site.register(Holidays)
admin.site.register(Activities)
admin.site.register(Slot)
admin.site.register(VenueImg)
admin.site.register(VenueMapAmenity)
admin.site.register(SlotCheck)
admin.site.register(Venue_Followers)
admin.site.register(Bookmark)
admin.site.register(Court_Reservation)
admin.site.register(VenueAdminPermissionMap)
admin.site.register(VenueManagerPermissionMap)
admin.site.register(Court_User)
