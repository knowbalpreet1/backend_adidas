from django.db import models
from authentication.models import Account	
from businessapp.models import BusinessUser
import players
from businessapp.models import BusinessUser, BasePermissions, ChildPermissions
from geopy import units, distance

class geoManager(models.Manager):
	def nearby(self, latitude, longitude, proximity):
		rough_distance = units.degrees(arcminutes=units.nautical(kilometers=proximity)) * 2
		queryset = self.get_queryset().filter(
			address__latitude__range = (latitude - rough_distance, latitude + rough_distance),
			address__longitude__range =(longitude - rough_distance, longitude + rough_distance) ,
			is_active=True
			)

		locations = []
		for obj in queryset:
			if obj.address.latitude and obj.address.longitude:
				exact_distance = distance.distance(
					(latitude, longitude),
					(obj.address.latitude, obj.address.longitude)
				).kilometers

				if exact_distance <= proximity:
					locations.append(obj)
		queryset = queryset.filter(id__in=[l.id for l in locations])
		return queryset

# class VenueManager(models.Manager):
# 	def create_venue():
# 		print('inside create>>>>>>>>>>>>>>')		
# 		pass

class Venue(models.Model):
	name =  models.CharField(max_length= 100,blank= True, null= True)
	time_of_opening = models.TimeField(null= True, blank= True)
	owner = models.ForeignKey(BusinessUser,null= True, blank= True)

	closing = models.TimeField(null = True, blank = True)
	contact_name = models.CharField(max_length = 100, blank = True, null = True)
	contact = models.CharField(max_length= 10, blank= True, null= True)
	contact1 = models.CharField(max_length = 10, blank = True, null = True)
	is_active = models.BooleanField(default = False)
	activated = models.BooleanField(default = True)
	bio = models.TextField(blank= True, null= True)
	created_at = models.DateField(blank=True,null=True,auto_now_add=True)
	is_rated = models.BooleanField(default= False)
	is_functional = models.BooleanField(default=False)
	average_rating = models.FloatField(default= 0.0)
	address = models.ForeignKey('players.Location',null=True)
	email = models.EmailField(null = True, blank=True)
	cost_per_game = models.FloatField(default= 0.0)


	objects = models.Manager()
	gis_manager= geoManager()
	def __str__(self):
		if self.name:
			return self.name	
		return '----'

	def __init__(self, *args, **kwargs):
		super(Venue, self).__init__(*args, **kwargs)
		self.__is_active = self.is_active
		self._is_active = self.is_active


class VenueImg(models.Model):
	url = models.ImageField(upload_to='media', null=True, blank=True, max_length = 10000)
	venue = models.ForeignKey(Venue,null=True)	

class Court(models.Model):
	name = models.CharField(max_length=100,blank=True,null=True)
	venue = models.ForeignKey(Venue,null=True)
	is_active = models.BooleanField(default = True,null=False)
	is_current = models.BooleanField(default=False,null=False)

	def __str__(self):
		if self.venue:
			return self.venue.name	
		return self.name


class Activities(models.Model):
	court = models.ForeignKey(Court,null=True)
	sport = models.ForeignKey('players.SportsType',null=True)

	def __str__(self):
		if self.court and self.sport:
			return self.court.venue.name +' - ' + self.sport.name
		return '-------'


class Slot(models.Model):
	court = models.ForeignKey(Court,null=True)
	day = models.CharField(max_length=10,blank=True,null=True)
	start_time = models.TimeField(null = True, blank = True)
	end_time = models.TimeField(null = True, blank = True)
	is_booked = models.BooleanField(null=False, default = False)
	price = models.FloatField(null=False,default=0.0)
	is_default = models.BooleanField(null=False,default=False)

	def __str__(self):
		if self.court:
			return self.court.name + '--' + self.day
		return self.day

class Holidays(models.Model):
	venue = models.ForeignKey(Venue,null=True)
	date = models.DateField(blank=True,null=True)
	
	def __str__(self):
		if self.venue and self.date:
			return self.venue.name + ' --> ' + self.date.__str__()
		return "-|-|-|-|-"


class Venue_Review(models.Model):
	rating = models.FloatField(blank=True, null=True)
	user = models.ForeignKey(Account,null= True)
	venue = models.ForeignKey(Venue, null = True)
	description = models.CharField(max_length = 600, blank = True, null= True)

	def __str__(self):
		if self.venue and self.user and self.user.name:
			return self.venue.name + '--->' + self.user.name
		return '-|-|-'

class Venue_Amenitie(models.Model):
	amen_name = models.CharField(max_length=100, blank = True, null = True)
	

	#def __str__(self):
	#	return self.venue.name	

class VenueMapAmenity(models.Model):
	venue = models.ForeignKey(Venue, null = True)
	amenity = models.ForeignKey(Venue_Amenitie, null=True,blank=True)


class SlotCheck(models.Model):
	court = models.ForeignKey(Court,null=True)
	day = models.CharField(max_length=10,null=True,blank=True)
	start_time = models.TimeField(null = True, blank = True)
	end_time = models.TimeField(null = True, blank = True)
	is_booked = models.BooleanField(null=False, default = False)
	price = models.FloatField(null=False,default=0.0)
	date = models.DateField(null=True,blank=True)
	def __str__(self):
		if self.court:
			return self.court.name + '--' + self.date.__str__()
		return self.day

class Venue_Followers(models.Model):
	player = models.ForeignKey('players.Players',null=True)
	venue = models.ForeignKey(Venue)
	def __str__(self):
		if self.player and self.venue:
			return self.player.user.name + ' ---> ' + self.venue.name
		return '-|-|-'
class Bookmark(models.Model):

	venue = models.ForeignKey(Venue,null=True)
	user = models.ForeignKey(Account,null=True)
	is_bookmark = models.BooleanField(default=False)
	
	def __str__(self):
		if self.venue and self.user:
			return self.user.name + ' ---> ' + self.venue.name
		return "-|-|-"
class Court_User(models.Model):
	name = models.CharField(max_length=100, null=True, blank=True)
	phone = models.CharField(max_length=13, null=True, blank=True)
	email = models.EmailField(max_length=252, null=True, blank=True)
	source = models.CharField(max_length=13, null=True, blank=True)


class Court_Reservation(models.Model):
	"""Court reservertion. It has slot relationship which has court relationship"""
	court_slot = models.ForeignKey(Slot, null=True, blank=True)
	user = models.ForeignKey(Account, null=True, blank=True)
	buser = models.ForeignKey(BusinessUser, null = True, blank = True)
	offline_user = models.ForeignKey(Court_User, null=True, blank=True)
	reserv_date = models.DateField(null=True, blank=True)
	amount_paid = models.CharField(max_length=50, null=True, blank=True)
	payment_mode = models.CharField(max_length=50, null=True, blank=True)
	check_in = models.BooleanField(default=False)
	reason = models.CharField(max_length= 400, null=True, blank=True)
	is_active = models.BooleanField(default=True)
	
	def __str__(self):
		return self.court_slot.court.venue.name + ' --------->  ' + self.reserv_date.strftime("%Y-%m-%d")



class VenueAdminPermissionMap(models.Model):
	user = models.ForeignKey(BusinessUser, related_name = 'venueownera')
	admin = models.ForeignKey(BusinessUser, related_name = 'venueadmin')
	permission = models.ManyToManyField(ChildPermissions, related_name = 'venueadminpermission')
	venue = models.ForeignKey(Venue)
	is_active = models.BooleanField(default = True)

	def __str__(self):
		return self.venue.name

class VenueManagerPermissionMap(models.Model):
	user = models.ForeignKey(BusinessUser, related_name = 'venueowenerm')
	manager = models.ForeignKey(BusinessUser, related_name = 'venuemanager')
	permission = models.ManyToManyField(ChildPermissions, related_name = 'venuemanagerpermission')
	venue = models.ForeignKey(Venue)
	is_active = models.BooleanField(default = True)

	def __str__(self):
		return self.venue.name
