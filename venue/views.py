from django.http import JsonResponse
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from venue.models import Venue,VenueImg, Venue_Review, Venue_Amenitie, Court, Activities, Slot,VenueMapAmenity,Holidays,Bookmark, Slot, VenueAdminPermissionMap
from django.conf import settings
from players.models import SportsType,City, State, Location
from scorecard.models import Match_Stat
from event.models import Event
from authentication.models import Account
from authentication.async_task import send_otp
import datetime,time
import traceback
import json
import re
from django.db.models import Q,Avg, Sum
from event.views import googledistance, distance as distance1
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from businessapp.models import ChildPermissions
import math
from businessapp.permissions import customPermission
from tournament.models import Tournament_Detail
weekday=['MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY','SUNDAY']
from dateutil.relativedelta import relativedelta


def cost_per_game(venue_id):
	try:
		day = datetime.today().strftime("%A")
		_ = Slot.objects.filter(court__venue_id = venue_id, day = str(day.upper())).aggregate(__ = Sum('price'))
		time_sum = 0
		slot = Slot.objects.filter(court__venue_id = venue_id, day = str(day.upper()))
		for i in slot:
			hour = i.end_time.hour - i.start_time.hour
			minu = i.end_time.minute - i.start_time.minute
			total = (hour*60 + minu)/60
			print(total)
			print('time diff ---> {0} - {1}==== '.format(i.end_time , i.start_time), 'slot----> ', i.day )
			print("{0}-{1} hours ----> {2}".format(i.end_time.hour , i.start_time.hour, i.end_time.hour - i.start_time.hour))
			print("{0}-{1} munites ----> {2}".format(i.end_time.minute , i.start_time.minute, i.end_time.minute - i.start_time.minute))
			time_sum += total 
		print(time_sum, _['__'])	
		price = 0.0
		venue = Venue.objects.get(id = venue_id)
#		time = venue.closing.hour - venue.time_of_opening.hour
#		print(time_sum, '-------------->>>>')
		if _['__'] and time_sum:
			price = float(_['__'])/time_sum
		return '%.0f'%price
		
	except Exception as e:
		traceback.print_exc()
		return str(e)

def event_on_address(venue_id):
        try:
                venue = Venue.objects.get(id = venue_id)
                address = venue.address
                event = Event.objects.filter(address = address).order_by("start_time")
                event_list = []

                for e in event:
                        image = None
                        if e.image:
                            image = e.image.image.url
                        start = e.start_time.strftime("%Y-%m-%d %I:%M %p")
                        end = e.end_time.strftime("%Y-%m-%d %I:%M %p")
                        event_list.append({'event_id':e.id,'event_name':e.name,'event_photo':image,'event_start_time':start,'event_end_time':end})
                #print(event_list)
                return event_list

        except Exception as e:
                traceback.print_exc()
                return str(e)

from django.db.models import Avg
def average_rating(venue_obj):
	try:
		venue_re_qs = Venue_Review.objects.filter(venue=venue_obj)
		rating = 0
		if venue_re_qs:
			rating = Venue_Review.objects.filter(venue = venue_obj).aggregate(Avg('rating'))['rating__avg']
			#print(rating, '------------------------>')

			venue_obj.average_rating = rating
			venue_obj.save()
			return round(rating)
		else:
			return None


#		rating = 0
#		print ("review ",venue_re_qs.count())
#		for i in venue_re_qs:
#			rating+= i.rating
#		if venue_re_qs.count()!=0:
#			i.venue.average_rating = rating
#			i.venue.save()
#			i.save()
#			return round(rating/venue_re_qs.count(), 1)
#		return None
	except Exception as e:
		traceback.print_exc()
		return None
#-------------------------------VENUE DETAILS----------------------------------------

class GetVenueList(APIView):
	"""GET THE LIST OF ALL VENUE ACCORDING TO USER FOR ADMIN APP"""
	permission_classes = [customPermission]
	def get(self,request):
		try:
			#print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
			#print(request.buser, '2343245235')
			page = request.GET['page']
			venue1 = [v for v in VenueAdminPermissionMap.objects.filter(admin = request.buser, venue__is_active = True, is_active = True)]
			#print(venue1)
			venue2 = [v for v in VenueManagerPermissionMap.objects.filter(manager = request.buser, venue__is_active = True, is_active = True)]
			#print(venue2)
			venue = venue1+venue2


		#	if not venue:
				#print(venue)
			paginator = Paginator(venue,10)
			#print('1')
			venue_obj=[]
			#print('2')
			#print(imgs)
			for i in paginator.page(page):
			#	print(i)
				imgs = VenueImg.objects.filter(venue=i.venue)
				try:
					imgs=imgs[0].url.url
				except:
					imgs=None
			#	print(imgs)
				obj={
					'venue_id':i.venue.id,
					'name':i.venue.name,
					'venue_img':imgs,
					'opening':i.venue.time_of_opening.strftime("%I:%M %p"),
					'closing':i.venue.closing.strftime("%I:%M %p"),
					'is_active':i.venue.activated}
				venue_obj.append(obj)
			#print(venue_obj)
			return Response(data={'data':venue_obj,'total_pages':paginator.num_pages},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data = {'error':str(e)},status=400)


class GetSearchVenue(APIView):
	"""GET THE LIST OF ALL VENUE FOR USER APP"""
	permission_classes = [TokenHasReadWriteScope]
	def get(self,request):
		try:
		#	print(request.query_params, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
			page = request.GET['page']
			lat = request.GET['lat']
			lng = request.GET['lng']
			status = request.GET['status']
			r_min = request.GET['r_min']
			r_max = request.GET['r_max']
			d_min = request.GET['d_min']
			d_max = request.GET['d_max']
			s_type = request.GET['s_type']
			venues = []
			venue = None
			#print(1,status,2,lat,lng)
			if(lat==""):
				lat = float(request.user.latitude)
				#print(lat)
			if(lng==""):
				lng = float(request.user.longitude)
			if(status=='true'):
				status = True
			else:
				status = False
			if(r_min=="" or r_min=="0.0"):
				r_min = 0.0
			else:
				r_min = float(r_min)
			if(r_max=="" or r_max=="5.0"):
				r_max = 5.0
			else:
				r_max=float(r_max)
			if(d_min=="" or d_min==0):
				d_min = 0
			if(d_max=="" or d_max==50):
				d_max = 200
#			print(lat,lng,status,r_min,r_max,d_min,d_max,s_type)
			c_time=datetime.now()
			if(r_min>=0.0 and r_max<=5.0):
#				print(r_min,r_max,1)
				venue = Venue.objects.filter(is_active = True , average_rating__gte = r_min, average_rating__lte= r_max).order_by('created_at')

			else:
#				print(4)
				venue = Venue.objects.all()
#				print(venue)

			if(status==True):
				venue=venue.filter(closing__gte=c_time,time_of_opening__lte=c_time, is_active=True)
			else:
				venue=venue.filter(is_active=True)
			venue_list=venue
		#	print(venue)
			#for v in venue:
			#	try:
			#		if s_type:
			#			sporttype = Activities.objects.filter(court__venue__id = v.id, sport__id= s_type )
			#		else:
			#			sporttype = Activities.objects.filter(court__venue__id = v.id)
			#		print("sporttype", sporttype,s_type)
			#		if sporttype:
			#			venue_list.append(v)
			#	except :
			#		traceback.print_exc()

		#	print(venue_list, '------')
			paginator = Paginator(venue_list,10)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			try:
				page_data = paginator.page(paging)
			except PageNotAnInteger:
				page_data = paginator.page(1)
			except EmptyPage:
				page_data = paginator.page(paginator.num_pages)
			for i in page_data.object_list:
		#		print(i)
				is_open = False
				opening = i.time_of_opening.strftime('%H%M%S')
				closing = i.closing.strftime('%H%M%S')
				current = datetime.now().strftime('%H%M%S')
				if(opening<current<closing):
					is_open = True

				dist = distance1(float(i.address.latitude),float(i.address.longitude),float(lat),float(lng))
#				if dist:
#					try:
#						print ("in dist")
#						dist = float(re.sub(',','',dist.split(' ')[0]))
#					except:
#						dist = 0
#				else:
#					dist = 0
				try:
					img = VenueImg.objects.filter(venue=i)[0].url.url

				except:
					dist = 0.0
					traceback.print_exc()
					img = None
				court = Court.objects.filter(venue=i)

				if float(d_min) <= dist <= float(d_max):
					obj={
						"id":i.id,
						"time_of_opening": i.time_of_opening.strftime('%I:%M %p'),
        					"bio": i.bio,
        					"name": i.name,
					        "is_functional": i.is_functional,
					        "avg_rating":i.average_rating,
					        "total_field": court.count(),
					        "closing": i.closing.strftime('%I:%M %p'),
					        "lng": i.address.latitude,
					        "is_active": i.activated,
					        "contact": i.contact,
					        "lat": i.address.latitude,
					        "created_at": i.created_at,
					        "cost_per_game": price,
					        "address": str(i.address.city.location_name),
					        "distance": dist,
						"venue_img":img,
						"is_open":is_open
					}
					venues.append(obj)
			#		print(obj)
				#print(venues)
				#print('123')
			return Response(data={'data':venues,'success':True,'last_page':last_page})
		except Exception as e:
			#print("------------> ", e)
			traceback.print_exc()
			return Response(data= {'error':str(e),'success':False},status=400)


class VenueDetail(APIView):

	permission_classes = [customPermission]
	def get(self, request, format=None):
		"""Return the detail of a venue"""
		# id = request.GET['id']
		# print(id)
		i_d = request.GET['id']
		try:
			venue_obj = Venue.objects.get(id=i_d)
			court = Court.objects.filter(venue=venue_obj, is_active=True)
			imgs = VenueImg.objects.filter(venue=venue_obj)
#			print(venue_obj)
			distance = None
		#	print(request.user.is_anonymous())
			if not request.user.is_anonymous():
				distance = googledistance(venue_obj.address.latitude,venue_obj.address.longitude,request.user.latitude,request.user.longitude)
			print(distance, '>>>>>>>>>>>>>>>>>>')
			is_open = False
			opening = venue_obj.time_of_opening.strftime('%H%M%S')
			closing = venue_obj.closing.strftime('%H%M%S')
			current = datetime.now().strftime('%H%M%S')
			#print(opening,closing,current)
			if(opening<current<closing):
				is_open = True
			address = {
				'city':venue_obj.address.city.name,
				'pin':venue_obj.address.city.pin,
				'state':venue_obj.address.city.state.name,
				'lat':venue_obj.address.latitude,
				'lng':venue_obj.address.longitude,
				'location':venue_obj.address.city.location_name
				}
			try:
				is_book = Bookmark.objects.get(venue= venue_obj,user=request.user).is_bookmark
				print(is_book)
			except:
				traceback.print_exc()
				is_book = False
			myrev=None
			try:
				rev=Venue_Review.objects.get(venue=venue_obj, user=request.user)
				img=None
				if(rev.user.profile_pic):
					img = rev.user.profile_pic.url
				myrev = {
					'user_id':rev.user.id,
					'name':rev.user.name,
					'profile_pic':img,
					'rating':rev.rating,
					'description':rev.description
					}
			except Exception as e:
					traceback.print_exc()

		# Description added..
			venue = {
				'id': venue_obj.id,
				'is_open': is_open,
				'name': venue_obj.name,
				'address': venue_obj.address.city.location_name,
				'time_of_opening': venue_obj.time_of_opening.strftime('%I:%M %p'),
				'closing': venue_obj.closing.strftime('%I:%M %p'),
				'contact': venue_obj.contact,
				'contact_2':venue_obj.contact1,
				'contact_name':venue_obj.contact_name,
				'full_address':address,
				# 'other_contact': venue_obj.other_contact,
				# 'dimension': venue_obj.dimension,
				# 'type_of_field': venue_obj.type_of_field,
				# 'organization': venue_obj.organization,
				# 'capacity': venue_obj.capacity,
				'distance':distance,
				'is_active': venue_obj.activated,
				'bio': venue_obj.bio,
				'cost_per_game': cost_per_game(venue_obj.id),
				'total_field': len(court),
				'created_at': venue_obj.created_at,
				# 'share_ground_link': venue_obj.share_ground_link,
				'is_functional': venue_obj.is_functional,
				'lat': venue_obj.address.latitude,
				'lng': venue_obj.address.longitude,
				'avg_rating':average_rating(venue_obj),
				# 'is_multi':venue_obj.is_multi,
				'email':venue_obj.email,
				'is_bookmarked':is_book,
				# 'type_of_sport':venue_obj.type_of_sport,
				'my_rev':myrev,
			}
			print(venue)

			#Activities added
			temp = []

			for j in court:
				act_obj = Activities.objects.filter(court = j)
				for i in act_obj:
#					print(i)
					temp.append(i.sport.name)

			temp = list(set(temp))


			#Amenities added
			amenitie = VenueMapAmenity.objects.filter(venue = venue_obj)
			amen_list=[]
			for i in amenitie:
				if(i!=None):
					amen={'name':i.amenity.amen_name,'id':i.amenity.id}
					amen_list.append(amen)
#			print(amen_list)


			#URLS added
			urls=[]
			for i in imgs:
				urls.append(i.url.url)
#			print(urls)

			#Reviews added
			revs={}
			revarr=[]
			reviews= Venue_Review.objects.filter(venue=venue_obj)
			count= len(reviews)
			if(count>0):
				for i in reviews[:3]:
#					print(reviews)
					img=None
					if(i.user.profile_pic):
						img = i.user.profile_pic.url
					revs={
						'user_id':i.user.id,
						'name':i.user.name,
						'profile_pic':img,
						'rating':i.rating,
						'description':i.description
					}
					revarr.append(revs)
#			print(revarr)
			#events added
			events = []
			events = event_on_address(i_d)

			#venue added
			venue_list=[]
			act = Activities.objects.filter(sport__name__in = temp).distinct("court__venue")
			print(temp)

#			act = Activities.objects.distinct('sport__name')
			print(act)
			for j in act:
#				print(j, '######################')
				if venue_obj.id != j.court.venue.id:
					venue_list.append(j.court.venue)
			similar = []

#			print(venue_list, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
			for i in venue_list[:5]:
#				print(i)
				img = VenueImg.objects.filter(venue=i)
				distance = None
				if not request.user.is_anonymous():
					distance = googledistance(img[0].venue.address.latitude,img[0].venue.address.longitude,request.user.latitude,request.user.longitude)
				if len(img)!=0:
					image = img[0].url.url
				else:
					image = None
	#			if venue_obj.id != i.id:
				ven = {
							'venue_id':i.id,
							'distance':distance,
							'name':i.name,
							'venue_img':image
						}
				similar.append(ven)
				#similar = [{'venue_id':104,'distance':'20.5','venue_img':'/media/media/images_1.jpeg',"name":"Firoz Shah"}]

			# raise Exception('ruk jaoo........')
#			print(events)
			return Response(data = {"desc":venue,'sport':temp,"img_urls":urls,'reviews':{'count':count,'data':revarr},'events':events,'similar_venue':similar,'amenities':amen_list,'success':True}, status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'success':False, 'error':str(e)},status=400)


	def post(self,request):
		"""Creates a new venue"""

		try:
			data= request.data
			#print("data",data)
			name = data.get('name')
			address = data.get('address')
			time_of_opening = data.get('time_of_opening')
			closing = data.get('closing')
			contact1 = data.get('contact1')
			contact2 = data.get('contact2')
			contact_name = data.get('contact_name')
			is_active = data.get('is_active')
			bio = data.get('bio')
			is_functional = data.get('is_functional')
			owner_id = request.buser
			cost_per_game = data.get('cost_per_game')
			img = data.get('img')
			email = data.get('email')
			amenity = data.get('amenity')
			# dimension = data.get('dimension')
			# organization = data.get('organization')
			# total_field = data.get('total_field')
			# created_at = data.get('created_at')
			# share_ground_link = data.get('share_ground_link')
			# lat = data.get('lat')
			# lng = data.get('lng')
			# capacity = data.GetReview('capacity')

			#print(request.buser.id)
			# validation for none and empty string..
			if(closing=="" or closing.startswith(' ')\
				 or closing== None or contact1==None or contact1=="" or contact1.startswith(' ')\
				 or is_active == None\

				 or is_functional == None or is_functional==""\
				 or owner_id=="" or owner_id==" " or owner_id==None\
				 # or other_contact=="" or other_contact== None\
				 # 1or dimension=="" or dimension.startswith(' ') or dimension == None\
				 # or type_of_sport == None or type_of_sport=="" or type_of_sport.startswith(' ')\
				 # or cost_per_game=="" or total_field==None\
				 # or total_field=="" or total_field<0\
				 # or share_ground_link == None or share_ground_link==""\
				 # or share_ground_link.startswith(' ') or is_default==None or is_default==""\
				 # or organization.startswith(' ') or organization=="" or organization==None\
				 ):
				#print(1)
				return Response(data={'error': 'Invalid Data'},status=400)
			else:

				# #Validate Owner id..
				# try:
				# 	if(not isinstance(owner_id,int)):
				# 		raise  Exception('Not a int')
				# 	owner = Account.objects.get(id=owner_id)
				# except Exception as e:
				# 	print(e)
				# 	return Response(data={'error':'owner id invalid'})

				#Validation for time..
				try:
					t1=datetime.strptime(time_of_opening, '%I:%M %p').strftime("%H%M%S")
					t2=datetime.strptime(closing, '%I:%M %p').strftime("%H%M%S")
					#print(t1,t2)
					#if(t1>=t2):
					#return Response(data={'error':'invalid time'},status=400)
				except ValueError as e:
					traceback.print_exc()
					return Response(data={'error':'invalid time'},status=400)

				#Validation for contact..
				try:
					if(not 0<len(contact1)<12):
						raise Exception('Invalid contact length..')
				except Exception as e:
					traceback.print_exc()
					#print(e)
					return Response(data={'error':str(e)},status=400)

				#Validation for cost_per_game, capacity, name, address, time_of_opening
				try:
					#if(not isinstance(cost_per_game,float) or cost_per_game<=0):
					#	raise Exception('Invalid cost per game')
					# if(capacity==None or capacity=="" or capacity<=0):
					# 	raise Exception('Invalid capacity')
					if(name==None or name=="" or name.startswith(' ') ):
						raise Exception('Invalid name')
					# if(address=="" or address == None):
					# 	raise Exception('Invalid address')
					if(time_of_opening==None or time_of_opening=="" or time_of_opening.startswith(' ')):
						raise Exception('Invalid time of opening')

					#print(owner_id,type(address))
					#Validating address
					state_name = State.objects.create(name=address['state'])
					city = City.objects.create(name = address['city'],pin=address['pin'], latitude=address['lat'], longitude= address['lng'],state=state_name,location_name = address['location'])
					location = Location.objects.create(city=city,latitude=address['lat'],longitude=address['lng'], name = address['location'])




				except Exception as e:
					traceback.print_exc()
					return Response(data={'error':str(e)},status=400)
				try:
					#owner = Account.objects.get(id=owner_id.id)
					venue_obj = Venue.objects.create(\
								owner = request.buser,\
								name = name ,\
								contact = contact1,\
								contact1 = contact2,\
								contact_name = contact_name,\
								address = location,\
								# other_contact = other_contact,\
								# dimension = dimension,\
								# type_of_sport = type_of_sport,\
								#organization = organization,\
								activated = is_active,\
								bio = bio,\
								#cost_per_game = cost_per_game,\
								# total_field = total_field,\
								# created_at = created_at,\
								# share_ground_link = share_ground_link,\
								is_functional = is_functional,\
								# lat=lat,\
								# lng=lng,\
								closing=datetime.strptime(closing,'%I:%M %p').strftime('%H:%M:%S'), \
								time_of_opening=datetime.strptime(time_of_opening,'%I:%M %p').strftime('%H:%M:%S'),\
								# capacity=capacity,\
								# is_multi = is_multi\
								email=email\
								)
					if(len(amenity)<1):
						raise Exception('invalid id..')
					for i in amenity:
						amenitys = Venue_Amenitie.objects.get(id = i)
						amen = VenueMapAmenity.objects.create(venue=venue_obj, amenity=amenitys)
						amen.save()

					venue_obj.save()
					chil = ChildPermissions.objects.filter(slug__in = settings.ADMIN_PERMISSIONS)
					#print(chil)
					v = VenueAdminPermissionMap.objects.create(user = request.buser, admin = request.buser, venue = venue_obj)
					v.permission.add(*chil)
					v.save()
					state_name.save()
					city.save()
					#send_otp('8800365323', '{0}  Requested For new venue: Name Of User: {1}, Phone Number: {2}, Email: {3}, Venue Name: {4}, Location: {5}'.format(venue_obj.owner.name, venue_obj.owner.name, venue_obj.owner.mobile, venue_obj.owner.email, venue_obj.name, venue_obj.address.city.location_name))
					return Response(data={'status':True, 'id':venue_obj.id}, status=201)
				except Exception as e:
					traceback.print_exc()
					return Response(data={'error':str(e)},status=400)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':'Invalid data'},status=400)

	def put(self,request, pk=None):
		"""Update Venue"""
		try:
			deactivate = request.data['deactivate']
			#print(request.data)
			venue_id = request.GET['id']
			if(deactivate==True):
				print('deactivating...',deactivate)
				venue = Venue.objects.filter(id=venue_id).update(activated=False)
			else:
				print('activating...')
				venue = Venue.objects.filter(id=venue_id).update(activated=True)
			return Response(data={'success':True},status=200)
		except Exception as e:
			traceback.print_exc()

		try:
			venue_id = request.GET['id']
			data= request.data
#			print("data",data)
			name = data.get('name')
			address = data.get('address')
			time_of_opening = data.get('time_of_opening')
			closing = data.get('closing')
			contact1 = data.get('contact1')
			contact2 = data.get('contact2')
			contact_name = data.get('contact_name')
			is_active = data.get('is_active')
			bio = data.get('bio')
			is_functional = data.get('is_functional')
			owner_id = request.buser
			cost_per_game = data.get('cost_per_game')
			img = data.get('img')
			email = data.get('email')
			amenity = data.get('amenity')
			delete = data.get('del')
			# dimension = data.get('dimension')
			# organization = data.get('organization')
			# total_field = data.get('total_field')
			# created_at = data.get('created_at')
			# share_ground_link = data.get('share_ground_link')
			# lat = data.get('lat')
			# lng = data.get('lng')
			# capacity = data.GetReview('capacity')

			#print(venue_id,request.buser)
			#-----------------GET VENUE--------------------------------
			venue = Venue.objects.filter(id=venue_id)
			# ------------------Get Address---------------------------
			#print(venue,closing, venue[0].address.name)
			state_id = venue[0].address.city.state.id
			city_id = venue[0].address.city.id
			location_id = venue[0].address.id
			#print(state_id,city_id)
			# validation for none and empty string..
			if(closing== None or contact1==None or contact1=="" or contact1.startswith(' ')\
				 or is_active == None\
				 or bio==None or bio=="" or bio.startswith(' ')

				 or is_functional == None or is_functional==""\
				 or owner_id=="" or owner_id==" " or owner_id==None\
				 # or other_contact=="" or other_contact== None\
				 # 1or dimension=="" or dimension.startswith(' ') or dimension == None\
				 # or type_of_sport == None or type_of_sport=="" or type_of_sport.startswith(' ')\
				 # or cost_per_game=="" or total_field==None\
				 # or total_field=="" or total_field<0\
				 # or share_ground_link == None or share_ground_link==""\
				 # or share_ground_link.startswith(' ') or is_default==None or is_default==""\
				 # or organization.startswith(' ') or organization=="" or organization==None\
				 ):
			#	print(1)
				return Response(data={'error': 'Invalid Data'},status=400)
			else:

				# #Validate Owner id..
				# try:
				# 	if(not isinstance(owner_id,int)):
				# 		raise  Exception('Not a int')
				# 	owner = Account.objects.get(id=owner_id)
				# except Exception as e:
				# 	print(e)
				# 	return Response(data={'error':'owner id invalid'})

				#Validation for time..
				try:
					t1=datetime.strptime(time_of_opening, '%I:%M %p').strftime("%H%M%S")
					t2=datetime.strptime(closing, '%I:%M %p').strftime("%H%M%S")
#					print(t1,t2)
					#if(t1>=t2):
					#	return Response(data={'error':'invalid time'},status=400)
				except ValueError as e:
					traceback.print_exc()
					return Response(data={'error':'invalid time'},status=400)

				#Validation for contact..
				try:
					if(not 0<len(contact1)<12):
						raise Exception('Invalid contact length..')
				except Exception as e:
					traceback.print_exc()
					return Response(data={'error':str(e)},status=400)

				#Validation for cost_per_game, capacity, name, address, time_of_opening
				try:
					#if(not isinstance(cost_per_game,float) or cost_per_game<=0):
					#	raise Exception('Invalid cost per game')
					# if(capacity==None or capacity=="" or capacity<=0):
					# 	raise Exception('Invalid capacity')
					if(name==None or name=="" or name.startswith(' ')):
						raise Exception('Invalid name')
					# if(address=="" or address == None):
					# 	raise Exception('Invalid address')
					if(time_of_opening==None or time_of_opening=="" or time_of_opening.startswith(' ')):
						raise Exception('Invalid time of opening')

					for i in delete:
						VenueImg.objects.get(url=i[7:]).delete()
					#Validating address
					state_name = State.objects.get(id=state_id)
					state_name.name=address['state']
					city = City.objects.get(id=city_id)
					city.name = address['city']
					city.pin=address['pin']
					city.latitude=address['lat']
					city.longitude= address['lng']
					city.state=state_name
					city.location_name = address['location']
					city.save()
					#print(city)
					location = Location.objects.filter(id=location_id).update(city=city,latitude=address['lat'],longitude=address['lng'])

					location = Location.objects.get(id=location_id)
					#print(state_name,city)

				except Exception as e:
					traceback.print_exc()
					return Response(data={'error':str(e)},status=400)
				try:
					#print('updating venue....')
					venue_obj = venue.update(
								owner = request.buser.id,
								name = name ,
								contact = contact1,
								contact1 = contact2,
								contact_name = contact_name,
								address = location,
								activated = is_active,
								bio = bio,
								is_functional = is_functional,
								closing=datetime.strptime(closing,'%I:%M %p').strftime('%H:%M:%S'), \
								time_of_opening=datetime.strptime(time_of_opening,'%I:%M %p').strftime('%H:%M:%S'),\
								email=email
								)
					if(len(amenity)<1):
						raise Exception('invalid id..')

					#print(venue[0].name)

					del_amen = VenueMapAmenity.objects.filter(venue=venue[0]).delete()
					for i in amenity:
						amenitys = Venue_Amenitie.objects.get(id = i)
						amen = VenueMapAmenity.objects.create(venue=venue[0], amenity=amenitys)

					print(request.data)
					return Response(data={'status':True}, status=201)
				except Exception as e:
					traceback.print_exc()
					return Response(data={'error':str(e)},status=400)

		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)

	def delete(self,request):
		"""Deletes a venue"""
		try:
			id = request.GET['id']
			venue = Venue.objects.get(id=id)
			venue.is_active = False
			venue.save()
			return Response({'status':True},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)


#---------------------------Court Details--------------------------------------

class CourtList(APIView):
	"""get the list of courts"""
	permission_classes = [customPermission]
	def get(self,request):
		try:
			id = request.GET['id']
			page =request.GET['page']
			court = Court.objects.filter(venue=id)
			paginator = Paginator(court,10)
			obj=[]
			for i in paginator.page(page):
				obj.append({'id':i.id,"name":i.name,'is_active':i.is_active})
			return Response(data={'data':obj,'total_pages':paginator.num_pages},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)


class CourtDetail(APIView):

	# permission_classes = [TokenHasReadWriteScop

	def get(self,request):
		try:
			court_id = request.GET['id']
			court = Court.objects.get(id=court_id)
			act = Activities.objects.filter(court=court)
			activity = []
			for i in act:
				obj = {
						'id':i.sport.id,
						'name':i.sport.name
				}
				activity.append(obj)

			slot = Slot.objects.filter(court=court)
			slotCheck = SlotCheck.objects.filter(court=court)

			#---------------------checking if default slot exist or not--------------

			default=[]
			for_current = False
			day=''

			#----------finding the day with default slot------------------------


			for i in slot:
				#print(i.is_default)
				if(i.is_default==True):
					day=i.day
					break
			#----------making the default slot-------------------------------
			s={}
			#print('################# PRINTING DEFAULT TIMINGS ##########################')
			for i in slot.filter(day=day):
				obj={
					'start_time':i.start_time.strftime('%I:%M %p'),
					'end_time':i.end_time.strftime('%I:%M %p'),
					'price':i.price
				}
				default.append(obj)

			s['default']=default


			#------------------now check for slot---------------------------
			days=weekday
			f=0
			if(court.is_current==False):
				for k in days:
					fill=[]
					is_filled=False
					for i in slot.filter(day=k).exclude(is_default=True):
						#print(i)
						obj={
							'start_time':i.start_time.strftime('%I:%M %p'),
							'end_time':i.end_time.strftime('%I:%M %p'),
							'price':i.price
						}
						fill.append(obj)
						is_filled=True
					if(is_filled==True):
						s[k]={'data':fill,'is_current':False}
			else:
#				print('#########################CALCULATING TIME###################')
				for i in days:
					fill=[]
					is_filled=False
					for j in slotCheck.filter(day=i):
#						print('----------------------',j)
						obj={
							'start_time':j.start_time.strftime('%I:%M %p'),
							'end_time':j.end_time.strftime('%I:%M %p'),
							'price':j.price
						}
						fill.append(obj)
						is_filled=True
					if(is_filled==True):
						s[i]={'data':fill,'is_current':True}

				for i in days:
					fill=[]
					is_filled=False
					for j in slot.filter(day=i).exclude(is_default=True):
						obj={
							'start_time':j.start_time.strftime('%I:%M %p'),
							'end_time':j.end_time.strftime('%I:%M %p'),
							'price':j.price
						}
						fill.append(obj)
						is_filled=True
			#			print(obj,'>>>>>>>>>>>>>>>>>>>>')
					if(is_filled==True):

						s[i]={'data':fill,'is_current':False}

			#print('######################### PRINTING SLOT ####################')
		#	print(s)
			s['is_current']=court.is_current

			return Response(data={'name':court.name,'activity':activity,'slot':s},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={},status=400)

	def post(self, request):
		try:
			venue_id = request.GET['id'] # venue ID
			data =  request.data
			# day = data.get('day')
			name = data.get('name')
			is_default = data.get('is_default')
			act_id = data.get('act_id')
			slots = []
			is_default = False
			day_given = []
			#print(data)
			#Validate id, name, act
			if(venue_id=="" or venue_id.startswith(' ') or venue_id==None):
				raise Exception('invalid id')
			if(name==None or name=="" or name.startswith(' ')):
				raise Exception('invalid name')
			# if(act==None):
			# 	raise Exception('invalid activities')
			#-----------------------------Creating Court------------------------------------
			venue = Venue.objects.get(id=venue_id)
			if(not data.get('slot')['is_current']):
				court = Court.objects.create(name = name,venue=venue) # Venue created
			else:
				court = Court.objects.create(name = name,venue = venue,is_current = True)
			# # ----------------------------Creating activities------------------------------
			for i in act_id:
				sport = SportsType.objects.get(id=i)
				activities = Activities.objects.create(court=court,sport = sport)
				activities.save()

			#-----------------------------Creating slot------------------------------------
			try:
				s = data.get('slot')
				#print(type(s))
				f=0
				for week in weekday:
				#	print('entering inside zone')
					for sl in s:
						if(sl==week):
				#			print(s[sl])
				#			print('================',s[sl]['is_current'])
							if(not s[sl]['is_current']):
				#				print('here')
								day_given.append(sl)
							slot = s[sl]['data']
							i,j=0,1
							while(i<len(slot)-1 and j<len(slot)):
				#				print(i,j)
				#				print(slot[i],slot[j])
								start = datetime.strptime(str(slot[i]['start_time']), "%I:%M %p").strftime("%H%M%S")
								end = datetime.strptime(str(slot[i]['end_time']), "%I:%M %p").strftime("%H%M%S")
								start_cmp = datetime.strptime(str(slot[j]['start_time']), "%I:%M %p").strftime("%H%M%S")
								end_cmp = datetime.strptime(str(slot[j]['end_time']), "%I:%M %p").strftime("%H%M%S")
				#				print(start>end , start_cmp>end_cmp , start<start_cmp<end , start<end_cmp<end , start<start_cmp<end_cmp<end , start_cmp<start<end<end_cmp)
								if(start>end or start_cmp>end_cmp or start<start_cmp<end or start<end_cmp<end or start<start_cmp<end_cmp<end or start_cmp<start<end<end_cmp or start==end):
									return Response({'error':'invalid slot timings','success':False}, status = 400)
								else:
									j+=1
								if(j==len(slot)):
									i+=1
									j=i+1
				#				print(i,j)
							t = slot
				#			print('======================Creating slot for==================: ',sl)
							for j in t:
								time = j
								#print(time,type(time['price']))
								#print('creating slot')
				#				#------------------Slot created------------------------
								if(not s[sl]['is_current']):
				#					print('PUSHING DIRECTLY IN SLOT')
									slot = Slot.objects.create(court=court,day = sl,start_time= datetime.strptime(time['start_time'],"%I:%M %p"),end_time=datetime.strptime(time['end_time'],"%I:%M %p"),price=time['price'])
								else:
				#					print('PUSHING INTO TEMP TABLE')
									slot = SlotCheck.objects.create(court=court,day = sl, start_time= datetime.strptime(time['start_time'],"%I:%M %p"),end_time=datetime.strptime(time['end_time'],"%I:%M %p"),price=time['price'])
								# print(slot)
								slot.save()

				temp_days = set(weekday)
				temp_days2 = set(day_given)

				#print(temp_days)
				try:
					slot=data.get('slot')['default']

					i,j=0,1
				#	print(slot)
					while(i<len(slot)-1 and j<len(slot)):
				#		print(i,j)
				#		print(slot[i],slot[j])
						start = datetime.strptime(str(slot[i]['start_time']), "%I:%M %p").strftime("%H%M%S")
						end = datetime.strptime(str(slot[i]['end_time']), "%I:%M %p").strftime("%H%M%S")
						start_cmp = datetime.strptime(str(slot[j]['start_time']), "%I:%M %p").strftime("%H%M%S")
						end_cmp = datetime.strptime(str(slot[j]['end_time']), "%I:%M %p").strftime("%H%M%S")
				#		print(start>end , start_cmp>end_cmp , start<start_cmp<end , start<end_cmp<end , start<start_cmp<end_cmp<end , start_cmp<start<end<end_cmp)
						if(start>end or start_cmp>end_cmp or start<start_cmp<end or start<end_cmp<end or start<start_cmp<end_cmp<end or start_cmp<start<end<end_cmp or start==end):
							raise Exception('invalid slot timings')
						else:
							j+=1
						if(j==len(slot)):
							i+=1
							j=i+1
				#		print(i,j)
					t = slot
					for i in temp_days-temp_days2:
				#		print('======================Creating slot for==================: ',i)
						for j in t:
							time = j
				#			#------------------Slot created------------------------
							slots = Slot.objects.create(court=court,day = i, start_time= datetime.strptime(time['start_time'],"%I:%M %p"),end_time=datetime.strptime(time['end_time'],"%I:%M %p"),price=time['price'],is_default=True)
							slots.save()


				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e), 'success':False}, status = 400)
				#send_otp('8800365323', '{0} created new court in venue {1}'.format(request.buser.name, venue.name))
				return Response(data={'status':True,'id':court.id},status=201)  # Court ID
			except Exception as e:
				traceback.print_exc()

		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)

	def put(self,request):

		"""Update Court"""


		try:
			deactivate = request.data['deactivate']
			court_id = request.GET['id']
			#print(request.data,type(deactivate))
			if(deactivate==True):
				#print('deactivate...')
				court = Court.objects.filter(id=court_id).update(is_active=False)
			else:
				#print('activate...')
				court = Court.objects.filter(id=court_id).update(is_active=True)
#			print(court)
			return Response(data={'success':True},status=200)
		except Exception as e:
			traceback.print_exc()

		try:
			court_id = request.GET['id'] # venue ID
			data =  request.data
			# day = data.get('day')
			name = data.get('name')
			act_id = data.get('act_id')
			slots = []
			is_default = False
			day_given = []
#			print(data)
			#Validate id, name, act
			#if(venue_id=="" or venue_id.startswith(' ') or venue_id==None):
			#	raise Exception('invalid id')
			if(name==None or name=="" or name.startswith(' ')):
				raise Exception('invalid name')
			# if(act==None):
			# 	raise Exception('invalid activities')
			#-----------------------------Creating Court------------------------------------
			if(not data.get('slot')['is_current']):
				court = Court.objects.filter(id=court_id).update(name = name) # Venue created
			else:
				court = Court.objects.filter(id=court_id).update(name = name,is_current = True)
			# # ----------------------------Creating activities------------------------------
			court = Court.objects.get(id=court_id)


			t3  = Activities.objects.filter(court = court)
			act_ids = []
			for i in t3:
				act_ids.append(i.id)
			t3.delete()
			for i in act_id:
				sport = SportsType.objects.get(id=i)
				activities = Activities.objects.create(court=court, sport = sport)

			prevSlot = []
			prevSlotCheck = []
			t1 = Slot.objects.filter(court = court)
			t2 = SlotCheck.objects.filter(court = court)
			#print(t1,t2)
			if(court.is_current==True):
				for i in t1:
					prevSlot.append(i.id)
					#print(prevSlot)
				for i in t2:
					prevSlotCheck.append(i.id)
			else:
				#print('is_current is false')
				for i in t1:
				#	print(i.id)
					prevSlot.append(i.id)
#			print('=====================',prevSlot)

			#-----------------------------Creating slot------------------------------------
			try:
				s = data.get('slot')
#				print(type(s))
				f=0
				for week in weekday:
				#	print('entering inside zone')
					for sl in s:
						if(sl==week):
				#			print('>>>>>>>>>>>>>>>>>',s[sl])
				#			print('================',s[sl]['is_current'])
							if(not s[sl]['is_current']):
				#				print('here')
								day_given.append(sl)
							slot = s[sl]['data']
							i,j=0,1
							while(i<len(slot)-1 and j<len(slot)):
				#				print('__________________________________',i,j)
				#				print(slot[i],slot[j])
								start = datetime.strptime(str(slot[i]['start_time']), "%I:%M %p").strftime("%H%M%S")
								end = datetime.strptime(str(slot[i]['end_time']), "%I:%M %p").strftime("%H%M%S")
								start_cmp = datetime.strptime(str(slot[j]['start_time']), "%I:%M %p").strftime("%H%M%S")
								end_cmp = datetime.strptime(str(slot[j]['end_time']), "%I:%M %p").strftime("%H%M%S")
				#				print(start>end , start_cmp>end_cmp , start<start_cmp<end , start<end_cmp<end , start<start_cmp<end_cmp<end , start_cmp<start<end<end_cmp)
								if(start>end or start_cmp>end_cmp or start<start_cmp<end or start<end_cmp<end or start<start_cmp<end_cmp<end or start_cmp<start<end<end_cmp or start==end):
									return Response({'error':'invalid slot timings', 'success':False}, status = 400)
								else:
									j+=1
								if(j==len(slot)):
									i+=1
									j=i+1
				#				print(i,j)
							t = slot
				#			print(t)
				#			print('======================Creating slot for==================: ',sl)
							for j in t:
				#				print('#########################')
								time = j
				#				print(time,type(time['price']))
				#				print('creating slot')
								#------------------Slot created------------------------
								if(not s[sl]['is_current']):
				#					print('PUSHING DIRECTLY IN SLOT')
									slot = Slot.objects.create(court=court,day = sl, start_time= datetime.strptime(time['start_time'],"%I:%M %p"),end_time=datetime.strptime(time['end_time'],"%I:%M %p"),price=time['price'])
									slot.save()
				#					print(slot.price, '>>>>>>>', time['price'])
								else:
				#					print('PUSHING INTO TEMP TABLE')
									slot = SlotCheck.objects.create(court=court,day = sl, start_time= datetime.strptime(time['start_time'],"%I:%M %p"),end_time=datetime.strptime(time['end_time'],"%I:%M %p"),price=time['price'])
									slot.save()
				#					print(slot.price ,'>>>>>>>', time['price'])
								# print(slot)
								slot.save()

				temp_days = set(weekday)
				temp_days2 = set(day_given)

				#print(temp_days)
				try:
					slot=data.get('slot')['default']

					i,j=0,1
				#	print(slot,"0---------------------->")
					while(i<len(slot)-1 and j<len(slot)):
				#		print(i,j,"##################")
				#		print(slot[i],slot[j])
						start = datetime.strptime(str(slot[i]['start_time']), "%I:%M %p").strftime("%H%M%S")
						end = datetime.strptime(str(slot[i]['end_time']), "%I:%M %p").strftime("%H%M%S")
						start_cmp = datetime.strptime(str(slot[j]['start_time']), "%I:%M %p").strftime("%H%M%S")
						end_cmp = datetime.strptime(str(slot[j]['end_time']), "%I:%M %p").strftime("%H%M%S")
				#		print(start>end , start_cmp>end_cmp , start<start_cmp<end , start<end_cmp<end , start<start_cmp<end_cmp<end , start_cmp<start<end<end_cmp)
						if(start>end or start_cmp>end_cmp or start<start_cmp<end or start<end_cmp<end or start<start_cmp<end_cmp<end or start_cmp<start<end<end_cmp or start==end):
							return Response({'error':'invalid slot timings', 'success':False}, status = 400)
						else:
							j+=1
						if(j==len(slot)):
							i+=1
							j=i+1
				#		print(i,j)
					t = slot
					for i in temp_days-temp_days2:
				#		print('======================Creating slot for==================: ',i)
						for j in t:
							time = j
				#			print(time, '4444444444444444')
				#			#------------------Slot created------------------------
							slots = Slot.objects.create(price = time['price'], court=court,day = i, start_time= datetime.strptime(time['start_time'],"%I:%M %p"),end_time=datetime.strptime(time['end_time'],"%I:%M %p"),is_default=True)
							slots.save()

				except Exception as e:
					traceback.print_exc()

			#	print(prevSlot)
				Slot.objects.filter(id__in=prevSlot).delete()
				SlotCheck.objects.filter(id__in=prevSlotCheck).delete()
				Activities.objects.filter(id__in=act_ids).delete()
				return Response(data={'status':True,'id':court.id},status=201)  # Court ID

			except Exception as e:
				traceback.print_exc()

		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)


	def delete(self,request):
		try:
			court_id = request.GET['id']
			#print('deleting court {}'.format(court_id))
			court = Court.objects.get(id=court_id)
			court.delete()
			return Response(data={'status':True},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={"error":e},status=400)

#------------------------------------Booking Slots--------------------------------------

class GetSlotDetail(APIView):

	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		"""Returns all the slot booked"""
		try:
			vid = request.GET['id']
			date = request.GET['date']
			try:
				day = datetime.datetime.strptime(date, '%Y-%m-%d').weekday()
			except ValueError:
				return Response(data={'error':'invalid date'},status=400)
			#print(weekday[day-1])
			venue = Venue.objects.get(id=vid, is_active = True)
			court = Court.objects.filter(venue=venue)

			#print(court)
			# slot_detail = VenueMapSport.objects.filter(venue=id)
			data=[]
			for i in court:
				slot_avail = Slot.objects.filter(day=weekday[day-1], court=i)
			#	print(slot_avail)
				days=[]
				for j in slot_avail:
			#		print(i)
					obj={
							'start_time':j.start_time,
							'end_time':j.end_time,
							'is_booked':j.is_booked,
						}
			#		print(obj)
					days.append(obj)
				data.append({i.name:days})
			#print(data)
			# raise Exception('ruk jaoo............')
			return Response(data =data, status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)

	def post(self,request):
		"""creates a new slot if unoccupied"""
		try:
			data= request.data
			vid = request.GET['id']
			user_id = request.GET['user_id']
			date = request.GET['date']

			try:
				datetime.datetime.strptime(date, '%Y-%m-%d')
			except ValueError:
				return Response(data={'error':'invalid date and time'},status=400)
			user = Account.objects.get(id=user_id)
			venue = Venue.objects.get(id=vid, is_active = True)

			raise Exception('ruk jaoo............')
			return Response(data = {'status':True},status=201)
		except Exception as e:
			traceback.print_exc()
#			traceback.print_exc()
			return Response(data={'error':'Account matching query does not exists'}, status=400)

	def delete(self,request):
		"""Deletes a slot"""
		pass

#--------------------------------REVIEW DETAILS-------------------------------------

class GetReview(APIView):

	permission_classes = [TokenHasReadWriteScope]
	def get(self,request):
		"""Return the all reviews of venue by users"""
		try:
			id = request.GET['id']
			reviews= Venue_Review.objects.filter(venue=id)
			try:
				rev=[]
				for i in reviews:
			#		print(i.user.profile_pic,'=-===============')
					try:
						pic=str(i.user.profile_pic.url)
					except:
						pic=None
					if(i!=None):
						revs={
							'name':i.user.name,
							'rating':i.rating,
							'description':i.description,
							'profile_pic':pic,
							'user_id':i.user.id,
						}
						rev.append(revs)
			#	print(rev)
				return Response(data={'reviews':rev,'success':True},status=200)
			except Exception as e:
				traceback.print_exc()
				return Response(data = {'error': 'No reviews..','success':False},status=400)
		except Exception as e:
#			print(e)
			traceback.print_exc()
			return Response(data=e,status=400)

	def post(self,request):
		"""create a new review"""
		try:
			data = request.data

			venue_id = request.GET['id']
			user = request.user
			rating = data.get('rating')
			description = data.get('description')
			venue_obj = Venue.objects.get(id=venue_id, is_active=True)
			venue_review_create_update = Venue_Review.objects.update_or_create(user=user, venue=venue_obj, defaults={"rating":rating, "description":description})
			return Response({"success":True},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':'Account matching query does not exists'},status=400)

	def delete(self,request):
		"""Deletes a review"""
		pass
		# try:
		# 	id = request.GET['id']

		# 	reviews = Venue_Review.objects.get(venue=venue)



#-------------------------------------AMENTITY DETAILS------------------------------------

class GetAmenitie(APIView):

	permission_classes = [customPermission]
	def get(self, request):
		"""Returns the list of amenities in a venue"""
		try:
			amenitie = Venue_Amenitie.objects.all()
			try:
				amen_list=[]
				for i in amenitie:
					if(i!=None):
						amen={
							'name':i.amen_name,
							'id':i.id
						}
						amen_list.append(amen)
				#print(amen_list)
				return Response(data= amen_list,status=200)
			except Exception as e:
				#print(e)
				traceback.print_exc()
				return Response(data=e, status=400)
		except Exception as e:
			return Response(data={'error': 'No amenities'},status=400)


	def delete(self,request):
		"""Deletes a amenities"""
		pass


class GetLeaderBoard(APIView):
	"""
		GET THE LIST OF ALL THE PLAYERS ACCORDING TO FILTER
	"""

	def get(self, request):
#		print('here')
		try:
			venue_id = request.GET['id']
			#print(venue_id)
			f = request.GET['f']
			role = request.GET['role']
			ac = request.GET['ac']
			level = request.GET['level']
			time = request.GET['time']
			#print(venue_id,f,role,ac,level,time)
			venue=Venue.objects.get(id=id)
			get_format = Match_Stat.objects.filter(m_format = 'T20',venue=venue, m_type=level,date_time__range = (start,end))
			for i in get_format:
				pass

			raise Exception('ruk jaoo...........')
		except Exception as e:
			return Response(data={'success':False, 'error':str(e)},status=400)





from datetime import timedelta, datetime
from collections import defaultdict
import json
from datetime import datetime
class VenueGroundApi(APIView):

	def get(self, request):
		venue_id = request.GET.get('venue_id')
		print(venue_id)
		#date = request.GET.get("date")
		#day = datetime.strptime(date,"%Y-%m-%d").day()
		try:
			result={}
			venue = Venue.objects.get(id = venue_id, is_active = True)
			court = Court.objects.filter(venue = venue)
			court_id = [i.id for i in court]
			court_list = [i.name for i in court]
			activities = Activities.objects.filter(court__id__in = [i.id for i in court])
			#print(activities)
			sport_list = activities.distinct("sport__name").values_list("sport__name", flat = True)
			result['sport_list'] = sport_list
			result['court_list'] = court_list
			holi = list(Holidays.objects.filter(venue = venue, date__gte = datetime.now().date()).distinct().values_list('date', flat = True))
			#print(holi)
#			slotcheck_id = []
			day = list(map(lambda x:x.strftime("%Y-%m-%d").upper(), holi))
			day1 = list(map(lambda x:x.strftime('%d/%m'), holi))
			#slots = Slot.objects.Filter(court = court).exclude(day__in = day)
			court_sport = []
			slotdays=[]
			for i in sport_list:
				court_sport.append({'sport_name':i, 'court':[]})
			for i in court_id:
				for act in activities:
					slotcheck_id=[]
					if i == act.court.id:
						#print("---------------",court_sport)
						for count in court_sport:
							if count['sport_name'] == act.sport.name:
								count['court'].append({'court':act.court.name,'court_id':act.court.id,'slots':[]})
								for ll in range(0,7):
									date = datetime.now() + timedelta(days=ll)
									today = str((datetime.now()+ timedelta(days=ll)).strftime('%A')).upper()
									#print(date, today)
									slots = Slot.objects.filter(court = act.court, day = today)
									#print(slots)
									#print('gggggggggggggggggggggg---------->',today)
									#for c in count['court']:
									#	for slot in slots:
									for slot in slots:
										#print('testttttttt',slot)
										for c in count['court']:
											if c['court_id'] == slot.court.id:
												if slot.day not in slotdays:
													now = datetime.now()
													c['slots'].append({'day':slot.day,'date':date.strftime("%d/%m"),'slot':[]})
													slotdays.append(slot.day)

												for obj in c['slots']:
													print(str(date.strftime('%Y-%m-%d')) not in day, date.date(), day)
													if str(date.strftime('%Y-%m-%d')) not in day:
														if obj['day'] == slot.day:
															# print(day[0], court, date.date())
															slotcheck = SlotCheck.objects.filter(day = slot.day, court = slot.court, date = date.date())

															#print(slotcheck)

															if slotcheck:
																for sc in slotcheck:
																	if sc.id not in slotcheck_id:
																		slotcheck_id.append(sc.id)
																		print(slotcheck_id)
																		obj['slot'].append({'start_time':sc.start_time.strftime("%I:%M %p"),'end_time':sc.end_time.strftime("%I:%M %p"),'price':sc.price, 'is_booked':sc.is_booked})
															else:
																obj['slot'].append({'start_time':slot.start_time.strftime("%I:%M %p"),'end_time':slot.end_time.strftime("%I:%M %p"),'price':slot.price, 'is_booked':slot.is_booked})
				#										print(obj)
						del slotdays[:]



			return Response({'sport':sport_list, 'court_sport':court_sport, 'holidays':day1, 'success':True}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class CourtSlotApi(APIView):

	def get(self,request):
		court_id = request.GET.get('court_id')
		try:
			court = Court.objects.get(id = court_id)

			holidays = list(Holidays.objects.filter(venue = court.venue).values_list('date', flat = True))
			day = list(map(lambda x:x.strftime("%A").upper(), holidays))

			slots = Slot.objects.Filter(court = court).exclude(day__in = day)
			slot_list = defaultdict(list)
			timings = []
			for slot in slots:
				slot_list[day].append({'start_time':slot.start_time, 'end_time':slot.end_time})

			return Response({'timings':slot_list})
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e)})




import ast
class ImageUpload(APIView):

	def post(self,request):
		try:
			print(123)
			user=request.buser
			img = request.data.get('img')
			venue_id = request.GET.get('id')
			venue = Venue.objects.get(id=venue_id)
#			print(request.FILES)
#			print(request.FILES.getlist(img))
			#img = ast.literal_eval(img[0])
#			for i in request.FILES.getlist('img'):
				#print(i)
			imgupload= VenueImg.objects.create(venue=venue,url=img)
			imgupload.save()
			#print(imgupload)
			return Response({'success':True},status=201)

		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)



class ImageUp(APIView):
	def post(self, request):
		try:
			#print("m here")
			return Response("bye")
		except:
			traceback.print_exc()
			return Response("hi", status= 400)



from scorecard.models import *
from django.utils import timezone
from datetime import timedelta
from operator import itemgetter



class VenueLeaderBoardApi(APIView):
	""" ****** LeaderBoard URl Format *******
	sports_format:

	ODI >>> O

	T20 >>> T

	Test Match >>> TM

	Role:

	Batsmen >>> B

	Bowler >>> BO

	Team >>> T

	Achievements:

	for batsmen:

	Higest run >>> HR

	Most run >>> MR

	for bowler

	Most Wicket >>> MW

	Highest Wicket >>> HW

	for Team

	Most Matches Played >>> MMP

	Most Wins  >>> MW

	Level:

	Local >>> LOC

	Institutional >>> INS

	National >>> NAT

	Internations >>> INT


	Time:

	Week >>> W

	Month >>> M

	Year >>> Y

	All Time >>> A"""
	def get(self,request):
		try:
			#print(1)
			sport_format = request.GET.get('format1')
			role = request.GET.get('role')
			level = request.GET.get('level')
			ach = request.GET.get('ach')
			time = request.GET.get('time')
			venue_id = request.GET.get('venue_id')
			#print(sport_format, role , level, ach, time, venue_id)
			v = Venue.objects.get(id=int(venue_id))
			#print(v)
			if time == "W":
				time = timezone.now().date() - timedelta(days= 7 )
			if time == "M":
				time = timezone.now().date() - timedelta(days = 30)
			if time == "Y":
				time = timezone.now().date() - timedelta(days = 365)
			if time == "A":
				time = ""
			stats = None
			if role == "B" and ach == "HR":
				if time:
					stats = Match_Stat_Batting.objects.filter(match_stat_team__match_stat__venue = v, match_stat_team__match_stat__date_time__date__gte = time).order_by("-run")[:10]
				else :
					stats = Match_Stat_Batting.objects.filter(match_stat_team__match_stat__venue = v).order_by("-run")[:10]
				player_run_list = []
				for stat in stats:
					pic = None
					if stat.player.user.profile_pic:
						pic = stat.player.user.profile_pic.url
					player_run_list.append({'player_id':stat.player.id,'profile_pic':pic, 'player_name':stat.player.user.name, 'run':stat.run})


				return Response({'result':player_run_list,'success':True}, status = 200)

			elif role == "B" and ach == "MR":
				if time:
					stats = Match_Stat_Batting.objects.filter(match_stat_team__match_stat__venue = v, match_stat_team__match_stat__date_time__date__gte = time)
				else :
					stats = Match_Stat_Batting.objects.filter(match_stat_team__match_stat__venue = v )
				l = []
				for stat in stats:
					pic = None
					if stat.player.user.profile_pic:
						pic = stat.player.user.profile_pic.url
					if not l:
						l.append({'id':stat.player.id,'profile_pic':pic, 'player_name':stat.player.user.name, 'run':stat.run})
						continue
					#print(l)
					p = [item for item in l if item["id"] == stat.player.id]
					#print(p)
					if p:
						p[0]['run'] = p[0]['run'] + stat.run
					else:
						l.append({'id':stat.player.id,'profile_pic':pic, 'player_name':stat.player.user.name, 'run':stat.run})

				newlist = sorted(l, key=itemgetter('run'), reverse = True)
				return Response({'result':newlist, 'success':True})

			if role == "BO" and ach == "HW":
				if time:
					stats = Match_Stat_Bowling.objects.filter(match_stat_team__match_stat__venue = v, match_stat_team__match_stat__date_time__date__gte = time).order_by('-wickets')[:10]
				else :
					stats = Match_Stat_Bowling.objects.filter(match_stat_team__match_stat__venue = v).order_by("-wickets")[:10]
				player_wicket_list = []
				for stat in stats:
					pic = None
					if stat.player.user.profile_pic:
						pic = stat.player.user.profile_pic.url
					player_wicket_list.append({'player_id':stat.player.id,'profile_pic':pic, 'player_name':stat.player.user.name, 'wicket':stat.wickets})
				return Response({'result':player_wicket_list, 'success':True})

			if  role == "BO" and ach == "MW":
				if time:
					stats = Match_Stat_Bowling.objects.filter(match_stat_team__match_stat__venue = v, match_stat_team__match_stat__date_time__date__gte = time)
				else:
					stats = Match_Stat_Bowling.objects.filter(match_stat_team__match_stat__venue = v)
				l = []
				for stat in stats:
					pic = None
					if stat.player.user.profile_pic:
						pic = stat.player.user.profile_pic.url
					if not l:
						l.append({'id':stat.player.id,'profile_pic':pic, 'player_name':stat.player.user.name, 'wicket':stat.wickets})
						continue
					p = [item for item in l if item["id"] == stat.player.id]
#					print(p)
					if p:
						p[0]['wicket'] = p[0]['wicket'] + stat.wickets
					else:
						l.append({'id':stat.player.id, 'wicket':stat.wickets,'profile_pic':pic, 'player_name':stat.player.user.name})
				newlist = sorted(l, key=itemgetter('wicket'), reverse = True)
				return Response({'result':newlist, 'success':True})
			elif role == "T" and ach == "MW":
				if time:
					stats = Match_Stat.objects.filter(venue = v, date_time__date__gte = time)
				else :
					stats = Match_Stat.objects.filter(venue = v)
				l = []
				for i in stats:
					pic1 = None
					if i.team1.team_pic:
						pic1 = i.team1.team_pic.url
					pic2 = None
					if i.team2.team_pic:
						pic2 = i.team2.team_pic.url
					if i.winner == i.team1.id:
						if not l:
							l.append({'id':i.team1.id,'name':i.team1.name,'team_pic':pic1 ,'count':0})
							continue
						p = [item for item in l if item["id"] == i.team1.id]
						print(p)
						if p:
							p[0]['count'] = p[0]['count'] + 1
						else:
							l.append({'id':i.team1.id,'name':i.team1.name,'team_pic':pic1, 'count':0 })
					elif i.winner == i.team2.id:
						if not l:
							l.append({'id':i.team2.id,'name':i.team2.name,'team_pic':pic2, 'count':0})
							continue
						p = [item for item in l if item["id"] == i.team2.id]
#						print(p)
						if p:
							p[0]['count'] = p[0]['count'] + 1
						else:
							l.append({'id':i.team2.id,'name':i.team2.name,'team_pic':pic2, 'count':0})

				newlist = sorted(l, key=itemgetter('count'), reverse = True)

				return Response({'result':newlist, 'success':True})



			elif role == "T" and ach == "MMP":
				if time:
					stats = Match_Stat_Team.objects.filter(match_stat__venue = v, match_stat__date_time__date__gte = time)

				else :
					stats = Match_Stat_Team.objects.filter(match_stat__venue = v)
				l = []
				for i in stats:
					pic1 = None
					if i.team.team_pic:
						pic1 = i.team.team_pic.url
					if not l:
						l.append({'id':i.team.id,'name':i.team.name,'team_pic':pic1 ,'count':0})
						continue
					p = [item for item in l if item["id"] == i.team.id]
#					print(p)
					if p:
						p[0]['count'] = p[0]['count'] + 1
					else:
						l.append({'id':i.team.id,'name':i.team.name,'team_pic':pic1, 'count':0 })
				newlist = sorted(l, key=itemgetter('count'), reverse = True)
				return Response({'result':newlist, 'success':True})
			else:
				return Response({'error':"Please Provide Correct Data"})
		except Exception as e:
			print(e)
			traceback.print_exc()
			return Response({'error':str(e), 'success':False})


class HolidayDetail(APIView):
	"""Holiday api"""
	#permission_classes = [TokenHasReadWriteScope]

	def get(self,request):
		try:
			venue_id=request.GET['id']
			venue = Venue.objects.get(id=venue_id)
			holiday=[]
			h = Holidays.objects.filter(venue=venue)
			for i in h:
				holiday.append(i.date)
			return Response(data={'holidays':holiday})
		except Exception as e:
			return Response(data={'error':str(e)},status=400)
	def post(self,request):
		try:
			holiday=request.data.get('holiday')
			venue_id = request.GET['id']
			venue = Venue.objects.get(id=venue_id)
			for i in holiday:
				hol = Holidays.objects.create(venue=venue,date=i)
			return Response(data={'success':True},status=201)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e)},status=400)
	def put(self, request):
                try:
                        holiday=request.data['holiday']
                        #print(holiday)
                        #print(request.data)
                        venue_id = request.GET.get('id')
                        venue = Venue.objects.get(id=venue_id)
                        Holidays.objects.filter(venue=venue).delete()
                        #h = Holidays.objects.filter(venue=venue)
                        for i in holiday:
                             Holidays.objects.create(venue=venue,date=i)
                        return Response(data={'success':True},status=201)
                except Exception as e:
                        traceback.print_exc()
                        return Response(data={'error':str(e)},status=400)



class Bookmarks(APIView):
	"""BOOKMARK A VENUE"""

	def post(self, request):
		try:
			venue_id = request.GET['id']
			bookmark = request.data.get('bookmark')
			user = request.user
			venue = Venue.objects.get(id=venue_id,is_active=True)
			bookmark_obj = Bookmark.objects.update_or_create(venue=venue, user=user, defaults={"is_bookmark":bookmark})
			return Response(data={'success':True},status=201)

			if(bookmark==True):
				bookmarked,created = Bookmark.objects.get_or_create(venue=venue, user=request.user,defaults={'is_bookmark':True})
				#print(bookmarked,created)
				if(not created):
					Bookmark.objects.filter(venue=venue, user=request.user).update(is_bookmark=True)
			else:
				bookmarked,created = Bookmark.objects.get_or_create(venue=venue, user=request.user)
				#print(bookmarked,created)
				if(not created):
				#	print('uncheck bookmark')
					Bookmark.objects.filter(venue=venue, user=request.user).update(is_bookmark=False)
			return Response(data={'success':True},status=201)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e),'success':True},status=400)


#--------------------------------------Search Venue----------------------------------------

class Search(APIView):
	"""Searching"""
	permission_classes  = [TokenHasReadWriteScope]
	def get(self,request):
		try:
			name = request.GET['name']
			page = request.GET['page']
			venue = Venue.objects.filter(name__icontains=name)
			venue_list=[]
			court_list=[]
			paginator = Paginator(venue,10)
			for i in paginator.page(page):
				court = Court.objects.filter(venue=i)
				for j in court:
					obj={
						'name':j.name,
						'id':j.id
					}
					court_list.append(obj)
				venue_list.append({'id':i.id,'name':i.name,'court_list':court_list})
			return Response(data={'data':venue_list,'count':paginator.num_pages,'success':True},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response(data={'error':str(e),'success':False},status=400)

class Search_Venue_Court_List(APIView): #made by nikhil
	def get(self, request):

		try:
			name = request.GET.get("name")
			tournament_id= request.GET.get('tournament_id')
			if not name or name=="":
				venue_qs = Venue.objects.filter(is_active=True) #is_functional=True
			tour_venue_map_qs= Tournament_Venue_Map.objects.filter(Q(tournament__id= tournament_id) & Q(is_active= True) & Q(is_dummy= False))

			venue_qs = Venue.objects.filter(name__istartswith=name,is_active=True).exclude(pk__in= [obj.court.venue.pk for obj in tour_venue_map_qs]) #is_functional=True
	#		result_dict = [{
	#				"venue_name":i.name,
	#				"venue_id":i.id,
	#				"court_list":list(Court.objects.filter(venue=i, is_active=True).values("id", "name", "is_active")),
	#				"is_active":i.is_active,
	#				"is_functional":i.is_functional
	#			} for i in venue_qs]

			list1=[]
			paginator = Paginator(venue_qs, 10)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			try:
				page_data = paginator.page(paging)
			except PageNotAnInteger:
				page_data = paginator.page(1)
			except EmptyPage:
				page_data = paginator.page(paginator.num_pages)
			for i in page_data:
				image= None
				img_qs= None
				#if i.court:
				img_qs= VenueImg.objects.filter(venue= i)
				if img_qs.exists():
					image= img_qs[0].url.url

				result_dict = {
                                        "venue_name":i.name,
                                        "venue_id":i.id,
                                        "court_list":list(Court.objects.filter(venue=i, is_active=True).values("id", "name", "is_active")),
                                        "is_active":i.is_active,
                                        "is_functional":i.is_functional,
					"venue_img":image
                                }

				list1.append(result_dict)
			return Response({"data":list1, "last_page":last_page},status=200)
		except Exception as e:
			traceback.print_exc()
			return Response({"error":str(e)},status=400)

class Search_Explore(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		try:
			pass
		except Exception as e:
			tracebeck.print_exc()
			return HttpResponse(json.dumps({"error":str(e)}),status=400)


class Check_Mobile_BTB(APIView):
	def get(self, request):
		key_ = request.GET.get('key')
		key_type_ = request.GET.get('key_type')
		if key_type_.strip() == "mobile":
			try:
				Account.objects.get(mobile= key_)
				return Response({"is_mobile_exist":True})
			except Exception as e:
				return Response({"is_mobile_exist":False})
		elif key_type_.strip() == "email":
			try:
				Account.objects.get(email= key_)
				return Response({"is_email_exist":True})
			except:
				return Response({"is_email_exist":False})
		else:
			return Response({"status":False, "message":"wrong key_type"}, status=400)


class Court_Reservation_View(APIView):
	"""get reserved or unreserved court list"""
	def get(self,request):
		date_ = request.GET.get('date')
		courtID = request.GET.get('court_id')
		try:
			court_reserv = Court_Reservation.objects.filter(reserv_date=date_,court_slot__court__id=courtID, is_active=True)
			court_id = [c.court_slot.id for c in court_reserv]
			dt_formated = datetime.strptime(date_, '%Y-%m-%d')
			day = dt_formated.strftime("%A").upper()
			court_unreserv = Slot.objects.filter(day=day, court__id=courtID).exclude(id__in=court_id)
			#print (court_unreserv,"   ",court_id,day,courtID)
			reserved_list = []
			unreserved_list = []
			for i in court_reserv:
				reserved_list.append({"check_in":i.check_in, "start_time":i.court_slot.start_time.strftime('%I:%M %p'),"end_time":i.court_slot.end_time.strftime('%I:%M %p'),"reserved_by":i.offline_user.name, "user_id":i.offline_user.id, "status":"reserved", "slot_id":i.court_slot.id})
			for j in court_unreserv:
				reserved_list.append({"check_in":False,"start_time":j.start_time.strftime('%I:%M %p'), "end_time":j.end_time.strftime('%I:%M %p'), "reserved_by":"", "status":"unreserved", "slot_id":j.id, "user_id":None})
			return Response({"slot_list":reserved_list})
		except Exception as e:
			traceback.print_exc()
			return Response({"status":False, "message":"something wrong happend"})


	def post(self, request):
		data = request.data
		print('=======================', data, '====================')
		slot_id = data.get('slot_id')
		name = data.get('name')
		phone = data.get('phone')
		email = data.get('email')
		source = data.get('source')
		amount_paid = data.get('amount_paid')
		mode_of_payment = data.get('amount_mode')
		date_ = data.get("date")
		try:
			#print(111)
			slot = Slot.objects.get(id=slot_id)
			h = list(Holidays.objects.filter(venue = slot.court.venue).values_list('date', flat = True))
			hlist = [i.strftime('%Y-%m-%d') for i in h]
			if date_ in hlist:
				return Response({'error':'Venue is closed on this date, please select another date.', 'success':False}, status = 400)
			print('>>>>>>>>>>>>>>>>>>', slot.court.name, '>>>>>>>>>>>>>>>>>', slot.court.venue.name)
			obj = Court_Reservation.objects.filter(court_slot=slot, reserv_date = date_)
			if obj:
				print(obj)
				return Response({"status":False, "error":"Court already reserved"}, status=400)
			user = Court_User.objects.create(name = name, email=email, phone=phone, source=source)
			obj = Court_Reservation(court_slot=slot)
			obj.offline_user = user
			obj.buser = request.buser
			obj.amount_paid = amount_paid
			obj.reserv_date = date_
			obj.payment_mode = mode_of_payment
			obj.save()
			return Response({"status":True, "message":"Court has been reserved"})
		except:
			traceback.print_exc()
			return Response({"status":False, "message":"something goes wrong"}, status=400)


class Get_Venue_Court(APIView):
	def get(self, request):
		venue_id = request.GET.get('venue_id')
		court_objs = list(Court.objects.filter(venue__id = venue_id, is_active=True).values("name", "id"))
		return Response(court_objs)


class Check_in_Reservation(APIView):
	def get(self,request):
		courtID = request.GET.get('court_id')
		date_ = request.GET.get('date')
		userID = request.GET.get('user_id')
		#print(courtID, date_, userID)
		print(request.buser)
		try:
			court_reserv = Court_Reservation.objects.filter(reserv_date=date_, court_slot__court__id=courtID, offline_user__id= userID, is_active=True)
			#print(court_reserv)
			reserved_list = []
			for i in court_reserv:
				reserved_list.append({"check_in":i.check_in,"start_time":i.court_slot.start_time.strftime('%I:%M %p'),"end_time":i.court_slot.end_time.strftime('%I:%M %p')})
			offline_user = Court_User.objects.get(id=userID)
			output_list = [{"slot_list":reserved_list, "name":offline_user.name, "reservation_no":None, "date":date_, "reserved_type":"ofline"}]
			#print(output_list)
			return Response(output_list)
		except:
			traceback.print_exc()
			return Response({"status":False, "message":"something goes wrong"}, status=400)


	def post(self, request):
		courtID = request.data['court_id']
		date_ = request.data['date']
		userID = request.data['user_id']
		slotID = request.data['slot_id']
		#print(date_,userID,courtID,slotID)
		court_reserv = Court_Reservation.objects.filter(reserv_date=date_, offline_user__id= userID, court_slot__id=slotID, court_slot__court__id=courtID, is_active=True, buser = request.buser)
		#print(court_reserv)
		for i in court_reserv:
			i.check_in = True
			i.save()
			#print(i.check_in)
		return Response({"status":True, "message":"check-in successfully"})


	def delete(self, request):
		#print(request.data)
		courtID = int(request.GET.get('court_id'))
		date_ = request.GET.get('date')
		userID = int(request.GET.get('user_id'))
		reason = request.GET.get('reason')
		court_reserv = Court_Reservation.objects.filter(reserv_date=date_, offline_user__id= userID,  court_slot__court__id=courtID, is_active=True).update(is_active=False)
		return Response({"status":True, "message":"slot cancel successfully"})





class VenueListApi(APIView):

	permissions_classes = [customPermission,]

	def get(self, request):
		try:
			user = request.buser
#			venues = Venue.objects.filter(owner = user, is_active = True)
			venues = [v for v in VenueAdminPermissionMap.objects.filter(admin = request.buser, venue__is_active = True, is_active = True)]
			v_list = []
			for v in venues:
				v_list.append({'name':v.venue.name,'id': v.venue.id})
			tour_qs= Tournament_Detail.objects.filter(Q(admin= user) & Q(is_active= True))
			serializer= []
			for obj in tour_qs:
				serializer.append({'id':obj.id, 'name':obj.name})
			return Response({'success':False, 'venues':v_list, 'tournament':serializer}, status = 200)


		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)





class VenuePermissionApi(APIView):
	permission_classes = [customPermission,]
	def get(self, request):
		try:
			venue_id = request.GET.get('venue_id')
			user = request.buser
			venue = Venue.objects.get(id = venue_id)
			try:
				admin = VenueAdminPermissionMap.objects.filter(venue = venue, admin = user)

				p_list = dict()
				for a in admin:
					permissions = a.permission.all()
					for p in permissions:
						p_list.update({p.slug.replace('-',''):True})
				if p_list:
					p_list.update({'success':True})
					return Response(p_list , status = 200)
			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False}, status= 400)

			try:
				manager = VenueManagerPermissionMap.objects.filter(venue = venue, manager = user)
				#print(manager)
				p_list = dict()
				for m in manager:
					permissions = m.permission.all()
					#print(permissions)
					for p in permissions:
						p_list.update({p.slug.replace('-',''):True})
				keys = [key for key, items	in p_list.items()]
				for i in ADMIN_PERMISSIONS:
					if i.replace('-','') not in keys:
						p_list.update({i.replace('-',''):False})
				p_list.update({'success':True})
				return Response	(p_list, status = 200)

			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False}, status= 400)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status= 400)
