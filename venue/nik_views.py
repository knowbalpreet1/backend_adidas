from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from venue.models import Venue,VenueImg, Venue_Review, Venue_Amenitie, Court, Activities, Slot,VenueMapAmenity,Holidays,Bookmark
from event.models import Event
from players.models import Players, Players_Preferred_Sport, City, Team, TeamPlayers, TeamAdmin,SportsType
from scorecard.models import Match_Stat, Match_Stat_Team, Match_Stat_Batting, Match_Stat_Bowling, Team_Match_Player,Match_Stat_Live
from authentication.models import Account
import traceback
import json
from venue.views import cost_per_game
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.utils import timezone
from django.db.models import Max
from notification.views import send_notification, save_notification
from authentication.async_task import TaskRegistration
from venue.views import average_rating
from datetime import datetime
from tournament.models import Team_Registration, Tournament_Detail, Teamowner
def get_admin(user, team_obj):
		try:
				team_player = TeamAdmin.objects.get(team=team_obj, player__user=user, is_active=True)
				return True
		except Exception as e:
				traceback.print_exc()
				return False


class Search_Explore(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		try:
			user = request.user
			lat = request.GET["lat"]
			longi = request.GET["long"]
			distance = 100
			if lat.strip()=='' or lat.strip()=="None":
				return HttpResponse(json.dumps({"success":False}),status=400)
			if longi.strip()=='' or longi.strip()=="None":
				return HttpResponse(json.dumps({"success":False}),status=400)

			user_qs = Account.gis_manager.nearby(float(lat), float(longi), distance).exclude(id=user.id)[0:10]
			user_list = []
			venue_qs = Venue.gis_manager.nearby(float(lat), float(longi), distance)[0:10]
			venue_list = []
			event_qs = Event.gis_manager.nearby(float(lat), float(longi), distance)[0:10]
			event_list = []
			torunament_qs = Tournament_Detail.gis_manager.nearby(float(lat), float(longi), 100)[0:10]
			print(torunament_qs)
			tournament_list = []
			for i in user_qs:
				try:
					player_id = Players.objects.get(user=i).id
				except:
					player_id = None
				image = None
				if i.profile_pic:
					image = i.profile_pic.url
	#				print(image)
				user_dict = {
					"name":i.name,
					"profile_pic":image,
					"id":i.id,
					"is_active":True,
					"player_id":player_id
				}
				user_list.append(user_dict)

			for j in venue_qs:
				venue_img = VenueImg.objects.filter(venue=j)
				if venue_img.count()==0:
					image=None
				else:
					image = venue_img[0].url.url
				venue_dict = {
					"name":j.name,
					"is_active":True,
					"id":j.id,
					"owner":j.owner.name,
					"image":image,
					"address":j.address.name
				}
				venue_list.append(venue_dict)

			for k in event_qs:
				image = None
				if k.image:
					image = k.image.image.url
				event_dict = {
					"name":k.name,
					"is_active":k.is_active,
					"is_private":k.is_private,
					"id":k.id,
					"image":image,
					"address":k.address.name
				}
				event_list.append(event_dict)
			for t in torunament_qs:
				cover_pic = None
				if t.cover_pic:
					cover_pic = t.cover_pic.url
				tournament_list.append({
						"name":t.name,
						"id":t.id,
						"image":cover_pic,
						"address":t.city.name,
					})
			result_dict = {
					"players":user_list,
					"venues":venue_list,
					"events":event_list,
					"tournament":tournament_list,
					"success":True}
	#	print("result_dict",result_dict)
			return HttpResponse(json.dumps(result_dict),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e)}),status=400)

class PlayerFilter(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		try:
			user = request.user
			lat = request.GET["lat"]
			longi = request.GET["long"]
			gender = request.GET["gender"]
			sports = request.GET["sports"]
			if lat.strip()=='' or longi.strip()=='':
				return HttpResponse(json.dumps({"error":False}),status=400)

			user_qs = Account.gis_manager.nearby(float(lat), float(longi), 20).exclude(id=user.id)
			if gender:
				if gender=="" or gender==" ":
					gender = "all"
				if gender.lower()=="all":

					user_qs = user_qs
				else:
					user_qs = user_qs.filter(gender=gender.upper())

			user_id = [i.id for i in user_qs]

			list1 = []
			key = 0
			if len(user_qs)!=0:

				player_qs = Players_Preferred_Sport.objects.filter(player__user__in=user_id, is_active=True, sport_name__name="Cricket")
				if sports!="":
					key = 1
					sports = list(sports)
					sports = [x for x in sports if not x==","]

					player_qs = Players_Preferred_Sport.objects.filter(player__user__in=user_id, is_active=True, sport_name__id__in=sports).distinct("player__user")

				for i in player_qs.select_related("player__user"):
					image = None
					if i.player.user.profile_pic:
						image = i.player.user.profile_pic.url
					if key==0:
						sports = list(Players_Preferred_Sport.objects.filter(player=i.player,sport_name__name="Cricket").values("sport_name__name"))
					else:
						sports =list(Players_Preferred_Sport.objects.filter(player=i.player).values("sport_name__name"))
					result_dict = {
						"name":i.player.user.name,
						"id":i.player.id,
						"account_id":i.player.user.id,
						"image":image,
						"gender":i.player.user.gender,
						"sports":sports,
						"address":{"city":i.player.user.city, "state":i.player.user.state}
					}
					list1.append(result_dict)
			paginator = Paginator(list1, 10)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			final_list = []
			try:
				page_data = paginator.page(paging)
			except PageNotAnInteger:
				page_data = paginator.page(1)
			except EmptyPage:
				page_data = paginator.page(paginator.num_pages)
			for i in page_data:
				final_list.append(i)
			return HttpResponse(json.dumps({"success":True, "data":final_list, "last_page":last_page}), status=200)

		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)


class Suggested_Players(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		try:
			city_obj = City.objects.get(id=request.GET["city"])
			city_name = city_obj.name
			user = request.user
			player_obj = Players.objects.select_related("user").get(user=user,is_active=True)
			print(player_obj)
			pic = None
			if player_obj.user.profile_pic:
				pic = player_obj.user.profile_pic.url
			user_dict = {
				"name":player_obj.user.name,
				"profile_pic":pic,
				"player_id":player_obj.id,
				"city":player_obj.user.city,
				"sport":Players_Preferred_Sport.objects.filter(player=player_obj,is_active=True)[0].sport_name.name}
			player_prefer_qs = Players_Preferred_Sport.objects.select_related("player__user").filter(player__user__city=city_name,sport_name__id=request.GET["sport"],is_active=True).exclude(player__user=user)[0:20]
			list1 = []
			for i in player_prefer_qs:
				image = None
				if i.player.user.profile_pic:
					image = i.player.user.profile_pic.url
				result_dict = {
					"name":i.player.user.name,
					"profile_pic":image,
					"player_id":i.player.id,
					"city":i.player.user.city,
					"sport":i.sport_name.name,
				}
				#result_dict.update(user_dict)
				list1.append(result_dict)

			list1.append(user_dict)
			return HttpResponse(json.dumps({"success":True, "data":list1}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)

class Player_Role(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def post(self, request):
		try:
			user = request.user
			data = request.data
			team_id = request.GET["team_id"]

			team_obj = Team.objects.get(id=team_id, is_active=True)
			is_admin = get_admin(user, team_obj)
			if is_admin:
				captain = data["captain"]
				wk = data["wicket_keeper"]

				team_update_qs = TeamPlayers.objects.filter(team=team_obj).update(is_captain=False, is_wk=False)
				team_player_obj = TeamPlayers.objects.get(player__id=captain, team=team_id, is_active=True)
				team_player_obj.is_captain=True
				team_player_obj.save()
				if len(wk)<=0:
					return HttpResponse(json.dumps({"error":"must chose atleast one wicket keeper", "success":False}),status=400)
				team_wk = TeamPlayers.objects.filter(player__id__in=wk,team=team_obj,is_active=True).update(is_wk=True)
#				p_id = wk
#				p_id.append(captain)
#				print(p_id,'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
#				p_qs = Players.objects.filter(id__in = p_id, is_active = True)
#				print(p_qs, '+++++++++++++++++++++++++++')
#				p_list = [p for p in p_qs]
#				print(p_list, '___________________________=====')
#				profile_pic = None
#				if team_player_obj.team.team_pic:
#					profile_pic = team_player_obj.team.team_pic.url
#				message_body = {"title":"", "body":"You are added to the team {0} by {1}. Let's Play! ".format(team_player_obj.team.name, team_player_obj.team.created_by.user.name), "profile_pic":profile_pic,"team_id":team_player_obj.team.id, "action":"following"}
#				send_notification((p_list, message_body))
#				for p in p_qs:
#					save_notification(player = p, instance = team_player_obj.team.created_by, notification_type = 'T', message = "You are added to the team {0} by {1}. Let's Play! ".format(team_player_obj.team.name, team_player_obj.team.created_by.user.name))
				return HttpResponse(json.dumps({"success":True}),status=200)
			else:
				return HttpResponse(json.dumps({"success":False, "is_admin":False,"detail":"Forbidden"}),status=403)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)

class Team_Overview(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		user = request.user
		try:
			team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
			is_admin = get_admin(user,team_obj)

			win_lose = []
			match_stat_qs = Match_Stat.objects.filter(Q(team1=team_obj) | Q(team2=team_obj),is_live=False, is_canceled=False, is_result_declared=True).order_by("-id")
			for i in match_stat_qs[0:6]:
				if i.winner==team_obj.id:
					win_lose.append("w")

				elif i.is_draw:
					win_lose.append("d")

				else:
					win_lose.append("l")

			result = None
			vs = None
			vs2 = None
			date = None
			date1 = None
			upcoming_address = None
			upcoming_city_id = None
			pr_address = None
			pr_city_id = None
			up_venue = None
			pr_venue = None
			pr_going = []
			if len(match_stat_qs)!=0:
				previous_match = match_stat_qs[0]


				match_stat_team_qs = Match_Stat_Team.objects.filter(match_stat__id=previous_match.id)

				team_match_player = Team_Match_Player.objects.select_related("player__user").filter(match_stat__id=previous_match.id, team__id=team_obj.id)
				for pr_player in team_match_player[0:3]:
					image = None
					if pr_player.player.user.profile_pic:
						image = pr_player.player.user.profile_pic.url

					pr_going.append({"player":pr_player.player.user.name, "profile_pic":image, "count":team_match_player.count()})

				#ms_qs = Match_Stat.objects.filter(Q(team1=team_obj) | Q(team2=team_obj) & Q(tournament_round__isnull=False)|Q(tournament_group__isnull=False),is_live=False, is_canceled=False, date_time__gt=timezone.now(), is_result_declared=False).order_by("date_time") #use when tournament comes in phase!!!
				ms_qs = Match_Stat.objects.filter(Q(team1=team_obj) | Q(team2=team_obj),is_live=False, is_canceled=False, date_time__gt=timezone.now(), is_result_declared=False).order_by("date_time")
				if len(ms_qs)!=0:
					if team_obj.name==ms_qs[0].team1.name:
						vs2 = ms_qs[0].team2.name
					else:
						vs2 = ms_qs[0].team1.name
					date = str(ms_qs[0].date_time)[:10]
					if ms_qs[0].venue:
						upcoming_address = ms_qs[0].venue.city.name
						upcoming_city_id = ms_qs[0].venue.city.id
						try:
							up_venue = VenueImg.objects.filter(venue=ms_qs[0].venue)[0].url.url
						except:
							pass
				team1_run = match_stat_team_qs.get(team=team_obj).total_run
				team1_wickets = match_stat_team_qs.get(team=team_obj).wickets
				team2_qs = match_stat_team_qs.exclude(team=team_obj)[0]
				team2_run = team2_qs.total_run
				team2_wickets = team2_qs.wickets

				vs = team2_qs.team.name
				if match_stat_team_qs.get(team=team_obj).match_stat.venue:

					pr_address = match_stat_team_qs.get(team=team_obj).match_stat.venue.address.city.name
					pr_city_id = match_stat_team_qs.get(team=team_obj).match_stat.venue.address.city.id
					try:
						pr_venue = VenueImg.objects.filter(venue=match_stat_team_qs.get(team=team_obj).match_stat.venue)[0].url.url
					except:
						pass
				date1 = str(previous_match.date_time)[:10]
				if previous_match.is_draw:
					result = "match was drawn"

				else:
					result = previous_match.won_by

			logo = None
			if team_obj.team_pic:
				logo = team_obj.team_pic.url
			result_dict = {
				"name":team_obj.name,
				"logo":logo,
				"recent_form":win_lose,
				"id":team_obj.id,
				"description":team_obj.description,
				"city":team_obj.city.name,
				"city_id":team_obj.city.id,
				"sport":team_obj.sport.name,
				"sport_id":team_obj.sport.id,
				"previous_match":{"result":result,"vs":vs, "date":date, "venue_address":pr_address, "city_id":pr_city_id, "venue_image":pr_venue, "going":pr_going},
				"upcoming_match":{"vs":vs2, "date":date, "venue_address":upcoming_address, "city_id":upcoming_city_id, "venue_image":up_venue, "going":[]},
				"trophies":[{"name":None, "position":None, "address":None, "date":None}]
			}
			""" tournament is not in this phase and going is empty in upcoming matches, when tournament phase comes then going list for upcoming matches will be made """
			return HttpResponse(json.dumps({"success":True, "data":result_dict, "is_admin":is_admin}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"success":False, "error":str(e)}),status=400)

class Team_Players(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		user = request.user
		try:
			team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
			is_admin = get_admin(user,team_obj)
			logo = None
			if team_obj.team_pic:
				logo = team_obj.team_pic.url


			team_player_qs = TeamPlayers.objects.filter(team=team_obj, is_active=True)
			list1 = []
			for i in team_player_qs.select_related("player__user"):
				image = None
				if i.player.user.profile_pic:
					image = i.player.user.profile_pic.url
				result_dict = {
					"name":i.player.user.name,
					"id":i.player.id,
					"image":image,
					"gender":i.player.user.gender,
					"sports":list(Players_Preferred_Sport.objects.filter(player=i.player).values("sport_name__name")),
					"address":{"city":i.player.user.city, "state":i.player.user.state,},
					"is_captain":i.is_captain,
					"is_wk":i.is_wk,
					"logo":logo
				}
				list1.append(result_dict)
			list1.sort(key=lambda x: (x['is_captain'], x['is_wk']))

			logo = None

			if team_obj.team_pic:
				logo = team_obj.team_pic.url

			team_detail = {
				"logo":logo,
				"team_name":team_obj.name,
				"team_id":team_obj.id,
				"description":team_obj.description,
				"sport":team_obj.sport.name,
				"sport_id":team_obj.sport.id,
				"id":team_obj.id,
				"city":team_obj.city.name,
				"city_id":team_obj.city.id,
			}
			team_owner = []
			teamowner_obj = Teamowner.objects.filter(team_id=request.GET["id"])
			print (teamowner_obj)
			if (teamowner_obj):
				for teamowner in teamowner_obj:
					team_owner.append({"owner":teamowner.name, "image":'media/'+str(teamowner.image_link)})
			
			
			return HttpResponse(json.dumps({"success":True, "data":list1[::-1], "is_admin":is_admin, "team_detail":team_detail, "team_owner":team_owner}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)


class TeamStatistic(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		user = request.user
		try:
			team_obj = Team.objects.get(id=request.GET["id"], is_active=True)

			is_admin = get_admin(user,team_obj)
			logo = None

			if team_obj.team_pic:
				logo = team_obj.team_pic.url

			player_id = [i.player.id for i in TeamPlayers.objects.filter(team=team_obj,is_active=True)]

			match_stat_batting = Match_Stat_Batting.objects.filter(match_stat_team__team=team_obj ,player__id__in=player_id, is_valid=True)
			max_run = 0
			max_wickets = 0
			batting_player = 0
			wicket_player = 0
			wicket_image = None
			bat_image = None
			name1 = None
			name2 = None

			if len(match_stat_batting)!=0:
				max_run = match_stat_batting.aggregate(Max("run"))["run__max"]
				try:
					batting_player = match_stat_batting.get(run=max_run).player.id
				except Exception as e:
					traceback.print_exc()
					batting_player = match_stat_batting.filter(run=max_run)[0].player.id
				player_obj = Players.objects.get(id=batting_player)
				name1 =player_obj.user.name
				if player_obj.user.profile_pic:
					bat_image = player_obj.user.profile_pic.url

			match_stat_bowling = Match_Stat_Bowling.objects.filter(match_stat_team__team=team_obj,player__id__in=player_id, is_valid=True)

			if len(match_stat_bowling)!=0:
				max_wickets = match_stat_bowling.aggregate(Max("wickets"))["wickets__max"]
				try:
					wicket_player = match_stat_bowling.get(wickets=max_wickets).player.id
				except Exception as e:
					traceback.print_exc()
					wicket_player = match_stat_bowling.filter(wickets=max_wickets)[0].player.id

				player_obj = Players.objects.get(id=wicket_player)
				name2 = player_obj.user.name

				if player_obj.user.profile_pic:
					wicket_image = player_obj.user.profile_pic.url

			win_lose = []
			win = 0
			draw = 0
			lost = 0
			match_stat_qs = Match_Stat.objects.filter(Q(team1=team_obj) | Q(team2=team_obj),is_live=False, is_canceled=False, is_result_declared=True).order_by("-id")
			for i in match_stat_qs:
				if i.winner==team_obj.id:
					win_lose.append("w")
					win+=1
				elif i.is_draw:
					win_lose.append("d")
					draw+=1
				else:
					win_lose.append("l")
					lost+=1

			result_dict = {
				"name":team_obj.name,
				"logo":logo,
				"description":team_obj.description,
				"sport":team_obj.sport.name,
				"sport_id":team_obj.sport.id,
				"id":team_obj.id,
				"city":team_obj.city.name,
				"city_id":team_obj.city.id,
				"max_wickets":{"name":name2, "player_id":wicket_player, "wickets":max_wickets, "profile_pic":wicket_image},
				"max_runs":{"name":name1,"player_id":batting_player, "runs":max_run, "profile_pic":bat_image},
				"recent_form":win_lose[:6],
				"played":match_stat_qs.count(),
				"lost":lost,
				"win":win,
				"draw":draw,
			}

			return HttpResponse(json.dumps({"success":True, "data":result_dict, "is_admin":is_admin}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)

class ManageAdmins(APIView):
    permission_classes = [TokenHasReadWriteScope]

    def post(self, request):
        user = request.user
        try:
            data = request.data
            team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
            is_admin = get_admin(user, team_obj)
            player_user = Players.objects.get(user=user)

            if is_admin:
                admin = [int(x) for x in data["admin"]]
                team_admin_id = [x.player.id for x in TeamAdmin.objects.filter(team=team_obj, is_active=True).exclude(player=player_user)]
                admin.remove(player_user.id)


                if admin == team_admin_id:
                    return HttpResponse(json.dumps({"success":True}), status=200)

                added_list = []
                remove_list = list(set(team_admin_id)- set(admin))
                print(remove_list, ">>>>>>>>>>>>>>>>>>>>", team_admin_id, "jdghshdghsagdhasgdhas")
                for i in admin:

                    player_obj = Players.objects.get(id=i, is_active=True)
                    try:
                        team_admin = TeamAdmin.objects.get(team=team_obj, player=player_obj)

                        if not team_admin.is_active:
                            print("inside if loop")
                            team_admin.is_active = True
                            added_list.append(i)
                            team_admin.save()

                    except Exception as e:
                        traceback.print_exc()
                        team_admin = TeamAdmin.objects.create(team=team_obj, player=player_obj, is_active=True)
                        print(team_admin.is_active, ">>>>>>>", type(team_admin.is_active))
                        added_list.append(team_admin.player.id)

                profile = None
                if team_obj.team_pic:
                    profile = team_obj.team_pic.url


                body = {"title":"goplaybook", "body":"You are now the admin of "+str(team_obj.name).title(),"profile_pic":profile, "team_id":team_obj.id, "action":"add_admin"}
                body1 = {"title":"goplaybook", "body":"You have been removed as an admin of "+str(team_obj.name).title(), "profile_pic":profile, "team_id":team_obj.id, "action":"remove_admin"}
                print(team_admin_id, "team admin id is printing")


                print(remove_list, "error is here")
                TeamAdmin.objects.filter(player__id__in=remove_list).update(is_active=False)
                TaskRegistration(send_notification, (added_list, body), 1).delay()
                TaskRegistration(send_notification, (remove_list, body1), 1).delay()
                for i in added_list:
                    p = Players.objects.get(id = i)
                    save_notification(player = p, instance = team_obj, notification_type = 'T', message = "You are now the admin of "+str(team_obj.name).title())
                for i in remove_list:
                    p = Players.objects.get(id = i)
                    save_notification(player = p, instance = team_obj, notification_type = 'T', message = "You have been removed as an admin of "+str(team_obj.name).title())
                return HttpResponse(json.dumps({"success":True}), status=201)

            else:
                return HttpResponse(json.dumps({"success":False, "detail":"Forbidden"}), status=403)

        except Exception as e:
                traceback.print_exc()
                return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

    def get(self, request):
        user = request.user
        try:
            team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
            is_admin = get_admin(user, team_obj)

            if is_admin:
                list1 = []
                team_player_qs = TeamAdmin.objects.filter(team=team_obj, is_active=True)
                for i in team_player_qs:
                    image = None
                    if i.player.user.profile_pic:
                            image = i.player.user.profile_pic.url
                    result_dict = {
                            "name":i.player.user.name,
                            "id":i.player.id,
                            "profile_pic":image,
                            "account_id":i.player.user.id,
                            "gender":i.player.user.gender,
                            "sports":list(Players_Preferred_Sport.objects.filter(player=i.player).values("sport_name__name")),
                            "address":{"city":i.player.user.city, "state":i.player.user.state}
                    }
                    list1.append(result_dict)
                return HttpResponse(json.dumps({"success":True, "data":list1}),status=200)
            else:
                return HttpResponse(json.dumps({"success":False, "detail":"Forbidden"}), status=403)
        except Exception as e:
            traceback.print_exc()
            return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

class AddRemovePlayer(APIView):

  permission_classes = [TokenHasReadWriteScope]

  def post(self, request):
      user = request.user
      try:
          team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
          is_admin = get_admin(user, team_obj)
          player_user = Players.objects.get(user=user)

          if is_admin:
              data = request.data
              add = [int(x) for x in data["add"]]
              team_players_id = [x.player.id for x in TeamPlayers.objects.filter(team=team_obj, is_active=True)]

              added_player = []
              remove_list = list(set(team_players_id)- set(add))

              for i in add:
                  print(i, '--------')
                  player_obj = Players.objects.get(id=i)
                  try:
                      team_player = TeamPlayers.objects.get(team=team_obj, player=player_obj)
                      if not team_player.is_active:
                          team_player.is_active=True
                          team_player.save()
                          added_player.append(i)

                  except Exception as e:
                      traceback.print_exc()
                      team_player = TeamPlayers.objects.create(team=team_obj, player=player_obj, is_active=True)
                      added_player.append(team_player.player.id)
              profile = None
              if team_obj.team_pic:
                  profile = team_obj.team_pic.url

              body = {"title":"goplaybook", "body":"You are added to the team "+str(team_obj.name).title()+" by "+str(user.name).title()+". Let's Play!", "profile_pic":profile ,"action":"add_player", "team_id":team_obj.id}
              body1 = {"title":"goplaybook", "body":"Uh oh! You have been removed from the team "+str(team_obj.name).title(), "profile_pic":profile, "action":"remove_player", "team_id":team_obj.id}
              TeamPlayers.objects.filter(player__id__in=remove_list).update(is_active=False)
              if player_user.id in added_player:
                  added_player.remove(player_user.id)
              if player_user.id in remove_list:
                  remove_list.remove(player_user.id)
              TaskRegistration(send_notification, (added_player, body), 1).delay()
              TaskRegistration(send_notification, (remove_list, body1), 1).delay()
              for i in added_player:
                  p = Players.objects.get(id = i)
                  save_notification(player = p, instance = team_obj, notification_type = 'T', message = "You are added to the team "+str(team_obj.name).title()+" by "+str(user.name).title()+". Let's Play!")
              for i in remove_list:
                  p = Players.objects.get(id = i)
                  save_notification(player = p, instance = team_obj, notification_type = 'T', message = "Uh oh! You have been removed from the team "+str(team_obj.name).title())
#              TeamPlayers.objects.filter(player__id__in=remove_list).update(is_active=False)
              return HttpResponse(json.dumps({"success":True}), status=201)
          else:
              return HttpResponse(json.dumps({"success":False, "detail":"Forbidden"}), status=403)
      except Exception as e:
          traceback.print_exc()
          return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

class EditTeamInfo(APIView):

	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		user = request.user
		try:
			team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
			is_admin = get_admin(user, team_obj)

			logo = None
			if team_obj.team_pic:
				logo = team_obj.team_pic.url

			result_dict = {
				"name":team_obj.name,
				"team_id":team_obj.id,
				"sport":team_obj.sport.name,
				"sport_id":team_obj.sport.id,
				"city":team_obj.city.name,
				"city_id":team_obj.city.id,
				"logo":logo,
				"description":team_obj.description,
			}
			return HttpResponse(json.dumps({"success":True, "data":result_dict, "is_admin":is_admin}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

	def put(self, request, pk=None):
		user = request.user
		try:
			data = request.data
			team_obj = Team.objects.filter(id=request.GET["id"], is_active=True)
			is_admin = get_admin(user, team_obj[0])
			if is_admin:
				description = data["description"]
				name = data["team_name"]
				city = data["city"]
				sport = data["sport"]

				city_obj = City.objects.get(id=int(city))
				sport_obj = SportsType.objects.get(id=int(sport), is_active=True)

				if len(set('[~!@#$%^&*()_+{}":;\']+$').intersection(name)):
					return HttpResponse(json.dumps({"error":"invalid characters in team name"}),status=400)

				if name.strip()=="" or len(name)==0:
					return HttpResponse(json.dumps({"error":"team name cannot be empty", "success":False}), status=400)

				team_obj.update(sport=sport_obj, city=city_obj, name=name.strip(), description=description)
				return HttpResponse(json.dumps({"success":True}), status=200)
			else:
				return HttpResponse(json.dumps({"success":False, "detail":"Forbidden"}), status=403)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)


def upcoming_matches(player_obj, paging):

	try:
		team_id = [i.team.id for i in TeamPlayers.objects.filter(player=player_obj)]
		match_stat_qs = Match_Stat.objects.filter(
				(Q(is_live=True) | Q(date_time__gt=timezone.now())) &
				Q(scorer1=player_obj.id) | Q(scorer2=player_obj.id)|
				Q(team1__id__in=team_id)|
				Q(team2__id__in=team_id),
				is_result_declared=False,
				is_canceled=False,
				).order_by("-id")

		live_list = []
		paginator = Paginator(match_stat_qs, 10)
		last_page = paginator.num_pages
		match_stat_team_id= None
		match_stat_team_id2= None
		try:
			page_data = paginator.page(paging)
		except PageNotAnInteger:
			page_data = paginator.page(1)
		except EmptyPage:
			page_data = paginator.page(paginator.num_pages)

		for i in page_data:
			match_stat = Match_Stat_Team.objects.filter(match_stat=i).distinct("id")
			is_scorer = False
			is_resume = False
			if i.is_live:
				is_live = True
				if player_obj.id == i.scorer1 or player_obj.id == i.scorer2:
					if i.match_state:
						is_resume = True
					is_scorer = True

				match_stat = Match_Stat_Team.objects.filter(match_stat=i).distinct("id")
				if match_stat.exists():
					match_stat_team_id =match_stat[0].id
					match_stat_team_id2 = match_stat[1].id
				else:
					match_stat_team_id = None
					match_stat_team_id2 = None

			else:
				is_live = False

			team1 = match_stat[0].team
			team2 = match_stat[1].team

			if team1.id in team_id:
				player_team = team1.name
				player_team_id = team1.id
				player_team_logo = None
				if team1.team_pic:
					player_team_logo = team1.team_pic.url

				vs_team = team2.name
				vs_team_id = team2.id
				vs_team_logo = None
				if team2.team_pic:
					vs_team_logo = team2.team_pic.url

			else:
				player_team = team2.name
				player_team_id = team2.id
				player_team_logo = None
				if team2.team_pic:
					player_team_logo = team2.team_pic.url

				vs_team = team1.name
				vs_team_id = team1.id
				vs_team_logo = None
				if team1.team_pic:
					vs_team_logo = team1.team_pic.url

			venue_name = None
			venue_address = None
			city_id = None
			venue_img = None

			if i.venue:
				venue = i.venue
				venue_name = venue.name
				venue_address = venue.address.city.name
				city_id = venue.address.city.id

				try:
					venue_img = VenueImg.objects.filter(venue=venue)[0].url.url
				except:
					pass

			try:
				striker = Match_Stat_Live.objects.get(match_stat=i)
			except:
				striker = None

			bowler_pic = None
			strike_pic = None
			non_strike_pic = None
			striker_player = None
			striker_player_id = None
			non_striker_player = None
			non_striker_player_id = None
			bowler_player= None
			bowler_player_id = None
			current_score = 0
			wickets_fall = 0
			striker_run = 0
			non_striker_run = 0
			striker_ball_faced = 0
			non_striker_ball_faced = 0
			bowler_over = 0
			overs = 0
			bowler_over = 0
			batting_team = None
			first_ining = None

			if striker!=None:
				match_stat_obj = striker.match_stat
				if striker.bowler:
					strikerr = striker
					striker_player = strikerr.strike.player.user.name

					striker_player_id = strikerr.strike.player.id
					non_striker_player = strikerr.non_strike.player.user.name
					non_striker_player_id = strikerr.non_strike.player.id
					bowler_player= strikerr.bowler.player.user.name
					bowler_player_id = strikerr.bowler.player.id

					if striker.strike.match_stat.first_ining == striker.strike.team.id:
							first_ining = True
							batting_team = striker.strike.team.name
					else:
						first_ining = False
						batting_team = striker.strike.team.name

					match_stat_batting = Match_Stat_Batting.objects.filter(Q(match_stat_team__match_stat=match_stat_obj)&Q(match_stat_team__team=striker.strike.team))
					if len(match_stat_batting)!=0:

						try:
							striker_run, striker_ball_faced = match_stat_batting.get(player=strikerr.strike.player).run, match_stat_batting.get(player=strikerr.strike.player).bowl

							non_striker_run, non_striker_ball_faced = match_stat_batting.get(player=strikerr.non_strike.player).run, match_stat_batting.get(player=strikerr.non_strike.player).bowl

						except Exception as e:
							traceback.print_exc()


						current_score = match_stat_batting[0].match_stat_team.total_run
						wickets_fall = match_stat_batting[0].match_stat_team.wickets
						overs = match_stat_batting[0].match_stat_team.overs

					try:
						match_stat_bowling = Match_Stat_Bowling.objects.get(match_stat_team__match_stat=match_stat_obj, player=striker.bowler.player)
						bowler_over = str(match_stat_bowling.overs)+"."+str(match_stat_bowling.bowl)+"-"+str(match_stat_bowling.maiden)+"-"+str(match_stat_bowling.run)+"-"+str(match_stat_bowling.wickets)
					except:
						pass


					if strikerr.bowler.player.user.profile_pic:
						bowler_pic = striker.bowler.player.user.profile_pic.url

					if strikerr.strike.player.user.profile_pic:
						strike_pic = strikerr.strike.player.user.profile_pic.url

					if strikerr.non_strike.player.user.profile_pic:
						non_strike_pic = strikerr.non_strike.player.user.profile_pic.url

			player_list = []

			team_match_players_qs = Team_Match_Player.objects.filter(match_stat=i,team__id=player_team_id)

			for j in team_match_players_qs.select_related('player__user'):
				image = None
				player = j.player
				user = player.user
				if user.profile_pic:
					image = user.profile_pic.url
				player_dict = {
					"name":user.name,
					"player_id":player.id,
					"profile_pic":image,
					"gender":user.gender,
					"address":{"city":user.city, "state":user.state},
					"sports":list(Players_Preferred_Sport.objects.filter(player=player).values("sport_name__name")),
				}
				player_list.append(player_dict)

			live_dict = {
				"player_team":player_team,
				"vs_team":vs_team,
				"player_team_id":player_team_id,
				"vs_team_id":vs_team_id,
				"vs_team_logo":vs_team_logo,
				"player_team_logo":player_team_logo,
				"striker_player":striker_player,
				"striker_player_id":striker_player_id,
				"strike_pic":strike_pic,
				"non_striker_player":non_striker_player,
				"non_striker_player_id":non_striker_player_id,
				"non_strike_pic":non_strike_pic,
				"venue":venue_name,
				"venue_address":venue_address,
				"city_id":city_id,
				"venue_img":venue_img,
				"bowler_player":bowler_player,
				"bowler_player_id":bowler_player_id,
				"bowler_pic":bowler_pic,
				"players":player_list,
				"date":str(i.date_time)[:10],
				"is_live":is_live,
				"is_resume":is_resume,
				"is_scorer":is_scorer,
				"current_score":current_score,
				"wickets_fall" : wickets_fall,
				"striker_run":striker_run,
				"non_striker_run":non_striker_run,
				"striker_ball_faced":striker_ball_faced,
				"non_striker_ball_faced":non_striker_ball_faced,
				"bowler_over":bowler_over,
				"batting_team":batting_team,
				"first_ining":first_ining,
				"match_stat_team_id2":match_stat_team_id2,
				"match_stat_team_id":match_stat_team_id,
				"match_stat_id":i.id,
				"venue_name":venue_name
			}
			live_list.append(live_dict)
		return (live_list, last_page)
	except Exception as e:
		traceback.print_exc()
		return "error"

def previous_matches(player_obj, paging):

	try:

		team_id = [i.team.id for i in TeamPlayers.objects.filter(player=player_obj)]
		print(player_obj.id, ">>>>>>>>>>>>>>")
		team_match_player = Team_Match_Player.objects.filter(
			Q(match_stat__is_result_declared=True) &
			Q(match_stat__is_canceled=False) &
			Q(match_stat__is_live=False),
			team__id__in=team_id).distinct("match_stat")

		match_stat_id = [x.match_stat.id for x in team_match_player]
		scorer = [x.id for x in Match_Stat.objects.filter(Q(scorer1=player_obj.id)|Q(scorer2=player_obj.id),is_live=False,is_result_declared=True,is_canceled=False)]
		match_stat_id.extend(scorer)
		print(match_stat_id, ">>>>>>>>>>>>>>>>>>>")

		match_stat_qs = Match_Stat.objects.filter(id__in=match_stat_id).order_by("-id")
		#match_stat_qs = Match_Stat.objects.filter(id__in=match_stat_id)
		list1 = []

		paginator = Paginator(match_stat_qs, 10)
		last_page = paginator.num_pages

		try:
			page_data = paginator.page(paging)
		except PageNotAnInteger:
			page_data = paginator.page(1)
		except EmptyPage:
			page_data = paginator.page(paginator.num_pages)

		for i in page_data:
			print(i, ">>>>>>>>>>>>>>>>>>>")
			match_stat = Match_Stat_Team.objects.filter(match_stat=i).distinct("id")
			if match_stat.exists():
				match_stat_team_id =match_stat[0].id
				match_stat_team_id2 = match_stat[1].id
			else:
				match_stat_team_id = None
				match_stat_team_id2 = None
			team1 = i.team1
			team2 = i.team2
			if team1.id in team_id:

				player_team_id = team1.id
				player_team_name = team1.name
				vs_team_id  = team2.id
				vs_team = team2.name
				player_team_logo = None
				vs_team_logo = None

				if i.winner==player_team_id:
					winner = player_team_name
					winner_id = player_team_id
				else:
					winner = vs_team
					winner_id = vs_team_id

				if team1.team_pic:
					player_team_logo = team1.team_pic.url

				if team2.team_pic:
					vs_team_logo = team2.team_pic.url

			else:
				player_team_id = team2.id
				player_team_name = team2.name
				vs_team_id  = team1.id
				vs_team = team1.name
				player_team_logo = None
				vs_team_logo = None

				if i.winner==player_team_id:
					winner = player_team_name
					winner_id = player_team_id
				else:
					winner = vs_team
					winner_id = vs_team_id
				if team2.team_pic:
					player_team_logo = team2.team_pic.url

				if team1.team_pic:
					vs_team_logo = team1.team_pic.url

			player_list = []
			team_match_players_qs = Team_Match_Player.objects.filter(match_stat=i,team__id=player_team_id)
			for j in team_match_players_qs.select_related('player__user'):
				image = None
				player = j.player
				user = player.user
				if user.profile_pic:
					image = user.profile_pic.url
				player_dict = {
					"name":user.name,
					"player_id":player.id,
					"profile_pic":image,
					"gender":user.gender,
					"address":{"city":user.city, "state":user.state},
					"sports":list(Players_Preferred_Sport.objects.filter(player=player).values("sport_name__name")),
				}
				player_list.append(player_dict)

			venue_name = None
			venue_address = None
			city_id = None
			venue_img = None
			if i.venue:
				venue = i.venue
				venue_name = venue.name
				venue_address = venue.address.city.name
				city_id = venue.address.city.id

				try:
					venue_img = VenueImg.objects.filter(venue=venue)[0].url.url
				except:
					pass

			result_dict = {
				"player_team":player_team_name,
				"player_team_id":player_team_id,
				"vs_team_id":vs_team_id,
				"vs_team":vs_team,
				"vs_team_logo":vs_team_logo,
				"player_team_logo":player_team_logo,
				"date":str(i.date_time)[:10],
				"won_by":str(i.won_by),
				"winner_team":winner,
				"winner_team_id":winner_id,
				"players":player_list,
				"venue":venue_name,
				"venue_address":venue_address,
				"city_id":city_id,
				"venue_img":venue_img,
				"match_stat_team_id":match_stat_team_id,
				"match_stat_team_id2":match_stat_team_id2,
				"match_stat_id":i.id,
			}
			list1.append(result_dict)
		#print(list1, ">fhdgsdghasgdhsgdh")
		return (list1, last_page)
	except Exception as e:
		traceback.print_exc()
		return "error"

class MyMatches(APIView):

	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		try:
			user = request.user
			player_obj = Players.objects.get(user=user, is_active=True)
			query = request.GET["query"]

			paging = request.GET.get('page')


			if query.lower()=="up":
				result = upcoming_matches(player_obj, paging)

			elif query.lower()=="pr":
				result = previous_matches(player_obj, paging)

			else:
				return HttpResponse(json.dumps({"error":"invalid quey parameter", "success":False}), status=400)

			if result=="error":
				return HttpResponse(json.dumps({"error":"some error", "success":False}), status=400)


			return HttpResponse(json.dumps({"success":True, "data":result[0], "last_page":result[1]}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

class Player_Teams(APIView):

	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		user = request.user
		try:
			team_id = [i.team.id for i in TeamPlayers.objects.filter(player__user=user, is_active=True)]
			team_id1 = [i.team.id for i in TeamAdmin.objects.filter(player__user=user, is_active=True)]
			team_id.extend(team_id1)
			team_qs = Team.objects.filter(id__in=team_id, is_active=True).order_by("-id")
			list1 = []
			for i in team_qs:
				logo = None
				if i.team_pic:
					logo = i.team_pic.url
				result_dict = {
				"name":i.name,
				"id":i.id,
				"sport":i.sport.name,
				"sport_id":i.sport.id,
				"created_by":i.created_by.user.name,
				"is_active":i.is_active,
				"team_pic":logo,
				"city":i.city.name
				}
				list1.append(result_dict)
			return HttpResponse(json.dumps({"success":True, "data":list1}),status=200)
		except Exception as e:
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

class EditPlayer(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		user = request.user
		try:
			player_obj = Players.objects.select_related("user").get(user=user, is_active=True)
			image = None
			if player_obj.user.profile_pic:
				image = player_obj.user.profile_pic.url
			sports_qs = SportsType.objects.filter(is_active=True)
			sport_list = []
			for i in sports_qs:
				try:
					prefer_sport = Players_Preferred_Sport.objects.get(player=player_obj, sport_name=i,is_active=True)
					is_selected = True
				except:
					is_selected = False
				sport_dict = {
					"name":i.name,
					"id":i.id,
					"is_selected":is_selected
				}
				sport_list.append(sport_dict)
			result_dict = {
				"name":player_obj.user.name,
				"dob":str(player_obj.user.dob),
				"city":player_obj.user.city,
				"sports":sport_list,
				"profile_pic":image
			}
			return HttpResponse(json.dumps({"success":True, "data":result_dict}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":True}), status=400)

	def post(self, request):
		user = request.user
		try:
			account_obj = Account.objects.get(id=user.id, is_active=True)
			player_obj = Players.objects.get(user=account_obj, is_active=True)

			data = request.data
			name = data["name"]
			dob = data["dob"]
			city = data["city_id"]
			sports = list(set(data["sports"]))
			if city!=0:
				city_obj = City.objects.get(id=city).name
				account_obj.city = city_obj
			sports_player_id = [z.id for z in Players_Preferred_Sport.objects.filter(player=player_obj)]
			sports_type = 	SportsType.objects.filter(id__in=sports,is_active=True)
			for i in sports_type:
				Players_Preferred_Sport.objects.create(sport_name=i, player=player_obj, is_active=True)
			if len(str(name).strip())==0:
				return HttpResponse(json.dumps({"error":"name cannot be empty", "success":False}),status=400)
			account_obj.name = name.strip()
			account_obj.dob = dob
			account_obj.save()
			Players_Preferred_Sport.objects.filter(id__in=sports_player_id).delete()
			return HttpResponse(json.dumps({"success":True}),status=201)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)

	def put(self, request, pk=None):
		try:
			user = request.user
			account_obj = Account.objects.get(id=user.id, is_active=True)
			player_obj = Players.objects.get(user=account_obj, is_active=True)
			data = request.data
			pic = data["image"]
			account_obj.profile_pic = pic
			account_obj.save()
			return HttpResponse(json.dumps({"success":True}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

class CityList(APIView):
	def get(self, request):
		try:
			city  = City.objects.all().distinct("name").exclude(name = '')
			appended = []
			final = []
			for c in city:
				if c.name:
					if c.name.casefold() not in appended:
						final.append({"id":c.id, "name":c.name, "pin":c.pin})
						appended.append(c.name.casefold())
#				print(appended)
#			city = [{"id":i.id, "name":i.name, "pin":i.pin} for i in City.objects.all().distinct("name").exclude(name='')]
			return Response({"data":final, "success":True}, status = 200)
#			return HttpResponse(json.dumps({"success":True, "data":final}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}),status=400)

class TeamSearch(APIView):

	def get(self, request):

		try:
			query = request.GET.get("query")
			tournament_id= request.GET.get('tournament_id')
			team_reg_qs= Team_Registration.objects.filter(Q(tournament__id= tournament_id) & Q(status= 'approve'))
			if query!="":
				team_qs = Team.objects.filter(is_active=True, name__istartswith=query).exclude(pk__in= [obj.team.id for obj in team_reg_qs])
			else:
				team_qs = Team.objects.filter(is_active=True).exclude(pk__in= [obj.team.id for obj in team_reg_qs])

			gen = (x for x in team_qs)
			list1 = []
			for i in gen:
				admin_qs= TeamAdmin.objects.filter(team= i, is_active= True)
				admin_serializer= []
				for obj in admin_qs:
					admin_serializer.append({'id':obj.player.user.id, 'name':obj.player.user.name, 'mobile':obj.player.user.mobile})
				result_dict = {"name":i.name, "id":i.id, 'admin':admin_serializer}
				list1.append(result_dict)
			print("data--->", list1)
			return HttpResponse(json.dumps({"success":True, "data":list1}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)

class BookmarkedVenue(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		user = request.user
		try:
			bookmark_qs = Bookmark.objects.select_related("venue").filter(user=user, is_bookmark=True).order_by("-id")
			list1 = []
			paging = request.GET.get('page')
			paginator = Paginator(bookmark_qs, 10)
			last_page = paginator.num_pages

			try:
				page_data = paginator.page(paging)
			except PageNotAnInteger:
				page_data = paginator.page(1)
			except EmptyPage:
				page_data = paginator.page(paginator.num_pages)
			for i in page_data:
				if i.venue:
					avg_rating = average_rating(i.venue)
					is_open = False
					opening = i.venue.time_of_opening.strftime('%H%M%S')
					closing = i.venue.closing.strftime('%H%M%S')
					current = datetime.now().strftime('%H%M%S')
					if(opening<current<closing):
						is_open = True
					venue_img = VenueImg.objects.filter(venue=i.venue)
					image = None
					if len(venue_img)!=0:
						image = venue_img[0].url.url
					result_dict = {
					"venue_img":image,
					"avg_rating":avg_rating ,
					"name": i.venue.name,
					"address":i.venue.address.name,
					"lat":i.venue.address.latitude,
					"contact": i.venue.contact,
					"created_at":str(i.venue.created_at),
					"id":i.venue.id,
					"distance":None,
					"time_of_opening":str(i.venue.time_of_opening),
					"total_field":Court.objects.filter(venue=i.venue, is_active=True).count(),
					"is_open":is_open,
					"bio":i.venue.bio,
					"lng":i.venue.address.longitude,
					"closing":str(i.venue.closing),
					"is_active":i.venue.is_active,
					"is_functional":i.venue.is_functional,
					"cost_per_game":cost_per_game(i.venue.id)
					}
					list1.append(result_dict)
			return HttpResponse(json.dumps({"success":True, "data":list1, "last_page":last_page}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)
