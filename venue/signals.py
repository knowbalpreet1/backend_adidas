from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete, pre_delete
from django.dispatch import receiver

from .models import BusinessUser
from notification.models import BusinessNotification
from authentication.async_task import send_otp
from venue.models import VenueAdminPermissionMap, VenueManagerPermissionMap, Venue, Court, Slot, Court_User, Court_Reservation
from notification.bviews import send_notification
from datetime import datetime


@receiver(post_save, sender=Venue)
def venue(sender, instance, created, **kwargs):
	print(instance.is_active, 'venue--------', instance._is_active)
	if not instance._is_active and not instance.is_active:
		'''when venue is created '''
		print(0)
		send_otp('8822242224', '{0}  has sent request for new venue: Name Of User: {1}, Phone Number: {2}, Email: {3}, Venue Name: {4}, Location: {5}'.format(instance.owner.name, instance.owner.name, instance.owner.mobile, instance.owner.email, instance.name, instance.address.city.location_name))

	if not instance._is_active and instance.is_active:
		'''when venue is approved by goplabook admin'''
		print('inside 1')
		send_otp(instance.owner.mobile, 'Congratulations, Your request for adding {0}  to your GOPLAYBOOK account is approved. It will be displayed among your venue lists.'.format(instance.name))
		message_body = {
				"title":"GoPlayBook", 
				"body":'Congratulations, Your request for adding {0}  to your GOPLAYBOOK account is approved. It will be displayed among your venue lists.'.format(instance.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.VenueList",
				"venue_id":instance.id
			}
		send_notification([instance.owner], message_body)
		b = BusinessNotification(user = instance.owner, message = 'Congratulations, Your request for adding {0}  to your GOPLAYBOOK account is approved. It will be displayed among your venue lists.'.format(instance.name))
		b.save()

	if instance._is_active and not instance.is_active:
		print('inside 2')
		send_otp('8822242224', '{0}  has sent request for deleting venue: Name Of User: {1}, Phone Number: {2}, Email: {3}, Venue Name: {4}, Location: {5}'.format(instance.owner.name, instance.owner.name, instance.owner.mobile, instance.owner.email, instance.name, instance.address.city.location_name))
		#send_notification([instance.owner], 'Congratulations, Your request for adding {0}  to your GOPLAYBOOK account is approved. It will be displayed among your venue lists.'.format(instance.name))		

@receiver(post_delete, sender=Venue)
def venue_delete(sender, instance, **kwargs):
	try:

		print(instance.is_active, 'venue--------', instance._is_active)
		if not instance._is_active and not instance.is_active:
			print('delete')
			send_otp(instance.owner.mobile, 'Dear {0}, your request for venue: {1} is not approved, our officals will be contacting you within 24hrs regarding the reasons of disapproval.'.format(instance.owner.name, instance.name))
			message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, your request for venue: {1} is not approved, our officals will be contacting you within 24hrs regarding the reasons of disapproval.'.format(instance.owner.name, instance.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.VenueList",
				"venue_id":instance.id
				}

			send_notification([instance.owner],message_body)
			b = BusinessNotification(user = instance.owner, message = 'Dear {0}, your request for venue: {1} is not approved, our officals will be contacting you within 24hrs regarding the reasons of disapproval.'.format(instance.owner.name, instance.name))
			b.save()
			print('after delete')
	except Exception as e:
		#traceback.print_exc()
		pass


@receiver(post_save, sender = Court_Reservation)
def courtReservation(sender, instance, created, **kwargs):
	ads = VenueAdminPermissionMap.objects.filter(venue = instance.court_slot.court.venue)
	managers = VenueManagerPermissionMap.objects.filter(venue = instance.court_slot.court.venue)
	admins = []
	mds = []
	if created:
		print(created, '++++++++++++++++++========================')
		if ads:
			admins = [a.admin for a in ads]
			for admin in ads: 
				send_otp(admin.admin.mobile, 'You have a booking from {0}, Venue : {1}, Ground : {2}, Slot {3} - {4}'.format(instance.offline_user.name, instance.court_slot.court.venue.name, instance.court_slot.court.name, instance.court_slot.start_time, instance.court_slot.end_time))
		if managers:
			mds = [m.manager for m in managers]
			for manager in managers:
				send_otp(manager.manager.mobile, 'You have a booking from {0}, Venue : {1}, Ground : {2}, Slot {3} - {4}'.format(instance.offline_user.name, instance.court_slot.court.venue.name, instance.court_slot.court.name, instance.court_slot.start_time, instance.court_slot.end_time))	

	# if ads:
	# 	for admin in ads:
	# 		send_otp(admin.admin.mobile, 'You have a booking from {0}, Venue : {1}, Ground : {2}, Slot {3} - {4}'.format(instance.offline_user.name, instance.court_slot.court.venue.name, instance.court_slot.court.name, instance.court_slot.start_time.strftime('%I:%M %p'), instance.court_slot.end_time).strftime('%I:%M %p'))

		b_list = admins+mds
		message_body = {
			"title":"GoPlayBook",
			"body":'You have a booking from {0}, Venue : {1}, Ground : {2}, Slot {3} - {4}'.format(instance.offline_user.name, instance.court_slot.court.venue.name, instance.court_slot.court.name, instance.court_slot.start_time, instance.court_slot.end_time), 
			"click_action":"fcm.ACTION.HELLO",
			"page":"example.Reservation",
			"venue_id":instance.court_slot.court.venue.id
			}

		send_notification(admins+mds,message_body)
		for b in b_list:
			b = BusinessNotification(user = b, message = 'You have a booking from {0}, Venue : {1}, Ground : {2}, Slot {3} - {4}'.format(instance.offline_user.name, instance.court_slot.court.venue.name, instance.court_slot.court.name, instance.court_slot.start_time, instance.court_slot.end_time))
			b.save()
		send_otp(instance.offline_user.phone, 'Your booking for {0}-{1} on {2} at {3} is confirmed'.format(instance.court_slot.start_time, instance.court_slot.end_time, instance.reserv_date, instance.court_slot.court.venue.name))
	if instance.check_in:
		admins = VenueAdminPermissionMap.objects.filter(venue = instance.court_slot.court.venue)
		managers  =VenueManagerPermissionMap.objects.filter(venue = instance.court_slot.court.venue)
		
		ads = []
		mds = []
		if admins:
			ads = [a.admin for a in admins]
			for admin in admins:
				send_otp(admin.admin.mobile, '{0} with confirmed booking just checked in at {1}'.format(instance.offline_user.name, instance.court_slot.court.venue.name))
		if managers:
			mds = [m.manager for m in managers]
			for manager in managers:
				send_otp(manager.manager.mobile, '{0} with confirmed booking just checked in at {1}'.format(instance.offline_user.name, instance.court_slot.court.venue.name))

		print("====================", ads+mds, "=====================================")
		message_body = {
			"title":"GoPlayBook",
			"body":'{0} with confirmed booking just checked in at {1}'.format(instance.offline_user.name, instance.court_slot.court.venue.name), 
			"click_action":"fcm.ACTION.HELLO",
			"page":"example.Reservation",
			"venue_id":instance.court_slot.court.venue.id
			}

		send_notification(ads + mds, message_body)
		b_list = ads+mds
		for b in b_list:
			b = BusinessNotification(user = b, message = '{0} with confirmed booking just checked in at {1}'.format(instance.offline_user.name, instance.court_slot.court.venue.name))
			b.save()



@receiver(post_save, sender = Court)
def courtnotify(sender, instance, created, **kwargs):
	if created:
		print('created', created)
		admins = VenueAdminPermissionMap.objects.filter(venue = instance.venue)
		managers = VenueManagerPermissionMap.objects.filter(venue = instance.venue)
		ads = []
		mds = []
		if admins:
			ads = [a.admin for a in admins]
			for admin in admins:
				send_otp(admin.admin.mobile, 'Dear {0}, New court {1} is succcessfully added to the venue {2}'.format(admin.admin.name, instance.name, instance.venue.name))
				message_body = {
					"title":"GoPlayBook",
					"body":'Dear {0}, New court {1} is succcessfully added to the venue {2}'.format(admin.admin.name, instance.name, instance.venue.name), 
					"click_action":"fcm.ACTION.HELLO",
					"page":"example.CourtList",
					"venue_id":instance.venue.id
					}
	
				send_notification([admin.admin], message_body)
				b = BusinessNotification(user = admin.admin, message = 'Dear {0}, New court {1} is succcessfully added to the venue {2}'.format(admin.admin.name, instance.name, instance.venue.name))
				b.save()
		if managers:
			for manager in managers:
				message_body = {
					"title":"GoPlayBook",
					"body":'Dear {0}, New court {1} is succcessfully added to the venue {2}'.format(manager.manager.name, instance.name, instance.venue.name), 
					"click_action":"fcm.ACTION.HELLO",
					"page":"example.CourtList",
					"venue_id":instance.venue.id
					}

				send_notification([manager.manager], message_body)
				b = BusinessNotification(user = manager.manager, message = 'Dear {0}, New court {1} is succcessfully added to the venue {2}'.format(manager.manager.name, instance.name, instance.venue.name))
				b.save()



@receiver(post_delete, sender = Court)
def courtdeletenotify(sender, instance, **kwargs):
	admins = VenueAdminPermissionMap.objects.filter(venue = instance.venue)
	managers = VenueManagerPermissionMap.objects.filter(venue = instance.venue)
	print(admins, managers, '>>>>>>>>>>>>>>>.')
	ads = []
	mds = []
	if admins:
		for a in admins:
			message_body = {
			"title":"GoPlayBook",
			"body":'Dear {0}, court {1} is succcessfully removed venue {2}'.format(a.admin.name, instance.name, instance.venue.name), 
			"click_action":"fcm.ACTION.HELLO",
			"page":"example.CourtList",
			"venue_id":instance.venue.id
			}

			send_notification([a.admin], message_body)			
			b = BusinessNotification(user = a.admin, message ='Dear {0}, court {1} is succcessfully removed venue {2}'.format(a.admin.name, instance.name, instance.venue.name))
			b.save()
	if managers:
		for m in managers:
			message_body = {
			"title":"GoPlayBook",
			"body":'Dear {0}, court {1} is succcessfully removed venue {2}'.format(m.manager.name, instance.name, instance.venue.name), 
			"click_action":"fcm.ACTION.HELLO",
			"page":"example.CourtList",
			"venue_id":instance.venue.id
			}

			send_notification([m.manager], message_body)
			b = BusinessNotification(user = m.manager, message ='Dear {0}, court {1} is succcessfully removed venue {2}'.format(a.admin.name, instance.name, instance.venue.name))
			b.save()


@receiver(pre_delete, sender = Court_Reservation)
def deletenotify(sender, instance, **kwargs):
	try:
		send_otp(instance.offline_user.phone, 'Your booking for {0}-{1} on {2} at {3} is cancelled'.format(instance.court_slot.start_time, instance.court_slot.end_time, instance.reserv_date, instance.court_slot.court.venue.name))
		
	except Exception as e:
		print(e)
		pass
