from django.shortcuts import render
from rest_framework.views import APIView
# Create your views here.
from players.models import Team,Players, Players_Preferred_Sport, TeamPlayers
from players.views import create_activity_thread
from venue.models import Venue, VenueImg
from .models import *
from django.http import HttpResponse
from datetime import datetime
import traceback
from django.utils import timezone
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.conf import settings
import time
from django.db.models import Q


def bowl_commentary(bowl):
	try:
		comm = ""
		over = bowl.match_stat_over
		bowler_name = bowl.bowler.user.name
		strike_name = bowl.strike.user.name
		non_strike_name = bowl.non_strike.user.name
		match_stat_team = over.match_stat_team
		match_stat = match_stat_team.match_stat

		if bowl.is_wicket:
			over.over_wicket += 1
			comm = bowler_name+" to "+strike_name+", OUT"
		if bowl.bowl_type == "wide":
			comm = bowler_name+" to "+strike_name+", wide"
		elif bowl.bowl_type == "noball":
			comm = bowler_name+" to "+strike_name+", (no ball)"
		elif bowl.bowl_type == "bye":
			comm = bowler_name+" to "+strike_name+", 1 bye"
		elif bowl.bowl_type == "legbye":
			comm = bowler_name+" to "+strike_name+", 1 leg bye"
		else:
			comm = bowler_name+" to "+strike_name

		if bowl.run == 0:
			comm = comm+", no run"
		elif bowl.run == 1:
			comm = comm+", 1 run"
		else:
			comm = comm+", "+str(bowl.run)+" runs"

		bowl.commentary = comm
		bowl.save()

		strike = Match_Stat_Batting.objects.get(match_stat_team=match_stat_team,player=bowl.strike)
		non_strike = Match_Stat_Batting.objects.get(match_stat_team=match_stat_team,player=bowl.non_strike)
		#if match_stat.team1 == match_stat_team.team:
		#match_stat_team1 = Match_stat_team.objects.get(team=match_stat.team2,match_stat=match_stat)
		#else:
		#match_stat_team1 = Match_stat_team.objects.get(team=match_stat.team1,match_stat=match_stat)
		bowler = Match_Stat_Bowling.objects.get(match_stat_team__match_stat=match_stat,player=bowl.bowler)

		with reversion.create_revision():

			over.strike_name = strike_name
			over.non_strike_name = non_strike_name
			over.bowl1_name = bowler_name

			over.strike_stat = str(strike.run)+"("+str(strike.bowl)+")"
			over.non_strike_stat = str(non_strike.run)+"("+str(non_strike.bowl)+")"

			over.bowl1_stat = str(bowler.overs)+"-"+str(bowler.maiden)+"-"+str(bowler.run)+"-"+str(bowler.wickets)

			if over.over_no!=0 and over.bowl2_stat!=None:
				over_no = over.over_no-1
				bowler2 = Match_Stat_Bowling.objects.get(match_stat_team=match_stat_team,player=Match_Stat_Over.objects.get(match_stat_team=match_stat_team,over_no=over_no).bowler)
				over.bowl2_stat = str(bowler2.overs)+"-"+str(bowler2.maiden)+"-"+str(bowler2.run)+"-"+str(bowler2.wickets)
			over.save()
			# reversion.set_user()
			reversion.set_comment(over.id)
	except Exception:
		traceback.print_exc()


# Updating each batsman data
def stat_bat(list1):

	try:
		match_stat_team_bat_id,strike,non_strike,bat_run,is_four,is_six,is_wicket,is_last_wicket,stat_out,bowl_type,bowler,who_player,bowl_no = list1
		match_stat_team = Match_Stat_Team.objects.get(id=match_stat_team_bat_id)
		with reversion.create_revision():
			bat_stat1 = Match_Stat_Batting.objects.get_or_create(match_stat_team=match_stat_team,player=strike)[0]
			if bowl_type !="bye" and bowl_type !="legbye":
				bat_stat1.run = bat_stat1.run + bat_run
			bat_stat1.is_strike=True
			total_run = bat_stat1.run
			if bowl_no != 0 and bowl_type != "wide":
				bat_stat1.bowl += 1
			if bat_stat1.bowl == 0:
				bat_stat1.strike_rate = 0.0
			else:
				bat_stat1.strike_rate = round((bat_stat1.run/bat_stat1.bowl)*100,2)

			if is_four and bowl_type != "bye" and bowl_type != "legbye":
				bat_stat1.fours += 1

			elif is_six and bowl_type !="bye" and bowl_type != "legbye":
				bat_stat1.six += 1

			elif is_wicket:

				if who_player.id!=strike.id:
					bat_stat = Match_Stat_Batting.objects.get_or_create(match_stat_team=match_stat_team,player=who_player)[0]
				else:
					bat_stat = bat_stat1
				bat_stat.is_out = True
				bat_stat.out_history = stat_out
				bat_stat.is_strike = False
				if is_last_wicket:
					bat_stat.is_last_wicket = True

				out_history = ""
				if stat_out.out_type == "caught":
					out_history = bat_stat.player.user.name+" c "+stat_out.whom.user.name+" b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				if stat_out.out_type == "caught&bowled":
					out_history = bat_stat.player.user.name+" c "+stat_out.whom.user.name+" b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				if stat_out.out_type == "caughtbehind":
					out_history = bat_stat.player.user.name+" c "+stat_out.whom.user.name+" b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "bowled":
					out_history = bat_stat.player.user.name+" b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "lbw":
					out_history = bat_stat.player.user.name+" lbw b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "runout":
					if stat_out.whom2 == 0 or stat_out.whom2 == None:
						out_history = bat_stat.player.user.name+" runout "+stat_out.whom.user.name+" b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
					else:
						out_history = bat_stat.player.user.name+" runout ("+stat_out.whom.user.name+", "+stat_out.whom2.user.name+") b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "stumped":
					out_history = bat_stat.player.user.name+" stumped "+stat_out.whom.user.name+" b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "hitwicket":
					out_history = bat_stat.player.user.name+" hit wicket b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "retired":
					bat_stat.is_retired = True
					out_history = bat_stat.player.user.name+" retired b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "retiredhurt":
					out_history = bat_stat.player.user.name+" retired hurt b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "retiredout":
					out_history = bat_stat.player.user.name+" retired out b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "obstructfield":
					out_history = bat_stat.player.user.name+" obstruct field b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "hittwice":
					out_history = bat_stat.player.user.name+" hit the ball twice b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "timedout":
					out_history = bat_stat.player.user.name+" timed out b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "handledball":
					out_history = bat_stat.player.user.name+" handled the ball b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)
				elif stat_out.out_type == "actionout":
					out_history = bat_stat.player.user.name+" action out b "+bowler.user.name+" "+str(total_run)+"("+str(bat_stat.bowl)+"b "+str(bat_stat.fours)+"x4 "+str(bat_stat.six)+"x6) SR: "+str(bat_stat.strike_rate)

				bat_stat.out_history = out_history
				stat_out.out_history = out_history
				if bat_stat.bowl == 0:
					bat_stat.strike_rate = 0.0
				else:
					bat_stat.strike_rate = round((bat_stat.run/bat_stat.bowl)*100,2)
				bat_stat.save()
				stat_out.save()
				#bat_stat.out_history = bat_stat.player.user.name+"to"+stat_out.strike.name+","+str(run)
			bat_stat1.save()
			Match_Stat_Batting.objects.get_or_create(match_stat_team=match_stat_team,player=non_strike)
			# reversion.set_user()
			reversion.set_comment(bat_stat1.id)
	except Exception:
		traceback.print_exc()


def fall_of_wickets(match_stat_team,is_wicket,bowl_no,overs,strike,bowler,total_run):

	try:
		stat = Match_Stat_Fall_Of_Wickets.objects.create(match_stat_team = match_stat_team)
		stat.player = strike
		stat.overs = str(overs)+"."+str(bowl_no)
		stat.score = total_run
		stat.save()
	except Exception:
		traceback.print_exc()


# Updating each bowler data
def stat_bowling(list2):

	try:
		match_stat_team_bowl_id,bowler,is_maiden,run,is_wicket,bowl_type,is_over_change,type_of_out = list2
		match_stat_team = Match_Stat_Team.objects.get(id=match_stat_team_bowl_id)
		bowl_stat = Match_Stat_Bowling.objects.get_or_create(match_stat_team=match_stat_team,player=bowler)[0]
		if is_maiden:
			bowl_stat.maiden += 1
		#if bowl_type != "bye" or bowl_type != "legbye" or bowl_type != "wide" or bowl_type != "noball":
		#	if run != 0:
		#		if bowl_stat.counter < 6:
		#			bowl_stat.counter += 1
		#			if bowl_stat.counter == 6:
		#				bowl_stat.maiden += 1
		#
		#		else:
		#			counter = 0
		#	else:
		#		counter = 0


		if bowl_type == "normal" or bowl_type == "bye" or bowl_type == "legbye":
			if bowl_type == "normal":
				bowl_stat.run += run
			if bowl_stat.bowl == 5:
				bowl_stat.overs += 1
				bowl_stat.bowl = 0
			else:
				bowl_stat.bowl += 1

		elif bowl_type == 'wide':
			bowl_stat.run += run
			bowl_stat.wide += 1

		elif bowl_type == 'noball':
			bowl_stat.run += run
			bowl_stat.no_ball += 1

		if bowl_stat.bowl == 0:
			if bowl_stat.overs == 0:
				bowl_stat.economy_rate = round((bowl_stat.run / 1),2)
			else:
				bowl_stat.economy_rate = round((bowl_stat.run / bowl_stat.overs),2)
		else:
			bowl_stat.economy_rate = round((bowl_stat.run / (bowl_stat.overs +(bowl_stat.bowl/6))),2)
		if is_wicket and type_of_out != "runout":
			bowl_stat.wickets += 1
		with reversion.create_revision():
			bowl_stat.save()
			reversion.set_comment(bowl_stat.id)

	except Exception:
		traceback.print_exc()
		return []



def match_stat_save(list5):
	try:
		with reversion.create_revision():
			match_stat,is_1st_ining_over,is_2nd_ining_over,i,match_state = list5
			match_stat.is_1st_ining_over = is_1st_ining_over
			match_stat.is_2nd_ining_over = is_2nd_ining_over
			if match_state != None:
				match_stat.match_state = json.loads(match_state)

			if i:
				if i['umpire1']:
					umpire1 = Players.objects.get(id=i['umpire1'],is_active=True)
					match_stat.umpire1 = umpire1.id
				if i['umpire2']:
					umpire2 = Players.objects.get(id=i['umpire2'],is_active=True)
					match_stat.umpire2 = umpire2.id
				if i['umpire3']:
					umpire3 = Players.objects.get(id=i['umpire3'],is_active=True)
					match_stat.umpire3 = umpire3.id
				if i['umpire4']:
					umpire4 = Players.objects.get(id=i['umpire4'],is_active=True)
					match_stat.umpire4 = umpire4.id
				if i['scorer1']:
					scorer1 = Players.objects.get(id=i['scorer1'],is_active=True)
					match_stat.scorer1 = scorer1.id
				if i['scorer2']:
					scorer2 = Players.objects.get(id=i['scorer2'],is_active=True)
					match_stat.scorer2 = scorer2.id
				if i['match_refree']:
					match_refree = Players.objects.get(id=i['match_refree'],is_active=True)
					match_stat.match_refree = match_refree.id
				if i['commentator']:
					commentator = Players.objects.get(id=i['commentator'],is_active=True)
					match_stat.commentator = commentator.id
			match_stat.save()

			# reversion.set_user()
			reversion.set_comment(match_stat.id)
	except Exception:
		traceback.print_exc()


def set_interval(match_stat,i,bowl,match_stat_team):
	try:

		strike = Match_Stat_Batting.objects.get(player=bowl.strike)
		non_strike = Match_Stat_Batting.objects.get(player=bowl.non_strike)

		Match_Stat_Interval.objects.create(match_stat=match_stat,bowl=bowl,bat1_run=strike.run,\
			bat2_run=non_strike.run,team_score=match_stat_team.total_run,team_wicket=match_stat_team.wickets,\
			notes=i['notes'],interval_type=i['interval_type'],date_time=datetime.now())
	except Exception:
		traceback.print_exc()

def scorer_note(match_stat,i,bowl):
	try:
		m_s_scorer = Match_Stat_Scorer.objects.create(match_stat=match_stat,scorer_type=i['scorer_type'],bowl=bowl,notes=i['notes'],\
			date_time=datetime.now())
	except Exception:
		traceback.print_exc()


def penalty(penalty_run,bowl,match_stat_team):
	try:
		for i in penalty_run:
			m = match_stat_team
			if i['match_stat_team_id'] != match_stat_team.id and i['run']>0:
				m = Match_Stat_Team.objects.get(id=i['match_stat_team_id'],match_stat=m.match_stat)
			m.total_run += i['run']
			m.penalty_run += i['run']
			m.extras += i['run']
			m.save()
			Match_Stat_Penalty.objects.create(match_stat_team=m,run=i['run'],bowl=bowl,date_time=datetime.now())
	except Exception:
		traceback.print_exc()

def set_partnership(bowl,stat_team):
	try:
		with reversion.create_revision():
			strike = Match_Stat_Batting.objects.get(player=bowl.strike,match_stat_team=stat_team)
			non_strike = Match_Stat_Batting.objects.get(player=bowl.non_strike,match_stat_team=stat_team)
			try:
				match = Match_Stat_Partnership.objects.get_or_create((Q(player1=strike.player) | Q(player1=non_strike.player) & Q(player2=strike.player) | Q(player2=non_strike.player)),match_stat_team=stat_team)
				if match[1]:
					match.player1_start_run = strike.run
					match.player2_start_run = non_strike.run
					match.player1_start_bowl = strike.bowl
					match.player2_start_bowl = non_strike.bowl
					match.part_start_score = stat_team.total_run
					match.start_bowl = bowl
				else:
					match.player1_end_run = strike.run
					match.player2_end_run = non_strike.run
					match.player1_end_bowl = strike.bowl
					match.player2_end_bowl = non_strike.bowl
					match.part_end_score = stat_team.total_run
					match.end_bowl = bowl

				if bowl.is_wicket:
					if bowl.match_stat_out.who == strike.player:
						match.who = strike.player
					else:
						match.who = non_strike.player
				match.save()
				# reversion.set_user()
				reversion.set_comment(match.id)
			except:
				pass
	except Exception:
		traceback.print_exc()

# Assign a player substitute
def set_supersub(data,match_id):
	try:
		for i in data:
			players = Team_Match_Player.objects.filter(match_stat=match_id,team=i['team_id'])
			players.update(super_sub=False)
			for j in i['player']:
				b = players.get(team=i["team_id"],player=j)
				b.super_sub = True
				b.save()
	except Exception:
		traceback.print_exc()

# Updating each batting team total summary data
def stat_team(list3):
	try:
		with reversion.create_revision():
			match_stat_team_bat_id,run,is_over_change,is_maiden,bowl_type,extra_run,bowl_no,is_wicket,strike,bowler,bowl,penalty_run,set_intervals,play_again,type_of_out,super_sub,team1_revised,team2_revised = list3

			match_stat_team = Match_Stat_Team.objects.get(id=match_stat_team_bat_id)

			match_stat_team.total_run += run

			if bowl_type == "normal" or bowl_type == "legbye" or bowl_type == "bye":

				if is_over_change:

					match_stat_team.overs += 1

					match_stat_team.run_against = 0
				else:
					if bowl_no != 0:
						match_stat_team.run_against += 1

			if bowl_type == "bye":
				match_stat_team.byes += extra_run

			if bowl_type == "legbye":
				match_stat_team.leg_byes += extra_run

			if bowl_type == "wide":
				match_stat_team.wide += extra_run

			if bowl_type == "noball":
				match_stat_team.no_ball += extra_run

			match_stat_team.extras += extra_run

			match_stat = match_stat_team.match_stat
			match_stat_team.is_submit = True

			if len(penalty_run)>0:
				penalty(penalty_run,bowl,match_stat_team)

			# Updating  of wickets table
			if is_wicket:
				if (type_of_out=="retired" or type_of_out=="retiredhurt") and play_again:
					pass
				else:
					match_stat_team.wickets += 1
					match_stat_team.save()
					fall_of_wickets(match_stat_team,is_wicket,bowl_no,match_stat_team.overs,strike,bowler,match_stat_team.total_run)
			time.sleep(0.25)
			bowl_commentary(bowl)

			if match_stat_team.run_against == 0:
				match_stat_team.run_rate = round((match_stat_team.total_run / (match_stat_team.overs+1)),2)

			else:
				match_stat_team.run_rate = round((match_stat_team.total_run / (match_stat_team.overs +(match_stat_team.run_against/6))),2)
			if super_sub:
				set_supersub(super_sub,match_stat_team.match_stat.id)

			match_stat_team.save()

			# if scorer_notes:
			# 	scorer_note(match_stat,scorer_notes)

			#if set_intervals:
			#	set_interval(set_intervals,bowl)
			set_partnership(bowl,match_stat_team)
			# reversion.set_user()
			reversion.set_comment(match_stat_team.id)
			if team1_revised:
				match_stat_team.revised_overs = team1_revised['revised_overs']
				match_stat_team.save()
			if team2_revised:
				team2 = Match_Stat_Team.objects.get(id=team2_revised['id'])
				team2.revised_overs = team2_revised['revised_overs']
				team2.revised_run = team2_revised['revised_run']
				team2.save()

	except Exception:
		traceback.print_exc()
		return []

# Updating live score of a match
def stat_live(list1):
	try:
		with reversion.create_revision():
			match_stat_id,strike,non_strike,bowler = list1
			strike_id = Team_Match_Player.objects.get(match_stat__id=match_stat_id,player=strike)
			non_strike_id = Team_Match_Player.objects.get(match_stat__id=match_stat_id,player=non_strike)
			bowler_id = Team_Match_Player.objects.get(match_stat__id=match_stat_id,player=bowler)
			match_stat_live = Match_Stat_Live.objects.get(match_stat=match_stat_id)
			match_stat_live.strike = strike_id
			match_stat_live.non_strike = non_strike_id
			match_stat_live.bowler = bowler_id
			match_stat_live.save()
			# reversion.set_user()
			reversion.set_comment(match_stat_live.id)
	except Exception:
		traceback.print_exc()
		return []

# Change keeper in a match
def change_keeper(keeper,bowl):
	try:
		team = bowl.match_stat_over.match_stat_team.team
		match_stat = bowl.match_stat_over.match_stat_team.match_stat
		old = keeper['old_keeper_id']
		new = keeper['new_keeper_id']

		old_1 = Team_Match_Player.objects.get(player=old,match_stat=match_stat)
		old_1.player_type = "batsman"
		old_1.save()

		new_1 = Team_Match_Player.objects.get(player=new,match_stat=match_stat)
		if new_1.player_type == "captain":
			new_1.player_type = "keeper&captain"
		else:
			new_1.player_type = "keeper"
		new_1.save()

	except Exception:
		traceback.print_exc()
		return []

# Updating each bowl module data
class Match_Bowl_Update(APIView):
	permission_classess = (TokenHasReadWriteScope,)
	def post(self,request,who=None):
		try:
			data = request.data['data']
			result = []
			for i in data:
				print (i)
				is_declared = i['is_declared']
				is_maiden = i['is_maiden']
				bowler_id = i['bowler_id']
				match_stat_team_bat_id = i['match_stat_team_bat_id']
				match_stat_team_bowl_id = i['match_stat_team_bowl_id']
				match_stat_id = i['match_stat_id']
				match_state = i.get('match_state')
				extra_run = i['extra_run']
				bat_run = i['bat_run']
				run = i['run']
				strike_id = i['strike_id']
				non_strike_id = i['non_strike_id']
				bowl_type = i['bowl_type']
				is_four = i['is_four']
				is_six = i['is_six']
				out_type = i['out_type']
				is_wicket = i['is_wicket']
				is_over_change = i['is_over_change']
				over_no = i['over_no']
				bowl_no = i['bowl_no']
				is_last_wicket = i['is_last_wicket']
				is_1st_ining_over = i['is_1st_ining_over']
				is_2nd_ining_over = i['is_2nd_ining_over']
				is_match_official = i['is_match_official']
				is_penalty = i['is_penalty']
				local_id = i['local_id']
				penalty_run = i['penalty_run']
				is_replace_bowler = i['is_replace_bowler']
				match_officials = i['match_officials']
				keeper = i['keeper']
				is_keeper_change = i['is_keeper_change']
				team1_revised = i.get('team1_revised')
				team2_revised = i.get('team2_revised')
				# scorer_notes = i['scorer_notes']
				stat_out = None
				super_sub = i.get('super_sub')
				undo_state = i.get('undo_state')

				type_of_out = out_type['type']
				who = out_type['who']
				whom = out_type['whom']
				whom2 = out_type['whom2']
				play_again = out_type['play_again']
				count_ball = out_type['count_ball']
				who_player = None
				whom_player = None
				whom2_player = None

				match_stat_team = Match_Stat_Team.objects.get(match_stat__id=match_stat_id,id=match_stat_team_bat_id)
				match_stat = match_stat_team.match_stat
				user = Players.objects.get(user=request.user)
				if user.id != match_stat.scorer1 and user.id != match_stat.scorer2:
					print ("11")
					return HttpResponse({"status":False,"message":"Not a Valid Scorer"},status=400)


				if int(match_stat.no_of_overs) <= over_no:
					print ("22")
					return HttpResponse(json.dumps({"status":False,"message":"Overs are completed"}),status=400)
				if match_stat.is_live != True or match_stat.is_canceled != False or match_stat.is_draw != False or match_stat.is_2nd_ining_over != False:
					print ("33")
					return HttpResponse(json.dumps({"status":False,"message":"Cannot bowl for this match"}),status=400)

				if is_declared:

					match_stat_team.is_submit = True
					match_stat_team.save()
					match_stat.is_1st_ining_over = True
					match_stat.save()
					break
				#  Creating or getting Over and updating runs if required
				stat_over = Match_Stat_Over.objects.get_or_create(match_stat_team=match_stat_team,over_no=over_no)[0]

				if run !=0:
					stat_over.run = stat_over.run+run
					stat_over.team_run = match_stat_team.total_run+run
					stat_over.save()

				with reversion.create_revision():
					# Creating Out object of a batsman in a bowl
					if is_wicket:
						if who != None:
							who_player = Players.objects.get(id=who)
						if whom != None:
							whom_player = Players.objects.get(id=whom)
							if whom2 != None:
								whom2_player = Players.objects.get(id=whom2)
						stat_out = Match_Stat_Out.objects.create(match_stat=match_stat.id,out_type=type_of_out,who=who_player,whom=whom_player,play_again=play_again,whom2=whom2_player)
						stat_out.save()
						if count_ball == False:
							print ("coming in count ball")
							result.append({"bowl_id":0,"local_id":local_id})
							continue

					try:
						strike = Team_Match_Player.objects.get(player__id=strike_id,match_stat__id=match_stat_id).player
						non_strike = Team_Match_Player.objects.get(player__id=non_strike_id,match_stat__id=match_stat_id).player
						bowler = Team_Match_Player.objects.get(player__id=bowler_id,match_stat__id=match_stat_id).player
					except :
						traceback.print_exc()
						return HttpResponse(json.dumps({"status":False,"message":"Not a valid player for this match"}),status=400)
					# Creating bowl entry to save per ball data
					try:
						Match_Stat_Bowl.objects.get(match_stat_over=stat_over,bowl_no=bowl_no)
						print ("Bowl Exist")
						result.append({"bowl_id":0,"local_id":local_id})
						continue
					except:
						pass
					if type_of_out == "retired" and play_again:
						is_wicket = False
					elif type_of_out == "retiredhurt" and play_again:
						is_wicket = False
					if bowl_no != 0:
						stat_bowl = Match_Stat_Bowl.objects.create(match_stat_over=stat_over,bowl_no=bowl_no,run=run,strike=strike,is_four=is_four,\
							non_strike=non_strike,bowler=bowler,bowl_type=bowl_type,is_wicket=is_wicket,match_stat_out=stat_out,bat_run=bat_run,\
							is_six=is_six,is_over_change=is_over_change,extra_run=extra_run,local_id=local_id,is_replace_bowler=is_replace_bowler)
						if undo_state:
							undo_state['over_id'] = stat_over.id
							undo_state['bowl_id'] = stat_bowl.id
							stat_bowl.undo_state = undo_state

						#if scorer_notes:
						#	stat_bowl.is_scorer = True
						#	stat_bowl.save()

						if is_penalty:
							stat_bowl.is_penalty = True

						if is_match_official:
							stat_bowl.is_match_official = True

						stat_bowl.save()
						#if set_intervals:
						#	stat_bowl.is_interval = True
						#	stat_bowl.save()
						# Update Live match table
						# reversion.set_user()
						if is_keeper_change:
							stat_bowl.is_keeper_change = True
							settings.LOOP.run_in_executor(None, change_keeper,keeper,stat_bowl)
						reversion.set_comment(stat_bowl.id)
				stat_bowl.request = i
				if bowl_no !=0:
					list1 = [match_stat_id,strike,non_strike,bowler]
					settings.LOOP.run_in_executor(None, stat_live,list1)
					#stat_live(list1)

					# Saving Bowler Detail data by async
					list2 = [match_stat_team_bowl_id,bowler,is_maiden,run,is_wicket,bowl_type,is_over_change,type_of_out]
					#settings.LOOP.run_in_executor(None, stat_bowling,list2)
					stat_bowling(list2)

				# Saving Batsman Detail data by async
				list3 = [match_stat_team_bat_id,strike,non_strike,bat_run,is_four,is_six,is_wicket,is_last_wicket,stat_out,bowl_type,bowler,who_player,bowl_no]
				#settings.LOOP.run_in_executor(None, stat_bat,list3)
				stat_bat(list3)

				# Updating Team score in a live match
				list4 = [match_stat_team_bat_id,run,is_over_change,is_maiden,bowl_type,extra_run,bowl_no,is_wicket,who_player,bowler,stat_bowl,penalty_run,match_officials,play_again,type_of_out,super_sub, team1_revised,team2_revised]
				#settings.LOOP.run_in_executor(None, stat_team,list4)
				stat_team(list4)

				# Updating Team score in a live match
				list5 = [match_stat,is_1st_ining_over,is_2nd_ining_over,match_officials,match_state]
				settings.LOOP.run_in_executor(None, match_stat_save,list5)
				#match_stat_save(list5)

				result.append({"bowl_id":stat_bowl.id,"over_id":stat_over.id,"local_id":local_id})
			print (result)
			return HttpResponse(json.dumps({"status":True,"data":result}),status=201)
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


#Creating a Match Stat object and assigning players to each team
class Team_Create_for_Match(APIView):
	def post(self,request):
		try:
			data = request.data
			team1_id = data['team1_id']
			team2_id = data['team2_id']
			team1_players = data['team1_players']
			team2_players = data['team2_players']
			no_of_overs = data['no_of_overs']
			no_of_players = data['no_of_players']
			venue_id = data['venue_id']
			type_of_bowl = data['type_of_bowl']
			umpire1 = data['umpire1']
			umpire2 = data['umpire2']
			umpire3 = data['umpire3']
			umpire4 = data['umpire4']
			match_id = data.get('match_id')
			scorer1 = data['scorer1']
			scorer2 = data['scorer2']
			match_refree = data['match_refree']
			commentator = data['commentator']
			saved_for_later = data['saved_for_later']
			venue = None
			venue_image= None
			print(data)

			if team1_id == team2_id:
				return HttpResponse(json.dumps({"status":False,"message":"Team cannot be same"}),status=400)
			if not match_id:
				venue = Venue.objects.get(id=venue_id)
			team1 = Team.objects.get(id=team1_id)
			team2 = Team.objects.get(id=team2_id)

			if match_id:
				match_stat = Match_Stat.objects.get(match_id= match_id)
				if not  match_stat.tournament_venue or not match_stat.date_time:
					return HttpResponse(json.dumps({"status":False,"message":"Match has been already played"}),status=400)
				if match_stat.is_result_declared or match_stat.is_live:
					return HttpResponse(json.dumps({"status":False,"message":"Match has been already played"}),status=400)
			else:
				match_stat = Match_Stat.objects.create(team1=team1,team2=team2,venue=venue)
			match_stat.no_of_overs=no_of_overs
			match_stat.no_of_players=no_of_players
			match_stat.type_of_bowl=type_of_bowl
			match_stat.umpire1=umpire1
			match_stat.umpire2=umpire2
			match_stat.scorer1=scorer1
			match_stat.scorer2=scorer2
			if not match_id:

				match_stat.venue=venue
			else:
				venue_image= None
				if match_stat.tournament_venue and not match_stat.tournament_venue.is_dummy:
					venue_image= VenueImg.objects.filter(venue=match_stat.tournament_venue.court.venue).first().url.url
			match_stat.match_refree=match_refree
			match_stat.commentator=commentator
			match_stat.m_type="L"
			match_stat.m_format="O"
			match_stat.umpire3=umpire3
			match_stat.umpire4=umpire4
			match_stat.save()
			# if len(team1_players)!=no_of_players or len(team2_players)!=no_of_players:
			# 	return HttpResponse(json.dumps({"status":False,"message":"No of players are unequal"}),status=400)

			for i in team1_players:

				player = Players.objects.get(id=i['id'])
				Team_Match_Player.objects.get_or_create(match_stat=match_stat,team=team1,player=player,player_type=i['player_type'])

			for i in team2_players:
				player = Players.objects.get(id=i['id'])
				Team_Match_Player.objects.get_or_create(match_stat=match_stat,team=team2,player=player,player_type=i['player_type'])

			if not saved_for_later:
				toss_win = data['toss_win']
				first_ining = data['first_ining']

				match_stat.toss_win = toss_win
				match_stat.first_ining = first_ining
				match_stat.date_time = datetime.now()
				match_stat.is_live = True
				match_stat.save()
				match_stat_team1 = Match_Stat_Team.objects.get_or_create(match_stat=match_stat,team=team1)[0]
				match_stat_team2 = Match_Stat_Team.objects.get_or_create(match_stat=match_stat,team=team2)[0]
				Match_Stat_Live.objects.create(match_stat=match_stat)

			if match_stat.venue:
				try:
					venue_pic = VenueImg.objects.get(venue=venue_id).url.url
				except:
					venue_pic = ""
			if match_id:
				venue_pic= venue_image
			return HttpResponse(json.dumps({"success":True,"match_stat_id":match_stat.id,"match_stat_team1_id":match_stat_team1.id,"match_stat_team2_id":match_stat_team2.id,"venue_pic":venue_pic}),status=201)
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


# Saving for later details of Match stat object

class Match_Stat_later(APIView):
	def post(self,request):
		try:
			data = request.data
			toss_win = data['toss_win']
			first_ining = data['first_ining']
			team1_id = data['team1_id']
			team2_id = data['team2_id']
			match_stat_id = data['match_stat_id']
			match_stat = Match_Stat.objects.get(id=match_stat_id)
			match_stat.first_ining = first_ining
			match_stat.toss_win = toss_win
			match_stat.is_live = True
			match_stat.save()

			team1 = Team.objects.get(id=team1_id)
			team2 = Team.objects.get(id=team2_id)

			Match_Stat_Team.objects.create(match_stat=match_stat,team=team1)
			Match_Stat_Team.objects.create(match_stat=match_stat,team=team2)
			Match_Stat_Live.objects.create(match_stat=match_stat)

			return HttpResponse(json.dumps({"success":True,"match_stat_id":match_stat.id}),status=201)
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


class Live_Matches(APIView):
	def get(self,request):
		try:
			matches = Match_Stat.objects.filter(is_live=True)
			result = []
			paginator=Paginator(matches,10)
			last_page = paginator.num_pages
			paging = request.GET.get("page")
			try:

				page_data = paginator.page(paging)

			except PageNotAnInteger:

				page_data = paginator.page(1)

			except EmptyPage:

				page_data = paginator.page(paginator.num_pages)
			for i in page_data.object_list:
				teams_data = list(Match_Stat_Team.objects.filter(match_stat=i).values('team__name','total_run','wickets','overs'))
				result.append({"date_time":datetime.strftime(i.date_time, "%Y/%m/%d %I:%M %p"),\
					"no_of_overs":i.no_of_overs,"toss_win":i.toss_win,"teams_data":teams_data,"is_1st_ining_over":\
					i.is_1st_ining_over,"is_2nd_ining_over":i.is_2nd_ining_over})
				teams_data = ""
			return HttpResponse(json.dumps({"data":result,"total_pages":last_page}),status=200)
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)

def bowl_data(m_s_over,match_stat_team):
	try:
		m_s_bowl = Match_Stat_Bowl.objects.filter(match_stat_over=m_s_over).order_by('bowl_no')
		if m_s_bowl.count() == 0:
			return {}
		player = m_s_bowl[0].bowler.user.name
		player_pic = ""
		if m_s_bowl[0].bowler.user.profile_pic:
			player_pic = m_s_bowl[0].bowler.user.profile_pic.url
		over_data = []
		bowler_data = []

		for i in m_s_bowl:
			if i.is_wicket:
				if i.run !=0:
					over_data.append(str(i.run)+"W")
				else:
					over_data.append("W")
			elif i.is_four:
				if i.bowl_type=="no_ball":
					over_data.append("4nb")
				elif i.bowl_type == "wide":
					over_data.append("4w")
				elif i.bowl_type == "legbye":
					over_data.append("4lb")
				else:
					over_data.append("4")
			elif i.is_six:
				if i.bowl_type=="noball":
					over_data.append("6nb")
				else:
					over_data.append("6")

			if i.bowl_type=="wide" and not i.is_four and not i.is_six:
				over_data.append("w")
			elif i.bowl_type=="bye" and not i.is_four and not i.is_six:
				over_data.append(str(i.extra_run)+"b")
			elif i.bowl_type=="legbye" and not i.is_four and not i.is_six:
				over_data.append(str(i.extra_run)+"lb")
			elif i.bowl_type == "noball" and not i.is_four and not i.is_six:
				over_data.append(str(i.extra_run)+"nb")
			elif i.bowl_type=="normal" and not i.is_four and not i.is_six and not i.is_wicket:
				over_data.append(str(i.bat_run))

		bowler = Match_Stat_Bowling.objects.get(match_stat_team=match_stat_team,player=m_s_bowl[0].bowler)
		bowler_data = {"overs":bowler.overs,"bowl":bowler.bowl,"maiden":bowler.maiden,"run":bowler.run,\
		"wickets":bowler.wickets,"economy":bowler.economy_rate,"over_data":over_data,"player":player, "player_pic":player_pic}

		return bowler_data
	except Exception:
		traceback.print_exc()
		return []



def non_live_summary(match,m_s_team):
	try:
		print ("team",m_s_team.id)
		m_s_batting = Match_Stat_Batting.objects.filter(match_stat_team=m_s_team).order_by('-run')
		if m_s_batting.count():
			strike = m_s_batting[0]
			non_strike = m_s_batting[1]

			strike_profile_pic = ""
			strike_player = ""
			strike_six = 0
			strike_fours = 0
			strike_bowl = 0
			strike_strike_rate = 0.0
			strike_run = 0
			strike_super_sub = False

			non_strike_profile_pic = ""
			non_strike_player = ""
			non_strike_six = 0
			non_strike_fours = 0
			non_strike_bowl = 0
			non_strike_strike_rate = 0.0
			non_strike_run = 0
			non_strike_super_sub = False

			t_m_1 = Team_Match_Player.objects.get(player=strike.player,team=m_s_team.team,match_stat=match)
			t_m_2 = Team_Match_Player.objects.get(player=non_strike.player,team=m_s_team.team,match_stat=match)
			if strike.player.user.profile_pic:
				strike_profile_pic = strike.player.user.profile_pic.url
			else:
				strike_profile_pic = ""
			strike_player = strike.player.user.name
			strike_six = strike.six
			strike_bowl = strike.bowl
			strike_fours = strike.fours
			strike_strike_rate = strike.strike_rate
			strike_run = strike.run
			strike_super_sub = t_m_1.super_sub

			if non_strike.player.user.profile_pic:
				non_strike_profile_pic = non_strike.player.user.profile_pic.url
			else:
				non_strike_profile_pic = ""
			non_strike_player = non_strike.player.user.name
			non_strike_six = non_strike.six
			non_strike_bowl = non_strike.bowl
			non_strike_fours = non_strike.fours
			non_strike_strike_rate = non_strike.strike_rate
			non_strike_run = non_strike.run
			non_strike_super_sub = t_m_1.super_sub

			bat_records = {"strike_bat":{"name":strike_player,"run":strike_run,"bowls":strike_bowl,\
			"super_sub":strike_super_sub,"strike_profile_pic":strike_profile_pic,"four":strike_fours,\
			"six":strike_six,"strike_rate":strike_strike_rate},"non_strike_bat":{"name":non_strike_player,\
			"run":non_strike_run,"bowls":non_strike_bowl,"super_sub":non_strike_super_sub,\
			"non_strike_profile_pic":non_strike_profile_pic,"four":non_strike_fours,"six":non_strike_six,\
			"strike_rate":non_strike_strike_rate}}
		print ("calling",bat_records)
		return bat_records
	except Exception:
		traceback.print_exc()
		return {"strike_bat":{"name":"","run":0,"bowls":0,"super_sub":False,"strike_profile_pic":"","four":0,\
			"six":0,"strike_rate":0.0},"non_strike_bat":{"name":"","run":0,"bowls":0,"super_sub":False,\
			"non_strike_profile_pic":"","four":0,"six":0,"strike_rate":0.0}}

class Match_Summary(APIView):
	def get(self,request):
		try:
			match_stat_id = request.GET.get('match_stat_id')
			match = Match_Stat.objects.get(id=match_stat_id)
			team1 = match.team1
			team2 = match.team2
			bowl_records_t1 = []
			bowl_records_t2 = []

			if match.first_ining==team1.id:
				first = team1
				second = team2
			else:
				first = team2
				second = team1
			winner = None
			if match.winner == team1.id:
				winner = team1.name
			elif match.winner == team2.id:
				winner = team2.name
			else:
				pass
			m_s_team1 = Match_Stat_Team.objects.get(team=first,match_stat=match)
			m_s_team2 = Match_Stat_Team.objects.get(team=second,match_stat=match)

			# summary data of 1st ininig of match
			t1_summary_data = {"name":m_s_team1.team.name,"total_runs":m_s_team1.total_run,"wickets":m_s_team1.wickets,\
				"overs":m_s_team1.overs,"bowl":m_s_team1.run_against}

			t2_summary_data = {"name":m_s_team2.team.name,"total_runs":m_s_team2.total_run,"wickets":m_s_team2.wickets,\
				"overs":m_s_team2.overs,"bowl":m_s_team2.run_against}

			try:
				m_s_live = Match_Stat_Live.objects.get(match_stat=match)
			except :
				return HttpResponse(json.dumps({"status":"Match is not start yet"}),status=400)



			t_m_player1 = m_s_live.strike

			t_m_player2 = m_s_live.non_strike

			m_s_batting = Match_Stat_Batting.objects.filter(match_stat_team=m_s_team1)

			try:
				if match.is_live:
					try:
						strike = m_s_batting.get(match_stat_team=m_s_team1,player=t_m_player1.player)

						non_strike = m_s_batting.get(match_stat_team=m_s_team1,player=t_m_player2.player)
					except Exception:
						traceback.print_exc()
						temp1 = m_s_batting.filter(match_stat_team=m_s_team1,is_out = False).distinct('id')
						strike1 = temp1.filter(is_out=False,is_strike=True)
						non_strike1 = temp1.filter(is_out=False,is_strike=False)

						print (temp1.count(),strike1.count(),non_strike1.count())

						# if no non_strike player in ining
						if temp1.count() == 1 and strike1.count() == 1:
							non_strike = m_s_batting.filter(match_stat_team=m_s_team1,is_out=True).order_by('-id')
							if non_strike:
								non_strike = non_strike[0]
							strike = strike1[0]
							print ("striker",strike.player.user.name)

						# if no player on strike in ining
						elif temp1.count() == 1 and non_strike1.count() == 1:
							non_strike = non_strike1[0]
							strike = m_s_batting.objects.filter(match_stat_team=m_s_team1,is_out=True).distinct('id')[0]

						elif strike1.count() == 1:
							strike = strike1[0]
							print(temp1)
							if strike1[0] == temp1[0]:
								non_strike = temp1[1]
							else:
								non_strike = temp1[0]
						else:
							print ("{enter")
							strike = temp1[0]
							non_strike = temp1[1]

					try:
						t_m_1 = Team_Match_Player.objects.get(player=strike.player,team=m_s_team1.team,match_stat=match)
						if non_strike:
							t_m_2 = Team_Match_Player.objects.get(player=non_strike.player,team=m_s_team1.team,match_stat=match)
						if strike.player.user.profile_pic:
							strike_profile_pic = strike.player.user.profile_pic.url
						else:
							strike_profile_pic = ""
						strike_player = strike.player.user.name
						strike_six = strike.six
						strike_bowl = strike.bowl
						strike_fours = strike.fours
						strike_strike_rate = strike.strike_rate
						strike_run = strike.run
						strike_super_sub = t_m_1.super_sub
					except:
						traceback.print_exc()
						strike_profile_pic = ""
						strike_player = ""
						strike_six = 0
						strike_fours = 0
						strike_bowl = 0
						strike_strike_rate = 0.0
						strike_run = 0
						strike_super_sub = False
					try:
						print ("striker name",strike_player)
						if non_strike.player.user.profile_pic:
							non_strike_profile_pic = non_strike.player.user.profile_pic.url
						else:
							non_strike_profile_pic = ""
						non_strike_six = non_strike.six
						non_strike_fours = non_strike.fours
						non_strike_bowl = non_strike.bowl
						non_strike_strike_rate = non_strike.strike_rate
						non_strike_player = non_strike.player.user.name
						non_strike_run = non_strike.run
						non_strike_super_sub = t_m_2.super_sub

					except :
						traceback.print_exc()
						non_strike_profile_pic = ""
						non_strike_player = ""
						non_strike_six = 0
						non_strike_fours = 0
						non_strike_bowl = 0
						non_strike_strike_rate = 0.0
						non_strike_run = 0
						non_strike_super_sub = False

					bat_records_t1 = {"strike_bat":{"name":strike_player,"run":strike_run,"bowls":strike_bowl,\
					"super_sub":strike_super_sub,"strike_profile_pic":strike_profile_pic,"four":strike_fours,\
					"six":strike_six,"strike_rate":strike_strike_rate},"non_strike_bat":{"name":non_strike_player,\
					"run":non_strike_run,"bowls":non_strike_bowl,"super_sub":non_strike_super_sub,\
					"non_strike_profile_pic":non_strike_profile_pic,"four":non_strike_fours,"six":non_strike_six,\
					"strike_rate":non_strike_strike_rate}}
				else:
					bat_records_t1 = non_live_summary(match,m_s_team1)
			except Exception:
				traceback.print_exc()
				bat_records_t1 = {}

			m_s_over = []
			if not match.is_live:
				print ("coming here1")
				bowler = Match_Stat_Bowling.objects.filter(match_stat_team=m_s_team2).order_by('-wickets','-economy_rate')[0]
				if bowler:
					print ("coming herer ww")
					m_s_over = Match_Stat_Over.objects.filter(match_stat_team=m_s_team1,bowl1_name=bowler.player.user.name)[:2]

			else:
				print ("coming here")
				m_s_over = Match_Stat_Over.objects.filter(match_stat_team=m_s_team1)[:2]
			bowler_name_t2 = ""
			if m_s_over:
				bowler_name_t2 = m_s_over[0].bowl1_name

			try:
				last_wicket = m_s_batting.get(is_last_wicket=True)

				t1_summary_data.update({"last_wicket":last_wicket.out_history})

			except Exception:
				t1_summary_data.update({"last_wicket":[]})

			for i in m_s_over:
				bowl_records_t2.append(bowl_data(i,m_s_team2))
			t1_summary_data.update({"bat_records":bat_records_t1})
			t1_summary_data.update({"bowl_records":bowl_records_t2})
			t1_summary_data.update({"bowler_name":bowler_name_t2})
			t1_summary_data.update({"bowler_super_sub":False})


			t_m_player1 = m_s_live.strike
			t_m_player2 = m_s_live.non_strike

			m_s_batting = Match_Stat_Batting.objects.filter(match_stat_team=m_s_team2)
			over_exist = Match_Stat_Over.objects.filter(match_stat_team=m_s_team2)

			try:
				if match.is_live and m_s_batting.count() and over_exist:

					try:
						strike = m_s_batting.get(match_stat_team=m_s_team2,player=t_m_player1.player)

						non_strike = m_s_batting.get(match_stat_team=m_s_team2,player=t_m_player2.player)
					except:
						temp2 = m_s_batting.filter(match_stat_team=m_s_team2,is_out = False).distinct('id')
						strike2 = temp2.filter(is_out=False,is_strike=True)
						non_strike2 = temp2.filter(is_out=False,is_strike=False)

						if strike2.count() == 1 and temp2.count() == 1:
							strike = strike2[0]
							non_strike = m_s_batting.filter(match_stat_team=m_s_team2,is_out=True).distinct('id')
							if non_strike:
								non_strike = non_strike[0]

						elif non_strike2.count() == 1 and temp2.count() == 1:
							non_strike = non_strike2[0]
							strike = m_s_batting.filter(match_stat_team=m_s_team2,is_out=True).distinct('id')[0]

						elif strike2.count() == 1:
							strike = strike2[0]
							if strike2[0] == temp2[0]:
								non_strike = temp2[1]
							else:
								non_strike = temp2[0]
						else:
							strike = temp2[0]
							non_strike = temp2[1]

					try:
						t_m_1 = Team_Match_Player.objects.get(player=strike.player,team=m_s_team2.team,match_stat=match)
						t_m_2 = Team_Match_Player.objects.get(player=strike.player,team=m_s_team2.team,match_stat=match)
						if strike.player.user.profile_pic:
							strike_profile_pic = strike.player.user.profile_pic.url
						else:
							strike_profile_pic =""
						strike_player = strike.player.user.name
						strike_six = strike.six
						strike_bowl = strike.bowl
						strike_fours = strike.fours
						strike_strike_rate = strike.strike_rate
						strike_run = strike.run
						strike_super_sub = t_m_1.super_sub
					except:
						strike_profile_pic = ""
						strike_player = ""
						strike_six = 0
						strike_fours = 0
						strike_bowl = 0
						strike_strike_rate = 0.0
						strike_run = 0
						strike_super_sub = False

					try:
						if non_strike.player.user.profile_pic:
							non_strike_profile_pic = non_strike.player.user.profile_pic.url
						else:
							non_strike_profile_pic = ""
						non_strike_six = non_strike.six
						non_strike_fours = non_strike.fours
						non_strike_bowl = non_strike.bowl
						non_strike_strike_rate = non_strike.strike_rate
						non_strike_player = non_strike.player.user.name
						non_strike_run = non_strike.run
						non_strike_super_sub = t_m_2.super_sub
					except :
						non_strike_profile_pic = ""
						non_strike_player = ""
						non_strike_six = 0
						non_strike_fours = 0
						non_strike_bowl = 0
						non_strike_strike_rate = 0.0
						non_strike_run = 0
						non_strike_super_sub = False

					bat_records_t2 = {"strike_bat":{"name":strike_player,"run":strike_run,"bowls":strike_bowl,\
					"strike_profile_pic":strike_profile_pic,"four":strike_fours,"six":strike_six,"strike_rate":strike_strike_rate\
					,"super_sub":strike_super_sub},"non_strike_bat":{"name":non_strike_player,"four":non_strike_fours,\
					"six":non_strike_six,"super_sub":non_strike_super_sub,"run":non_strike_run,"bowls":non_strike_bowl,\
					"non_strike_profile_pic":non_strike_profile_pic,"strike_rate":non_strike_strike_rate}}

				else:
					if over_exist:
						bat_records_t2 = non_live_summary(match,m_s_team2)
			except Exception:
				traceback.print_exc()
				bat_records_t2 = {}

			m_s_over = []
			if not match.is_live and over_exist:
				bowler = Match_Stat_Bowling.objects.filter(match_stat_team=m_s_team1).order_by('-wickets','-economy_rate')[0]
				if bowler:
					m_s_over = Match_Stat_Over.objects.filter(match_stat_team=m_s_team2,bowl1_name=bowler.player.user.name)[:2]
			else:
				m_s_over = Match_Stat_Over.objects.filter(match_stat_team=m_s_team2)[:2]
				bat_records_t2 = {}

			bowler_name_t1 = ""
			if m_s_over:
				bowler_name_t1 = m_s_over[0].bowl1_name
			for i in m_s_over:

				bowl_records_t1.append(bowl_data(i,m_s_team1))

			try:
				last_wicket = m_s_batting.get(is_last_wicket=True)

				t2_summary_data.update({"last_wicket":last_wicket.out_history})

			except Exception:
				t2_summary_data.update({"last_wicket":[]})

			t2_summary_data.update({"bat_records":bat_records_t2})
			t2_summary_data.update({"bowl_records":bowl_records_t1})
			t2_summary_data.update({"bowler_name":bowler_name_t1})
			t2_summary_data.update({"bowler_super_sub":False})

			result = {"name":match.name,"no_of_overs":match.no_of_overs,"team1_summary":t1_summary_data,"team2_summary":t2_summary_data,\
			"toss_win":match.toss_win,"umpire1":match.umpire1,"umpire2":match.umpire2,"scorer1":match.scorer1,"scorer2":match.scorer2,\
			"match_refree":match.match_refree,"is_1st_ining_over":match.is_1st_ining_over,"is_2nd_ining_over":match.is_2nd_ining_over,\
			"is_canceled":match.is_canceled,"is_break":match.is_break,"is_draw":match.is_draw,"winner":winner,"won_by":\
			match.won_by,"dls_won":match.dls_won}

			print (result)
			return HttpResponse(json.dumps(result),status=200)
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)

def bowl_data_commentary(m_s_over,type1):
	try:
		if type1 == "wicket":
			m_s_bowl = Match_Stat_Bowl.objects.filter(match_stat_over=m_s_over,is_wicket=True).order_by('-id')
		elif type1 == "boundary":
			m_s_bowl = Match_Stat_Bowl.objects.filter(Q(is_four=True) | Q(is_six=True),match_stat_over=m_s_over).order_by('-id')
		else:
			m_s_bowl = Match_Stat_Bowl.objects.filter(match_stat_over=m_s_over).order_by('-id')
		bowl_data = []
		out_history = None
		strike_pic = ""
		non_strike_pic = ""
		bowl_data1 = []
		bowler_pic = ""
		try:
			m_s_bowl.get(is_over_change=True)
			over_complete = True
		except:
			over_complete = False

		for i in m_s_bowl:
			bowl_type = ""
			a1 = ""
			out_history = ""
			if i.is_wicket:
				bowl_type = "wicket"
				out_history = i.match_stat_out.out_history
				if i.run !=0:
					a1 = (str(i.run)+"W")
				else:
					bowl_type = "wicket"
					a1 = "W"
			elif i.is_four:
				if i.bowl_type=="no_ball":
					bowl_type = "noball"
					a1 = "4nb"
				else:
					bowl_type = "normal"
					a1 = "4"
			elif i.is_six:
				if i.bowl_type=="no_ball":
					bowl_type = "noball"
					a1 = "6nb"
				else:
					bowl_type = "normal"
					a1= "6"

			if i.bowl_type=="wide" and not i.is_four and not i.is_six and bowl_type == "":
				a1 = "w"
				bowl_type = "wide"
			elif i.bowl_type=="bye" and not i.is_four and not i.is_six and bowl_type == "":
				a1 = str(i.extra_run)+"b"
				bowl_type = "bye"
			elif i.bowl_type=="legbye" and not i.is_four and not i.is_six and bowl_type == "":
				a1 = str(i.extra_run)+"lb"
				bowl_type = "legbye"
			elif i.bowl_type == "noball" and not i.is_four and not i.is_six and bowl_type == "":
				bowl_type = "noball"
				a1 = str(i.extra_run)+"nb"
			elif i.bowl_type=="normal" and not i.is_four and not i.is_six and bowl_type == "":
				bowl_type = "normal"
				a1 = str(i.bat_run)
			bowl_data.append({"bowl_no":i.bowl_no,"commentary":i.commentary,"bowl_id":i.id,\
				"outcome":a1,"out_history":out_history,"bowl_type":bowl_type})
		if m_s_bowl.count()>0:
			if m_s_bowl[0].strike.user.profile_pic:
				strike_pic = m_s_bowl[0].strike.user.profile_pic.url
			if m_s_bowl[0].non_strike.user.profile_pic:
				non_strike_pic = m_s_bowl[0].non_strike.user.profile_pic.url
			if m_s_bowl[0].bowler.user.profile_pic:
				bowler_pic = m_s_bowl[0].bowler.user.profile_pic.url
			bowl_data1 = {"bowl_data":bowl_data,"strike_pic":strike_pic,"non_strike_pic":non_strike_pic,"over_complete":over_complete,\
				"bowler_pic":bowler_pic}
		return json.dumps(bowl_data1)
	except Exception:
		traceback.print_exc()
		return []

class Match_Commentary(APIView):
	def get(self,request):
		try:
			type1 = "full"
			type1 = request.GET.get('type')
			team1_data = {}
			team2_data = {}

			if not type1:
				type1 = "full"
			match_stat_team_id = request.GET.get("match_stat_team_id")
			match_stat_team = Match_Stat_Team.objects.get(id=match_stat_team_id)
			m_s_over = Match_Stat_Over.objects.filter(match_stat_team=match_stat_team).order_by('-over_no')
			#if m_s_over.count() == 0:
			#	return HttpResponse(json.dumps({"status":False,"message":"Innings not started"}),status=400)
			#print("over",m_s_over[0].id)
			match_stat = match_stat_team.match_stat
			team1 = match_stat.team1
			team2 = match_stat.team2
			if team1 == match_stat_team.team:
				team1_data = {"team_name":team1.name,"run":match_stat_team.total_run,"wickets":match_stat_team.wickets}
				m_s_team = Match_Stat_Team.objects.get(match_stat=match_stat,team=team2)
				team2_data = {"team_name":team2.name,"run":m_s_team.total_run,"wickets":m_s_team.wickets}
			else:
				team1_data = {"team_name":team2.name,"run":match_stat_team.total_run,"wickets":match_stat_team.wickets}
				m_s_team = Match_Stat_Team.objects.get(match_stat=match_stat,team=team1)
				team2_data = {"team_name":team1.name,"run":m_s_team.total_run,"wickets":m_s_team.wickets}
			#m_s_bowl = Match_Stat_Bowl.objects.filter(match_stat_over=m_s_over)[0]

			result = []

			paginator=Paginator(m_s_over,5)
			last_page = paginator.num_pages
			paging = request.GET.get("page")
			try:

				page_data = paginator.page(paging)

			except PageNotAnInteger:

				page_data = paginator.page(1)

			except EmptyPage:

				page_data = paginator.page(paginator.num_pages)

			for i in page_data.object_list:
				bowl_data = json.loads(bowl_data_commentary(i,type1))

				if bowl_data:

					over_data = {"strike_name":i.strike_name,"strike_stat":i.strike_stat,"non_strike_name":i.non_strike_name,\
					"bowl1_name":i.bowl1_name,"bowl2_name":i.bowl2_name,"bowl1_stat":i.bowl1_stat,"bowl2_stat":\
					i.bowl2_stat,"strike_pic":bowl_data['strike_pic'],"non_strike_pic":bowl_data['non_strike_pic'],\
					"bowl2_stat":i.bowl2_stat,"over_wicket":i.over_wicket,"run":i.run,"total_run":i.team_run,\
					"over_complete":bowl_data['over_complete'],"total_wicket":match_stat_team.wickets,"non_strike_stat":\
					i.non_strike_stat,"bowler1_pic":bowl_data['bowler_pic']}

					result.append({"over_no":i.over_no,"bowl_data":bowl_data['bowl_data'],"over_data":over_data})
				else:
					continue
			return HttpResponse(json.dumps({"data":result,"total_pages":last_page,"my_team":team1_data,"other_team":team2_data}))
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


def batsman_records(match_stat_team,match,team):
	try:

		m_s_bat = []
		for i in Match_Stat_Batting.objects.filter(match_stat_team=match_stat_team).order_by('id'):
			#super_sub = Team_Match_Player.objects.get(team=team,match_stat=match,player=i.player).super_sub
			super_sub = False
			image = ""
			#.split('(')[0][:-2]
			user = i.player.user
			if user.profile_pic:
				image = user.profile_pic.url
			out_history = i.out_history
			if out_history:
				out_history = out_history.rsplit('(',1)[0][:-2].replace(user.name,"")
			m_s_bat.append({"player__user__name":user.name,"player__user__profile__pic":image,"player__id":i.player.id,\
			"run":i.run,"bowl":i.bowl,"strike_rate":i.strike_rate,"is_out":i.is_out,"out_history":out_history,\
			"is_strike":i.is_strike,"fours":i.fours,"six":i.six,"super_sub":super_sub})

		return m_s_bat
	except Exception:
		traceback.print_exc()
		return []

def bowler_records(match_stat_team,match,team):
	try:
		m_s_bowl = []
		for i in Match_Stat_Bowling.objects.filter(match_stat_team=match_stat_team).order_by('id'):
			#super_sub = Team_Match_Player.objects.get(team=team,match_stat=match,player=i.player).super_sub
			super_sub = False
			image = ""
			if i.player.user.profile_pic:
				image = i.player.user.profile_pic.url
			m_s_bowl.append({"player__user__name":i.player.user.name,"player__user__profile__pic":image,"player__id":i.player.id\
			,"overs":i.overs,"maiden":i.maiden,"run":i.run,"wickets":i.wickets,"wide":i.wide,"no_ball":i.no_ball,\
			"economy_rate":i.economy_rate,"bowl":i.bowl,"super_sub":super_sub})
		return m_s_bowl
	except Exception:
		traceback.print_exc()
		return []


def wickets_scorecard(match_stat_team):
	try:
		f_o_wickets = []
		for i in Match_Stat_Fall_Of_Wickets.objects.filter(match_stat_team=match_stat_team):
			image = ""
			if i.player.user.profile_pic:
				image = i.player.user.profile_pic.url
			f_o_wickets.append({"player__user__name":i.player.user.name,"player__user__profile__pic":image,"player__id":i.player.id,"overs":i.overs,"score":i.score})
		return f_o_wickets
	except Exception:
		traceback.print_exc()
		return []


class Match_Scorecard(APIView):
	def get(self,request):
		try:
			match_stat_id = request.GET.get("match_stat_id")
			match = Match_Stat.objects.get(id=match_stat_id)
			team1 = match.team1
			team2 = match.team2

			if match.first_ining==team1.id:
				first = team1
				second = team2
			else:
				first = team2
				second = team1

			m_s_team1 = Match_Stat_Team.objects.get(team=first,match_stat=match)
			m_s_team2 = Match_Stat_Team.objects.get(team=second,match_stat=match)


			extras_t1 = {"total":m_s_team1.extras,"wide":m_s_team1.wide,"leg_bye":m_s_team1.leg_byes,"bye":m_s_team1.byes,\
				"no_ball":m_s_team1.no_ball,"penalty":m_s_team1.penalty_run}

			extras_t2 = {"total":m_s_team2.extras,"wide":m_s_team2.wide,"leg_bye":m_s_team2.leg_byes,"bye":m_s_team2.byes,\
				"no_ball":m_s_team2.no_ball,"penalty":m_s_team2.penalty_run}
			t1_summary_data = {"name":m_s_team1.team.name,"total_runs":m_s_team1.total_run,"wickets_count":m_s_team1.wickets,\
				"overs":m_s_team1.overs,"bowl":m_s_team1.run_against,"extras":extras_t1}

			t2_summary_data = {"name":m_s_team2.team.name,"total_runs":m_s_team2.total_run,"wickets_count":m_s_team2.wickets,\
				"overs":m_s_team2.overs,"bowl":m_s_team2.run_against,"extras":extras_t2}


			# TEAM 1 MATCH SCORECARD
			t1_bat_records = batsman_records(m_s_team1,match,team1)

			t2_bowl_records = bowler_records(m_s_team2,match,team2)

			t1_wickets = wickets_scorecard(m_s_team1)

			t1_summary_data.update({"bat_records":t1_bat_records,"bowl_records":t2_bowl_records,"wickets":t1_wickets})

			# TEAM 2 MATCH SCORECARD
			t2_bat_records = batsman_records(m_s_team2,match,team2)

			t1_bowl_records = bowler_records(m_s_team1,match,team1)

			t2_wickets = wickets_scorecard(m_s_team2)

			t2_summary_data.update({"bat_records":t2_bat_records,"bowl_records":t1_bowl_records,"wickets":t2_wickets})

			m_s_data = {"name":match.name,"no_of_overs":match.no_of_overs,"datetime":datetime.strftime(match.date_time, "%Y/%m/%d %I:%M %p"),"toss_win":match.toss_win,\
			"is_live":match.is_live,"is_canceled":match.is_canceled,"is_draw":match.is_draw,"is_break":match.is_break,"team1":\
			t1_summary_data,"team2":t2_summary_data}

			return HttpResponse(json.dumps(m_s_data))

		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)

class Match_Players(APIView):
	def get(self,request):
		try:
			venue_id= None
			venue_name= None
			is_dummy= True
			try:
				match_id = request.GET.get("match_id")
				match =	Match_Stat.objects.get(match_id=match_id)
				if match.is_live == True or match.is_result_declared == True:
					return HttpResponse(json.dumps({"status":"Cannot play this match"}),status=400)
				if match.tournament_venue:
					venue_obj= match.tournament_venue
					if venue_obj.is_dummy:
						venue_id= venue_obj.id
						venue_name= venue_obj.court_name
						is_dummy=True
					else:
						venue_id= venue_obj.id
						venue_name= venue_obj.court.venue.name + str(" ( ")+ venue_obj.court.name+str(" )")
						is_dummy= False
			except:
				match_stat_id = request.GET.get("match_stat_id")
				match =	Match_Stat.objects.get(id=match_stat_id)
#			match_stat_id = request.GET.get("match_stat_id")
#			match = Match_Stat.objects.get(id=match_stat_id)
			img= None
			if match.team1.team_pic:
				img= match.team1.team_pic.url
			team_a= {'id':match.team1.id, 'team_name':match.team1.name, 'image':img}
			team1 = list(TeamPlayers.objects.filter(team=match.team1, is_active= True).values("player__user__name","id","player__id","player__user__id", "player__user__profile_pic"))
			team_a.update({'players':team1})

			img= None
			if match.team2.team_pic:
				img= match.team2.team_pic.url
			team_b= {'id':match.team2.id, 'team_name':match.team2.name, 'image':img}
			team2 = list(TeamPlayers.objects.filter(team=match.team2).values("player__user__name","id","player__id","player__user__id", "player__user__profile_pic"))
			team_b.update({'players':team2})
			return HttpResponse(json.dumps({"team1":team_a,"team2":team_b, 'success':True, 'venue_id':venue_id, 'venue_name':venue_name, 'is_dummy':is_dummy}))
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


class Match_Set(APIView):
	def post(self,request):
		try:
			print (request.data)
			match_stat_id = request.data["match_stat_id"]
			is_draw = request.data["is_draw"]
			is_break = request.data['is_break']
			is_result_declared = request.data['is_result_declared']
			#declared_type_bat = request.data['declared_type_bat']
			won_by = request.data['won_by']
			player_id = request.data['player_id']
			dls_won = request.data['dls_won']
			winner = request.data['winner']

			match = Match_Stat.objects.get(id=match_stat_id)
			if match.is_live:
				if not is_draw:
					match.is_draw = False
				else:
					match.is_draw = is_draw
				match.is_break = is_break
				match.is_result_declared = is_result_declared
				if match.tournament_round:
					match.tournament_round.is_result_declared = is_result_declared
					match.tournament_round.save()
				match.winner = winner
				match.end_datetime = datetime.now()
				match.is_live = False
				match.won_by = won_by
				match.is_2nd_ining_over = True
				match.dls_won = dls_won
				player = None
				try:
					player = Team_Match_Player.objects.get(match_stat=match,player=player_id)
					match.man_of_the_match = player.player.id
				except:
					return HttpResponse(json.dumps({"status":False,"message":"Not a valid player for man of the match"}),status=400)

				match.save()
			match_players = [team_player.player for team_player in Team_Match_Player.objects.filter(team__id = winner)]
			for p in match_players:
				create_activity_thread(Players.objects.get(id = p.id), match, 'M', 'Won a match')
			create_activity_thread(Players.objects.get(id = player_id), match, 'M', 'man of the match')

			return HttpResponse(json.dumps({"status":True}),status=200)
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


from reversion.models import Version
def bowl_undo(params_data):
	try:
		over_id,match_stat_team_bowl_id,match_stat_team_bat_id,strike_id,non_strike_id,bowler_id,is_strike_change,first_bowl,super_sub,stat_bowl = params_data

		# REVERT INTERVAL DATA IN A BOWL

		#if stat_bowl.is_interval:
		#	Match_Stat_Interval.objects.get(bowl=stat_bowl).delete()

		# REVERT SCORER DATA IN A BOWL

		#if stat_bowl.is_scorer:
		#	Match_Stat_Scorer.objects.get(bowl=stat_bowl).delete()

		# REVERT PENALTY OF ANY TEAM

		state_save = False
		if stat_bowl.match_stat_over.over_no == 0 and stat_bowl.bowl_no == 2:
			state_save = True
		if stat_bowl.is_penalty:
			penalty = Match_Stat_Penalty.objects.filter(bowl=stat_bowl)
			for i in penalty:
				i.match_stat_team.run -= i.run
				i.match_stat_team.save()
				i.delete()

		# REVERT MATCH STAT BOWLING DATA

		m_s_bowl = Match_Stat_Bowling.objects.get(player=bowler_id,match_stat_team=match_stat_team_bowl_id)
		version = Version.objects.get_for_object(m_s_bowl)
		if len(version)==1:
			m_s_bowl.delete()
			version.delete()
		else:
			version[1].revision.revert()
			version[0].delete()


		if stat_bowl.is_keeper_change and stat_bowl.last_keeper != None:
			new = Team_Match_Player.objects.get(Q(player_type='keeper') & Q(player_type='keeper&captain'),team=stat_bowl.over.match_stat_team.team)
			change_keeper({"old_keeper_id":stat_bowl.last_keeper,"new_keeper_id":new.id})


		# REVERT MATCH STAT OVER DATA

		m_s_over = Match_Stat_Over.objects.get(id=over_id)
		version = Version.objects.get_for_object(m_s_over)
		if len(version)==1:
			m_s_over.delete()
			version.delete()
		else:
			version[1].revision.revert()
			version[0].delete()

		# REVERT MATCH STAT TEAM DATA

		m_s_team = Match_Stat_Team.objects.get(id=match_stat_team_bat_id)
		version = Version.objects.get_for_object(m_s_team)
		if len(version)==1:
			m_s_team.total_run = 0
			m_s_team.run_against = 0
			m_s_team.wickets = 0
			m_s_team.overs = 0
			m_s_team.bowl = 0
			m_s_team.byes = 0
			m_s_team.leg_byes = 0
			m_s_team.wide = 0
			m_s_team.wide = 0
			m_s_team.no_ball = 0
			m_s_team.extras = 0
			m_s_team.run_rate = 0.0
			m_s_team.is_submit = False
			m_s_team.save()
			version.delete()
		else:
			version[1].revision.revert()
			version[0].delete()

		# REVERT MATCH STAT PARTNERSHIP DATA

		#m_s_partnership = Match_Stat_Partnership.objects.get(match_stat_team=m_s_team,\
		#	player1=stat_bowl.strike,player2=stat_bowl.non_strike)
		#version = Version.objects.get_for_object(m_s_partnership)
		#if len(version)==1:
		#	m_s_partnership.delete()
		#	verison.delete()
		#else:
		#	version[1].revision.revert()
		#	version[0].delete()


		# REVERT MATCH STAT BATTING DATA
		#print (strike_id,match_stat_team_bat_id,m_s_over.over_no)
		try:
			m_s_bat = Match_Stat_Batting.objects.get(player=strike_id,match_stat_team__id=match_stat_team_bat_id)
			version = Version.objects.get_for_object(m_s_bat)
			print ("strike",strike_id)
			if len(version)==1:
				m_s_bat.delete()
				version.delete()
			else:
				if m_s_bat.bowl==1 or first_bowl:
					if stat_bowl.bowl_type != "wide":
						print ("enter1")
						temp1 = Match_Stat_Bowl.objects.filter(strike=strike_id,match_stat_over__match_stat_team__id=match_stat_team_bat_id)
						if temp1.count() == 2 and temp1[0].bowl_type == "wide":
							version[1].revision.revert()

						else:
							m_s_bat.delete()
				else:
					print ("enter2")
					version[1].revision.revert()
				version[0].delete()
		except:
			traceback.print_exc()
			pass
		m_s_bat1 = Match_Stat_Batting.objects.get(player=non_strike_id,match_stat_team__id=match_stat_team_bat_id)
		print ("batting id",m_s_bat1.run)
		#version = Version.objects.get_for_object(m_s_bat1)
		#if len(version)==1:
		#	m_s_bat1.delete()
		#	version.delete()
		#else:
		#	version[1].revision.revert()
		#	version[0].delete()

		# REVERT SUPER SUBSTITUTE DATA

		if super_sub:
			set_supersub(super_sub,m_s_team.match_stat.id)

		# REVERT MATCH STAT LIVE DATA

		m_s_live = Match_Stat_Live.objects.get(match_stat=m_s_team.match_stat)
		version = Version.objects.get_for_object(m_s_live)
		if is_strike_change:
			version[1].revision.revert()
			version[0].delete()

		# DELETE OUT OBJECT IF OUT IN BOWL
		out = stat_bowl.is_wicket
		m_s_out = stat_bowl.match_stat_out
		if out:
			Match_Stat_Fall_Of_Wickets.objects.get(match_stat_team=m_s_team,player=m_s_out.who).delete()
			m_s_out.delete()


		# REVERT MATCH STAT DATA
		if not state_save:
			print ('resvert state')
			version = Version.objects.get_for_object(m_s_team.match_stat)
			if len(version)==1:
				version.delete()
			else:
				version[1].revision.revert()
				version[0].delete()

		print ("before deletion",stat_bowl.id)
		stat_bowl.delete()
		print ("bowl deleted")
	except Exception:
		traceback.print_exc()

class Bowl_Undo(APIView):
	def post(self,request):
		try:
			print (request.data)
			data = request.data['data']
			temp = 0
			for i in data:
				bowl_id = i['bowl_id']
				over_id = i['over_id']
				match_stat_team_bowl_id = i['match_stat_team_bowl_id']
				match_stat_team_bat_id = i['match_stat_team_bat_id']
				strike_id = i['strike_id']
				non_strike_id = i['non_strike_id']
				bowler_id = i['bowler_id']
				is_strike_change = i['is_strike_change']
				first_bowl = i['first_bowl']
				super_sub = i['super_sub']

				if bowl_id == 0:
					m_s_t_bat = Match_Stat_Team.objects.get(id=match_stat_team_bat_id)
					m_s_out = Match_Stat_Out.objects.filter(match_stat=m_s_t_bat.match_stat.id).order_by('id')
					if m_s_out.count()>0:
						version = Version.objects.get_for_object(m_s_out[0])
						version.delete()
					else:
						return HttpResponse(json.dumps({"status":True}))

				try:
					stat_bowl = Match_Stat_Bowl.objects.get(id=bowl_id)
				except:
					continue

				params_data = [over_id,match_stat_team_bowl_id,match_stat_team_bat_id,strike_id,non_strike_id,bowler_id,is_strike_change,first_bowl,super_sub,stat_bowl]
				temp += 1
				if temp==1:
					print ("caling undo")
					bowl_undo(params_data)

			return HttpResponse(json.dumps({"status":True}))
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"error":"Something went wrong"}),status=400)


class Add_Substitute(APIView):
	def get(self,request):
		try:
			match_stat_team_id = request.data['match_stat_team_id']
			m_s_team = Match_Stat_Team.objects.get(id=match_stat_team_id)
			match_stat = m_s_team.match_stat
			if not match_stat.is_live or match_stat.date_time<datetime.now():
				return HttpResponse(json.dumps({"status":False,"message":"Match not valid"}),status=400)
			team_players = Team_Match_Player.objects.filter(match_stat=match_stat,team=m_s_team.team)
			for i in request.data["players"]:
				if team_players.filter(player_type="substitute").count()>=3:
					break
				player_id = i
				player = Players.objects.get(id=player_id,is_active=True)
				Team_Match_Player.objects.get_or_create(match_stat=match_stat,team=m_s_team.team,player=player,player_type="substitute")
			return HttpResponse(json.dumps({"status":True}))
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)


class GetTeamMatches(APIView):
	#permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		try:
			user = request.user
			team_id = request.GET.get('team_id')
			query = request.GET["query"]

			paging = request.GET.get('page')

			if query.lower() == "up":
				result = upcoming_team_matches(team_id, paging)

			elif query.lower() == "pr":
				result = previous_team_matches(team_id, paging)

			else:
				return HttpResponse(json.dumps({"error": "invalid quey parameter", "success": False}), status=400)

			if result == "error":
				return HttpResponse(json.dumps({"error": "some error", "success": False}), status=400)

			if result == None:
				return HttpResponse(json.dumps({"success": True, "data": [], "last_page": 1}), status=200)
			return HttpResponse(json.dumps({"success": True, "data": result[0], "last_page": result[1]}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error": str(e), "success": False}), status=400)


def upcoming_team_matches(team_id, paging):
	match_stat_qs = Match_Stat.objects.filter(
		Q(team1__id__in=team_id) |
		Q(team2__id__in=team_id),
		Q(is_live=True) | Q(date_time__gt=timezone.now()),
		is_result_declared=False,
		is_canceled=False,
	)

	live_list = []
	paginator = Paginator(match_stat_qs, 10)
	last_page = paginator.num_pages

	try:
		page_data = paginator.page(paging)
	except PageNotAnInteger:
		page_data = paginator.page(1)
	except EmptyPage:
		page_data = paginator.page(paginator.num_pages)

	for i in page_data:
		if i.is_live:
			is_live = True
			try:
				match_stat_team = Match_Stat_Team.objects.filter(match_stat=i).distinct("id")
				match_stat_team_id = match_stat_team[0].id
				match_stat_team_id2 = match_stat_team[1].id
			except Exception as e:
				traceback.print_exc()
				match_stat_team_id = None
				match_stat_team_id2 = None

		else:
			is_live = False
		match_stat = Match_Stat_Team.objects.filter(match_stat=i)
		team1 = match_stat[0].team
		team2 = match_stat[1].team

		if team1.id in team_id:
			player_team = team1.name
			player_team_id = team1.id
			player_team_logo = None
			if team1.team_pic:
				player_team_logo = team1.team_pic.url

			vs_team = team2.name
			vs_team_id = team2.id
			vs_team_logo = None
			if team2.team_pic:
				vs_team_logo = team2.team_pic.url

		else:
			player_team = team2.name
			player_team_id = team2.id
			player_team_logo = None
			if team1.team_pic:
				player_team_logo = team2.team_pic.url

			vs_team = team1.name
			vs_team_id = team1.id
			vs_team_logo = None
			if team1.team_pic:
				vs_team_logo = team1.team_pic.url

		venue_name = None
		venue_address = None
		city_id = None
		venue_img = None
		if i.venue:
			venue = i.venue
			venue_name = venue.name
			venue_address = venue.address.city.name
			city_id = venue.address.city.id

			try:
				venue_img = VenueImg.objects.filter(venue=venue)[0].url.url
			except:
				pass

		try:
			striker = Match_Stat_Live.objects.get(match_stat=i)
		except:
			striker = None

		bowler_pic = None
		strike_pic = None
		non_strike_pic = None
		striker_player = None
		striker_player_id = None
		non_striker_player = None
		non_striker_player_id = None
		bowler_player = None
		bowler_player_id = None
		current_score = 0
		wickets_fall = 0
		striker_run = 0
		non_striker_run = 0
		striker_ball_faced = 0
		non_striker_ball_faced = 0
		bowler_over = 0
		overs = 0
		bowler_over = 0
		batting_team = None
		first_ining = None

		if striker != None:
			match_stat_obj = striker.match_stat
			if striker.bowler:
				strikerr = striker
				striker_player = strikerr.strike.player.user.name

				striker_player_id = strikerr.strike.player.id
				non_striker_player = strikerr.non_strike.player.user.name
				non_striker_player_id = strikerr.non_strike.player.id
				bowler_player = strikerr.bowler.player.user.name
				bowler_player_id = strikerr.bowler.player.id

				match_stat_batting = Match_Stat_Batting.objects.filter(match_stat_team__match_stat=match_stat_obj)

				if len(match_stat_batting) != 0:
					if match_stat_batting[0].match_stat_team.match_stat.first_ining == match_stat_batting[
						0].match_stat_team.team.id:
						first_ining = True
					else:
						first_ining = False
					try:
						striker_run, striker_ball_faced = match_stat_batting.get(
							player=striker.strike.player).run, match_stat_batting.get(player=striker.strike.player).bowl

						non_striker_run, non_striker_ball_faced = match_stat_batting.get(
							player=striker.non_strike.player).run, match_stat_batting.get(
							player=striker.non_strike.player).bowl
					except Exception as e:
						traceback.print_exc()
						striker_run, striker_ball_faced = match_stat_batting.filter(player=striker.strike.player)[
															  0].run, \
														  match_stat_batting.filter(player=striker.strike.player)[
															  0].bowl
						non_striker_run, non_striker_ball_faced = \
						match_stat_batting.filter(player=striker.non_strike.player)[0].run, \
						match_stat_batting.filter(player=striker.non_strike.player)[0].bowl
					batting_team = match_stat_batting[0].match_stat_team.team.name
					current_score = match_stat_batting[0].match_stat_team.total_run
					wickets_fall = match_stat_batting[0].match_stat_team.wickets
					overs = match_stat_batting[0].match_stat_team.overs

				try:
					match_stat_bowling = Match_Stat_Bowling.objects.get(match_stat_team__match_stat=match_stat_obj,
																		player=striker.bowler.player)
					bowler_over = str(match_stat_bowling.overs) + "." + str(match_stat_bowling.bowl) + "-" + str(
						match_stat_bowling.maiden) + "-" + str(match_stat_bowling.run) + "-" + str(
						match_stat_bowling.wickets)
				except:
					pass

				if strikerr.bowler.player.user.profile_pic:
					bowler_pic = striker.bowler.player.user.profile_pic.url

				if strikerr.strike.player.user.profile_pic:
					strike_pic = strikerr.strike.player.user.profile_pic.url

				if strikerr.non_strike.player.user.profile_pic:
					non_strike_pic = strikerr.non_strike.player.user.profile_pic.url

		player_list = []

		team_match_players_qs = Team_Match_Player.objects.filter(match_stat=i, team__id=player_team_id)

		for j in team_match_players_qs.select_related('player__user'):
			image = None
			player = j.player
			user = player.user
			if user.profile_pic:
				image = user.profile_pic.url
			player_dict = {
				"name": user.name,
				"player_id": player.id,
				"profile_pic": image,
				"gender": user.gender,
				"address": {"city": user.city, "state": user.state},
				"sports": list(Players_Preferred_Sport.objects.filter(player=player).values("sport_name__name")),
			}
			player_list.append(player_dict)

		live_dict = {
			"player_team": player_team,
			"vs_team": vs_team,
			"player_team_id": player_team_id,
			"vs_team_id": vs_team_id,
			"vs_team_logo": vs_team_logo,
			"player_team_logo": player_team_logo,
			"striker_player": striker_player,
			"striker_player_id": striker_player_id,
			"strike_pic": strike_pic,
			"non_striker_player": non_striker_player,
			"non_striker_player_id": non_striker_player_id,
			"non_strike_pic": non_strike_pic,
			"venue": venue_name,
			"venue_address": venue_address,
			"city_id": city_id,
			"venue_img": venue_img,
			"bowler_player": bowler_player,
			"bowler_player_id": bowler_player_id,
			"bowler_pic": bowler_pic,
			"players": player_list,
			"date": str(i.date_time)[:10],
			"is_live": is_live,
			"current_score": current_score,
			"wickets_fall": wickets_fall,
			"striker_run": striker_run,
			"non_striker_run": non_striker_run,
			"striker_ball_faced": striker_ball_faced,
			"non_striker_ball_faced": non_striker_ball_faced,
			"bowler_over": bowler_over,
			"batting_team": batting_team,
			"first_ining": first_ining,
			"match_stat_team_id2": match_stat_team_id2,
			"match_stat_team_id": match_stat_team_id,
			"match_stat_id": i.id,
			"venue_name": venue_name
		}
		live_list.append(live_dict)
		return  live_list


def previous_team_matches(team_id, paging):
	try:
		team_match_player = Team_Match_Player.objects.filter(
			Q(match_stat__is_result_declared=True) &
			Q(match_stat__is_canceled=False) &
			Q(match_stat__is_live=False),
			team__id=team_id).distinct("match_stat")

		team_id, match_stat_id = [i.team.id for i in team_match_player.distinct("team")], [x.match_stat.id for x in
																						   team_match_player]

		match_stat_qs = Match_Stat.objects.filter(Q(Q(team1__id__in=team_id) | Q(team2__id__in=team_id)) & Q(id__in=match_stat_id))
		list1 = []
		paginator = Paginator(match_stat_qs, 10)
		last_page = paginator.num_pages

		try:
			page_data = paginator.page(paging)
		except PageNotAnInteger:
			page_data = paginator.page(1)
		except EmptyPage:
			page_data = paginator.page(paginator.num_pages)

		for i in page_data:
			team1 = i.team1
			team2 = i.team2
			if team1.id in team_id:

				player_team_id = team1.id
				player_team_name = team1.name
				vs_team_id = team2.id
				vs_team = team2.name
				player_team_logo = None
				vs_team_logo = None

				if i.winner == player_team_id:
					winner = player_team_name
					winner_id = player_team_id
				else:
					winner = vs_team
					winner_id = vs_team_id

				if team1.team_pic:
					player_team_logo = team1.team_pic.url

				if team2.team_pic:
					vs_team_logo = team2.team_pic.url

			else:
				player_team_id = team2.id
				player_team_name = team2.name
				vs_team_id = team1.id
				vs_team = team1.name
				player_team_logo = None
				vs_team_logo = None

				if i.winner == player_team_id:
					winner = player_team_name
					winner_id = player_team_id
				else:
					winner = vs_team
					winner_id = vs_team_id
				if team2.team_pic:
					player_team_logo = team2.team_pic.url

				if team1.team_pic:
					vs_team_logo = team1.team_pic.url

			player_list = []
			team_match_players_qs = Team_Match_Player.objects.filter(match_stat=i, team__id=player_team_id)
			for j in team_match_players_qs.select_related('player__user'):
				image = None
				player = j.player
				user = player.user
				if user.profile_pic:
					image = user.profile_pic.url
				player_dict = {
					"name": user.name,
					"player_id": player.id,
					"profile_pic": image,
					"gender": user.gender,
					"address": {"city": user.city, "state": user.state},
					"sports": list(Players_Preferred_Sport.objects.filter(player=player).values("sport_name__name")),
				}
				player_list.append(player_dict)

			venue_name = None
			venue_address = None
			city_id = None
			venue_img = None
			if i.venue:
				venue = i.venue
				venue_name = venue.name
				venue_address = venue.address.city.name
				city_id = venue.address.city.id

				try:
					venue_img = VenueImg.objects.filter(venue=venue)[0].url.url
				except:
					traceback.print_exc()
					pass

			result_dict = {
				"player_team": player_team_name,
				"player_team_id": player_team_id,
				"vs_team_id": vs_team_id,
				"vs_team": vs_team,
				"vs_team_logo": vs_team_logo,
				"player_team_logo": player_team_logo,
				"date": str(i.date_time)[:10],
				"won_by": str(i.won_by),
				"winner_team": winner,
				"winner_team_id": winner_id,
				"players": player_list,
				"venue": venue_name,
				"venue_address": venue_address,
				"city_id": city_id,
				"venue_img": venue_img,
			}
			list1.append(result_dict)

		return (list1, last_page)
	except Exception as e:
		traceback.print_exc()
		return "error"


def get_match_detail(match_stat_pk):
	match_serializer= {}
	match_obj= Match_Stat.objects.get(pk= match_stat_pk)
	match_serializer.update({'is_live':match_obj.is_live, 'is_1st_ining_over':match_obj.is_1st_ining_over,})
	team_qs= Match_Stat_Team.objects.filter(match_stat= match_obj)
	a= []
	for obj in team_qs:
		a.append({'name':obj.team.name, 'id':obj.id})
	match_serializer.update({'teams':a})
	player_qs= Team_Match_Player.objects.filter(match_stat= match_obj)
	p= []
	for obj in player_qs:
		p.append({'name':obj.player.user.name, 'player_id':obj.player.id})
	match_serializer.update({'players':p})
	return match_serializer
