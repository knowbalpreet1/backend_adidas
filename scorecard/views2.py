from django.shortcuts import render
from rest_framework.views import APIView
# Create your views here.
from players.models import Team,Players
from venue.models import Venue
from .models import *
from django.http import HttpResponse
from datetime import datetime
import traceback
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope



class Man_Of_Match(APIView):
	def get(self,request):
		try:
			m_s_id = request.GET.get('match_stat_id')


			match_stat = Match_Stat.objects.get(id=m_s_id)
			man_of_the_match = match_stat.man_of_the_match
			result = []
			if match_stat.man_of_the_match != None:
				t_m_player = Team_Match_Player.objects.get(match_stat=match_stat,player=man_of_the_match)
				if t_m_player.team == match_stat.team1:
					my_team = match_stat.team1.name
					against_team = match_stat.team2.name
				else:
					my_team = match_stat.team2.name
					against_team = match_stat.team1.name
				player = t_m_player.player
				t_name = None
				v_name = None
				try:
					profile_pic = player.user.profile_pic.url
				except:
					profile_pic = ""
				if match_stat.tournament_round != None:
					t_name  = match_stat.tournament_round.tournament.name
				if match_stat.venue :
					v_name = match_stat.venue.name
				elif match_stat.tournament_venue and match_stat.tournament_venue.court:
					v_name = match_stat.tournament_venue.court.venue.name
				elif match_stat.tournament_venue and match_stat.tournament_venue.is_dummy:
					v_name = match_stat.tournament_venue.court_name
				result = {"name":player.user.name,"tournament_name":t_name,"venue":v_name,"profile_pic":profile_pic,\
				"date":str(match_stat.date_time.date()),"my_team":my_team,"against_team":against_team}

				try:
					m_s_batting = Match_Stat_Batting.objects.get(player=player,match_stat_team__match_stat=match_stat)
					bat_records = {"run":m_s_batting.run,"bowls":m_s_batting.bowl,"fours":m_s_batting.fours,\
					"six":m_s_batting.six,"strike_rate":m_s_batting.strike_rate,"is_strike":m_s_batting.is_strike}
				except :
					bat_records = {}

				try:
					m_s_bowling = Match_Stat_Bowling.objects.get(player=player,match_stat_team__match_stat=match_stat)
					bowl_records = {"run":m_s_bowling.run,"overs":m_s_bowling.overs,"maiden":m_s_bowling.maiden,\
					"wickets":m_s_bowling.wickets,"econ":m_s_bowling.economy_rate,"bowl":m_s_bowling.bowl}
				except :
					bowl_records = {}

				try:
					m_s_out = Match_Stat_Out.objects.filter(Q(whom=player) | Q(whom=player),match_stat=match_stat)
					stumped,catches,runout = 0,0,0
					stumped = m_s_out.filter(out_type="stumped").count()
					catches = m_s_out.filter(Q(out_type="caught&bowled") | Q(out_type="caughtbehind") | Q(out_type="caught")).count()
					runout = m_s_out.filter(out_type="runout")
					out_records = {"stumped":stumped,"catches":catches,"runout":runout}

				except:
					out_records = {}
				result.update({"bat_records":bat_records,"bowl_records":bowl_records,"out_records":out_records})
			else:
				return HttpResponse(json.dumps({"status":False,"message":"Sorry, no man of match for this game"}),status=400)
			return HttpResponse(json.dumps({"status":True,"data":result}))
		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)

from .views import bowl_undo
class Match_State(APIView):
	permission_classess = (TokenHasReadWriteScope,)
	def get(self,request):
		try:
			player = Players.objects.get(user=request.user)
			match_id = request.GET.get('match_id')
			match = Match_Stat.objects.get(id=match_id)
			if match.scorer1 == player.id or match.scorer2 == player.id:
				match_state = match.match_state
			else:
				print ("not a valid scorer")
				return HttpResponse(json.dumps({"status":False,"message":"Not a valid Scorer"}),status=400)

			bowls = Match_Stat_Bowl.objects.filter(id__gt=match_state['currentBowlId'],match_stat_over__match_stat_team__match_stat=match).order_by('-id')
			print (bowls.values('id'))
			for i in bowls:
				print ("undo bowl",i.id)
				# over_id,match_stat_team_bowl_id,match_stat_team_bat_id,strike_id,non_strike_id,bowler_id,is_strike_change,first_bowl,super_sub,stat_bowl
				undo_state = i.undo_state
				try:
					list1 = undo_state['over_id'],undo_state['match_stat_team_bowl_id'],undo_state['match_stat_team_bat_id'],undo_state['strike_id'],undo_state['non_strike_id'],undo_state['bowler_id'],undo_state['is_strike_change'],undo_state['first_bowl'],undo_state['super_sub'],i
					bowl_undo(list1)
					print ("undo complete")
				except Exception:
					traceback.print_exc()
					continue
			return HttpResponse(json.dumps({"match_state":match_state}),status=200)

		except Exception:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"Something went wrong"}),status=400)
