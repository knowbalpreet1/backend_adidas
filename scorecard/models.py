from django.db import models
from tournament.models import Tournament_Round, Tournament_Group, Tournament_Venue_Map
from players.models import Players,Team
from venue.models import Venue
# Create your models here.
import reversion
from django.contrib.postgres.fields import JSONField

""" Match Meta Data of Two Teams """
@reversion.register(ignore_duplicates = False)
class Match_Stat(models.Model):
	M_TYPES = (
		('S', 'Social'),
		('L', 'Local'),
	)
	FORMAT = (
		('O', 'ODI'),
		('T', 'T20'),
	)
	tournament_round= models.ForeignKey(Tournament_Round, blank= True, null= True) ########## by Sumit
	tournament_group= models.ForeignKey(Tournament_Group, blank= True, null= True) ########## by Sumit
	name = models.CharField(max_length=100,blank=True,null=True)
	match_id = models.CharField(max_length=10,blank=True,null=True)	
	team1 = models.ForeignKey(Team,blank=True,null=True,related_name='team1')
	team2 = models.ForeignKey(Team,blank=True,null=True,related_name='team2')
	no_of_overs = models.CharField(max_length=100,blank=True,null=True)
	no_of_players = models.CharField(max_length=100,blank=True,null=True)
	venue = models.ForeignKey(Venue,blank=True,null=True,related_name='venue')
	tournament_venue= models.ForeignKey(Tournament_Venue_Map, null= True, blank= True)
	date_time = models.DateTimeField(blank=True,null=True)
	end_datetime = models.DateTimeField(blank=True,null=True)
	type_of_bowl = models.CharField(max_length=100,blank=True,null=True)
	toss_win = models.PositiveIntegerField(blank=True,null=True)
	umpire1 = models.PositiveIntegerField(blank=True,null=True)
	umpire2 = models.PositiveIntegerField(blank=True,null=True)
	umpire3 = models.PositiveIntegerField(blank=True,null=True)
	umpire4 = models.PositiveIntegerField(blank=True,null=True)
	scorer1 = models.PositiveIntegerField(blank=True,null=True)
	scorer2 = models.PositiveIntegerField(blank=True,null=True)
	commentator = models.PositiveIntegerField(blank=True,null=True)
	match_refree = models.PositiveIntegerField(blank=True,null=True)
	first_ining = models.PositiveIntegerField(blank=True,null=True)
	winner = models.PositiveIntegerField(blank=True,null=True)
	is_live = models.BooleanField(default=False)
	is_canceled = models.BooleanField(default=False)
	is_draw = models.BooleanField(default=False)
	is_1st_ining_over = models.BooleanField(default=False)
	is_2nd_ining_over = models.BooleanField(default=False)
	is_break = models.BooleanField(default=False)
	is_result_declared= models.BooleanField(default= False)
	match_state = JSONField(default={},blank=True,null=True)
	man_of_the_match = models.PositiveIntegerField(blank=True,null=True)
	# declare_type_2 = models.CharField(blank=True,null=True)
	dls_won = models.CharField(max_length=500, null=True, blank=True)
	m_type = models.CharField(max_length=1, choices=M_TYPES,blank=True,null=True)
	m_format = models.CharField(max_length=1, choices=FORMAT,blank=True,null=True)
	won_by = models.CharField(max_length=500, null=True, blank=True)

	def __str__(self):
		if self.tournament_group:
			return self.tournament_group.name
		elif self.tournament_round:
			return self.tournament_round.name
		else:
			return self.team1.name +" "+self.team2.name


""" List Of Players Playing in that match for that Team """
@reversion.register(ignore_duplicates = True)
class Team_Match_Player(models.Model):

	match_stat = models.ForeignKey(Match_Stat,blank=True,null=True)
	team = models.ForeignKey(Team,blank=True,null=True)
	player = models.ForeignKey(Players,blank=True,null=True)
	player_type = models.CharField(max_length=100,blank=True,null=True)
	super_sub = models.BooleanField(default=False)

""" Information of Match Current Total Score and Over of a particular Team"""
@reversion.register(ignore_duplicates = True)
class Match_Stat_Team(models.Model):
	match_stat = models.ForeignKey(Match_Stat,blank=True,null=True)
	team = models.ForeignKey(Team,blank=True,null=True)
	total_run = models.PositiveIntegerField(default=0) ########### by Sumit
	run_against= models.PositiveIntegerField(default=0) ########## by Sumit
	penalty_run = models.PositiveIntegerField(default=0)
	wickets = models.PositiveIntegerField(default=0)
	overs = models.PositiveIntegerField(default=0)
	bowl = models.PositiveIntegerField(default=0)
	byes = models.PositiveIntegerField(default=0)
	leg_byes = models.PositiveIntegerField(default=0)
	wide = models.PositiveIntegerField(default=0.0)
	no_ball = models.PositiveIntegerField(default=0)
	extras = models.PositiveIntegerField(default=0)
	revised_overs = models.PositiveIntegerField(default=0)
	revised_run = models.PositiveIntegerField(default=0)
	run_rate = models.DecimalField(decimal_places=0,default=0.0,max_digits=5)
	is_submit= models.BooleanField(default= False)  ############## by Sumit


""" Information of Batting Per Players Of a Team in a Match"""
@reversion.register(ignore_duplicates = False)
class Match_Stat_Batting(models.Model):
	match_stat_team = models.ForeignKey(Match_Stat_Team,blank=True,null=True)
	player = models.ForeignKey(Players,blank=True,null=True)
	run = models.PositiveIntegerField(default=0)
	bowl = models.PositiveIntegerField(default=0)
	fours = models.PositiveIntegerField(default=0)
	six = models.PositiveIntegerField(default=0)
	strike_rate = models.FloatField(default=0.0)
	is_out = models.BooleanField(default=False)
	is_last_wicket = models.BooleanField(default=False)
	out_history = models.CharField(max_length=100,blank=True,null=True)
	is_strike = models.BooleanField(default=False)
	is_valid = models.BooleanField(default=True)
	is_retired = models.BooleanField(default=False)


""" Information of Bowling Per Players Of a Team in a Match"""
@reversion.register(ignore_duplicates = True)
class Match_Stat_Bowling(models.Model):
	match_stat_team = models.ForeignKey(Match_Stat_Team,blank=True,null=True)
	player = models.ForeignKey(Players,blank=True,null=True)
	overs = models.PositiveIntegerField(default=0)
	bowl = models.PositiveIntegerField(default=0)
	maiden = models.PositiveIntegerField(default=0)
	run = models.PositiveIntegerField(default=0)
	wickets = models.PositiveIntegerField(default=0)
	wide = models.PositiveIntegerField(default=0)
	no_ball = models.PositiveIntegerField(default=0)
	economy_rate = models.FloatField(default=0.0)
	counter = models.PositiveIntegerField(default=0)
	is_valid = models.BooleanField(default=True)


""" Information of Bowling Per Players Of a Team in a Match"""
@reversion.register(ignore_duplicates = True)
class Match_Stat_Fall_Of_Wickets(models.Model):
	match_stat_team = models.ForeignKey(Match_Stat_Team,blank=True,null=True)
	player = models.ForeignKey(Players,blank=True,null=True,related_name="player")
	overs = models.CharField(max_length=100,default=0,blank=True,null=True)
	score = models.PositiveIntegerField(default=0)

""" Live Status Of a Match Of Two Teams """
@reversion.register(ignore_duplicates = False)
class Match_Stat_Live(models.Model):
	match_stat = models.ForeignKey(Match_Stat,blank=True,null=True)
	strike = models.ForeignKey(Team_Match_Player,blank=True,null=True,related_name="strike")
	non_strike = models.ForeignKey(Team_Match_Player,blank=True,null=True,related_name="non_strike")
	bowler = models.ForeignKey(Team_Match_Player,blank=True,null=True,related_name="bowler")


""" Per Over Status In a Match Of a Team """
@reversion.register(ignore_duplicates = True)
class Match_Stat_Over(models.Model):
	match_stat_team = models.ForeignKey(Match_Stat_Team,blank=True,null=True)
	over_no = models.PositiveIntegerField(default=0)
	run = models.PositiveIntegerField(default=0)
	team_run = models.PositiveIntegerField(default=0)
	strike_name = models.CharField(max_length=100,blank=True,null=True)
	non_strike_name = models.CharField(max_length=100,blank=True,null=True)
	bowl1_name = models.CharField(max_length=100,blank=True,null=True)
	bowl2_name = models.CharField(max_length=100,blank=True,null=True)
	strike_stat = models.CharField(max_length=50,blank=True,null=True)
	non_strike_stat = models.CharField(max_length=50,blank=True,null=True)
	# bat2_run = models.PositiveIntegerField(default=0)
	# bat2_bowl = models.PositiveIntegerField(default=0)
	bowl1_stat = models.CharField(max_length=100,blank=True,null=True)
	bowl2_stat = models.CharField(max_length=100,blank=True,null=True)
	over_wicket = models.PositiveIntegerField(default=0)


""" Per Out Status In a Match Of a Team"""
@reversion.register(ignore_duplicates = True)
class Match_Stat_Out(models.Model):
	match_stat = models.PositiveIntegerField(blank=True,null=True)
	out_type = models.CharField(max_length=100,blank=True,null=True)
	who = models.ForeignKey(Players,blank=True,null=True,related_name="who")
	whom = models.ForeignKey(Players,blank=True,null=True,related_name="whom")
	whom2 = models.ForeignKey(Players,blank=True,null=True,related_name="whom2")
	out_history = models.CharField(max_length=100,blank=True,null=True)
	play_again = models.BooleanField(default=False)

""" Per Bowl Status In a Match Of a Team"""
@reversion.register(ignore_duplicates = True)
class Match_Stat_Bowl(models.Model):
	match_stat_over = models.ForeignKey(Match_Stat_Over,blank=True,null=True)
	bowl_no = models.PositiveIntegerField(default=1)
	local_id = models.PositiveIntegerField(default=0)
	run = models.PositiveIntegerField(default=0)
	extra_run = models.PositiveIntegerField(default=0)
	bat_run = models.PositiveIntegerField(default=0)
	strike = models.ForeignKey(Players,blank=True,null=True,related_name='strike')
	non_strike = models.ForeignKey(Players,blank=True,null=True,related_name='next_strike')
	bowler = models.ForeignKey(Players,blank=True,null=True,related_name='bowler')
	bowl_type = models.CharField(max_length=100,blank=True,null=True)
	last_keeper = models.PositiveIntegerField(default=0)
	undo_state = JSONField(blank=True,null=True)
	is_keeper_change = models.BooleanField(default=False)
	is_wicket = models.BooleanField(default=False)
	is_four = models.BooleanField(default=False)
	is_six = models.BooleanField(default=False)
	is_over_change = models.BooleanField(default=False)
	# is_scorer = models.BooleanField(default=False)
	is_match_official = models.BooleanField(default=False)
	is_interval = models.BooleanField(default=False)
	is_penalty = models.BooleanField(default=False)
	is_replace_bowler = models.BooleanField(default=False)
	commentary = models.CharField(max_length=100,default=0,blank=True,null=True)
	match_stat_out = models.ForeignKey(Match_Stat_Out,blank=True,null=True)
	request = models.TextField(blank=True,null=True)


""" MATCH SOCRER INTERVAL """
@reversion.register(ignore_duplicates = True)
class Match_Stat_Interval(models.Model):
	INTERVAL_TYPES = (
		('LUNCH', 'Lunch'),
		('TIMEOUT', 'Strategic Out'),
		('RAIN', 'Rain'),
		('DRINKS', 'Drinks'),
		('OTHERS', 'Other'),
	)

	match_stat = models.ForeignKey(Match_Stat,blank=True,null=True)
	interval_type = models.CharField(choices=INTERVAL_TYPES,max_length=150,blank=True,null=True)
	bowl = models.ForeignKey(Match_Stat_Bowl,blank=True,null=True)
	bat1_run = models.PositiveIntegerField(blank=True,null=True)
	bat2_run = models.PositiveIntegerField(blank=True,null=True)
	team_score = models.PositiveIntegerField(blank=True,null=True)
	team_wicket = models.PositiveIntegerField(blank=True,null=True)
	notes = models.CharField(max_length=150,blank=True,null=True)
	date_time = models.DateTimeField(blank=True,null=True)



""" MATCH SOCRER NOTES """
@reversion.register(ignore_duplicates = True)
class Match_Stat_Scorer(models.Model):
	
	SCORER_TYPES = (
		('NEW', 'New Ball Change'),
		('OLD', 'Old Ball Change'),
		('LAST', 'Last Hour Start'),
		('OTHER', 'Other'),
	)

	match_stat = models.ForeignKey(Match_Stat,blank=True,null=True)
	scorer_type = models.CharField(choices=SCORER_TYPES,max_length=150,blank=True,null=True)
	bowl_no = models.PositiveIntegerField(Match_Stat_Bowl,blank=True,null=True)
	notes = models.CharField(max_length=150,blank=True,null=True)
	date_time = models.DateTimeField(blank=True,null=True)


""" Per Penalty In a Match Of a Team"""
@reversion.register(ignore_duplicates = True)
class Match_Stat_Penalty(models.Model):

	match_stat_team = models.ForeignKey(Match_Stat_Team,blank=True,null=True)
	run = models.PositiveIntegerField(default=0)
	bowl = models.ForeignKey(Match_Stat_Bowl,blank=True,null=True)
	date_time = models.DateTimeField(blank=True,null=True)


""" MATCH EACH PAIR PARTNERSHIP RECORDS """

@reversion.register(ignore_duplicates = True)
class Match_Stat_Partnership(models.Model):

	match_stat_team = models.ForeignKey(Match_Stat_Team,blank=True,null=True)
	player1 = models.ForeignKey(Players,blank=True,null=True,related_name="player1")
	player2 = models.ForeignKey(Players,blank=True,null=True,related_name="player2")
	player1_start_run = models.PositiveIntegerField(default=0)
	player2_start_run = models.PositiveIntegerField(default=0)
	player1_end_run = models.PositiveIntegerField(blank=True,null=True)
	player2_end_run = models.PositiveIntegerField(blank=True,null=True)
	player1_start_bowl = models.PositiveIntegerField(blank=True,null=True)
	player2_start_bowl = models.PositiveIntegerField(blank=True,null=True)
	player1_end_bowl = models.PositiveIntegerField(blank=True,null=True)
	player2_end_bowl = models.PositiveIntegerField(blank=True,null=True)
	part_start_score = models.PositiveIntegerField(blank=True,null=True)
	part_end_score = models.PositiveIntegerField(blank=True,null=True)
	start_bowl = models.ForeignKey(Match_Stat_Bowl,related_name='start_bowl',blank=True,null=True)
	end_bowl = models.ForeignKey(Match_Stat_Bowl,related_name='end_bowl',blank=True,null=True)
	who = models.ForeignKey(Players,blank=True,null=True,related_name="who1")

class Player_Honors_Awards_Model(models.Model):
	S_TYPES = (
		('c', 'cricket'),
		('f', 'football'),
	)
	s_type = models.CharField(max_length=1, choices=S_TYPES,blank=True,null=True)
	award_name = models.CharField(max_length=100, null=True, blank=True)
	position = models.CharField(max_length=10, null=True, blank=True)
	status = models.CharField(max_length=50)
	player_match = models.ForeignKey(Team_Match_Player, null=True, blank=True)
	date = models.DateField(auto_now=True)
