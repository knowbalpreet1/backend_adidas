from django.conf.urls import url, include
from .views import *
from scorecard.views2 import *

urlpatterns = [
    url(r'^team_create_for_match/', Team_Create_for_Match.as_view(), name = 'team_create_for_match' ),
    url(r'^match_bowl_update/', Match_Bowl_Update.as_view() , name = 'match_bowl_update' ),
    url(r'^match_stat_later/', Match_Stat_later.as_view() , name = 'match_bowl_later' ),
    url(r'^live_matches/', Live_Matches.as_view() , name = 'live_matches'),
    url(r'^match_summary/', Match_Summary.as_view() , name = 'match_summary'),
    url(r'^match_commentary/', Match_Commentary.as_view() , name = 'match_commentary'),
    url(r'^match_scorecard/', Match_Scorecard.as_view() , name = 'match_scorecard'),
    url(r'^match_players/', Match_Players.as_view() , name = 'match_players'),
    url(r'^match_set/', Match_Set.as_view() , name = 'match_set'),
    url(r'^bowl_undo/', Bowl_Undo.as_view() , name = 'bowl_undo'),
    url(r'^add_substitute/', Add_Substitute.as_view() , name = 'add_substitute'),
    url(r'^man_of_match/', Man_Of_Match.as_view() , name = 'man_of_match'),
    url(r'^team_matches/', GetTeamMatches.as_view(), name = 'team_matches'),
    url(r'^match_state/', Match_State.as_view(), name = 'match_state'),
]
