from django.apps import AppConfig


class ScorecardConfig(AppConfig):
    name = 'scorecard'
    def ready(self):
       import scorecard.signals
