from django.dispatch import receiver
from django.db.models.signals import post_delete

from scorecard.models import Match_Stat
from players.models import Activity_Storage

import traceback

@receiver(post_delete, sender = Match_Stat)
def match_stat_activity_delete(sender, instance, **kwargs):
	try:
		Activity_Storage.objects.get(object_id = instance.id).delete()


	except Exception as e:
		traceback.print_exc()
		
