from django.contrib import admin
from .models import *
# Register your models here.
from reversion.models import Revision, Version
from reversion.admin import VersionAdmin

#class Match_Stat_BowlingAdmin1(admin.ModelAdmin):
    
#    list_display = ('match_stat__team__name','player__user__name')

admin.site.register(Revision)

admin.site.register(Version)

admin.site.register(Player_Honors_Awards_Model)

@admin.register(Match_Stat)
class Macth_StatAdmin(VersionAdmin):

    pass

@admin.register(Team_Match_Player)
class Team_Match_PlayerAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Team)
class Match_Stat_TeamAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Batting)
class Match_Stat_BattingAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Bowling)#, Match_Stat_BowlingAdmin1)
class Match_Stat_BowlingAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Fall_Of_Wickets)
class Match_Stat_Fall_Of_WicketsAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Live)
class Match_Stat_LiveAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Over)
class Match_Stat_OverAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Out)
class Match_Stat_OutAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Bowl)
class Match_Stat_BowlAdmin(VersionAdmin):

    pass

@admin.register(Match_Stat_Partnership)
class Match_Stat_PartnershipAdmin(VersionAdmin):

	pass

@admin.register(Match_Stat_Penalty)
class Match_Stat_PenaltyAdmin(VersionAdmin):

        pass
