from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.utils.timezone import now, timedelta
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from authentication.models import Account
from authentication.views import send_OTP
from oauth2_provider.models import Application
from .models import Players, City, Team, TeamPlayers,SportsType, TeamRole, Player_Honors_Awards, Players_Preferred_Sport, Player_Activity_Map, SportsType, Players_Followers, Activity_Storage
import traceback
from scorecard.models import Team_Match_Player, Match_Stat_Batting, Match_Stat_Out, Match_Stat_Bowling
import json
from datetime import datetime
from threading import Thread
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from multiprocessing import Queue
from django.db.models import Avg
from django.db.models import Sum
#from Queue import Queue
import threading
import logging
import time
from multiprocessing import Process, Queue
from authentication.async_task import *




class Player_Main_Stat(APIView):
	"""This view is used for get basic statistics"""

	def get(self, request):
		playerID = request.GET.get('playerID')
		if not playerID:
			return HttpResponse(json.dumps({"status":False,"message":"Please provide valid playerID"}),status=400)
		try:
			output = get_match_win_loss_data(playerID)			
			return HttpResponse(json.dumps([{"status":True,"sport_name":"cricket","stat":{"total_matchs":output['total_matchs'], "total_win":output['total_win']}}]))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"something goes wrong","err":str(e)}),status=400)


class Player_All_Stat_View(APIView):
	"""This view is used for get all statistics of player"""

	def get(self, request):
		playerID = request.GET.get('playerID')
		if not playerID:
			return HttpResponse(json.dumps({"status":False,"message":"Please provide valid playerID"}),status=400)
		try:
			q = dict()
			p1 = threading.Thread(target=get_match_win_loss_dataODI, args=(playerID,q))
			p2 = threading.Thread(target=get_match_win_loss_dataT20, args=(playerID,q))
			p3 = threading.Thread(target=get_match_avg_recordODI, args=(playerID,q))
			p4 = threading.Thread(target=get_match_avg_recordT20, args=(playerID,q))
			p1.start()
			p2.start()
			p3.start()
			p4.start()
			count = 0
			output1 = {}
			output2 = {}
			output_list = []
			while True:
				if len(q) == 4:	
					#output.append({"T20":q[0]['first_t20']}.update(q['sec_t20']))
					output1.update(q['sec_t20'])
					output1.update(q['first_t20'])
					output2.update(q['first_odi'])
					output2.update(q['sec_odi'])
					output_list.append({"T20":output1,"ODI":output2})
					return HttpResponse(json.dumps(output_list))		
		
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"something goes wrong","err":str(e)}),status=400)



def get_match_win_loss_data(playerID):
	team_player = Team_Match_Player.objects.filter(player__id=playerID)
	win_count = 0	
	loss_count = 0
	draw_count = 0
	for i in team_player:
		if int(i.match_stat.winner) == i.team.id:
			win_count = win_count + 1
		elif i.match_stat.is_draw:
			draw_count = draw_count + 1
		else:
			loss_count = loss_count + 1
	return {"total_matchs":team_player.count(), "total_win":win_count, "total_loss":loss_count, "draw_count":draw_count}


def get_match_win_loss_dataODI(playerID, q):
	"""It is for player's match record like total_win , total_loss etc"""
	print("thread_ODI1")
	team_player = Team_Match_Player.objects.filter(player__id=playerID, match_stat__m_format='O')
	win_count = 0	
	loss_count = 0
	draw_count = 0
	for i in team_player:
		if not i.match_stat.winner == None:
			if int(i.match_stat.winner) == i.team.id:
				win_count = win_count + 1
			elif i.match_stat.is_draw:
				draw_count = draw_count + 1
			else:
				loss_count = loss_count + 1
	print(1,team_player.count())
	q.update({"first_odi":{"total_matchs":team_player.count(), "total_win":win_count, "total_loss":loss_count, "draw_count":draw_count}})

def get_match_avg_recordODI(playerID, q):
	"""It is for player's match avg record like avg_score , avg_wicket etc"""
	print("thread_ODI2")

	total_matchs = Team_Match_Player.objects.filter(player__id=playerID, match_stat__m_format='O').count()
	avg_run = Match_Stat_Batting.objects.filter(player__id=playerID, match_stat_team__match_stat__m_format='O').aggregate(Avg('run'))
	

	avg_wicket = Match_Stat_Bowling.objects.filter(player__id=playerID, match_stat_team__match_stat__m_format='O').aggregate(Avg('wickets'))
	
	out_list = ['caught', 'caught&bowled', 'caughtbehind']
	total_wicket = Match_Stat_Out.objects.filter(who__id=playerID, out_type__in=out_list).count()
	avg_wicket = 0
	if not total_matchs == 0:
		avg_wicket = total_wicket / total_matchs

	#avg_wicket = total_wicket / total_matchs
	total_batting = Match_Stat_Batting.objects.filter(player__id=playerID, match_stat_team__match_stat__m_format='O')
	total_run = total_batting.aggregate(Sum('run'))
	total_bowl = total_batting.aggregate(Sum('bowl'))
	strike_rate = 0
	if not total_bowl['bowl__sum'] == None:
		strike_rate = (total_run['run__sum'] / total_bowl['bowl__sum']) * 100
	print(2)
	q.update({"sec_odi":{"avg_score":avg_run['run__avg'], "avg_wicket":avg_wicket, "total_wicket":total_wicket, "strike_rate":strike_rate}})


def get_match_win_loss_dataT20(playerID, q):
	"""It is for player's match record like total_win , total_loss etc"""
	print("thread_T201")
	team_player = Team_Match_Player.objects.filter(player__id=playerID, match_stat__m_format='T')
	win_count = 0	
	loss_count = 0
	draw_count = 0
	for i in team_player:
		if int(i.match_stat.winner) == i.team.id:
			win_count = win_count + 1
		elif i.match_stat.is_draw:
			draw_count = draw_count + 1
		else:	
			loss_count = loss_count + 1
	print(3)
	q.update({"first_t20":{"total_matchs":team_player.count(), "total_win":win_count, "total_loss":loss_count, "draw_count":draw_count}})

def get_match_avg_recordT20(playerID, q):
	"""It is for player's match avg record like avg_score , avg_wicket etc"""
	print("T202")
	total_matchs = Team_Match_Player.objects.filter(player__id=playerID, match_stat__m_format='T').count()
	avg_run = Match_Stat_Batting.objects.filter(player__id=playerID, match_stat_team__match_stat__m_format='T').aggregate(Avg('run'))
	#avg_wicket = Match_Stat_Bowling.objects.filter(player__id=playerID, match_stat_team__match_stat__m_format='T').aggregate(Sum('wickets'))
	out_list = ['caught', 'caught&bowled', 'caughtbehind']
	total_wicket = Match_Stat_Out.objects.filter(who__id=playerID, out_type__in=out_list).count()
	avg_wicket = 0
	if not total_matchs == 0:
		avg_wicket = total_wicket / total_matchs
	total_batting = Match_Stat_Batting.objects.filter(player__id=playerID, match_stat_team__match_stat__m_format='T')
	total_run = total_batting.aggregate(Sum('run'))
	print(avg_run['run__avg'])
	total_bowl = total_batting.aggregate(Sum('bowl'))
	strike_rate = 0
	if not total_bowl['bowl__sum'] == None:
		strike_rate = (total_run['run__sum'] / total_bowl['bowl__sum']) * 100
	avg_w = 0
	try:
		avg_w = avg_wicket['wicket__avg']
	except:
		pass	
	print(4)
	q.update({"sec_t20":{"avg_score":avg_run['run__avg'], "avg_wicket":avg_w, "total_wicket":total_wicket, "strike_rate":strike_rate}})


def display(name):
	time.sleep(5)
	print("Hello ",name)


class TestAsync(APIView):
	def get(self,request):
		TaskRegistration(display, "Gaurav", 1).delay()
		return Response("success")
		
from django.db.models import Max, Sum, Q, Avg

class PlayerStats(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def get(self, request):
		
		try:
			player_id = request.GET["id"]
			user = Players.objects.get(id=player_id)
			team_match_player_qs = Team_Match_Player.objects.filter(Q(match_stat__is_live=False)&Q(match_stat__is_canceled=False)&Q(match_stat__is_result_declared=True),player=user).distinct("match_stat")

			total_matches = team_match_player_qs.count()
			won = 0
			lost = 0
			draw = 0
			for i in team_match_player_qs:
				if i.match_stat.is_draw:
					draw = draw + 1
				elif i.match_stat.winner == i.team.id:
					won +=1
				else:
					lost+=1
			matches_dict = {"played":total_matches, "won":won, "lost":lost, "draw":draw}
			batting_qs = Match_Stat_Batting.objects.filter(is_valid=True, player=user)
			max_score = batting_qs.aggregate(Max("run"))["run__max"]
			total_runs = batting_qs.aggregate(Sum("run"))["run__sum"]

			print(max_score, total_runs, "fffhjjfhgojhg")
			if not max_score:
				max_score = 0
			if not total_runs:
				total_runs = 0

			try:
				batting_average = round(total_runs/total_matches, 2)
			except:
				batting_average = 0
			half_centuries = batting_qs.filter(run__gte=50, run__lt=100).count()
			centuries = batting_qs.filter(run__gte=100).count()
			batting_dict = {
				"runs":total_runs,
				"batting_average":batting_average,
				"half_centuries":half_centuries,
				"centuries":centuries,
				"highest_runs":max_score
			}
			bowling_qs = Match_Stat_Bowling.objects.filter(player=user, is_valid=True)
			wickets = bowling_qs.aggregate(Sum("wickets"))["wickets__sum"]
			if not wickets:
				wickets = 0
			try:
				ec_rate = round(bowling_qs.aggregate(Avg("economy_rate"))["economy_rate__avg"], 2)
			except:
				ec_rate = 0.0
			maidens = bowling_qs.aggregate(Sum("maiden"))["maiden__sum"]
			
			f_wickets = bowling_qs.filter(wickets__gte=5).count()
			h_wickets = bowling_qs.aggregate(Max("wickets"))["wickets__max"]
			if not maidens:
				maidens = 0
			if not h_wickets:
				h_wickets = 0
			bowling_dict = {
				"wickets":wickets,
				"bowling_economy":ec_rate,
				"maidens":maidens,
				"five_wickets":f_wickets,
				"highest_wickets":h_wickets
			}
			return HttpResponse(json.dumps({"success":True, "matches":matches_dict, "batting_stat":batting_dict, "bowling_stat":bowling_dict}), status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)
