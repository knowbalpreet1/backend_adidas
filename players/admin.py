from django.contrib import admin


from .models import TeamRole, Players, Team, TeamPlayers, Players_Followers,Player_Honors_Awards , Activity_Storage, Player_Activity_Map, State, City, SportsType, Players_Preferred_Sport, Location, CompetitionType, TeamAdmin

# Register your models here.

class Player_Custom(admin.ModelAdmin):
	search_fields = ['user__name', 'user__mobile']

admin.site.register(Player_Honors_Awards)
admin.site.register(State)
admin.site.register(City)
admin.site.register(TeamRole)
admin.site.register(Players, Player_Custom)
admin.site.register(Team)
admin.site.register(TeamPlayers)
admin.site.register(Players_Followers)
admin.site.register(Location)
admin.site.register(Activity_Storage)
admin.site.register(Player_Activity_Map)
admin.site.register(SportsType)
admin.site.register(Players_Preferred_Sport)
admin.site.register(CompetitionType)
admin.site.register(TeamAdmin)

