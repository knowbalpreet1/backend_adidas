from django.conf.urls import url, include
from players import views, player_extra_views

urlpatterns = [
	url(r'teamcreate/$', views.TeamCreate.as_view(),name="to_create_team"),

	url(r'teampic/$', views.TeamPicUpload.as_view(), name="upload_pic_of_team"),

	url(r'teamplayers/$',views.Team_Players.as_view(), name="to_add_and_get_players_team"),
	url(r'player_activities/$',views.Player_Activity_View.as_view(), name="get_player_activity"),
	url(r'player_profile/$',views.Player_Profile_View.as_view(), name="get_player_profile"),
	url(r'get_basic_info/$',views.Player_Basic_Info.as_view(), name="get_basic_info"),

	url(r'teampic/$', views.TeamPicUpload.as_view(), name="upload_pic_of_team"),
	url(r'teamplayers/$',views.Team_Players.as_view(), name="to_add_and_get_players_team"),

	url(r'profile/$',views.Player_Self_Profile_View.as_view(), name="player's profile"),	
	url(r'follow/$',views.Follow.as_view(), name="follow"),
	url(r'playersearch/$', views.PlayerSearch.as_view(), name="player_search"),
	url(r'player_following/$',views.playerFollowingView.as_view(), name="players following"),
	url(r'allplayers/$', views.MyPlayers.as_view(), name="players_in_all_teams"),
	url(r'get_stat/$', player_extra_views.Player_Main_Stat.as_view(), name="get_player_stat"),
	url(r'get_all_stat/$', player_extra_views.Player_All_Stat_View.as_view(), name="get_player_stat"),
	url(r'testapi/$', player_extra_views.TestAsync.as_view(), name="get_player_stat"),
	url(r'search_team/$', views.Serach_Team_View.as_view(), name="search_team"),
	url(r'my_teams_scoreboard/$', views.My_Teams_Scoreboard.as_view(), name="my_teams_scoreboard"),
	url(r'players_follower/$',views.players_followers.as_view(), name="player's profile"),
	url(r'create_team/$',views.Create_Team_Player.as_view(), name="player's profile"),
	url(r'comp_list/$',views.Competition_Type.as_view(), name="competition_list"),
	url(r'sportType/$', views.SportTypeView.as_view(), name="SportTypeView"),
	url(r'city/$', views.CityView.as_view(), name="CityView"),
	url(r'player_stat/$',player_extra_views.PlayerStats.as_view(), name="playerstat"),
	url(r'newsfeed/$', views.NewsFeedApi.as_view(), name = "newsfeed"),
]

