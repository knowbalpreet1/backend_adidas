from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.utils.timezone import now, timedelta
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.utils.timesince import timesince

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from authentication.models import Account
from authentication.views import send_OTP
from oauth2_provider.models import Application
from .models import Players, City, Team, TeamPlayers,SportsType, TeamRole, Player_Honors_Awards, Players_Preferred_Sport, Player_Activity_Map, SportsType, Players_Followers, Activity_Storage, TeamPlayers, CompetitionType, TeamAdmin
import traceback
from scorecard.models import Player_Honors_Awards_Model
import json
from scorecard.models import Team_Match_Player, Match_Stat_Team
from event.views import googledistance
from event.models import Event, EventPlayerMap
from datetime import datetime
from threading import Thread
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.conf import settings
import threading
import logging
import time
import threading
from django.db.models import Q
from notification.views import send_notification, save_notification
from haystack.query import SearchQuerySet

loop = settings.LOOP

def get_player_distance(self_player, follow_player):
	distance = googledistance(self_player.user.latitude, self_player.user.longitude, follow_player.user.latitude, follow_player.user.longitude)
	return str(distance)

class Player_Profile_View(APIView):
	"""This view is for player's profile information like personal detail"""

	def get(self,request):
		requested_user = request.user
		playerID = request.GET.get('playerID')
		player_pic = None
		print("---------------------------->")
		try:
			try:
				print("testtt")
				follow_player = Players.objects.get(id=playerID, is_active=True)
				print(11)
				self_player = Players.objects.get(user=requested_user, is_active=True)
			except Exception as e:
				return HttpResponse(json.dumps({"status":False,"message":"You are not player!!","err_msg":str(e)}))
			if follow_player.user.profile_pic:
				player_pic = follow_player.user.profile_pic.url
			is_follow = False
			#print(player,self_player)
			try:
				Players_Followers.objects.get(player=self_player, followers=follow_player)
				is_follow = True
			except Exception as e:
				print(e)
				pass
			total_follower = Players_Followers.objects.filter(followers=follow_player).count()
			total_following = Players_Followers.objects.filter(player = follow_player).count()
			today = datetime.now().date()
			born = follow_player.user.dob
			age = None
			if born:
				logging.warning(born)
				age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
			team_id = [z.team.id for z in TeamPlayers.objects.filter(player=follow_player)]
			total_matches = Team_Match_Player.objects.filter(team__id__in=team_id, player=follow_player, match_stat__is_live=False, match_stat__is_canceled=False, match_stat__is_result_declared=True).distinct('match_stat').count()
			activity = get_player_activity()
			player_profile = {
			"status":True,

			"player_name": follow_player.user.name,
			"player_pic": player_pic,
			"gender": follow_player.user.gender,
			"distance_from_me":get_player_distance(self_player, follow_player),
			"age": age,
			"is_follow": is_follow,
			"total_follower":total_follower,
			"total_following":total_following,
			"total_matches":total_matches,
			"address":follow_player.user.city
			}
			return HttpResponse(json.dumps(player_profile))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False, "messgage":"something goes wrong", "err_msg":str(e)}), status=400)


class Player_Self_Profile_View(APIView):
	"""This view is for player's profile information like personal detail"""

	def get(self,request):
		requested_user = request.user
		#playerID = request.GET.get('playerID')
		player_pic = None
		try:
			try:
				#player = Players.objects.get(id=playerID)
				player = Players.objects.get(user=requested_user)
			except Exception as e:
				return HttpResponse(json.dumps({"status":False, "message":"You are not player", "err_msg":str(e)}))
			total_follower = Players_Followers.objects.filter(followers=player).count()
			total_following = Players_Followers.objects.filter(player = player).count()
			if player.user.profile_pic:
				player_pic = player.user.profile_pic.url

			today = datetime.now().date()
			born = player.user.dob
			age = None
			if born:
				logging.warning(born)
				age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
			team_id = [z.team.id for z in TeamPlayers.objects.filter(player=player)]
			total_matches = Team_Match_Player.objects.filter(player=player, team__id__in=team_id,match_stat__is_result_declared=True, match_stat__is_canceled= False, match_stat__is_live=False).distinct('match_stat').count()
			activity = get_player_activity()
			player_profile = {
			"status":True,
			"player_name": player.user.name,
			"player_pic": player_pic,
			"gender": player.user.gender,
			"address":player.user.city,
			"age": age,
			"total_follower": total_follower,
			"total_following": total_following,
			"total_matches": total_matches,
			"address": player.user.city
			}

			return HttpResponse(json.dumps(player_profile))
		except Exception as e:
			return HttpResponse(json.dumps({"status":False, "messgage":"something goes wrong", "err_msg":str(e)}), status=400)


class Follow(APIView):
	"""This view is used for follow any player"""
	permission_class = [TokenHasReadWriteScope]
	def post(self,request):
		requested_user = request.user
		data = request.data
		print(data)
		try:

			follower_playerID = data.get('playerID')
			is_follow = data.get('is_follow')
			try:
				player = Players.objects.get(user=requested_user, is_active=True)
				follower_player = Players.objects.get(id = follower_playerID, is_active=True)
			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"status":False, "message":"You are not player", "err_msg":str(e)}),status=400)
			if not is_follow:
				try:
					p = Players_Followers.objects.get(player=player,followers=follower_player)
					p.delete()
					return HttpResponse(json.dumps({"status":False,"message":"You have successfully unfollow"}))
				except:
					traceback.print_exc()
					return HttpResponse(json.dumps({"status":False,"message":"you entered wrong input"}), status=400)
			obj, created = Players_Followers.objects.get_or_create(player=player,followers=follower_player)
			if not created:
#				print('not created>>>>>>>>>>>>>>>>>>>')
				return HttpResponse(json.dumps({"status":False,"message":"You have already followed"}),status=400)
			t = threading.Thread(target=create_activity_thread, args=(player, follower_player,'F','following'))
			t.start()
			profile_pic = None
			if player.user.profile_pic:
				profile_pic = player.user.profile_pic.url
			message_body = {"title":"", "body":"You are followed by "+player.user.name, "profile_pic":profile_pic,"player_id":player.id, "action":"following"}
			try:
				loop.run_in_executor(None, send_notification, [[follower_player], message_body])
				loop.run_in_executor(None, save_notification,[follower_player, player, 'P', "You are followed by "+player.user.name])
#				send_notification(([follower_player], message_body))
#				save_notification(player = follower_player, instance = player, notification_type = 'P', message = "You are followed by "+player.user.name)
			except:
				print(e)
				traceback.print_exc()
				pass
#			print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.')
			return HttpResponse(json.dumps({"status":True,"message":"You have successfully follow"}))
		except Exception as e:
			print(e)
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"something goes wrong","err":str(e)},status=400))

def create_follower_activity_thread(player, follower_player):
	activity = Activity_Storage.objects.get_or_create(activity_type='F', match_type='following', content_object=follower_player)[0]
	player_activity_obj = Player_Activity_Map.objects.get_or_create(player=player,activity=activity)
	if created_1:
		player_activity_obj.joining_time = datetime.now()
	else:
		player_activity_obj.joining_time = datetime.now()
	player_activity_obj.save()




def create_follower_activity_thread(player, follower_player):
	logging.warning("Thread executing")
	obj = Activity_Storage.objects.create(activity_type='F', match_type='following', content_object=follower_player)
	#Player_Activity_Map.objects.get(player)
	player_activity_obj = Player_Activity_Map.objects.create(player=player,activity=obj,joining_time=datetime.now().replace(second=0, microsecond=0) )
	logging.warning(player_activity_obj)
	logging.warning("Thread finished")


class Player_Activity_View(APIView):
	"""This views is for get player's activity, basic info and statistic"""

	def get(self, request):
		#requested_user = request.user
		playerID = request.GET.get('playerID')
		try:
			activity = Player_Activity_Map.objects.filter(Q(player__id=playerID) & Q(is_active=True)).order_by('-joining_time')
			try:
				requested_player = Players.objects.get(id=playerID)
			except:
				traceback.print_exc()
				return Response({"status":False, "message":"you are not player"}, status=400)
#			logging.warning(activity.count())
			activity_list = []
			paginator = Paginator(activity, 4)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			try:
				page_data = paginator.page(paging)
			except PageNotAnInteger:
				page_data = paginator.page(1)
			except EmptyPage:
				page_data = paginator.page(paginator.num_pages)
			for act in page_data:
				#-------get all activity record
				print(act.activity.content_object,act.activity.id, '------------->activity')
				get_all_activity(act, requested_player, activity_list)

			activity = Player_Activity_Map.objects.filter(player__id=playerID, is_active=True, activity__match_type="following").order_by('-joining_time').first()
#			print("---------Actttttt",activity.id)
			if not activity == None:
				profile_pic = None
				logo = None
				day = (datetime.now().date() - activity.joining_time.date()).days
				if day == 0:
					#----method for eg- 2 minute ago----------
					ago = get_time_ago(activity)
				else:
					ago = str(day) + " day ago"

				if requested_player.user.profile_pic:
					profile_pic = requested_player.user.profile_pic.url
				if activity.activity.logo:
					logo = activity.activity.logo.url

				following_player = Players_Followers.objects.filter(player__id= playerID)
				pic = None
				if following_player:
					if following_player.first().followers.user.profile_pic:
						pic = following_player.first().followers.user.profile_pic.url
					activity = {"logo":None,"player_team":None, "won_team_id":None, "stadium":None, "los_team_id":None, "versus_team":None, "won_team":None,"result":None,"player_name":requested_player.user.name,"following_player_name":following_player.first().followers.user.name,"following_playerPic":pic,"message":"started following", "type":activity.activity.match_type,  "activity":"following", "following_count":following_player.count()-1, "player_profile_pic":profile_pic, "time":ago}
					activity_list.append(activity)
			return HttpResponse(json.dumps({"status":True,"activity_list":activity_list, "last_page":last_page}))



		except Exception as e:
			traceback.print_exc()
			return Response({"err":e})


def get_all_activity(act, requested_player,activity_list):
	logging.warning(act.activity.activity_type)
	ago = None
	if act.activity.activity_type == 'E':
		print("lllll")
		day = (datetime.now().date() - act.joining_time.date()).days
		if day == 0:
			#----method for eg- 2 minute ago----------
			ago = get_time_ago(act)
		else:
			ago = str(day) + " days ago"
		try:

			logo = None
			profile_pic = None
			if requested_player.user.profile_pic:
				profile_pic = requested_player.user.profile_pic.url
			if act.activity.content_object.image.image:
				logo = act.activity.content_object.image.image.url
			activity = {"following_playerPic":None,"following_player_name":None,"logo":logo, "player_team":None, "versus_team":None,"won_team":None,"result":None,"type":act.activity.match_type,"player_profile_pic":profile_pic, "player_name":requested_player.user.name, "activity":"going", "stadium":act.activity.content_object.name, "message":"is going to event", "time":ago}
			activity_list.append(activity)
			print('l--------------->')
		except:
			traceback.print_exc()
			pass
	elif act.activity.activity_type == "T":

		day = (datetime.now().date() - act.joining_time.date()).days
		if day == 0:
			#----method for eg- 2 minute ago----------
			ago = get_time_ago(act)
		else:
			ago = str(day) + " days ago"
		try:
			logo = None
			print(act.activity.match_type == "playing")
			player_activity = "tournament_playing"
			if str(act.activity.match_type).strip() == "playing":
				profile_pic = None

				if requested_player.user.profile_pic:
					profile_pic = requested_player.user.profile_pic.url
				if act.activity.logo:
					logo = act.activity.logo.url
				activity = {"logo":logo, "player_team":None, "versus_team":None, "won_team":None, "result":None, "player_profile_pic":profile_pic, "type":act.activity.match_type, "activity":player_activity, "player_name":requested_player.user.name, "following_count":None,"following_player_name":None,"time":ago, "stadium":act.activity.content_object.name}
				activity_list.append(activity)
				print("<-------->",activity_list)
		except:
			traceback.print_exc()
			pass
	elif act.activity.activity_type == 'M' and act.activity.match_type == 'Won a match':
		profile_pic = None
		player_activity = "match"
		if requested_player.user.profile_pic:
			profile_pic = requested_player.user.profile_pic.url

		day = (datetime.now().date() - act.joining_time.date()).days
		if day == 0:
			#----method for eg- 2 minute ago----------
			ago = get_time_ago(act)
		else:
			ago = str(day) + " days ago"
		logo = None
		if act.activity.logo:
					logo = act.activity.logo.url
		matches = Team_Match_Player.objects.filter(Q(match_stat__is_canceled=False) & Q(match_stat__is_result_declared=True) & Q(match_stat__is_live=False) & Q(player=requested_player)).order_by('-match_stat__end_datetime').first()
		versus_team = None
		if not matches.team.id == matches.match_stat.team1.id:
			versus_team = matches.match_stat.team1.name
		else:
			versus_team = matches.match_stat.team2.name
			print("---------->",versus_team)
		print(matches.team.name)
		won_team = None
		los_team = None
		is_draw = False
		won_team_id = None
		los_team_id = None

		if matches.match_stat.is_draw:
			is_draw = True
#		elif matches.match_stat.winner == matches.team.id:
#			won_team = matches.team.name
#			won_team_id = matches.team.id
#		else:
#			los_team = matches.team.name
#			los_team_id = matches.team.id
		else:
			won_team = Team.objects.get(id = matches.match_stat.winner).name
			won_team_id = matches.match_stat.winner

		activity = {"logo":logo, "player_team":matches.team.name, "message":"", "versus_team":versus_team, "won_team":won_team, "won_team_id":won_team_id, "los_team":los_team, "los_team_id":los_team_id, "result":matches.match_stat.won_by, "player_profile_pic":profile_pic, "type":act.activity.match_type, "activity":player_activity, "player_name":requested_player.user.name, "following_count":None,"following_player_name":None,"time":ago, "stadium":None}
		print(activity)
		activity_list.append(activity)

	else:
		pass

def get_time_ago(act):

	tm = timezone.now() - act.joining_time
	totsec = tm.total_seconds()
	h = int(totsec//3600)
	m = int(totsec//3600)
	logging.error(m)
	if h>0:
		ago = str(h) + " hour ago"
	elif m>0:
		ago = str(m) + " minutes ago"
	else:
		ago = "just now"
	return ago
class Player_Basic_Info(APIView):
	"""This view is used for get basic info of player"""

	def get(self, request):
		#requested_user = request.user
		playerID = request.GET.get('playerID')
		try:
			player = Players.objects.get(id=playerID, is_active=True)
			logging.warning("player"+str(player.user.name))
		except Exception as e:
			return HttpResponse({"status":False, "message":"Player does not exist", "error":str(e)},status=400)
		#dob = None
		try:
			award = Player_Honors_Awards_Model.objects.filter(player_match__player__id=playerID)
			output_list = []
			t_name = ""
			award_list = []
			for a in award:
				if not a.player_match.match_stat.tournament_group == None:
					print("111")
					t_name = a.player_match.match_stat.tournament_group.tournament_round.tournament.name
				else:
					if not a.player_match.match_stat.tournament_round == None:
						t_name = a.player_match.match_stat.tournament_round.tournament.name
				location = ""
				if not a.player_match.match_stat.venue == None:
					if not a.player_match.match_stat.venue.address == None:
						location = a.player_match.match_stat.venue.address.city.name
				print("------------------->")
				award_list.append({"tournament_name":t_name, "award_name":a.award_name, "location":location, "date":str(a.date), "position":a.position,"status":a.status})
			team_id = [i.team.id for i in TeamAdmin.objects.filter(player=player, is_active=True)]
			total_team = Team.objects.filter(id__in=team_id).distinct('id')
			player_sport = Players_Preferred_Sport.objects.filter(player=player, is_active=True).values('sport_name__name','id')
			logging.warning(player_sport)
			player_info = {
			"status":True,
			"dob": str(player.user.dob),
			"address":player.user.city,
			"sport": list(player_sport),
			"awards":award_list,
			"teams":[{"id":i.id,"name":i.name } for i in total_team]
			}
			return HttpResponse(json.dumps(player_info))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"something goes wrong","error":str(e)}),status=400)





def get_player_activity():
	return Response({"activity":None})



class TeamCreate(APIView):
	""" to create a team. Team name will always be unique no matter what"""
	permission_classes = [TokenHasReadWriteScope]

	def post(self, request):
		if request.user.is_active:
			try:
				sport = request.GET.get("sport")
				if sport=='' or sport==None:
					sport=1
				data = request.data

				city = data["city"]
				name = data["name"]

				if not isinstance(city, int):
					return HttpResponse(json.dumps({"error":"city must be integer"}),status=400)

				if name==None or name=='' or len(name.replace(" ", ""))==0:
					return HttpResponse(json.dumps({"error":"team name cannot be none and empty"}),status=400)
				user = request.user
				player = Players.objects.get(user=user)

				try:
					city = City.objects.get(id=city)
					sport_obj = SportsType.objects.get(id=sport, is_active=True)

				except Exception as e:
					traceback.print_exc()
					return HttpResponse(json.dumps({"error":str(e), "message":"no such city"}),status=400)

				try:
					team = Team.objects.get(name__iexact=str(name.strip()))
					if team.is_active:
						return HttpResponse(json.dumps({"team_exists":True}),status=400)
				except Exception as e:
					traceback.print_exc()

				team = Team.objects.create(
					city = city,
					sport = sport_obj,
					name = name.strip(),
					created_by = player,
					created_at = timezone.now(),
					is_active = True
					)

				return HttpResponse(json.dumps({
					"name":team.name,
					"city":team.city.name,
					"state":team.city.state.name,
					"created_by":team.created_by.user.name,
					"created_at":team.created_at.strftime("%B %d,%Y @ %H:%M"),
					"is_active":team.is_active,
					"id":team.id,
					"sport":team.sport.name,
					"sport_id":team.sport.id,
					}))

			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)

		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)

	def get(self, request):
		if request.user.is_active:
			try:
				admin_teams = []
				player_team = []
				list1 = []
				user = request.user
				sport = request.GET.get("sport")
				if sport==None or sport=='':
					sport=1
				team_admin = Team.objects.filter(created_by__user = user, sport__id = sport, is_active = True)
				team_player = TeamPlayers.objects.filter(player__user = user, team__sport = sport, is_active = True)

				pic = None

				for x in team_admin:
					if x.team_pic:
						pic = x.team_pic.url
					result_dict = {

					"team_name":x.name,
					"city":x.city.name,
					"state":x.city.state.name,
					"team_pic":pic,
					"is_active":x.is_active,
					"sport":x.sport.name,
					"sport_id":x.sport.id,
					"id":x.id,
					"owner":True
					}
					admin_teams.append(result_dict)

				for y in team_player:
					if y.team.team_pic:
						pic = y.team.team_pic.url
					result_dict1 = {
					"team_name":y.team.name,
					"city":y.team.city.name,
					"state":y.team.city.state.name,
					"team_pic":pic,
					"is_active":y.team.is_active,
					"sport":y.team.sport.name,
					"sport_id":y.team.sport.id,
					"id":y.team.id,
					"owner":False
					}
					player_team.append(result_dict1)

				player_team.extend(admin_teams)
				result_list = list({v['id']:v for v in player_team}.values()) #to get unique data
				paginator = Paginator(result_list, 10)
				last_page = paginator.num_pages
				paging = request.GET.get('page')
				try:
					page_data = paginator.page(paging)

				except PageNotAnInteger:
					page_data = paginator.page(1)

				except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

				for i in page_data:

					list1.append(i)

				return HttpResponse(json.dumps({"data":list1, "last_page":last_page}),status=200)
			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)

	def delete(self, request, pk=None):
		if request.user.is_active:

			try:
				user = request.user
				data = request.data
				sport = request.GET.get("sport")
				if sport==None or sport=='':
					sport=1
				i_d = data["id"]

				if not isinstance(i_d, int):
					return HttpResponse(json.dumps({"error":"id must be integer"}), status=400)

				team = Team.objects.get(id=i_d, created_by__user=user, is_active=True, sport__id=sport)
				team.is_active = False
				team.save()
				team_players = TeamPlayers.objects.filter(team=team)

				for i in team_players:
					i.is_active = False
					i.save()
				return HttpResponse(json.dumps({"success":True}),status=200)

			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)

	def put(self, request, pk=None):
		if request.user.is_active:
			try:
				data = request.data
				user = request.user
				sport = request.GET.get("sport")
				if sport==None or sport=='':
					sport=1
				i_d = data["id"]

				name = data["name"]
				city = data["city"]

				if len(set('[~!@#$%^&*()_+{}":;\']+$').intersection(name)):
					return HttpResponse(json.dumps({"error":"invalid characters in team name"}),status=400)

				if name==None or name=='' or len(name.replace(" ", ""))==0:
					return HttpResponse(json.dumps({"error":"team name cannot be none and empty"}),status=400)

				if not isinstance(city, int) or not isinstance(i_d, int):
					return HttpResponse(json.dumps({"error":"id/city must be integer"}),status=400)

				try:
					city_obj = City.objects.get(id=city)
				except Exception as e:
					return HttpResponse(json.dumps({"error":str(e)}),status=400)
				team_obj = Team.objects.get(id=i_d, created_by__user=user, sport__id=sport ,is_active=True)
				team_qs = Team.objects.filter(name__iexact=name.strip(), is_active=True)

				if len(team_qs)>0:
					return HttpResponse(json.dumps({"team_exists":True}))

				team_obj.name = name.strip()
				team_obj.city = city_obj
				team_obj.save()
				return HttpResponse(json.dumps({"success":True}),status=200)

			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)

from venue.nik_views import get_admin
class TeamPicUpload(APIView):
	permission_classes = [TokenHasReadWriteScope]

	def put(self, request, pk=None):
		try:
			data = request.data

			team_obj = Team.objects.get(id=request.GET["id"], is_active=True)
			pic = data["image"]
			user = request.user
			is_admin = get_admin(user, team_obj)
			if is_admin:
				team_obj.team_pic = pic
				team_obj.save()
				return HttpResponse(json.dumps({"success":True, "id":team_obj.id, "name":team_obj.name, "team_pic":team_obj.team_pic.url}),status=200)
			else:
				return HttpResponse(json.dumps({"success":False, "detail":"forbidden"}), status=400)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e)}),status=400)



class Team_Players(APIView):
	"""To add and get all players registered in a team"""

	permission_classes = [TokenHasReadWriteScope]

	def post(self, request):
		if request.user.is_active:
			try:
				sport = request.GET.get("sport")
				if sport==None or sport=='':
					sport = 1
				data = request.data
				user = request.user
				team_id = data["team_id"]
				mobile = data["mobile"]
				player_id = data["player_id"]
				name = data["name"]
				is_captain = data["is_captain"]

				role_obj = TeamRole.objects.get_or_create(role="Bat")[0]

				if not isinstance(team_id, int) or not isinstance(player_id, int):
					return HttpResponse(json.dumps({"error":"team and player must be integer"}),status=400)

				try:
					team_obj = Team.objects.get(id=team_id, sport__id=sport, created_by__user=user ,is_active=True)
				except Exception as e:
					traceback.print_exc()
					return HttpResponse(json.dumps({"error":str(e)}),status=400)


				if not player_id==0 and mobile=="" or mobile==None:
					try:
						player_obj = Players.objects.get(id=player_id, is_active=True)
					except Exception as e:
						traceback.print_exc()

				elif player_id==0 and str(mobile).isdigit():
					if len(str(mobile))!=10:
						return HttpResponse(json.dumps({"error":"mobile number should be of 10 digits"}),status=400)
					if name==None or name=="":
						return HttpResponse(json.dumps({"error":"name cannot be empty"}),status=400)

					try:
						account_obj = Account.objects.get(mobile=mobile)
						return HttpResponse(json.dumps({"mobile_exists":True}),status=400)
					except Exception as e:
						traceback.print_exc()


					account_obj = Account.objects.create(name=name.strip(), mobile=mobile, username=mobile)
					Application.objects.create(user=account_obj)
					t = Thread(target=send_OTP, args=(mobile, account_obj,))
					t.start()
					t.join()

					player_obj = Players.objects.create(user=account_obj, is_active=True)
				else:
					return HttpResponse(json.dumps({"detail":"Authentication credentials were not provided"}),status=401)


				is_admin = False
				team_players_qs = TeamPlayers.objects.filter(team=team_obj, is_active=True, is_captain=True).count()

				if is_captain==True and team_players_qs>0:
					is_captain= False

				verified = True
				if not player_obj.user.is_active:
					verified = False

				if player_obj.user==user:
					is_admin=True
					if (team_players_qs)==0:
						is_captain = True

				try:
					team_player_obj = TeamPlayers.objects.get(player = player_obj, team = team_obj)
					if team_player_obj.is_active:
						return HttpResponse(json.dumps({"player_exist":True}),status=400)

					if not team_player_obj.is_active:
						team_player_obj.is_active=True
						team_player_obj.save()

				except Exception as e:
					traceback.print_exc()
					team_player_obj = TeamPlayers.objects.create(
						team = team_obj,
						player = player_obj,
						role = role_obj,
						is_active = True,
						is_captain = is_captain,
						is_admin = is_admin,
						added_time = timezone.now()
						)


				return HttpResponse(json.dumps({
					"success":True,
					"id":team_player_obj.id,
					"team_name":team_player_obj.team.name,
					"team_sport":team_player_obj.team.sport.name,
					"team_sport_id":team_player_obj.team.sport.id,
					"is_active":team_player_obj.is_active,
					"is_captain":team_player_obj.is_captain,
					"is_admin":team_player_obj.is_admin,
					"verified":verified,
					"role":team_player_obj.role.role,
					"player_id":team_player_obj.player.id
					}),status=200)

			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)


	def get(self, request):
		if request.user.is_active:
			try:
				list1 = []
				sport = request.GET.get("sport")
				user = request.user
				if sport==None or sport=='':
					sport=1

				team_id = request.GET.get("team_id")

				if not team_id.isdigit():
					return HttpResponse(json.dumps({"error":"team id must be integer"}),status=400)

				team_obj = Team.objects.get(created_by__user=user, id=team_id, is_active=True)
				team_player_qs = TeamPlayers.objects.filter(team=team_obj, is_active=True).order_by("-id")

				paginator = Paginator(team_player_qs, 10)
				last_page = paginator.num_pages
				paging = request.GET.get('page')

				try:
					page_data = paginator.page(paging)

				except PageNotAnInteger:
					page_data = paginator.page(1)

				except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

				for i in page_data:
					pic = None
					if i.player.user.profile_pic:
						pic = i.player.user.profile_pic.url
					result_dict = {
					"team_name":i.team.name,
					"team_id":i.team.id,
					"is_captain":i.is_captain,
					"is_admin":i.is_admin,
					"is_active":i.is_active,
					"player_id":i.player.id,
					"role":i.role.role,
					"id":i.id,
					"player_pic":pic,
					"player_name":i.player.user.name
					}
					list1.append(result_dict)
				return HttpResponse(json.dumps({"data":list1, "last_page":last_page}),status=200)
			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)

	def delete(self, request, pk=None):
		if request.user.is_active:
			try:
				data = request.data
				user = request.user
				team_id = data["team_id"]
				player_id = data["player_id"]
				sport = request.GET.get("sport")

				if sport==None or sport=="":
					sport=1
				team_obj = Team.objects.get(id=team_id, created_by__user=user, is_active=True)
				#remove a player from a particular team

				if not isinstance(team_id, int) or not isinstance(player_id, int):
					return HttpResponse(json.dumps({"error":"team/player id must be integer"}), status=400)

				team_player_obj = TeamPlayers.objects.get(team=team_obj, player__id=player_id, is_active=True)

				if team_player_obj.is_captain:
					team_player_obj.is_captain = False


				team_player_obj.is_active = False
				team_player_obj.save()

				team_player_qs = TeamPlayers.objects.filter(team=team_obj,is_active=True)
				if len(team_player_qs)>0:
					try:

						team_admin = team_player_qs.get(is_admin=True)
						print(team_admin, ">>>>>>>>>>>>>>>>>>>>>>")
						team_admin.is_captain=True
						team_admin.save()
					except Exception as e:

						traceback.print_exc()
						for i in team_player_qs:
							i.is_captain=True
							i.save()
							break

				return HttpResponse(json.dumps({"success":True}),status=200)
			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)


	# def put(self, request, pk=None):
	# 	if request.user.is_active:
	# 		try:
	# 			data = request.data
	# 			user = request.user
	# 			sport = request.GET.get("sport")
	# 			if sport==None or sport=='':
	# 				sport = 1

	# 			team_id = data["team_id"]
	# 			player_id = data["player_id"]
	# 			role = data.get("role")
	# 			if not isinstance(team_id, int) or not isinstance(player_id, int):
	# 				return HttpResponse(json.dumps({"error":"team/player id must be integer"}), status=400)

	# 		except Exception as e:
	# 			traceback.print_exc()
	# 			return HttpResponse(json.dumps({"error":str(e)}),status=400)
	# 	else:
	# 		return HttpResponse(json.dumps({"is_active":False}),status=400)

class PlayerSearch(APIView):
	"""to search players in our database"""
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		if request.user.is_active:

			try:
				list1 = []
				name = request.GET.get("name")
				user = request.user
				if name==None or name=="" or not name:
					player_qs = Players.objects.select_related("user").filter(is_active=True).exclude(user=user).order_by("-id")

				player_qs = Players.objects.select_related("user").filter(user__name__istartswith=name,is_active=True).exclude(user=user).order_by("-id")
				paginator = Paginator(player_qs, 10)
				last_page = paginator.num_pages
				paging = request.GET.get('page')

				try:
					page_data = paginator.page(paging)

				except PageNotAnInteger:
					page_data = paginator.page(1)

				except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

				for i in page_data:
					mobile = None
					pic = None
					if i.user.profile_pic:
						pic = i.user.profile_pic.url

					if i.user.mobile and len(str(i.user.mobile))==10:
						mobile = str(i.user.mobile)[:2]+"x"*6+str(i.user.mobile)[-2:]

					result_dict = {
					"name":i.user.name,
					"mobile":mobile,
					"account_id":i.user.id,
					"player_id":i.id,
					"is_active":i.is_active,
					"profile_pic":pic,
					"is_active":i.user.is_active					}
					list1.append(result_dict)
				return HttpResponse(json.dumps({"data":list1, "last_page":last_page}),status=200)

			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)

		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)

class MyPlayers(APIView):
	"""players of all the team he/she is present in """
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		if request.user.is_active:
			try:
				user = request.user
				list1 = []
				team_ids = [x.id for x in Team.objects.filter(created_by__user=user,is_active=True)]
				player_team_ids = [y.team.id for y in TeamPlayers.objects.filter(player__user=user, is_active=True)]

				player_team_ids.extend(team_ids)
				final_team_id_list = (list(set(player_team_ids)))

				team_players_qs = TeamPlayers.objects.select_related("player").filter(team__id__in=player_team_ids, is_active=True)#.distinct("player")

				paginator = Paginator(team_players_qs, 10)
				last_page = paginator.num_pages
				paging = request.GET.get('page')

				try:
					page_data = paginator.page(paging)

				except PageNotAnInteger:
					page_data = paginator.page(1)

				except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

				for i in page_data.object_list:
					pic = None
					mobile = None
					if i.player.user.mobile and len(str(i.player.user.mobile))==10:
						mobile = str(i.player.user.mobile)[:2]+"x"*6+str(i.player.user.mobile)[-2:]
					if i.player.user.profile_pic:
						pic = i.player.user.profile_pic.url

					result_dict = {
					"player_id":i.player.id,
					"player_name":i.player.user.name,
					"pic":pic,
					"account_id":i.player.user.id,
					"mobile":mobile
					}
					list1.append(result_dict)
				return HttpResponse(json.dumps({"data":list1, "last_page":last_page}),status=200)


			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"error":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)



class Serach_Team_View(APIView):
	def get(self,request):
		search_key = request.GET.get('key')
		sport = request.GET.get('sport')
		if sport == None or sport == "":
			sport = 1
		try:
			all_team = Team.objects.filter(name__istartswith=search_key,is_active=True)
			team_list = []
			list1 = []
			print(all_team)
			for team in all_team:
				team_pic = None
				if team.team_pic:
					team_pic = team.team_pic.url
				team_players = TeamPlayers.objects.filter(team= team, is_active=True)
				players = []
				for i in team_players.select_related("player__user"):
					image = ""
					if i.player.user.profile_pic:
						image = i.player.user.profile_pic.url
					players.append({"player__user__name":i.player.user.name,"player__user__profile_pic":image,"player__id":i.player.id})
				#player__pic_list = [p.player.user.profile_pic.url if p.player.user.profile_pic else None for p in team_players]
				team_list.append({"team_name":team.name, "players":players,"id":team.id,"team_pic":team_pic})

			paginator = Paginator(team_list, 10)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			try:
				page_data = paginator.page(paging)

			except PageNotAnInteger:
				page_data = paginator.page(1)

			except EmptyPage:
				page_data = paginator.page(paginator.num_pages)

			for i in page_data.object_list:
				list1.append(i)

			return HttpResponse(json.dumps({"status":True,"team_list":list1,"last_page":last_page}))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False, "team_list":[]}))

class My_Teams_Scoreboard(APIView):

	def get(self, request):
		if request.user.is_active:
			try:
				admin_teams = []
				player_team = []
				list1 = []
				user = request.user
				sport = request.GET.get("sport")
				if sport==None or sport=='':
					sport=1
				team_admin = Team.objects.filter(created_by__user = user, sport__id = sport, is_active = True)
				team_player = TeamPlayers.objects.filter(player__user = user, team__sport = sport, is_active = True)

				pic = None

				for x in team_admin:
					if x.team_pic:
						pic = x.team_pic.url
					team_players = TeamPlayers.objects.filter(team= x, is_active=True)
					players = []
					for i in team_players.select_related('player'):
						image = ""
						if i.player.user.profile_pic:
							image = i.player.user.profile_pic.url
						players.append({"player__user__name":i.player.user.name,"player__user__profile_pic":image,"player__id":i.player.id})
					result_dict = {

					"team_name":x.name,
					"players":players,
					#"player_pic_list":[p.player.user.profile_pic.url if p.player.user.profile_pic else None for p in team_players],
					"team_pic":pic,
					"id":x.id,
					}
					admin_teams.append(result_dict)

				for y in team_player:
					if y.team.team_pic:
						pic =y.team.team_pic.url
					team_players = TeamPlayers.objects.filter(team= y.team, is_active=True)
					players = []
					for i in team_players.select_related('player'):
						image = ""
						if i.player.user.profile_pic:
							image = i.player.user.profile_pic.url
						players.append({"player__user__name":i.player.user.name,"player__user__profile_pic":image,"player__id":i.player.id})
					result_dict1 = {
					"team_name":y.team.name,
					"players":players,
					#"player_pic_list":[p.player.user.profile_pic.url if p.player.user.profile_pic else None for p in team_players],
					"team_pic":pic,
					"id":y.team.id,
					}

					player_team.append(result_dict1)

				player_team.extend(admin_teams)
				result_list = list({v['id']:v for v in player_team}.values()) #to get unique data
				paginator = Paginator(result_list, 10)
				last_page = paginator.num_pages
				paging = request.GET.get('page')
				try:
					page_data = paginator.page(paging)

				except PageNotAnInteger:
					page_data = paginator.page(1)

				except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

				for i in page_data.object_list:

					list1.append(i)

				return HttpResponse(json.dumps({"status":True,"data":list1, "last_page":last_page}),status=200)
			except Exception as e:
				traceback.print_exc()
				return HttpResponse(json.dumps({"status":False,"message":str(e)}),status=400)
		else:
			return HttpResponse(json.dumps({"is_active":False}),status=400)


class players_followers(APIView):
	def get(self,request):
		playerID = request.GET.get('playerID')
		try:
			player = Players.objects.get(id=playerID,  is_active=True)
			following = Players_Followers.objects.filter(followers= player)
			team_list = []
			requested_user = request.user
			#print(followers)
			for follower in following:
				team_pic = None
				is_follow = False
				print("------->",follower.player.id,player.id)
				try:
					Players_Followers.objects.get(player= player,followers=follower.player)
					is_follow = True
				except:
					traceback.print_exc()
					pass
				you_follow = False
				try:
					Players_Followers.objects.get(player__user=requested_user, followers= follower.player)
					you_follow = True
				except:
					#traceback.print_exc()
					pass
				if follower.player.user.profile_pic:
					team_pic = follower.player.user.profile_pic.url

				if player.user == requested_user:

					team_list.append({"is_follow":is_follow, "you_follow":is_follow, "player__user__name":follower.player.user.name, "player__user__profile_pic":team_pic, "player__id":follower.player.id})
				else:
					team_list.append({"is_follow":is_follow, "you_follow":you_follow, "player__user__name":follower.player.user.name, "player__user__profile_pic":team_pic, "player__id":follower.player.id})

			paginator = Paginator(team_list, 10)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			try:
					page_data = paginator.page(paging)

			except PageNotAnInteger:
					page_data = paginator.page(1)

			except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

			#for i in page_data.object_list:
			#		list1.append(i)

			return HttpResponse(json.dumps({"status":True,"player_list":page_data.object_list,"last_page":last_page}))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":True, "team_list":[]}))


class Create_Team_Player(APIView):
	"""create team and add player api"""

	def post(self, request):
		try:
				data = request.data
				sport = data.get('sport')
				team_name = data.get('team_name')
				city = data.get('city')
				player_list = data.get('player_list')
				if sport=='' or sport==None:
					sport=1




				#if not city:
				#	return HttpResponse(json.dumps({"error":"city can't blank"}),status=400)

				if team_name==None or team_name=='' or len(team_name.replace(" ", ""))==0:
					return HttpResponse(json.dumps({"error":"team name cannot be none and empty","status":False}),status=400)
				requested_user = request.user
				try:
					team = Team.objects.get(name__iexact=str(team_name.strip()))
					if team.is_active:
						return HttpResponse(json.dumps({"is_team_exist":True,"status":False}),status=200)
				except Exception as e:
					traceback.print_exc()
				try:
					player = Players.objects.get(user=requested_user)
				except Exception as e:
					return HttpResponse(json.dumps({"status":False, "message":"You are not player"}),status=400)

				try:
					city = City.objects.get(id=city)
					sport_obj = SportsType.objects.get(id=sport, is_active=True)

				except Exception as e:
					traceback.print_exc()
					return HttpResponse(json.dumps({"error":str(e), "message":"no such city","status":False}),status=400)

				team = Team.objects.create(
					city = city,
					sport = sport_obj,
					name = team_name.strip(),
					created_by = player,
					created_at = timezone.now(),
					is_active = True
					)
				profile_pic = ""
				if team.team_pic:
					profile_pic = team.team_pic.url

				message_body = {"title":"", "body":"You are added to the team {0} by {1}. Let's Play! ".format(team.name, team.created_by.user.name), "profile_pic":profile_pic,"team_id":team.id, "action":"add_player"}

				for plr in player_list:
					team_player = Players.objects.get(id= plr)
					if team_player.user.id != request.user.id:
						loop.run_in_executor(None, send_notification,[[team_player], message_body])
#						send_notification(([team_player], message_body))
						loop.run_in_executor(None, save_notification,[team_player, team, 'T', "You are added to the team {0} by {1}. Let's Play! ".format(team.name, team.created_by.user.name)])
#						save_notification(player = team_player, instance = team, notification_type = 'T', message = "You are added to the team {0} by {1}. Let's Play! ".format(team.name, team.created_by.user.name))

					teamp, created = TeamPlayers.objects.get_or_create(team= team, player= team_player)
					print (created,'createed-----',teamp.is_active,'iddd',teamp.id)
					if not created and not teamp.is_active:
						teamp.is_active = True
						teamp.save()
				a = TeamAdmin.objects.update_or_create(team=team,player=player,defaults={'is_active':True})
				return HttpResponse(json.dumps({"status":True, "team_name":team.name, "id":team.id, "message":"Team and players are added successfully"}))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "message":"somthing goes wrong in creating team and adding players","status":False}),status=400)

class Competition_Type(APIView):

	def get(self, request):
		try:
			i_d = request.GET.get("id")
			try:
				sports_obj = SportsType.objects.get(id=i_d)
			except Exception as e:
				return Response({"error":str(e)},status=400)
			comp_qs = CompetitionType.objects.filter(sport_type=sports_obj, is_active=True)
			result = [{
					"name":i.name,
					"sports_type":i.sport_type.name,
					"sports_id":i.sport_type.id,
					"is_active":i.is_active,
					"id":i.id
				} for i in comp_qs]
			return Response(result, status=200)
		except Exception as e:
			traceback.print_exc()
			return Response({"error":str(e)},status=400)


class SportTypeView(APIView):
	def get(self, request):
		try:
			qs= SportsType.objects.filter(is_active= True)
			serializer= []
			for obj in qs:
				serializer.append({'id':obj.id, 'name':obj.name})
			return Response(serializer)
		except Exception:
			traceback.print_exc()
			return Response({'success':False}, status= 400)

class CityView(APIView):
	def get(self, request):
		try:
			data= request.GET
			value= data.get('value')
			qs= City.objects.filter(name__startswith= value).distinct('name')[:10]

			serializer= []
			for obj in qs:
				serializer.append({'id':obj.id, 'name':obj.name})
			return Response(serializer)
		except Exception:
			traceback.print_exc()
			return Response({'success':False}, status= 400)


class playerFollowingView(APIView):
	def get(self, request):
		playerID = request.GET.get('playerID')
		player = Players.objects.get(id= playerID, is_active=True)
		following = Players_Followers.objects.filter(player= player)
		print("userrrr",request.user.id)
		you_player = Players.objects.get(user__id=request.user.id, is_active=True)
		team_list = []
		print(following, player)
		requested_user = request.user
		try:
			for follower in following:
				team_pic = None
				if follower.followers.user.profile_pic:
					team_pic = follower.followers.user.profile_pic.url

				you_follow = False
				try:
					following = Players_Followers.objects.get(player= you_player, followers=follower.player)
					you_follow = True
				except Exception as e:
					traceback.print_exc()
					pass
				if player.user == requested_user:
					team_list.append({"is_follow":True, "you_follow":True, "player__user__name":follower.followers.user.name, "player__user__profile_pic":team_pic, "player__id":follower.followers.id})
				else:


					team_list.append({"is_follow":True, "you_follow":you_follow, "player__user__name":follower.followers.user.name, "player__user__profile_pic":team_pic, "player__id":follower.followers.id})

			paginator = Paginator(team_list, 10)
			last_page = paginator.num_pages
			paging = request.GET.get('page')
			try:
				page_data = paginator.page(paging)

			except PageNotAnInteger:
				page_data = paginator.page(1)

			except EmptyPage:
					page_data = paginator.page(paginator.num_pages)

			return HttpResponse(json.dumps({"status":True,"player_list":page_data.object_list,"last_page":last_page}))
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False, "team_list":[]}))

def create_activity_thread(player, content_object, activity_type, match_type):
#	print(player, type(content_object), activity_type, match_type)
	content_obj = ContentType.objects.get_for_model(content_object)
	try:
		activity = Activity_Storage.objects.get(activity_type=activity_type, match_type=match_type, content_type__id=content_obj.id, object_id=content_object.id)
	except:
		print("u here --------------------")
		activity = Activity_Storage.objects.create(activity_type=activity_type, match_type=match_type,content_object=content_object)
	player_activity_obj, created= Player_Activity_Map.objects.get_or_create(player=player,activity=activity)
	player_activity_obj.joining_time = datetime.now()
	player_activity_obj.save()




class NewsFeedApi(APIView):

	def get(self, request):
		try:
#			print(request.user)
			player = Players.objects.get(user = request.user, is_active = True)
#			print(player)
			followers = Players_Followers.objects.filter(player = player)
			f_list = [f.followers.id for f in followers]
			follow_activity = Player_Activity_Map.objects.filter(player__id__in = f_list).order_by("-joining_time")
			final = []
			page = request.GET.get('page')
			paginator = Paginator(follow_activity, 7)
			try:
				data = paginator.page(page)
			except PageNotAnInteger:
				data = paginator.page(1)
			except EmptyPage:
				data = paginator.page(paginator.num_pages)
			print(len(data))
			for fa in data:
				print(player,'inside first loop>>>>>>>')
				fa_pic = None
				if fa.player.user.profile_pic:
					fa_pic = fa.player.user.profile_pic.url
				if not fa.activity.content_object:
					print(fa.activity.activity_type, fa.activity.id)
					continue
				if fa.activity.activity_type == 'F':
					follow = fa.activity.content_object
					try:
						print('player>>>>>', player, 'follow>>>>>>', follow)
						Players_Followers.objects.get(player = player, followers = follow)
						is_follow = True
					except :
#						traceback.print_exc()
						print(follow, player)
						is_follow = False
					f_follower = Players_Followers.objects.filter(player = follow).count()
					f_following = Players_Followers.objects.filter(followers = follow).count()
					match_played = Team_Match_Player.objects.filter(player = follow, match_stat__is_result_declared = True).distinct("match_stat__id").count()

					f_pic = None
					if follow.user.profile_pic:
						f_pic = follow.user.profile_pic.url
					final.append({
							'player_id':fa.player.id,
							'player_name':fa.player.user.name,
							'player_profile_pic':fa_pic,
							'follow_name':follow.user.name,
							'follow_id':follow.id,
							'follow_profile_pic':f_pic,
							'follower':f_follower,
							'following':f_following,
							'match_played':match_played,
							'follow_gender':follow.user.gender,
							'follow_dob':follow.user.dob,
							'follow_address':follow.user.address,
							'follow_city':follow.user.city,
							'follow_state':follow.user.state,
							'type':'Follow',
							'is_follow':is_follow,
							'date_time':fa.joining_time.strftime("%Y-%m-%d %I:%M %p"),
							"ago":timesince(fa.joining_time).split(' ', 2)[0].replace(',', '')+' ago',
				})

				if fa.activity.activity_type == 'E':
					print('inside Event')
					event = fa.activity.content_object
					going = EventPlayerMap.objects.filter(event = event, going = True)
					going_count = going.count()
					going_list = []
					if going_count >= 2:
						for g in going:
							going_pic = None
							if g.player.user.profile_pic:
								going_pic = g.player.user.profile_pic.url
							going_list.append({
										'going_player_id':g.player.id,
										'going_player_name':g.player.user.name,
										'going_player_profile_pic':going_pic
									})
					event_image = None
					if event.image.image:
						event_image = event.image.image.url
					final.append({
							'event_id':event.id,
							'event_name':event.name,
							'event_image':event_image,
							'going':going_list,
							'going_count':going_count,
							'player_id':fa.player.id,
							'player_name':fa.player.user.name,
							'player_profile_pic':fa_pic,
							'date_time':fa.joining_time.strftime("%Y-%m-%d %I:%M %p"),
							'type':'event',
							"ago":timesince(fa.joining_time).split(' ', 2)[0].replace(',', '')+' ago',
							})
				if fa.activity.activity_type == 'M' and fa.activity.match_type == 'man of the match':
					match = fa.activity.content_object
					player1 = fa.player
					venue = None
					tounament_name = None
					tounament_id = None
#					if not match.venue or not match.tournament_venue:

					if match.tournament_round:
						tounament_name = match.tournament_round.tournament.name
						tournament_id = match.tournament_round.tournament.id
						if match.tournament_venue.court and not match.tournament_venue.is_dummy:
							venue = match.tournament_venue.court.venue.name
						elif match.tournament_venue.is_dummy:
							venue = match.tournament_venue.court_name

					elif match.venue:
						venue = match.venue.name
					player1_pic = None
					if fa.player.user.profile_pic:
						player1_pic = fa.player.user.profile_pic.url
					team1 = Match_Stat_Team.objects.get(match_stat = match, team = match.team1)
					team2 = Match_Stat_Team.objects.get(match_stat = match, team = match.team2)
					final.append({
							'player_id':player1.id,
							'player_profile_pic':player1_pic,
							'player_name':player1.user.name,
							'type':'man_of_the_match',
							'tounament_name':tounament_name,
							'tounament_id':tounament_id,
							'match_id':match.id,
							'match_stat_team1_id':team1.id,
							'match_stat_team2_id':team2.id,
							'venue_name':venue,
							'match_date_time':match.date_time.strftime("%Y-%m-%d %I:%M %p"),
							'date_time':fa.joining_time.strftime("%Y-%m-%d %I:%M %p"),
							"ago":timesince(fa.joining_time).split(' ', 2)[0].replace(',', '')+' ago',

							})
				if fa.activity.activity_type == 'M' and fa.activity.match_type == 'Won a match':
					match = fa.activity.content_object
					player_id = fa.player.id
					player_name = fa.player.user.name
					team1_name = match.team1.name
					team1_id = match.team1.id
					team2_name = match.team2.name
					team2_id = match.team2.id
					winner_name = None
					if match.winner == team1_id:
						winner_name = team1_name
					else:
						winner_name = team2_name
					winner_id = match.winner
					team1_stat = Match_Stat_Team.objects.get(match_stat = match, team__id = team1_id)
					team2_stat = Match_Stat_Team.objects.get(match_stat = match, team__id = team2_id)
					team1_run = team1_stat.total_run
					team1_over = team1_stat.overs
					team1_wicket = team1_stat.wickets
					team1_extra = team1_stat.extras
					team2_run = team2_stat.total_run
					team2_over = team2_stat.overs
					team2_wicket = team2_stat.wickets
					team2_extra = team2_stat.extras
					won = match.won_by
					final.append({
							"player_id":player_id,
							"player_name":player_name,
							"player_profile_pic":fa_pic,
							"type":"match",
							"date_time":fa.joining_time.strftime("%Y-%m-%d %I:%M %p"),
							"team1_name":team1_name,
							"team1_id":team1_id,
							"team2_name":team2_name,
							"team2_id":team2_id,
							"winner_name":winner_name,
							"winner_id":winner_id,
							"match_id":match.id,
							"team1_run":team1_run,
							"team1_over":team1_over,
							"team1_wicket":team1_wicket,
							"team1_extra":team1_extra,
							"team2_run":team2_run,
							"team2_over":team2_over,
							"team2_wicket":team2_wicket,
							"team2_extra":team2_extra,
							"won_by":won,
							"match_stat_team1_id":team1_stat.id,
							"match_stat_team2_id":team2_stat.id,
							"ago":timesince(fa.joining_time).split(' ', 2)[0].replace(',', '')+' ago',
							})

			return Response({'data': final, 'page':paginator.num_pages, 'success':True}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)
