from django.dispatch import receiver
from django.db.models.signals import post_delete
from players.models import Players_Followers, Activity_Storage
import traceback

@receiver(post_delete, sender = Players_Followers)
def deletefollowfeed(sender, instance, 	**kwargs):
	try:
		player = instance.followers
		Activity_Storage.objects.get(object_id = player.id).delete()
	except Exception as e:
		traceback.print_exc()
