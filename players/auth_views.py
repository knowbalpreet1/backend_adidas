from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.utils.timezone import now, timedelta

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.settings import oauth2_settings
from oauthlib.common import generate_token
from oauth2_provider.models import AccessToken, Application, RefreshToken

import json
import random
import requests
import traceback

from django.contrib.auth.models import User as Account

# from .serializers import SportSerializer
# Create your views here.


def get_token_json(access_token):
	"""
	Takes an AccessToken instance as an argument
	and returns a JsonResponse instance from that
	AccessToken
	"""
	token = {
		'access_token': access_token.token,
		'expires_in': oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
		'token_type': 'Bearer',
		'refresh_token': access_token.refresh_token.token,
		'scope': access_token.scope
	}
	return token



def get_access_token(user):
	"""
	Takes a user instance and return an access_token as a JsonResponse
	instance.
	"""
	app = Application.objects.get(user=user)
	token = generate_token()
	# we generate a refresh token
	refresh_token = generate_token()

	expires = now() + timedelta(seconds=oauth2_settings.
	ACCESS_TOKEN_EXPIRE_SECONDS)
	scope = "read write"

	# we create the access token
	access_token = AccessToken.objects.\
		create(user=user,
			   application=app,
			   expires=expires,
			   token=token,
			   scope=scope)

	# we create the refresh token
	RefreshToken.objects.\
		create(user=user,
			   application=app,
			   token=refresh_token,
			   access_token=access_token)
	# we call get_token_json and returns the access token as json
	return get_token_json(access_token)


def send_OTP(mobile_number,instance):

	try:
		print(instance.otp_send_time)
		number = random.randint(100000,999999)
		if instance.otp_send_time:
			time_diff = timezone.now()-instance.otp_send_time
			diff_in_minutes = time_diff.seconds / 60
			if diff_in_minutes > 30:
				
				instance.otp = number
				instance.otp_send_time= timezone.now()
				instance.save()
				return {"otp":number}

			else :
				return {"otp":instance.otp}

		else:
			instance.otp = number
			# print "RANDOM",instance.random
			instance.otp_send_time =timezone.now()
			instance.save()
			types1 = "otp"
			name = instance.name
			email_subject = "OTP Verify mail"
			#send_cel_OTP.delay(mobile_number,number)
			#otp_activate_mail.delay(email_subject,types1,name,instance.random,[instance.email])
			return {'otp':number}
	except Exception as e:
		# print e
		return {'error':str(e)}


class SignUp(APIView):

	def post(self, request):
		try:
			mobile = request.data.get("mobile")
			try:
				obj = Account.objects.get(mobile = mobile)
				otp = send_OTP(mobile, obj)		
				print(otp)
				traceback.print_exc()		
				return Response({'is_new':False,'otp':otp['otp']})
			except ObjectDoesNotExist :
				try:
					obj = Account.objects.create(mobile = mobile)
					otp = send_OTP(mobile, obj)
					print(otp['otp'])
					return Response({'is_new':True, 'otp': otp['otp']})
				except Exception as e:
					traceback.print_exc()
					return Response({'error':e})
		except Exception as e:
			traceback.print_exc()
			return Response({'error':e})

def verify_otp(account, otp):

	# if account.otp_verified :
	# 	return {"message":"already verified",'success':False}
	time_diff = timezone.now()-account.otp_send_time
	diff_in_minutes = time_diff.seconds / 60
	if diff_in_minutes > 30:
		account.otp = None
		account.otp_send_time= None
		account.save()
		return {"sucess":False,"message":"OTP has been expired"}
	print ("1111")
	phone_number = account.mobile
	otp = int(otp)
	print ("222",otp)
	if account.otp == otp:
		print ("True")
		account.is_active=True
		#account.otp = None
		# account.otp_send_time = None
		account.otp_verified = True
		account.save()
		print("after save()")
		return {"success":True, 'message':'OTP Verified'}
	else:
		return {"success":False, 'message':'Invalid OTP'}


class OTPVerify(APIView):

	def post(self, request):
		try:
			otp = request.data.get("otp")
			mobile = request.data.get("mobile")
			try:
				account = Account.objects.get(mobile = mobile)
				result = verify_otp(account, otp)

				print(result)
				if account.fb_id:
					application=Application.objects.get_or_create(user=account)[0]
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
					
					r = {'token':token_info,
						'info':{'id':account.id,
						'client_id':application.client_id, 
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email},
						'result': result}

					return Response(r)
				else :
					return Response(result)

			except Exception as e:
				traceback.print_exc()
				return Response({"error":str(e)})

		except Exception as e:
			traceback.print_exc()
			return Response({"error":str(e)})


class FacebookSignUp(APIView):

	def post(self, request):
		try:
			mobile = request.data.get('mobile')
			access_token = request.data.get('access_token')

			try:
				param = {"access_token": access_token}
				print(param , "param is printing")
				r = requests.get('https://graph.facebook.com/me?fields=id,name,gender,picture.type(large),email,verified',params=param)
				fb_data=r.json()
				print(fb_data)
				idd = fb_data['id']
				user_name = fb_data['name']
				gender = fb_data['gender'][0].upper()
				print(gender)
				name= fb_data['name']
				picture = fb_data['picture']['data']['url']
				email = ''
				try:
					email = fb_data['email']

				except Exception as e:
					traceback.print_exc()
					email = idd +"@facebook.com"

				try:
					account = Account.objects.get(mobile = mobile)
					account.name = name
					account.username = user_name
					account.fb_id = idd
					account.profile_pic = picture
					account.email = email			
					account.gender = gender
					print(len(picture))
					application=Application.objects.get_or_create(user=account)[0]
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
					
					r = {'token':token_info,
						'info':{'id':account.id,
						'client_id':application.client_id, 
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email}}

					if not account.otp_verified:
						print("inside otp verify")
						#send_cel_OTP.delay(account.mobile, account.random)
					account.save()					
					return Response(r)
			
				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e)})

			except Exception as e:
				traceback.print_exc()
				print("inside second last else")
				return Response({'error':e})

		except Exception as e:
			traceback.print_exc()
			return Response({"success":False, "error":str(e)})




class FacebookLogIn(APIView):

	def post(self, request):
		try:
			mobile = request.data.get('mobile')
			access_token = request.data.get('access_token')

			try:
				param = {"access_token": access_token}
				print(param , "param is printing")
				r = requests.get('https://graph.facebook.com/me?fields=id,name,gender,picture.type(large),email,verified',params=param)
				fb_data=r.json()
				print(fb_data)
				idd = fb_data['id']
				
				try:
					account = Account.objects.get(fb_id = idd)
					application=Application.objects.get_or_create(user=account)[0]
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
					
					r = {'token':token_info,
						'info':{'id':account.id,
						'client_id':application.client_id, 
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email}}

					if not account.otp_verified:
						print("inside otp verify")
						#send_cel_OTP.delay(account.mobile, account.random)
					account.save()					
					return Response(r)
			
				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e)})

			except Exception as e:
				traceback.print_exc()
				print("inside second last else")
				return Response({'error':e})

		except Exception as e:
			traceback.print_exc()
			return Response({"success":False, "error":str(e)})



# class SportApi(APIView):


# 	permission_classes = (permissions.IsAdminUser,)
# 	def get(self, request):
# 		sports = Sport.objects.all()
# 		data = SportSerializer(sports, many = True).data
# 		return Response(data)