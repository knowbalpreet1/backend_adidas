from django.db import models
from authentication.models import Account as User
# Create your models here.
from venue.models import Venue
#from tournament.models import Tournament_Detail
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
import reversion

class State(models.Model):
	name = models.CharField(max_length=100, null=True, blank=True)
	state_code = models.CharField(max_length=100, null=True, blank=True)

	def __str__(self):
		if self.name:
			return self.name
		return "-----"

class City(models.Model):
	state = models.ForeignKey(State, null=True, blank=True)
	name = models.CharField(max_length=1000, null=True, blank=True)
	pin = models.CharField(max_length=100, null=True, blank=True)
	location_name = models.CharField(max_length=1000, null = True, blank = True)
	latitude = models.FloatField(blank=True, null= True)
	longitude = models.FloatField(blank=True, null= True)

	def __str__(self):
		if self.name:
			return self.name
		return "-----"
class Location(models.Model):
	city = models.ForeignKey(City, null= True, blank = True)
	name = models.CharField(max_length = 1000, null = True, blank = True)
	latitude = models.FloatField(blank=True, null= True)
	longitude = models.FloatField(blank=True, null= True)

	def __str__(self):
		if self.name:
			return self.name
		return "------"

class SportsType(models.Model):

	name = models.CharField(max_length=100, null=True, blank=True)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		if self.name:
			return self.name
		else:
			return 'none'

class CompetitionType(models.Model):

	sport_type = models.ForeignKey(SportsType, null=True, blank=True)
	name = models.CharField(max_length=100, null=True, blank=True)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		if self.name:
			return self.name
		else:
			return 'none'

class TeamRole(models.Model):
	"""role of a player in a team"""
	
	cricket_role = (
		("Bat", "Batsmen"),
		("Bowl", "Bowler"),
		("All", "Allrounder"),
		("Wicket", "Wicketkeeper")
		)
	role = models.CharField(max_length=10, null=True, blank=True, default=None, choices=cricket_role, help_text="cricket_role")

	def __str__(self):
		return str(self.role)

@reversion.register(ignore_duplicates = True)
class Players(models.Model):
	""" player table """

	user = models.OneToOneField(User)
	is_active = models.BooleanField(default=True)

	# honors_awards = models.ForeignKey(sameersir)

	def __str__(self):
		return str(self.user.name)

@reversion.register(ignore_duplicates = True)
class Team(models.Model):
	"""team of a particular player"""
	sport = models.ForeignKey(SportsType, related_name="sport", null = True)	
	city = models.ForeignKey(City)
	name = models.CharField(max_length=500)
	created_by = models.ForeignKey(Players)
	created_at = models.DateTimeField(null=True, blank=True)
	team_pic = models.ImageField(upload_to = "media",null=True, blank=True)
	is_active =models.BooleanField(default=True)
	description = models.TextField(null=True, blank=True)
	def __str__(self):
		return str(self.name)


class TeamPlayers(models.Model):
	""" players of a prticular Team"""

	team = models.ForeignKey(Team)
	player = models.ForeignKey(Players)
	role = models.ForeignKey(TeamRole, null=True, blank=True)
	is_admin = models.BooleanField(default=False)
	is_captain = models.BooleanField(default=False)
	is_wk = models.BooleanField(default=False)
	added_time = models.DateTimeField(null=True, blank=True)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return str(self.team.name)

	class Meta:
		ordering = ['-id']
class TeamAdmin(models.Model):
	team = models.ForeignKey(Team)
	player = models.ForeignKey(Players)
	is_active = models.BooleanField(default=False)
	
	def __str__(self):
		return self.team.name
class Players_Followers(models.Model):
	""""""
	player = models.ForeignKey(Players, related_name="players")
	followers = models.ForeignKey(Players, related_name="followers_of_the_palyers")

	def __str__(self):
		return str(self.followers.user.name)

# class Tournament_Followers(models.Model):

# 	player = models.ForeignKey(Players)
# 	tournament = models.ForeignKey(Tournament)

# 	def __str__(self):
# 		return str(self.tournament.name)

class Players_Preferred_Sport(models.Model):
	"""docstring for Preferred_Sport"""
	#SPORT_TYPES = (
	#	('cricket', 'Cricket'),
	#	('football', 'Football'),
	#)
	sport_name = models.ForeignKey(SportsType)
	player = models.ForeignKey(Players, null=True, blank=True)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return str(self.sport_name)


class Player_Honors_Awards(models.Model):
	"""This model is a temprary after sometime it will be change"""

	player = models.ForeignKey(Players, null=True, blank=True)
	awards_name = models.CharField(max_length=500,null=True,blank=True)
	date = models.DateField()
	rank = models.CharField(max_length=10, null=True, blank=True)
	is_active = models.BooleanField(default=True)

class Activity_Storage(models.Model):
	"""This model stored player's activities"""

	ACTIVITY_TYPES = (
		('P', 'Played'),
		('J', 'Joined'),
		('F', 'Follow'),
		('R', 'Removed'),
		('E', 'Event'),
		('T', 'Tournament'),
		('M', 'Match')
	)

	activity_type = models.CharField(max_length=1, choices=ACTIVITY_TYPES)
	date = models.DateTimeField(auto_now_add=True)
	logo = models.ImageField(upload_to='media', null=True, blank=True)
	match_type = models.CharField(max_length=500, null=True, blank=True)
	content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name = 'content')
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return str(self.activity_type)

class Player_Activity_Map(models.Model):
	"""This model is map between activity and player"""
	
	player = models.ForeignKey(Players, null=True, blank=True)
	tournament = models.ForeignKey('tournament.Tournament_Detail', null = True, blank = True,  related_name = "tournament")
	activity = models.ForeignKey(Activity_Storage, null=True, blank=True)
	is_active = models.BooleanField(default=True)
	joining_time = models.DateTimeField(null=True, blank=True)

	def __str__(self):
		return str(self.player.user.name)

