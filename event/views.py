from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import IntegrityError
from django.utils import timezone
from django.utils.timezone import now, timedelta
from django.db.models import Q


from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.settings import oauth2_settings
from oauthlib.common import generate_token
from oauth2_provider.models import AccessToken, Application, RefreshToken

import json
import re
import string
import random
import requests
import traceback
import googlemaps
from datetime import datetime
from GoPlayBook.static import *
from math import sin, cos, sqrt, atan2, radians


from .models import Event, Ticket, TicketOption, EventPlayerMap, PhotoTemplate, Conversation, EventSport
from players.models import Players, City, State, SportsType, Location
#from players.views import create_activity_thread
from venue.models import Venue

def distance(lat1, lon1, lat2, lon2):
	R = 6373.0
	print("desttttt")
	lat1 = radians(lat1)
	lon1 = radians(lon1)
	lat2 = radians(lat2)
	lon2 = radians(lon2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1

	a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
	c = 2 * atan2(sqrt(a), sqrt(1 - a))

	distance = R * c

	return distance


def googledistance(lat1, lon1, lat2, lon2):
	try:
		gmaps = googlemaps.Client(key='AIzaSyAQvIJTSpslkx_FvtkcSCx6Zv8ckpJ1hpQ')
		now = datetime.now()
		print((lat1,lon1))
		mylocation = gmaps.reverse_geocode((lat1, lon1))
		print(mylocation[0]['formatted_address'])
		print((lat2,lon2))
		destination = gmaps.reverse_geocode((lat2, lon2))
		#print(destination)
		print(destination[0]['formatted_address'])
		directions_result = gmaps.directions(mylocation[0]['formatted_address'],
							destination[0]['formatted_address'],
							mode="walking",
							departure_time=now)
		#print(directions_result)
		return directions_result[0]['legs'][0]['distance']['text']
	except KeyError:
		traceback.print_exc()
		return 0.0
	except IndexError:
		traceback.print_exc()
		return 0.0
	except Exception as e:
		traceback.print_exc()
		return 0.0

class EventApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def put(self, request):
		data = request.data
		name = data['name']
		is_private = data['is_private']
		contributor = data['contributor']
		player = Players.objects.get(user = request.user)
		contributor.append(player.id)
		start_time = data['start_time']
		end_time = data['end_time']
		description = data['description']
		created_at = datetime.now()
		city = data['city']
		state = data['state']
		location_name = data['location_name']
		lat = data['lat']
		lon = data['lon']
		if lat == lon:
			return Response({'error':'Invalid Latitude ({0}) And Longitude ({1})'.format(lat, lon), 'success':False}, status = 400)
		sport = data['sport']
		start_time = datetime.strptime(start_time, "%Y-%m-%d %I:%M %p")
		end_time = datetime.strptime(end_time, "%Y-%m-%d %I:%M %p")
		start = start_time.strftime("%Y-%m-%d %I:%M %p")
		end = end_time.strftime("%Y-%m-%d %I:%M %p")
		#image = data['image']
		#image_id = data['image_id']
		print(request.data)


		print(created_at, start_time, end_time)
		for key, value in data.items():
			if not data[key]:
				if key != 'is_private':
					return Response({'error':'All Fields Are Mandatory', 'success':False}, status = 400)
			if type(data[key]) == str:
				if data[key].startswith(' ') or len(data[key].replace(" ","")) == 0 :
					return Response({'error':'Provide Valid Data', 'success':False}, status = 400)

		if data['lat'] == 0.0 or data['lon'] == 0.0 or data['lat'] == 0 or data['lon'] == 0:
			return Response({'error':'Invalid Latitude or Longitude','success':False}, status = 400)
		if isinstance(data['city'], int) or isinstance(data['state'], int):
			return Response({'error':'invalid City Or State', 'success':False}, status =400)
		if not re.match("^[a-zA-Z_ ]*$", data['city']) or not re.match("^[a-zA-Z_. ]*$", data['state']):
			return Response({'error':'invalid city or state', 'success':False}, status = 400)
		if end_time <= start_time :
                        return Response({'error':'Invalid Date or Time ', 'success':False }, status = 400)

		if not sport:
			return Response({'error':'plaease select sport', 'success':False}, status = 400)
		if 0 in contributor or 0 in sport:
			return Response({'error':'please provide valid values', 'success':False}, status = 400)
		try:
			state1 = None
			try:
				state1 = State.objects.get_or_create(name = state)[0]
			except MultipleObjectsReturned:
				state1 = State.objects.filter(name = state)[0]
			c1 = None
			try:

				c1 = City.objects.get_or_create(name = city, state = state1)[0]
				loc = Location.objects.get_or_create(name = location_name, latitude = lat, longitude = lon, city = c1)[0]
			except MultipleObjectsReturned:
				c1 = City.objects.filter(name = city,state = state1)[0]
				loc =Location.objects.get_or_create(name = location_name, latitude = lat, longitude = lon, city = c1)[0]

			sportobj = SportsType.objects.filter(id__in = sport)
			if not sportobj or len(sportobj) != len(sport):
				return Response({'error':'Please Provide Correct Sports', 'success':False}, status = 400)
			sport_list = []
			event = Event.objects.get(id = data['event_id'], is_active = True)
			if event.created_at >= start_time:
				return Response({'error':'Invalid Date Time', 'success':False}, status = 400)
			event.name = name
			event.is_private = is_private
			event.start_time = start_time
			event.end_time = end_time
			event.description = description
			event.address = loc
			event.save()
			EventSport.objects.filter(event = event).delete()
			for s in sportobj :
				EventSport.objects.create(event = event, sport = s)
				sport_list.append({'id':s.id,'name':s.name})
				event.save()

			#cid = [c['id'] for c in contributor]


			eventplayer = EventPlayerMap.objects.filter(event = event, is_host = True)
			for ep in eventplayer:
				if ep.player.id not in contributor:
					ep.is_host = False
					ep.save()
					eventplayer = eventplayer.exclude(player__id = ep.player.id)

			con = Players.objects.filter(id__in = contributor)
			if not con or len(con)!=len(contributor):
				return Response({'error':'Please Provide Valid Contributors', 'success':False}, status = 400)
			for c in con:
				ep = EventPlayerMap.objects.get_or_create(event = event, player = c)[0]
				ep.is_host = True
				ep.save()

			con = EventPlayerMap.objects.filter(event = event, is_host = True)
			con_list = []
			for c in con :
				con_list.append({'id':c.player.id, 'name':c.player.user.name })

			result = {
						'id':event.id,
						'name':event.name,
						'is_private':event.is_private,
						'start_time':event.start_time.strftime("%Y-%m-%d %I:%M %p"),
						'end_time':event.end_time.strftime("%Y-%m-%d %I:%M %p"),
						'created_at':event.created_at.strftime("%Y-%m-%d %I:%M %p"),
						'description':event.description,
						'contributors':con_list,
						'city':event.address.city.name,
						'state':event.address.city.state.name,
						'location':event.address.name,
						'sport':sport_list,
						'success':True
					}

			return Response(result, status = 201)
		except ObjectDoesNotExist:
			return Response({'error':'Event Does Not Exist', 'success':False }, status = 400)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e)}, status = 400)


	def post(self, request):

		data = request.data
#		print(data)
		name = data['name']
		is_private = data['is_private']
		contributor = data['contributor']
		player = Players.objects.get(user = request.user)
		contributor.append(player.id)
		start_time = data['start_time']
		end_time = data['end_time']
		description = data['description']
		created_at = datetime.now()
		city = data['city']
		state = data['state']
		location_name = data['location_name']
		lat = data['lat']
		lon = data['lon']
		if lat == lon:
			return Response({'error':'Invalid Latitude ({0}) And Longitude ({1})'.format(lat, lon), 'success':False}, status = 400)
		sport = data['sport']
		#image = data.get('image')
		#image_id = data.get('image_id')
		start_time = datetime.strptime(start_time, "%Y-%m-%d %I:%M %p")
		end_time = datetime.strptime(end_time, "%Y-%m-%d %I:%M %p")
		start = start_time.strftime("%Y-%m-%d %I:%M %p")
		end = end_time.strftime("%Y-%m-%d %I:%M %p")
		created = created_at.strftime("%Y-%m-%d %I:%M %p")
		print(request.data)
		print(created, start, end)
		for key, value in data.items():
			if not data[key]:
				if key != 'is_private':
					return Response({'error':'All Fields Are Mandatory', 'success':False}, status = 400)
			if type(data[key]) == str:
				if data[key].startswith(' ') or len(data[key].replace(" ","")) == 0 :
					return Response({'error':'Provide Valid Data', 'success':False}, status = 400)

		if data['lat'] == 0.0 or data['lon'] == 0.0 or data['lat'] == 0 or data['lon'] == 0:
			return Response({'error':'Invalid Latitude or Longitude','success':False}, status = 400)
		if isinstance(data['city'], int) or isinstance(data['state'], int):
			return Response({'error':'invalid City Or State', 'success':False}, status =400)
		if not re.match("^[a-zA-Z_ ]*$", data['city']) or not re.match("^[a-zA-Z_. ]*$", data['state']):
			return Response({'error':'invalid city or state', 'success':False}, status = 400)
		if created_at >= start_time or end_time <= start_time:
			return Response({'error':'Invalid Date or Time ', 'success':False }, status = 400)

		if not sport:
			return Response({'error':'please select sport', 'success':False}, status = 400)
		if 0 in contributor or 0 in sport:
			return Response({'error':'please provide valid values', 'success':False}, status = 400)
		try:
			state1 = None
			c1 = None
			try:
 				state1 = State.objects.get_or_create(name__istartswith = state)[0]
			except MultipleObjectsReturned:
				state1 = State.objects.filter(name__istartswith = state)[0]
			except Exception as e:
				traceback.print_exc()
			try:
				c1 = City.objects.get_or_create(name__istartswith = city, state = state1)
				loc = Location.objects.get_or_create(name = location_name, latitude = lat, longitude = lon, city = c1[0])[0]
			except MultipleObjectsReturned:
				c1 = City.objects.filter(name = city, state = state1)[0]
				loc = Location.objects.get_or_create(name = location_name, latitude = lat, longitude = lon, city = c1)[0]
			except Exception as e:
				traceback.print_exc()
			sportobj = SportsType.objects.filter(id__in = sport)
			if not sportobj or len(sportobj) != len(sport):
				return Response({'error':'Please Provide Correct Sports', 'success':False}, status = 400)
			sport_list = []
			event = Event(
					name = name,
					is_private = is_private,
					start_time = start_time,
					end_time = end_time,
					created_at = created_at,
					description = description,
					address = loc,
					created_by = player,
					is_active = False )
			event.save()

			for s in sportobj:
				a = EventSport.objects.create(event = event, sport = s)

				sport_list.append({'id':s.id,'name':s.name})


			#cid = [c['id'] for c in contributor]

			con = Players.objects.filter(id__in = contributor)
			if not con or len(con) != len(contributor):
                                return Response({'error':'Please Valid Provide Contributors', 'success':False}, status = 400)

			for c in con:
				mapobject = EventPlayerMap(player = c, event = event, is_host = True)
				mapobject.save()
			con_list = []
			for c in con:
				con_list.append({'id':c.id, 'name':c.user.name })

			result = {
						'id':event.id,
						'name':event.name,
						'is_private':event.is_private,
						'start_time':event.start_time.strftime("%Y-%m-%d %I:%M %p"),
						'end_time':event.end_time.strftime("%Y-%m-%d %I:%M %p"),
						'created_at':created_at.strftime("%Y-%m-%d %I:%M %p"),
						'description':event.description,
						'contributors':con_list,
						'city':event.address.city.name,
						'state':event.address.city.state.name,
						'location':event.address.name,
						'sport':sport_list,
						'success':True
					}

			return Response(result, status = 201)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e)}, status = 400)

	def delete(self, request):
		player = Players.objects.get(user = request.user)
		event_id = request.GET.get('event_id')
		try:
			event = Event.objects.get(id = event_id, is_active = True)
		except ObjectDoesNotExist:
			return Response({'error':'Event Does Not Exist', 'success':False}, status = 400)

		try:
			eventplayer = EventPlayerMap.objects.get(event = event, player= player, is_host = True)
			if eventplayer.event.is_active:
				eventplayer.event.is_active = False
				eventplayer.event.save()
				eventplayer.save()
			else :
				return Response({'success':False, 'message':'Event Does Not Exist'}, status = 400 )
			return Response({'success':True, 'message': 'Event Deleted Successfully' }, status = 200)
		except ObjectDoesNotExist:
			return Response({'error':'Action Not Allowed', 'success':False}, status = 400)
		except Exception as e:
			return Response({'error':str(e), 'success':False}, status = 400)




class TicketOptionApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		options = TicketOption.objects.all()
		option_list = []
		for op in options:
			option_list.append({'id':op.id, 'name':op.name, 'price':op.price, 'quantity': op.quantity})


		return Response({'options':option_list, 'success':True}, status = 200)


	def post(self,request):
		name = request.data.get('name')
		price = request.data.get('price')
		quantity = request.data.get('quantity')
		for key, value in request.data.items():
                        if not request.data[key]:
                                return Response({'error':'All Fields Are Mandatory', 'success':False}, status = 400)
		if quantity <= 0 :
			return Response({'error':'Quantity Cannot Be Zero', 'success':False}, status = 400)

		if isinstance(price, float) or isinstance(price, str):
			return Response({'error':'Please Provide Valid Response', 'success':False}, status = 400)
		try:
			option = TicketOption(name = name, price = price, quantity = quantity)
			option.save()

			return Response({'id':option.id, 'name':option.name, 'price':option.price, 'quantity':option.quantity, 'success':True}, status = 201)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)




class TicketApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		op_id = request.GET.get('id')
		if op_id:
			print('inside if')
			try:
				ticket = Ticket.objects.get(id = op_id)
				return Response({
								'id':ticket.id,
								'name':ticket.option.name,
								'price':ticket.option.price,
								'quantity':ticket.option.quantity,
								'is_external':ticket.is_external,
								'link':ticket.link,
								'event':ticket.event.name,
								'event_id':ticket.event.id,
								'success':True}, status = 200)

			except ObjectDoesNotExist :
				traceback.print_exc()
				return Response({'error':'Ticket Does Not Exist', 'success':False}, status = 400)

			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False}, status = 400)

		print('outside if')
		try:
			ticket = Ticket.objects.all()
			ticket_list = []
			for t in ticket:
				ticket_list.append({
							'id':t.id,
							'name':t.option.name,
							'price':t.option.price,
							'quantity':t.option.quantity,
							'is_external':t.is_external,
							'link':t.link,
							'event':t.event.name,
							'event_id':t.event.id})

			return Response({'ticket':ticket_list, 'success':True}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


	def post(self, request):
		try:
			is_external = request.data.get('is_external')
			link = request.data.get('link')

			if is_external and not link:
				return Response({'error':'Provide External Ticket Link '})



			option = request.data.get('option')
			if is_external and link and option :
				return Response({'error':'Cannot Provide Options When External Source Is Selected'}, status = 400)
			event_id = request.data.get('event_id')

			event = Event.objects.get(id = event_id)
			ticket_list = []
			if option:
				for op in option:
					option = TicketOption.objects.get(id = op)
					ticket = Ticket(is_external = is_external, link = link, event = event, option = option)
					ticket.save()
					ticket_list.append(ticket)
			else:
				ticket = Ticket(is_external = is_external, link = link, event = event, option = option)
				ticket.save()
				ticket_list.append(ticket)

			result = []
			for t in ticket_list:
				option_id = option_name = option_price = option_quantity = None
				if t.option:
					option_id = t.option.id
					option_name = t.option.name
					option_price = t.option.price
					option_quantity = t.option.quantity
				result.append({
						'id':t.id,
						'is_external':t.is_external,
						'link':t.link,
						'event':t.event.name,
						'event_id':t.event.id,
						'option_id':option_id,
						'option_name':option_name,
						'option_price':option_price,
						'option_quantity':option_quantity
							})
			return Response({'result':result, 'success':True}, status = 201)


		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class EventPlayerApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):

		event_id = request.GET.get('event')
		print(event_id, '>>>>>>')
		try:
			event = None
			try:
				event = Event.objects.get(id = event_id, is_active = True)
			except ObjectDoesNotExist:
				traceback.print_exc()
				return Response({'error':'Event Does Not Exist', 'success':False }, status = 400)
			except Exception as e:
				traceback.print_exc()
			eventplayer = None
			print(event)
			if event:
				try:
					current_host = False
					is_going = False
					is_not_going = False
					is_maybe = False
					print('1')
					eventplayer = EventPlayerMap.objects.filter(event = event)
					count = {'going':0,'not_going':0,'maybe':0}
					going = []
					not_going = []
					maybe = []
					host = []
					for ep in eventplayer:
						image = None
						if ep.player.user.profile_pic:
							image = ep.player.user.profile_pic.url
						if ep.going:
							if request.user == ep.player.user:
								is_going = True
							going.append({
											'player_id':ep.player.id,
											'name':ep.player.user.name,
											'profile_pic':image,
											'going':ep.going,
											'not_going':ep.not_going,
											'maybe':ep.maybe,
											'is_host':ep.is_host

							})
							count['going'] += 1


						if ep.not_going:
							if request.user == ep.player.user:
								is_not_going = True
							not_going.append({
											'player_id':ep.player.id,
											'name':ep.player.user.name,
											'profile_pic':image,
											'going':ep.going,
											'not_going':ep.not_going,
											'maybe':ep.maybe,
											'is_host':ep.is_host

							})
							count['not_going'] += 1

						if ep.maybe:
							if request.user == ep.player.user:
								is_maybe = True
							maybe.append({
											'player_id':ep.player.id,
											'name':ep.player.user.name,
											'profile_pic':image,
											'going':ep.going,
											'not_going':ep.not_going,
											'maybe':ep.maybe,
											'is_host':ep.is_host

							})

							count['maybe'] += 1

						if ep.is_host:
							if request.user == ep.player.user:
								current_host = True
							host.append({
											'player_id':ep.player.id,
											'name':ep.player.user.name,
											'profile_pic':image,
											'going':ep.going,
											'not_going':ep.not_going,
											'maybe':ep.maybe,
											'is_host':ep.is_host

							})

					image = None
					if event.image:
						image = event.image.image.url

					epdistance = googledistance(event.address.latitude, event.address.longitude, request.user.latitude, request.user.longitude)
					ticket = Ticket.objects.filter(event = event)
					print(ticket)
					ticket_list = []
					for t in ticket:
						option_id = option_name = option_price = option_quantity = None
						if t.option:
							option_id = t.option.id
							option_name = t.option.name
							option_price = t.option.price
							option_quantity = t.option.quantity
						ticket_list.append({'is_external':t.is_external,'link':t.link,'ticket_id':t.id,'name':option_name,'price':option_price,'quantity':option_quantity})
					sport_list = []
					sport = EventSport.objects.filter(event = event)
					for s in sport:
						sport_list.append({'id':s.sport.id, 'name':s.sport.name})
					result = {
								'event':event.name,
								'event_id':event.id,
								'event_is_private':event.is_private,
								'event_start_time':event.start_time.strftime("%Y-%m-%d %I:%M %p"),
								'event_end_time':event.end_time.strftime("%Y-%m-%d %I:%M %p"),
								'event_created_at':event.created_at.strftime("%Y-%m-%d %I:%M %p"),
								'event_description':event.description,
								'event_image':image,
								'event_address_name':event.address.name,
								'event_address_city':event.address.city.name,
								'event_address_state':event.address.city.state.name,
								'event_lat':event.address.latitude,
								'event_lon':event.address.longitude,
								'distance': epdistance,
								'host':host,
								'going':going,
								'sport':sport_list,
								'not_going':not_going,
								'maybe':maybe,
								'count':count,
								'ticket':ticket_list,
								'is_host':current_host,
								'is_maybe':is_maybe,
								'is_going':is_going,
								'is_not_going':is_not_going,
								'success':True


					}

					return Response(result, status = 200)

				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e)}, status=400)

				else:
					traceback.print_exc()
					return Response({'error':'Please provide correct event', 'success':False}, status = 400)



			else:
				traceback.print_exc()
				return Response({'error':'Please Provide Correct Event', 'success':False}, status =400)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


	def post(self, request):
		try:
			data = request.data
			event_id = data['event_id']
			try:
				event = Event.objects.get(id = event_id)
			except ObjectDoesNotExist:
				return Response({'error':'Event Does Not Exist.', 'success':False}, status = 400)

			try:
				player = Players.objects.get(user = request.user)
			except ObjectDoesNotExist :
				return Response({'error':'Player Does Not Exist.', 'success':False}, status = 400)

			going = data['going']
			maybe = data['maybe']
			not_going = data['not_going']

			if going and maybe:
				return Response({'error':'Select One Going Or Maybe.', 'success':False}, status = 400)
			if going and not_going:
				return Response({'error':'Select One Going Or Not Going.', 'success':False}, status = 400)
			if not_going and maybe:
				return Response({'error':'Select One Not Going Or Maybe.', 'success':False}, status = 400)

			#is_host = data['is_host']

			eventplayer = EventPlayerMap.objects.get_or_create(event = event, player = player)[0]
			eventplayer.going = going
			eventplayer.not_going = not_going
			eventplayer.maybe = maybe
			eventplayer.save()

			result = {
						'event_id':eventplayer.event.id,
						'event_name':eventplayer.event.name,
						'player_id':eventplayer.player.id,
						'player_name':eventplayer.player.user.name,
						'going':eventplayer.going,
						'not_going':eventplayer.not_going,
						'maybe':eventplayer.maybe,
						'is_host':eventplayer.is_host,
						'success':True
			}
			#if going:
			#	create_activity_thread(player = player, content_object = event, activity_type = 'E', match_type = 'event')
			return Response(result, status = 201)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class EventImageApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self,request):
		temp = PhotoTemplate.objects.filter(is_default = True)
		temp_list = []
		for i in temp:
			temp_list.append({'id':i.id, 'image':i.image.url})

		return Response({'Templates':temp_list, 'success':True }, status = 200)


	def post(self,request):
		image = request.data.get('image')
		if len(image) >= MAX_SIZE_TOURNAMENT_LOGO:
			return Response({'error':'Image Size Should Be Less Than 5 MB'})
		image_id = request.data.get('image_id')
		event_id = request.data.get('event_id')
		player = EventPlayerMap.objects.filter(event__id = event_id, is_host = True).values_list('player__user__id', flat = True)
		print(player)
		if request.user.id not in player:
			print(request.user.id)
			return Response({'error':'Not Allowed', 'success':False}, status = 400)



		try:
			if image_id:
				image = PhotoTemplate.objects.get(id = image_id, is_default = True)
				event = Event.objects.get(id = event_id)
				event.image = image
				event.is_active = True
				event.save()

			elif image:
				print(image)
				if not (image.name.endswith(('.jpg','.jpeg','.png', '.JPG','.PNG','.JPEG'))):
					return Response({'error':'Invalid File Type', 'success':False}, status = 400)
				image = PhotoTemplate.objects.create(image = image, is_default = False)
				event = Event.objects.get(id = event_id)
				event.image = image
				event.is_active = True
				event.save()

			else :
				return Response({'error':'Please Provide Valid Input', 'success':False}, status = 400)

			return Response({'message':'Image Uploaded', 'success':True}, status = 201)

		except ObjectDoesNotExist:
			return Response({'error':'please provide correct information', 'success':False}, status = 400)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)




class ConversationApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		try:
			event_id = request.GET.get('event_id')
			print(event_id)
			event = Event.objects.get(id = event_id)
			player_name = EventPlayerMap.objects.filter(event = event).values_list("player__user__id")
			print(player_name)

			convo = Conversation.objects.filter(event = event).order_by('-timestamp')
			convo_list = []
			for c in convo:
				image = None
				if c.player.user.profile_pic:
					image = c.player.user.profile_pic.url

				convo_list.append({
									'id':c.id,
									'content':c.content,
									'player_id':c.player.id,
									'player_name':c.player.user.name,
									'player_image':image,
									'event_id':event.id})


			return Response({'conversation':convo_list, 'success':True}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


	def post(self, request):
		try:
			event_id = request.data.get('event_id')
			event = Event.objects.get(id = event_id)
			content = request.data.get('content')
			player = Players.objects.get(user = request.user)
			if len(content.replace(" ", "")) == 0:
				return Response({'error':'Cannot Post Empty Comment', 'success':False}, status = 400)
			c = Conversation.objects.create(event = event, player =player, content = content)
			image = None
			if c.player.user.profile_pic:
				image = c.player.user.profile_pic.url
			return Response({
							'id':c.id,
							'content':c.content,
							'player_id':c.player.id,
							'player_name':c.player.user.name,
							'player_image':image,
							'event_id':event.id,
							'success':True
				}, status = 201)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class LocationSuggestionApi(APIView):

	def get(self,request):
		q = request.GET.get('q')
		if q:
			try:
				state = City.objects.filter(name__istartswith = q).distinct('name')[:10]

				address = []
				check = []
				for s in state:
					if s.name not in check:
						check.append(s.name)
						address.append({'id':s.id, 'city':s.name,'state':s.state.name})

				return Response({'location':address, 'success':True}, status = 200)
			except Exception as e:
				return Response({'error':str(e), 'success':False }, status = 400)
		else:
			try:
				state = Location.objects.all().order_by('name').distinct('name')[:10]
				address = []
				print(state)
				for s in state:
					if s.city and s.city.state:
						address.append({'location_name':s.name,'latitude':s.latitude, 'longitude':s.longitude,  'city':s.city.name,'state':s.city.state.name})
				return Response({'location':address, 'success':True}, status = 200)
			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False }, status = 400)

from django.core.paginator import Paginator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class EventListApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		try:
			events = Event.objects.filter(is_active = True).order_by("-start_time")
			page = request.GET.get('page')
			lat = request.user.latitude
			lon = request.user.longitude

			e_list = []
			for event in events:
				lat1 = event.address.latitude
				lon1 = event.address.longitude
				dis = distance(lat, lon, lat1, lon1)

				if dis <= 30:
					ep = EventPlayerMap.objects.filter(event = event, going = True)
					going_list = []
					print(event.name)
					for e in ep:
						image = None
						if e.player.user.profile_pic:
							image = e.player.user.profile_pic.url
						going_list.append({
								'player_id':e.player.id,
								'player_name':e.player.user.name,
								'profile_pic':image,
						})
					image = None
					if event.image and event.image.image:
						image = event.image.image.url
					e_list.append({
							'event_id':event.id,
							'name':event.name,
							'image':image,
							'start_time':event.start_time.strftime("%Y-%m-%d %I:%M %p"),
							'end_time':event.end_time.strftime("%Y-%m-%d %I:%M %p"),
							'location_name':event.address.name,
							'city':event.address.city.name,
							'state':event.address.city.state.name,
							'going':going_list,
							'going_count':ep.count(),
							})
			paginator = Paginator(e_list, 10)
			try:
				data = paginator.page(page)
			except PageNotAnInteger:
				data = paginator.page(1)
			except EmptyPage:
				data = paginator.page(paginator.num_pages)
			print(data)
			print(paginator.num_pages)
			return Response({'event':data.object_list,'last_page':paginator.num_pages, 'success':True}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


class UserEventApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def get(self, request):
		try:
			user = request.user
			#print(user.name)
			time = request.GET.get('time')
			page = request.GET.get('page')
			today = datetime.now()
			#print('today>>>',today,'time>>>', time,'page>>', page)
			player = Players.objects.get(user = user, is_active = True)
			print(player)
			event = None
			data = []
			if time == 'P':
				event = EventPlayerMap.objects.filter(player = player, is_host = True,event__start_time__lt = today, event__end_time__lt = today, event__is_active = True).order_by("-event__start_time")

			elif time == 'F':
				event = EventPlayerMap.objects.filter(player = player, is_host = True,  event__end_time__gt  = today, event__is_active = True).order_by("-event__start_time")

				#print()
			going_list = []
			e_list = []
			if event:

				for e in event:
					going_list = []
					ep  = EventPlayerMap.objects.filter(event = e.event, going = True)
					#print(ep, '>>>>>', e, '>>>>', ep.count())
					for p in ep:
						image = None
						if p.player.user.profile_pic:
						        image = p.player.user.profile_pic.url
						going_list.append({
					                'player_id':p.player.id,
					                'player_name':p.player.user.name,
					                'profile_pic':image,
						})

					image = None
					if e.event.image and e.event.image.image:
						image = e.event.image.image.url
					e_list.append({
				                'event_id':e.event.id,
				                'name':e.event.name,
				                'image':image,
				                'start_time':e.event.start_time.strftime("%Y-%m-%d %I:%M %p"),
				                'end_time':e.event.end_time.strftime("%Y-%m-%d %I:%M %p"),
				                'location_name':e.event.address.name,
				                'city':e.event.address.city.name,
				                'state':e.event.address.city.state.name,
				                'going':going_list,
				                'going_count':ep.count(),
				                })

				paginator = Paginator(e_list, 10)
				try:
					data = paginator.page(page)
					data = data.object_list
				except PageNotAnInteger:
					data = paginator.page(1)
					data = data.object_list
				except EmptyPage:
					data = paginator.page(paginator.num_pages)
					data = data.object_list
				except Exception as e:
					traceback.print_exc()
					data = []
				return Response({'event':data,'last_page':paginator.num_pages, 'success':True}, status = 200)
			else:
				return Response({'event':[], 'last_page':1, 'success':True}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)
