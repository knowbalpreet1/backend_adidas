from django.conf.urls import url, include
from .views import *

urlpatterns = [
    url(r'^$', EventApi.as_view(), name = 'eventapi' ),
    # url(r'^playersearch/', PlayerSearchApi.as_view(), name = 'playersearchapi'),
    url(r'^ticketoption/', TicketOptionApi.as_view(), name = 'ticketoptionapi'),
    url(r'^ticket/', TicketApi.as_view(), name = 'ticketapi'),
    url(r'^eventplayer/', EventPlayerApi.as_view(), name = 'eventplayerapi'),
    url(r'^eventimage/', EventImageApi.as_view(), name = 'eventimageuploadapi'),
    url('^conversation/', ConversationApi.as_view(), name = 'conversation'),
    url('^locationsearch/', LocationSuggestionApi.as_view(), name ='locationsearch' ),
    url('^eventlist/', EventListApi.as_view(), name = 'eventlist'),
    url('^userevent/', UserEventApi.as_view(), name = 'userevent'),    
]
