from django.db import models
from django.utils import timezone
import datetime
from rest_framework.response import Response
import django
from players.models import City, Players, SportsType, Location
from geopy import units, distance

class geoManager(models.Manager):
	def nearby(self, latitude, longitude, proximity):
		rough_distance = units.degrees(arcminutes=units.nautical(kilometers=proximity)) * 2
		queryset = self.get_queryset().filter(
			address__latitude__range = (latitude - rough_distance, latitude + rough_distance),
			address__longitude__range =(longitude - rough_distance, longitude + rough_distance) ,
			is_active=True
			)

		locations = []
		for obj in queryset:
			if obj.address.latitude and obj.address.longitude:
				exact_distance = distance.distance(
					(latitude, longitude),
					(obj.address.latitude, obj.address.longitude)
				).kilometers

				if exact_distance <= proximity:
					locations.append(obj)
		queryset = queryset.filter(id__in=[l.id for l in locations])
		return queryset

# Create your models here.
class PhotoTemplate(models.Model):
	image = models.ImageField()
	is_default = models.BooleanField(default = True)
	def __str__(self):
		return (self.image.url).split('/')[-1]


class Event(models.Model):
	name = models.CharField(max_length = 50)
	is_private = models.BooleanField(default = True)
	start_time= models.DateTimeField(null = True, blank = True)
	end_time = models.DateTimeField(null = True, blank = True)
	created_at = models.DateTimeField(null = True, blank = True)
	description = models.TextField()
	image = models.ForeignKey(PhotoTemplate, blank = True, null = True)
	address = models.ForeignKey(Location)
	#sport = models.ForeignKey(SportsType, null = True)
	created_by = models.ForeignKey(Players, null = True, blank = True)
	is_active = models.BooleanField(default = True)
	gis_manager= geoManager()
	objects = models.Manager()
	
	def __init__(self, *args, **kwargs):
		super(Event, self).__init__(*args, **kwargs)
		self.__is_active = self.is_active
		self._is_active = self.is_active

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

class EventSport(models.Model):
	sport = models.ForeignKey(SportsType, null = True)
	event = models.ForeignKey(Event, null = True)
	
	def __str__(self):
		return self.sport.name + '  ' + self.event.name




class TicketOption(models.Model):
	name = models.CharField(max_length = 50)
	price = models.FloatField()
	quantity = models.IntegerField(default = 0)

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name


class Ticket(models.Model):
	is_external = models.BooleanField(default = False)
	link = models.URLField(max_length = 500, null = True, blank = True)
	option = models.ForeignKey(TicketOption, null = True, blank = True)
	event = models.ForeignKey(Event, null = True, blank = True)

	# def save(self, *args, **kwargs):
	# 	if self.is_external or self.link:
	# 		print(self.link)
	# 		return Response({"error":"please provide a valid link"}, status = 400)
	# 	else:
	# 		super(Ticket, self).save(*args, **kwargs) # Call the "real" save() method.


	def __str__(self):
		if self.is_external:
			return self.event.name + '  External Ticket  '
		return self.event.name + '  GoPlayBook Ticket  ' + self.option.name

	def __unicode__(self):
		if self.is_external:
			return self.event.name + 'External Ticket' + self.option.name
		return self.event.name + 'GoPlayBook Ticket' + self.option.name

	




class EventPlayerMap(models.Model):
	event = models.ForeignKey(Event)
	player = models.ForeignKey(Players)
	going = models.BooleanField(default = False)
	not_going = models.BooleanField(default = False)
	maybe = models.BooleanField(default = False)
	is_host = models.BooleanField(default = False)

	def __init__(self, *args, **kwargs):
		super(EventPlayerMap, self).__init__(*args, **kwargs)
		self.__is_host = self.is_host
		self._is_host = self.is_host


	def __str__(self):
		return self.player.user.name + '-' + self.event.name

	def __unicode__(self):
		return self.player.user.name + '-' + self.event.name



class Conversation(models.Model):
	content = models.CharField(max_length = 500)
	event = models.ForeignKey(Event)
	player = models.ForeignKey(Players)
	timestamp = models.DateTimeField(default = django.utils.timezone.now)

	def __str__(self):
		return self.player.user.name + ' Commented ' + self.content + ' On ' + self.event.name

	def __unicode__(self):
		return self.player.user.name + ' Commented ' + self.content + ' On ' + self.event.name
