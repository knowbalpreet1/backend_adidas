from django.contrib.auth.models import User

from django.db.models.signals import post_save, pre_delete
from django.core.signals import  request_started
from django.conf import settings
from django.db.backends.signals import connection_created
from django.dispatch import receiver

from notification.models import BusinessNotification, UserNotification
from event.models import Event, EventPlayerMap, Conversation
from players.models import Players, City, Players_Followers
from players.views import create_activity_thread
from notification.views import send_notification, save_notification
import traceback

loop = settings.LOOP
#@receiver(connection_created)
#def abc(sender , connection, **kwargs):
#	print(connection.get_connection_params(), connection.ensure_timezone(), connection.init_connection_state(), connection.check_constraints(), connection.chunked_cursor(), connection.init_connection_state())
#	cur = connection.chunked_cursor()
#	print(cur)
#	a = cur.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public'")
#	print(a)




def getfollowers(player):

	try:
		followers = Players_Followers.objects.filter(player = player)
		follow_list = [follower.followers for follower in followers]
		return follow_list
	except Exception as e:
		traceback.print_exc()
		return []

def getgoing(event):
	try:
		going = EventPlayerMap.objects.filter(event = event, going = True, player__user__city__icontains = event.address.city.name)
		print('going>>>>>>>>>>>>>>>>>>>>>>>>>', going)
		return going

	except Exception as e:
		traceback.print_exc()
		return None


@receiver(post_save, sender = Event)
def Eventnotify(sender, instance, created, **kwargs):
	try:
		if not instance._is_active and instance.is_active:
			city = instance.address.city
			print(city, '-----------------------')
			players = Players_Followers.objects.filter(followers__user__city__icontains = city.name, player = instance.created_by)
			print(players, '----------------------')
			p_list = [player.followers for player in players]
			pic = None
			if instance.created_by.user.profile_pic:
				pic = instance.created_by.user.profile_pic.url
			message_body = {"title":"GoPlayBook", "body":'{0} is hosting the event {1} happening near you. Check it out now!'.format(instance.created_by.user.name, instance.name), "profile_pic":pic, 'action':"event_host", 'event_id':instance.id}
			send_notification((p_list, message_body))
			for p in p_list:
				save_notification(player = p, instance = instance.created_by, notification_type = 'E', message = '<b>{0}</b> is hosting the event <b>{1}</b> happening near you. Check it out now!'.format(instance.created_by.user.name, instance.name), activity_id = instance.id)
				# BusinessNotification.objects.create(player = p, message = '<b>{0}</b> is hosting the event <b>{1}</b> happening near you. Check it out now!'.format(instance.created_by.user.name, instance.name))
			going = getgoing(instance)
			if going:
				count = going.count()
				for g in going:
					message_body = {"title":"GoPlayBook", "body":'Hey {0}, {1} people are going to {2} hapenning near you. Join the party!'.format(g.player.user.name, count, instance.name), "profile_pic":pic, 'action':"event_host", 'event_id':instance.id}
#					loop.run_in_executor(None, send_notification,[g, message_body])
					send_notification((g, message_body ))
					save_notification(player = g.player, instance = instance.event.created_by, notification_type = 'E', message = 'Hey <b>{0}</b>, <b>{1}</b> people are going to <b>{2}</b> hapenning near you. Join the party!'.format(g.player.user.name, count, instance.name), activity_id = instance.id)
					# BusinessNotification.objects.create(player = g.player, message = 'Hey <b>{0}</b>, <b>{1}</b> people are going to <b>{2}</b> hapenning near you. Join the party!'.format(g.player.user.name, count, instance.name))
			eps = EventPlayerMap.objects.filter(event = instance, is_host = True).exclude(player = instance.created_by)
			
			for ep in eps:
				message_body = {"title":"GoPlayBook", "body":'{0} has added you as a contributor for the "{1}"'.format(ep.event.created_by.user.name, ep.event), "profile_pic":pic, 'action':'event_contributor', 'event_id':ep.event.id}
				send_notification(([ep.player],message_body))
				save_notification(player = ep.player, instance = instance.created_by, notification_type = 'E', message = '<b>{0}</b> has added you as a contributor for the <b>"{1}"</b>'.format(ep.event.created_by.user.name, ep.event.name), activity_id = instance.id)
			
	except Exception as e:
		traceback.print_exc()
		pass


@receiver(post_save, sender = EventPlayerMap)
def playernotify(sender, instance, created, **kwargs):
	try:
		print(instance, '>>>>>>>>>>>>>>>')
		if instance.player != instance.event.created_by and not instance._is_host and instance.is_host:
			pic = None
			if instance.event.created_by.user.profile_pic:
				pic = instance.event.created_by.user.profile_pic.url
			message_body = {"title":"GoPlayBook", "body":'{0} has added you as a contributor for the "{1}"'.format(instance.event.created_by.user.name, instance.event), "profile_pic":pic, 'action':'event_contributor', 'event_id':instance.event.id}
			send_notification(([instance.player],message_body))
			BusinessNotification.objects.create(player = instance.player, message = '<b>{0}</b> has added you as a contributor for the "<b>{1}</b>"'.format(instance.event.created_by.user.name, instance.event), activity_id = instance.event.id)
		if instance.going:
			create_activity_thread(instance.player, instance.event, activity_type = 'E', match_type = 'going')
	except Exception as e:
		traceback.print_exc()
		print(e)
		pass


@receiver(post_save, sender = Conversation)
def commentnotify(sender, instance ,created, **kwargs):

	try:
		if created:
			pic = None
			if instance.player.user.profile_pic:
				pic = instance.player.user.profile_pic.url
			print(instance,'>>>>>>>>>>>' )
			message_body = {"title":"GoPlayBook", "body":'{0} commented on your event : "{1}"'.format(instance.player.user.name, instance.content), "profile_pic":pic, 'action':'event_comment', 'event_id':instance.event.id}
			players = [c.player for c in EventPlayerMap.objects.filter(event = instance.event, is_host = True).exclude(player = instance.player)]
			send_notification((players, message_body))
			# BusinessNotification.objects.create(player = instance.event.created_by, message = '<b>{0}</b> commented on your event : "{1}"'.format(instance.player.user.name, instance.content))
			if instance.player.id != instance.event.created_by.id:
				save_notification(player = instance.event.created_by, instance = instance.player, notification_type = 'E', message = '<b>{0}</b> commented on your event : "{1}"'.format(instance.player.user.name, instance.content), activity_id = instance.event.id)
	except Exception as e:
		traceback.print_exc()
		print(e)
		pass

from players.models import Activity_Storage
@receiver(pre_delete, sender = Event)
def EventPreDelete(sender, instance , **kwargs):
	try:
		Activity_Storage.objects.get(object_id = instance.id).delete()
		UserNotification.objects.get(object_id = instance.id).delete()
	except Exception as e:
		pass
	
