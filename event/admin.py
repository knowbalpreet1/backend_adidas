from django.contrib import admin
from .models import Event, Ticket, EventPlayerMap, TicketOption, PhotoTemplate, Conversation, EventSport
# Register your models here.

admin.site.register(Event)
admin.site.register(Ticket)
admin.site.register(EventSport)
admin.site.register(Conversation)
admin.site.register(PhotoTemplate)
admin.site.register(TicketOption)
admin.site.register(EventPlayerMap)
