from django.db import models
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from geopy import units, distance
from datetime import datetime

from oauth2_provider.models import AccessToken, Application, RefreshToken

class geoManager(models.Manager):
    def nearby(self, latitude, longitude, proximity):
        rough_distance = units.degrees(arcminutes=units.nautical(kilometers=proximity)) * 2
        queryset = self.get_queryset().filter(
            latitude__range = (latitude - rough_distance, latitude + rough_distance),
            longitude__range =(longitude - rough_distance, longitude + rough_distance) ,
            is_active=True
            )

        locations = []
        for obj in queryset:
            if obj.latitude and obj.longitude:
                exact_distance = distance.distance(
                    (latitude, longitude),
                    (obj.latitude, obj.longitude)
                ).kilometers

                if exact_distance <= proximity:
                    locations.append(obj)
        queryset = queryset.filter(id__in=[l.id for l in locations])
        return queryset

class AccountManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        #email = self.normalize_email(email)
        username = self.model.normalize_username(username)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault("is_active", True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, password, **extra_fields)



class Account(AbstractBaseUser,PermissionsMixin):
    """This model is maintain user information and if client signup through google or facebook then maintain it."""
    GENDER = (
            ('M', 'MALE'),
            ('F', 'FEMALE'),
            ('N', 'NEUTER'),
    )
    username = models.CharField(max_length=150, unique=True, null = True)
    name = models.CharField(max_length=100, blank=True, null = True)
    #first_name = models.CharField(_('first name'), max_length=30, blank=True)
    #last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(blank=True, null = True)
    mobile = models.CharField(max_length=15, unique=True,null=True)
    dob = models.DateField(null = True, blank = True)
    otp = models.IntegerField(default=False, null =True)
    otp_send_time = models.DateTimeField(null=True, blank= True)
    fb_id = models.CharField(max_length=500,blank=True,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    otp_verified=models.BooleanField(default=False)
    profile_pic= models.ImageField(upload_to='media',blank= True, null= True, max_length = 500)
    gender=models.CharField(max_length=7,choices=GENDER,null=True,blank=True)
    address = models.CharField(max_length=300,blank=True,null=True)
    brand_name = models.CharField(max_length=300,blank=True,null=True)
    city = models.CharField(max_length=100, blank = True)
    state = models.CharField(max_length=100, null = True)
    latitude = models.FloatField(blank=True, null= True)
    longitude = models.FloatField(blank=True, null= True)
    is_staff = models.BooleanField(default = False)
    is_superuser = models.BooleanField(default = False)
    is_active = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    # default_password = models.CharField(max_length = 100, null = True, blank = True)
    # password_sent_time = models.DateTimeField(null = True, blank = True)
    # INTEREST_CHOICE = (
    #         ('V', 'Venue'),
    #         ('T', 'Tournament'),
    #         ('B', 'Both')
    #     )
    # intrested_in = models.CharField(choices = INTEREST_CHOICE, max_length = 12, null = True, blank = True)
    objects = AccountManager()
    gis_manager= geoManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['mobile']

    # def __init__(self, *args, **kwargs):
    #     super(Account, self).__init__(*args, **kwargs)
    #     self.__is_active = self.is_active


    # def save(self, *args, **kwargs):
    #     print(self.__is_active, '........')
    #     if not self.__is_active and self.is_active:
    #         #self.set_password('{0}{1}'.format(self.name, self.mobile))
    #         # self.default_password = '{0}{1}'.format(self.name, self.mobile)
    #         self.password_sent_time = datetime.now()
    #     super(Account, self).save(*args, **kwargs)

    def get_full_name(self):
        return str(self.mobile)
    def get_short_name(self):
        return self.name
    class Meta:
        app_label = 'authentication'

    def __str__(self):
        return str(self.username) or u''
