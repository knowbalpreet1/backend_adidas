from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import IntegrityError
from django.utils import timezone
from django.utils.timezone import now, timedelta

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.settings import oauth2_settings
from oauthlib.common import generate_token
from oauth2_provider.models import AccessToken, Application, RefreshToken
from players.models import Players, SportsType, Players_Preferred_Sport, City, State, Location
import json
import random
import requests
import traceback


from .async_task import send_otp
from .models  import Account
# from .serializers import SportSerializer
# Create your views here.


def get_token_json(access_token):
	"""
	Takes an AccessToken instance as an argument
	and returns a JsonResponse instance from that
	AccessToken
	"""
	token = {
		'access_token': access_token.token,
		'expires_in': oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
		'token_type': 'Bearer',
		'refresh_token': access_token.refresh_token.token,
		'scope': access_token.scope
	}
	return token



def get_access_token(user):
	"""
	Takes a user instance and return an access_token as a JsonResponse
	instance.
	"""
	app = Application.objects.get(user=user)
	token = generate_token()
	# we generate a refresh token
	refresh_token = generate_token()

	expires = now() + timedelta(seconds=oauth2_settings.
	ACCESS_TOKEN_EXPIRE_SECONDS)
	scope = "read write"

	# we create the access token
	access_token = AccessToken.objects.\
		create(user=user,
			   application=app,
			   expires=expires,
			   token=token,
			   scope=scope)

	# we create the refresh token
	RefreshToken.objects.\
		create(user=user,
			   application=app,
			   token=refresh_token,
			   access_token=access_token)
	# we call get_token_json and returns the access token as json
	return get_token_json(access_token)


def send_OTP(mobile_number,instance):
	try:
		print(instance.otp_send_time)
		number = random.randint(1000,9999)
		if instance.otp_send_time:
			time_diff = timezone.now()-instance.otp_send_time
			diff_in_minutes = time_diff.seconds / 60
			if diff_in_minutes > 30:

				instance.otp = number
				instance.otp_send_time= timezone.now()
				instance.save()
				return {"otp":number}

			else :
				instance.otp = number
				instance.otp_send_time = timezone.now()
				instance.save()
				return {"otp":instance.otp}

		else:
			instance.otp = number
			# print "RANDOM",instance.random
			instance.otp_send_time =timezone.now()
			instance.save()
			types1 = "otp"
			name = instance.name
			email_subject = "OTP Verify mail"
			#send_cel_OTP.delay(mobile_number,number)
			#otp_activate_mail.delay(email_subject,types1,name,instance.random,[instance.email])
			return {'otp':number}
	except Exception as e:
		# print e
		return {'error':str(e)}


class SignUp(APIView):

	def post(self, request):
		try:
			mobile = request.data.get("mobile")
			brand_name = request.data.get("brand_name", 'goplaybook')

			if type(mobile) == str:
				if not mobile.isnumeric():
					return Response({'error':'Mobile Number Format Not Supported', 'success':False},status=400)
				if len(mobile)<10 or len(mobile) >10:
					return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False},status=400)

			if len(str(mobile)) < 10 or len(str(mobile)) > 10 :
				return Response({'error':'Mobile Number Must Be Of 10 Digits','success':False},status=400)
			print(mobile, brand_name)
			try:
				print("1")
				obj = Account.objects.get(mobile = mobile)
				print('Obj', obj)
				obj.set_password("mobile")
				obj.save()
				print("2")
				otp = send_OTP(mobile, obj)
				print(otp)
				send_otp(mobile, "Your OTP is {0}. Thank you for downloading Go Playbook. Let's Play!".format(otp['otp']))
				#send_otp(obj.mobile, otp['otp'],)
				traceback.print_exc()
				print("inside SignUp First try::=========")
				return Response({'is_new':False,'otp':otp['otp'],'success':True},status = 201)
			except ObjectDoesNotExist :
				print("inside except")
				try:
					print("inside try except")
					obj = Account.objects.create(mobile = mobile, username =str(mobile), is_active = False, brand_name = brand_name)
					player = Players.objects.create(user = obj, is_active = False)
					otp = send_OTP(mobile, obj)
					send_otp(mobile, "Your OTP is {0}. Thank you for downloading Go Playbook. Let's Play!".format(otp['otp']))
					print(otp['otp'])
					return Response({'is_new':True, 'otp': otp['otp'], 'success':True}, status = 201)
				except Exception as e:
					traceback.print_exc()
					return Response({'error':e,'success':False}, status = 400)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e),'success':False}, status = 400)

def verify_otp(account, otp):
	if account.mobile == '9540736797':
		if otp == '1234':
			account.is_active = True
			account.otp = None
			account.otp_send_time = None
			account.otp_verified = True
			account.save()
			return {'success':True, 'message':'OTP verified'}
		else:
			return {'success':False, 'message':'Invalid OTP'}
	# if account.otp_verified :
	# 	return {"message":"already verified",'success':False}
	time_diff = timezone.now()-account.otp_send_time
	diff_in_minutes = time_diff.seconds / 60
	if diff_in_minutes > 30:
		account.otp = None
		account.otp_send_time= None
		account.save()
		return {"success":False,"message":"OTP has been expired"}
	print ("1111")
	phone_number = account.mobile
	otp = int(otp)
	print ("222",otp)
	if account.otp == otp:
		print ("True")
		account.is_active=True
		account.otp = None
		account.otp_send_time = None
		account.otp_verified = True
		account.save()
		print("after save()")
		return {"success":True, 'message':'OTP Verified'}
	else:
		return {"success":False, 'message':'Invalid OTP'}


class OTPVerify(APIView):

	def post(self, request):
		try:
			otp = request.data.get("otp")
			mobile = request.data.get("mobile")
			if type(mobile) == str:
				if not mobile.isnumeric():
					return Response({'error':'Mobile Number Format Not Supported','success':False}, status=400)
				if len(mobile) < 10 or len(mobile) > 10:
                                        return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False},status=400)
			if type(otp) == str:
                                if not mobile.isnumeric():
                                        return Response({'error':'OTp Not Supported','success':False}, status=400)
                                if len(otp) < 4 or len(otp) > 4:
                                        return Response({'error':'OTP Must Be Of 4 Digits', 'success':False},status=400)

			if len(str(otp)) < 4 or len(str(otp)) > 4 :
                                return Response({'error':'OTP Must Be Of 4 Digits', 'success':False}, status = 400)


			if len(str(mobile)) < 10 or len(str(mobile)) > 10 :
                                return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False}, status = 400)
			try:
				account = Account.objects.get(mobile = mobile)
				result = verify_otp(account, otp)

				print(result)
				print(1)
				if (account.fb_id or account.email) and result['success']:
					print(2)
					application=Application.objects.get_or_create(user=account,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)[0]
					player = Players.objects.get(user = account)
					player = Players.objects.get(user = account)
					is_sport_selected = False
					p = Players_Preferred_Sport.objects.filter(player = player)
					if p:
						is_sport_selected = True
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					print(token_info)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
					p = Players.objects.get(user = account)
					r = {'token':token_info,
						'info':{'id':account.id,
						'player_id':p.id,
						'client_id':application.client_id,
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email,
						'is_sport_selected':is_sport_selected},
						'result': result}

					return Response(r, status = 201)
				elif result['success'] :
					account.is_active = True
					account.save()
					traceback.print_exc()
					print(result)
					return Response(result, status = 201)
				else :
					print(result)
					return Response(result, status = 400)

			except ObjectDoesNotExist:
				traceback.print_exc()
				return Response({"error":"Mobile No Is Not Registered", "is_exist":False,'success':False})
			except Exception as e:
				traceback.print_exc()
				return Response({"error":str(e), 'success':False}, status = 400)

		except Exception as e:
			traceback.print_exc()
			return Response({"error":str(e), 'success':False}, status = 400)

from django.core.files.base import ContentFile

class FacebookSignUp(APIView):

	def post(self, request):
		try:
			mobile = request.data.get('mobile')
			lat = request.data.get('lat')
			lon = request.data.get('lon')
			city = request.data.get('city')
			state = request.data.get('state')
			address = request.data.get('address')
			print(type(city), type(state))
			access_token = request.data.get('access_token')
			print(request.data)
			if type(city) != str or type(state) != str:
				return Response({'error':'Invalid City Or State', 'success':False}, status = 400)

			if type(mobile) == str:
				if not mobile.isnumeric():
					return Response({'error':'Number Must be Integer', 'success':False}, status = 400)
				if len(mobile)<10 or len(mobile) >10:
                                        return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False},status = 400)


			if len(str(mobile)) < 10 or len(str(mobile)) > 10 :
                                return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False}, status = 400)
			try:
				param = {"access_token": access_token}
				print(param , "param is printing")
				r = requests.get('https://graph.facebook.com/me?fields=id,name,gender,picture.type(large),email,verified',params=param)
				fb_data=r.json()
				print(fb_data)
				idd = fb_data['id']
				try:
					a = Account.objects.get(fb_id = idd)
					return Response({'error':'User Already Exist', 'success':False}, status = 400)
				except ObjectDoesNotExist:
					pass
				except MultipleObjectsReturned:
					return Response({'error':'User Already Exist','success':False}, status = 400)
				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e), 'success':False}, status = 400)
				user_name = fb_data['name']
				gender = fb_data['gender'][0].upper()
				print(gender)
				name= fb_data['name']
				picture = None
				# picture = fb_data['picture']['data']['url']
				try:
					image_content = ContentFile(requests.get(fb_data['picture']['data']['url']).content)
					picture = image_content
				except Exception as e:
					traceback.print_exc()
					pass
				email = ''
				try:
					email = fb_data['email']

				except Exception as e:
					traceback.print_exc()
					email = idd +"@facebook.com"

				try:
					account = Account.objects.get(mobile = mobile)
					if account.fb_id:
						return Response({'error':'User Already Exists', 'success':False}, status=400)
					account.name = name
					account.username = account.mobile
					account.fb_id = idd
					account.latitude = lat
					account.longitude = lon
					account.address = address
					account.city = city
					s = None
					try:
						s =  State.objects.get_or_create(name = state)[0]
						c = City.objects.get_or_create( state = s, name = city)[0]
						l = Location.objects.get_or_create(name = address)[0]
						l.latitude = lat
						l.longitude = lon
						l.city = c
						l.save()
					except Exception as e:
						traceback.print_exc()
					account.state = state
					account.profile_pic.save(str(idd)+'.jpg', picture)
					if not account.email:
						account.email = email
					account.is_active = True
					account.gender = gender
					print(len(picture))
					application=Application.objects.get_or_create(user=account,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)[0]
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
					p = Players.objects.get(user=account)
					p.is_active = True
					p.save()
					r = {'token':token_info,
						'info':{'id':account.id,
						'player_id':p.id,
						'client_id':application.client_id,
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email,'is_sport_selected':False}, 'success':True}

					if not account.otp_verified:
						print("inside otp verify")
						#send_cel_OTP.delay(account.mobile, account.random)
					account.save()

					return Response(r, status = 201)

				except IntegrityError:
					traceback.print_exc()
					return Response({'error':'User Already Exist.', 'success':False}, status = 400)

				except ObjectDoesNotExist:
					traceback.print_exc()
					return Response({'error':'User Does Not Exist.', 'success':False}, status = 400)


				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e), 'success':False}, status = 400)

			except Exception as e:
				traceback.print_exc()
				print("inside second last else")
				return Response({'error':str(e), 'success':False}, status = 400)

		except Exception as e:
			traceback.print_exc()
			return Response({"success":False, "error":str(e)}, status = 400)


class EmailSignUp(APIView):

	def post(self, request):
		try:
			mobile = request.data.get('mobile')
			lat = request.data.get('lat')
			lon = request.data.get('lon')
			city = request.data.get('city')
			email = request.data.get('email')
			name = request.data.get('first_name') + " " + request.data.get("last_name") 
			state = request.data.get('state')
			address = request.data.get('address')

			if type(city) != str or type(state) != str:
				return Response({'error':'Invalid City Or State', 'success':False}, status = 400)

			if type(mobile) == str:
				if not mobile.isnumeric():
					return Response({'error':'Number Must be Integer', 'success':False}, status = 400)
				if len(mobile)<10 or len(mobile) >10:
                                        return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False},status = 400)


			if len(str(mobile)) < 10 or len(str(mobile)) > 10 :
                                return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False}, status = 400)
			try:
				#param = {"access_token": access_token}
				#print(param , "param is printing")
				#r = requests.get('https://graph.facebook.com/me?fields=id,name,gender,picture.type(large),email,verified',params=param)
				#fb_data=r.json()
				#print(fb_data)
				#idd = fb_data['id']
				try:
					a = Account.objects.get(email = email)
					return Response({'error':'User Already Exist', 'success':False}, status = 400)
				except ObjectDoesNotExist:
					pass
				except MultipleObjectsReturned:
					return Response({'error':'User Already Exist','success':False}, status = 400)
				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e), 'success':False}, status = 400)
				#user_name = fb_data['name']
				#gender = fb_data['gender'][0].upper()
				#print(gender)
				#name= fb_data['name']
				#picture = None
				# picture = fb_data['picture']['data']['url']
				#try:
				#	image_content = ContentFile(requests.get(fb_data['picture']['data']['url']).content)
				#	picture = image_content
				#except Exception as e:
				#	traceback.print_exc()
				#	pass
				#email = ''
				#try:
				#	email = fb_data['email']

				#except Exception as e:
				#	traceback.print_exc()
				#	email = idd +"@facebook.com"

				try:
					account = Account.objects.get(mobile = mobile)
					#if account.mobile:
					#	return Response({'error':'User Already Exists', 'success':False}, status=400)
					account.name = name
					account.username = account.mobile
					#account.fb_id = idd
					account.latitude = lat
					account.longitude = lon
					account.address = address
					account.city = city
					s = None
					try:
						s =  State.objects.get_or_create(name = state)[0]
						c = City.objects.get_or_create( state = s, name = city)[0]
						l = Location.objects.get_or_create(name = address)[0]
						l.latitude = lat
						l.longitude = lon
						l.city = c
						l.save()
					except Exception as e:
						traceback.print_exc()
					account.state = state
					#account.profile_pic.save(str(idd)+'.jpg', picture)
					if not account.email:
						account.email = email
					account.is_active = True
					#account.gender = gender
					#print(len(picture))
					application=Application.objects.get_or_create(user=account,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)[0]
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
					p = Players.objects.get(user=account)
					p.is_active = True
					p.save()
					r = {'token':token_info,
						'info':{'id':account.id,
						'player_id':p.id,
						'client_id':application.client_id,
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email,'is_sport_selected':False}, 'success':True}

					if not account.otp_verified:
						print("inside otp verify")
						#send_cel_OTP.delay(account.mobile, account.random)
					account.save()

					return Response(r, status = 201)

				except IntegrityError:
					traceback.print_exc()
					return Response({'error':'User Already Exist.', 'success':False}, status = 400)

				except ObjectDoesNotExist:
					traceback.print_exc()
					return Response({'error':'User Does Not Exist.', 'success':False}, status = 400)


				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e), 'success':False}, status = 400)

			except Exception as e:
				traceback.print_exc()
				print("inside second last else")
				return Response({'error':str(e), 'success':False}, status = 400)

		except Exception as e:
			traceback.print_exc()
			return Response({"success":False, "error":str(e)}, status = 400)




class FacebookLogIn(APIView):

	def post(self, request):
		try:
			mobile = request.data.get('mobile')
			access_token = request.data.get('access_token')

			try:
				param = {"access_token": access_token}
				print(param , "param is printing")
				r = requests.get('https://graph.facebook.com/me?fields=id,name,gender,picture.type(large),email,verified',params=param)
				fb_data=r.json()
				print(fb_data)
				idd = fb_data['id']

				try:
					account = Account.objects.get(fb_id = idd)
					application=Application.objects.create(user=account,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)
					image = ''
					if account.profile_pic :
						image = account.profile_pic.url
					else :
						image = None
					token_info= get_access_token(account)
					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})

					r = {'token':token_info,
						'info':{'id':account.id,
						'client_id':application.client_id,
						'client_secret': application.client_secret ,
						'social_pic': image,
						'name':account.name,
						'email':account.email}}

					if not account.otp_verified:
						print("inside otp verify")
						#send_cel_OTP.delay(account.mobile, account.random)
					account.save()
					return Response(r)

				except Exception as e:
					traceback.print_exc()
					return Response({'error':str(e)})

			except Exception as e:
				traceback.print_exc()
				print("inside second last else")
				return Response({'error':e})

		except Exception as e:
			traceback.print_exc()
			return Response({"success":False, "error":str(e)})


class SportApi(APIView):
# 	permission_classes = (permissions.IsAdminUser,)
	def get(self, request):
		sports = SportsType.objects.all()
		data = []
		for s in sports:
			data.append({'id':s.id,'name':s.name})
		return Response({'sports':data, 'success':True}, status = 200)

	def post(self, request):
		sid = request.data.get('id')
		pid = request.data.get('player_id')
		try:
			sport = SportsType.objects.filter(id__in = sid)
			player = Players.objects.get(id = pid)
			r = []
			for i in sport:
				obj, created = Players_Preferred_Sport.objects.get_or_create(sport_name = i, player = player, is_active = True)
				if created:
					obj.save()
				else:
					continue

			return Response({'success':True, 'player_id':player.id}, status = 201)
		except ObjectDoesNotExist:
			return Response({'success':False, 'error':'Player Or Sport Does Not Exist'}, status = 400)



class DeleteMobile(APIView):

	def delete(self, request):
		try:
			mobile = request.data.get("mobile")
			acc = Account.objects.get(mobile=mobile)
			acc.delete()
			return Response({'msg':'number deleted'})
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e)})





class InviteApi(APIView):
	permission_classes = [TokenHasReadWriteScope]
	def post(self, request):
		try:
			mobile = request.data.get("mobile")
			name = request.data.get("name")
			if not name.strip():
				return Response({'error':'Please Enter Name', 'success':False}, status = 400)
			if not name:
				return Response({'error':'Please Enter Name', 'success':False}, status = 400)
			if type(mobile) == str:
				if not mobile.isnumeric():
					return Response({'error':'Mobile Number Format Not Supported', 'success':False},status=400)
				if len(mobile)<10 or len(mobile) >10:
					return Response({'error':'Mobile Number Must Be Of 10 Digits', 'success':False},status=400)

			if len(str(mobile)) < 10 or len(str(mobile)) > 10 :
				return Response({'error':'Mobile Number Must Be Of 10 Digits','success':False},status=400)
			print(mobile, name)
			try:
				print("1")
				obj = Account.objects.get(mobile = mobile)
				player = Players.objects.get(user = obj)
				profile = None
				if player.user.profile_pic:
					profile = player.user.profile_pic.url
				return Response({'error':'User Already Registered On GoPlayBook', 'player_id':player.id, 'player_name':player.user.name ,'profile_pic':profile, 'success':True, 'mobile':player.user.mobile },status = 200)
			except ObjectDoesNotExist :
				print("inside except")
				try:
					print("inside try except")
					obj = Account.objects.create(mobile = mobile, username =str(mobile), name = name, is_active = False)
					player = Players.objects.create(user = obj, is_active = False)
					print(mobile)
					otp = send_otp(mobile, "Hey you are added to a team on GoPlayBook by one of your friend, Find out by downloading the app, Let's play,ios:https://tinyurl.com/y94gx5c9, android:https://tinyurl.com/ydh36ysj")
					print(otp)
					#profile = None
					#if player.user.profile_pic:
					#	profile = player.user.profile_pic.url
					return Response({'message':'SuccessFully Invite','player_id':player.id, 'player_name':player.user.name, 'success':True}, status = 201)
				except IntegrityError:
					traceback.print_exc()
					return Response({'error':'User Already Registered On GoPlayBook', 'success':True},status = 200)
				except Exception as e:
					traceback.print_exc()
					return Response({'error':e,'success':False}, status = 400)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e),'success':False}, status = 400)
