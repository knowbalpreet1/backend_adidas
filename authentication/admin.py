# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Account
# Register your models here.
class Authentication_Custom(admin.ModelAdmin):
    list_display = ('mobile', 'email','id')
    search_fields=['email','mobile',]
    list_filter= ['is_active','otp_verified']



admin.site.register(Account,Authentication_Custom)
