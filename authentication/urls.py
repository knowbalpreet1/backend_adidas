from django.conf.urls import url, include
from .views import *
# from .admin_views import *
urlpatterns = [
    url(r'^signup/', SignUp.as_view(), name = 'signup' ),
    url(r'^fbsignup/', FacebookSignUp.as_view() , name = 'fbsign' ),
    url(r'^emailsignup/', EmailSignUp.as_view() , name = 'emailsign' ),
    url(r'sporttype/', SportApi.as_view(), name = 'sport_api'),
    url(r'^otpverify/', OTPVerify.as_view(), name = 'otpverify'),
    url(r'^deletemobile/', DeleteMobile.as_view(), name = 'delete_mobile'),
    url(r'^invite/$', InviteApi.as_view(), name = 'invite_user'),
    #admin app urls
    # url(r'^admin/signup/$', SignUpApi.as_view(), name = "AdminSignUp" ),
    # url(r'^admin/login/$', LoginApi.as_view(), name = "AdminLoign" ),
    # url(r'^admin/forgotpassword/$', ForgotPasswordApi.as_view(), name= "AdminForgotPassword"),
    # url(r'^admin/changepassword/$', ChangePasswordApi.as_view(), name= "AdminChangePassword"),
]
