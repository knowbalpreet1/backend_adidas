# from django.shortcuts import render
# from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
# from django.db import IntegrityError
# from django.utils import timezone
# from django.utils.timezone import now, timedelta

# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework import status, permissions

# from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
# from oauth2_provider.settings import oauth2_settings
# from oauthlib.common import generate_token
# from oauth2_provider.models import AccessToken, Application, RefreshToken
# from players.models import Players, SportsType, Players_Preferred_Sport, City, State, Location
# import json
# import uuid
# import random
# import requests
# import traceback
# from datetime import datetime


# from .models  import Account
# from .views import get_token_json, get_access_token

# # def get_token_json(access_token):
# # 	"""
# # 	Takes an AccessToken instance as an argument
# # 	and returns a JsonResponse instance from that
# # 	AccessToken
# # 	"""
# # 	token = {
# # 		'access_token': access_token.token,
# # 		'expires_in': oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
# # 		'token_type': 'Bearer',
# # 		'refresh_token': access_token.refresh_token.token,
# # 		'scope': access_token.scope
# # 	}
# # 	return token



# # def get_access_token(user):
# # 	"""
# # 	Takes a user instance and return an access_token as a JsonResponse
# # 	instance.
# # 	"""
# # 	app = Application.objects.get(user=user)
# # 	token = generate_token()
# # 	# we generate a refresh token
# # 	refresh_token = generate_token()

# # 	expires = now() + timedelta(seconds=oauth2_settings.
# # 	ACCESS_TOKEN_EXPIRE_SECONDS)
# # 	scope = "read write"

# # 	# we create the access token
# # 	access_token = AccessToken.objects.\
# # 		create(user=user,
# # 			   application=app,
# # 			   expires=expires,
# # 			   token=token,
# # 			   scope=scope)

# # 	# we create the refresh token
# # 	RefreshToken.objects.\
# # 		create(user=user,
# # 			   application=app,
# # 			   token=refresh_token,
# # 			   access_token=access_token)
# # 	# we call get_token_json and returns the access token as json
# # 	return get_token_json(access_token)

# class SignUpApi(APIView):
# 	def post(self, request):
# 		data = request.data
# 		for key, value in data.items():
# 			if not value:
# 				return Response({'error':'All Fields Are Mandatory', 'success':False}, status = 400)
# 			if type(data[key]) == str:
# 				if data[key].startswith(' ') or len(data[key].replace(" ","")) == 0 :
# 					return Response({'error':'Provide Valid Data', 'success':False}, status = 400)

# 		print(data)
# 		name = data.get('name')
# 		email = data.get('email')
# 		phone = data.get('phone')
# 		intrested = data.get('intrested')

# 		try:
# 			acc = Account.objects.get(email = email)
# 			return Response({'message':'User Already Exists','name':acc.name, 'email':acc.email, 'phone':acc.mobile, 'intrested':acc.intrested_in, 'success':True}, status = 200)
# 		except ObjectDoesNotExist:
# 			traceback.print_exc()
# 			acc = Account.objects.create(name = name, email = email, mobile = phone, intrested_in = intrested, is_active = False)
# 			pas = name+str(uuid.uuid4().hex)[:6]
# 			acc.set_password(pas)
# 			acc.default_password = pas
# 			acc.save()
# 			player = Players.objects.create(user = acc, is_active = False)
# 			# application=Application.objects.get_or_create(user=account,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)[0]
# 			# token_info= get_access_token(account)
# 			# token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
# 			return Response({'success':True,'message':'User Created', 'account_id':acc.id}, status = 200)

# 		except Exception as e:
# 			traceback.print_exc()
# 			return Response({'error':str(e), 'success':False}, status = 400)

# class LoginApi(APIView):
# 	def post(self, request):
# 		data = request.data
# 		email = data.get('email')
# 		password = data.get('pass')
# 		mobile = data.get('mobile')
# 		try:
# 			acc= None
# 			if mobile:
# 				acc = Account.objects.get(mobile = mobile, is_active = True)
# 			elif email:
# 				acc = Account.objects.get(email = email, is_active = True)
# 			else:
# 				return Response({'error':'Please Provide Email Or Mobile No', 'success':False}, status = 400)
# 			now = datetime.now()
# 			if acc.password_sent_time:
# 				check = acc.password_sent_time + timedelta(days = 1)
# 				print(now,">>>>>", check)
# 				if now > check:
# 					return Response({'error':'Password Expired', 'success':False}, status = 400)
# 				elif now < check:
# 					if password.strip() != acc.default_password.strip():
# 						print(password.strip(), acc.default_password.strip())
# 						print('in 1 if')
# 						return Response({'error':'Enter Correct Email/Password', 'success':False}, status = 400)

# 					else:
# 						application=Application.objects.get_or_create(user=acc,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)[0]
# 						token_info= get_access_token(acc)
# 						token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
# 						return Response({
# 										'name':acc.name,
# 										'id':acc.id,
# 										'mobile':acc.mobile,
# 										'email':acc.email,
# 										'intrested_in':acc.intrested_in,
# 										'token_info':token_info,
# 										'success':True
# 							}, status = 200)
# 			else:
# 				if acc.check_password(password.strip()):
# 					application=Application.objects.get_or_create(user=acc,client_type=Application.CLIENT_CONFIDENTIAL,authorization_grant_type=Application.GRANT_PASSWORD)[0]
# 					token_info= get_access_token(acc)
# 					token_info.update({'client_id':application.client_id, 'client_secret': application.client_secret})
# 					return Response({
# 									'name':acc.name,
# 									'id':acc.id,
# 									'mobile':acc.mobile,
# 									'email':acc.email,
# 									'intrested_in':acc.intrested_in,
# 									'token_info':token_info,
# 									'success':True
# 						}, status = 200)					
# 				else:
# 					print('in 2 if')
# 					return Response({'error':'Enter Correct Email/Password'}, status = 400)
# 		except ObjectDoesNotExist:
# 			traceback.print_exc()
# 			return Response({'error':'User Does Not Exists', 'success':False}, status = 400)
# 		except Exception as e:
# 			traceback.print_exc()
# 			return Response({'error':str(e), 'success':False}, status = 400)


# class ForgotPasswordApi(APIView):
# 	def post(self, request):
# 		data = request.data
# 		mobile = data.get('mobile')
# 		try:
# 			acc = Account.objects.get(mobile = mobile, is_active = True)
# 			#Code For sending Notification
			
# 			acc.default_password = str(uuid.uuid4().hex)[:8]
# 			acc.password_sent_time = datetime.now()
# 			acc.save()
# 			return Response({'message':'Password Changed', 'pass':acc.default_password}, status = 200)
				
# 		except ObjectDoesNotExist:
# 			return Response({'error':'User Does Not Exist', 'success':False}, status = 400)
# 		except Exception as e:
# 			traceback.print_exc()
# 			return Response({'error':str(e), 'success':False}, status = 400)



# class ChangePasswordApi(APIView):
# 	permissions_classes = [TokenHasReadWriteScope]
# 	def post(self, request):
# 		data = request.data
# 		user = request.user
# 		for key, value in data.items():
# 			if not value:
# 				return Response({'error':'All Fields Are Mandatory', 'success':False}, status = 400)
# 		old_password = data.get('old_password')
# 		new_password1 = data.get('new_password1')
# 		new_password2 = data.get('new_password2')

# 		try:
# 			if user.check_password(old_password):
# 				if new_password1.strip() == new_password2.strip():
# 					if new_password1.strip() == old_password.strip():
# 						return Response({'error':'Old Password And New Password Are Same', 'success':False }, status = 400)
# 					user.set_password(new_password1.strip())
# 					user.save()
# 					return Response({'message':'Password Changed SuccessFully', 'success':True}, status = 200)
# 				else:
# 					return Response({'error':'Password Does Not Match','success':False}, status = 400)

# 			else:
# 				return Response({'error':'Old Password Does Not Match', 'success':False}, status = 400)
# 		except Exception as e:
# 			traceback.print_exc()
# 			return Response({'error':str(e), 'success':False}, status = 400)
