from django.conf.urls import url, include
from . import views

urlpatterns  = [
	# url(r'player_result/$', views.PlayerSearch.as_view(), name="player_search"),
	# url(r'venue_result/$', views.VenueSearch.as_view(), name="venue_search"),
	url(r'all_data/$', views.Search_Data.as_view(), name="all_data")
]