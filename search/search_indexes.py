from haystack import indexes
from players.models import Players,Players_Preferred_Sport, Team
from venue.models import Venue, Venue_Review
from tournament.models import Tournament_Detail
from event.models import Event

template_file = "/home/ubuntu/webapps/goplaybook/templates/search/indexes"

class PlayerIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True, template_name=template_file+"/players/player_text.txt")
	user = indexes.CharField(model_attr="user", null=True)

	def get_model(self):
		return Players

	def prepare(self, obj):
		self.prepared_data = super(PlayerIndex, self).prepare(obj)

		search_name = [obj.user.name]
		self.prepared_data['search_name'] = ' '.join(
			[x.lower() for x in search_name if x is not None]
			)
		return self.prepared_data

	def index_queryset(self, using=None):
		return self.get_model().objects.filter(is_active=True, user__is_active=True)

class VenueIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True, template_name=template_file+"/venue/venue_text.txt")
	name = indexes.CharField(model_attr="name", null=True)

	def get_model(self):
		return Venue

	def prepare(self, obj):
		self.prepared_data = super(VenueIndex, self).prepare(obj)

		search_name = [obj.name]
		self.prepared_data['search_name'] = ' '.join(
			[x.lower() for x in search_name if x is not None]
			)

		return self.prepared_data

	def index_queryset(self, using=None):
		return self.get_model().objects.filter(is_active=True)

class TournamentIndex(indexes.SearchIndex, indexes.Indexable):

	text = indexes.CharField(document=True, use_template=True, template_name=template_file+"/tournament/tournament_text.txt")
	name = indexes.CharField(model_attr="name", null=True)

	def get_model(self):
		return Tournament_Detail

	def prepare(self, obj):
		self.prepared_data = super(TournamentIndex, self).prepare(obj)

		search_name = [obj.name]
		self.prepared_data['search_name'] = ' '.join(
			[x.lower() for x in search_name if x is not None]
			)

		return self.prepared_data

	def index_queryset(self, using=None):
		return self.get_model().objects.filter(is_deactivated=False, is_certified=True, is_active=True)


class EventIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True, template_name=template_file+"/event/event_text.txt")
	name = indexes.CharField(model_attr="name", null=True)

	def get_model(self):
		return Event

	def prepare(self, obj):
		self.prepared_data = super(EventIndex, self).prepare(obj)
		search_name = [obj.name]
		self.prepared_data['search_name'] = ' '.join(
			[x.lower() for x in search_name if x is not None]
			)

		return self.prepared_data

	def index_queryset(self, using=None):
		return self.get_model().objects.filter(is_active=True)

class TeamIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True, template_name=template_file+"/team/team_text.txt")
	name = indexes.CharField(model_attr="name", null=True)

	def get_model(self):
		return Team

	def prepare(self, obj):
		self.prepared_data = super(TeamIndex, self).prepare(obj)
		search_name = [obj.name]
		self.prepared_data['search_name'] = ' '.join(
			[x.lower() for x in search_name if x is not None]
			)

		return self.prepared_data

	def index_queryset(self, using=None):
		return self.get_model().objects.filter(is_active=True)
