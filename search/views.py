from django.shortcuts import render
from django.utils import timezone
from django.utils.timezone import now, timedelta
from rest_framework.views import APIView
from rest_framework import status
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from authentication.models import Account
from players.models import Players, Team, Players_Preferred_Sport
from venue.models import Venue, Court, VenueImg
import traceback
import json
from datetime import datetime
from django.http import HttpResponse
from haystack.query import SearchQuerySet
from haystack.inputs import Clean
from event.views import distance
from operator import itemgetter
from venue.views import average_rating, cost_per_game
from tournament.models import Tournament_Detail, Team_Registration, Tournament_Venue_Map, GameWeek_Matches
from event.models import Event, PhotoTemplate,EventPlayerMap
# Create your views here.


def player_search(name):
	sqs = SearchQuerySet().models(Players)

	if name or name!='':
		print("hello world")
		sqs = sqs.filter(search_name__startswith=Clean(name))
	else:

		sqs = sqs
	player_list = []

	for i in sqs:
		if i.object:
			image = None
			if i.object.user.profile_pic:
				image = i.object.user.profile_pic.url
				
			result_dict = {
				"name":i.object.user.name,
				"id":i.pk,
				"account_id":i.object.user.id,
				"image":image,
				"gender":i.object.user.gender,
				"brand_name": i.object.user.brand_name,
				"date_joined":i.object.user.date_joined.strftime("%Y-%m-%d %I:%M %p"),
				"sports":list(Players_Preferred_Sport.objects.filter(player=i.object).values("sport_name__name")),
				"address":{"city":i.object.user.city, "state":i.object.user.state},
			}
			player_list.append(result_dict)

	return sorted(player_list, key=itemgetter("date_joined"), reverse = True)


def match_search(name):
	sqs = GameWeek_Matches.objects.filter()
	print ('Matches ' , '#'*100)
	match_list = []

	for i in sqs:
		if (i.gameweek.tournament.id == 96):
			if name or name!='':
				if name.lower() in i.team1.name.lower() or name.lower() in i.team2.name.lower():
					match_data = {
					"first_team_pic": 'media/' + str(i.team1.team_pic),
					"second_team_pic": 'media/' + str(i.team2.team_pic),
					"first": i.team1.name,
					"second":i.team2.name,
					"time":i.date.strftime("%B %d, %Y"),
					"score":"",
					"status":str(i.status)
					}
					match_list.append(match_data)
			else:
				match_data = {
				"first_team_pic": 'media/' + str(i.team1.team_pic),
				"second_team_pic": 'media/' + str(i.team2.team_pic),
				"first": i.team1.name,
				"second":i.team2.name,
				"time":i.date.strftime("%B %d, %Y"),
				"score":"",
				"status":str(i.status)
				}
				match_list.append(match_data)

	return sorted(match_list, key=itemgetter("time"), reverse = True)


def venue_search(name):
	sqs = SearchQuerySet().models(Venue)

	if name or name!="":
		sqs = sqs.filter(search_name__startswith=Clean(name))
	else:
		sqs = sqs
	venue_list = []
	for i in sqs:
		if i.object:
			avg_rating = average_rating(i.object)
			is_open = False

			opening = i.object.time_of_opening.strftime('%H%M%S')
			closing = i.object.closing.strftime('%H%M%S')
			current = datetime.now().strftime('%H%M%S')

			if(opening<current<closing):
				is_open = True
			venue_img = VenueImg.objects.filter(venue=i.object)
			image = None
			if len(venue_img)!=0:
				image = venue_img[0].url.url

			result_dict = {
				"venue_img":image,
				"avg_rating":avg_rating ,
				"name": i.object.name,
				"address":i.object.address.name,
				"lat":i.object.address.latitude,
				"contact": i.object.contact,
				"created_at":str(i.object.created_at),
				"id":i.pk,
				"distance":None,
				"time_of_opening":str(i.object.time_of_opening),
				"total_field":Court.objects.filter(venue=i.object, is_active=True).count(),
				"is_open":is_open,
				"bio":i.object.bio,
				"lng":i.object.address.longitude,
				"closing":str(i.object.closing),
				"is_active":i.object.is_active,
				"is_functional":i.object.is_functional,
				"cost_per_game":cost_per_game(i.pk)
			}
			venue_list.append(result_dict)
	return sorted(venue_list, key=itemgetter("created_at"), reverse = True)

def tournament_search(name):
	sqs = SearchQuerySet().models(Tournament_Detail)

	if name or name!="":
		sqs = sqs.filter(search_name__startswith=Clean(name))
	else:
		sqs = sqs
	tournament_list = []
	for i in sqs:
		if i.object:
			logo = None
			cover_pic = None
			if i.object:
				team_count = []
				city = None
				if i.object.city:
					city = i.object.city.name
				for x in Team_Registration.objects.filter(tournament=i.object,status="approve"):
					team_logo = None
					if x.team.team_pic:
						team_logo = x.team.team_pic.url

					team_count.append({
						"team_name":x.team.name,
						"team_id":x.team.id,
						"team_logo":team_logo})

				if i.object.logo:
					logo = i.object.logo.url
				if i.object.cover_pic:
					cover_pic = i.object.cover_pic.url
				comp = None
				sport = None

				if i.object.competition_type:
					comp = i.object.competition_type.name
					sport = i.object.competition_type.sport_type.name
				result_dict = {
					"name":i.object.name,
					"id":i.pk,
					"logo":logo,
					"cover_pic":cover_pic,
					"rating":i.object.rating,
					"is_cerified":i.object.is_certified,
					"is_active":True,
					"registration_fee":i.object.registration_fee,
					"max_no_of_teams":i.object.max_no_of_teams,
					"team":team_count,
					"tournament_type":i.object.tournament_type,
					"registration_start_date":str(i.object.registration_start_date)[:10],
					"registration_end_date":str(i.object.registration_end_date)[:10],
					"start_date":str(i.object.start_date)[:10],
					"end_date":str(i.object.end_date)[:10],
					"description":i.object.description,
					"tag_line":i.object.tag_line,
					"tournament_format":i.object.tournament_format,
					"competition_type":comp,
					"sport":sport,
					"email":i.object.email,
					"mobile":i.object.mobile1,
					"tournament_level":i.object.tournament_level,
					"prize":i.object.prize,
					"reward":i.object.reward,
					"tournament_city":city,
					"tournament_venue":list(Tournament_Venue_Map.objects.filter(tournament=i.object, is_active=True).values("court_name", "is_dummy", "is_active", "address", "pincode", "city", "state", "latitude", "longitude"))
				}
				tournament_list.append(result_dict)

	return sorted(tournament_list, key=itemgetter("id"), reverse = True)

def team_search(name):
	# sqs = SearchQuerySet().models(Team)
	#
	# if name or name!="":
	# 	sqs = sqs.filter(search_name__startswith=Clean(name))
	# else:
	# 	sqs = sqs
	team_list = []
	# for i in sqs:
	# 	if i.object:
	image = None

	for x in Team_Registration.objects.filter(tournament_id=96,status="approve"):
		if name or name != '':
			if name.lower() in x.team.name.lower():
				if x.team.team_pic:
					image = x.team.team_pic.url
				result_dict = {
					"name":x.team.name,
					"id":x.team.id,
					"sport":x.team.sport.name,
					"sport_id":x.team.sport.id,
					"created_by":x.team.created_by.user.name,
					"is_active":x.team.is_active,
					"team_pic":image,
					"created_at":x.team.created_at.strftime("%Y-%m-%d %I:%M %p"),
					"city":x.team.city.name
				}
				team_list.append(result_dict)
		else:
			if x.team.team_pic:
				image = x.team.team_pic.url
			result_dict = {
				"name":x.team.name,
				"id":x.team.id,
				"sport":x.team.sport.name,
				"sport_id":x.team.sport.id,
				"created_by":x.team.created_by.user.name,
				"is_active":x.team.is_active,
				"team_pic":image,
				"created_at":x.team.created_at.strftime("%Y-%m-%d %I:%M %p"),
				"city":x.team.city.name
			}
			team_list.append(result_dict)

	return sorted(team_list, key=itemgetter("created_at"), reverse = True)

def event_search(name):
	sqs = SearchQuerySet().models(Event)

	if name or name!="":
		sqs = sqs.filter(search_name__startswith=Clean(name))
	else:
		sqs = sqs
	event_list = []
	for i in sqs:
		if i.object:
			ep = EventPlayerMap.objects.filter(event = i.object, going = True)
			going_list = []
			for e in ep:
				pic = None
				if e.player.user.profile_pic:
					pic = e.player.user.profile_pic.url
				going_list.append({'player_id':e.player.id,
						'player_name':e.player.user.name,
						'profile_pic':pic})

			image = None
			if i.object.image:
				image = i.object.image.image.url
			result_dict={
				'event_id':i.object.id,
				'name':i.object.name,
				'image':image,
				'start_time':str(i.object.start_time.strftime("%Y-%m-%d %I:%M %p")),
				'end_time':str(i.object.end_time.strftime("%Y-%m-%d %I:%M %p")),
				'location_name':i.object.address.name,
				'city':i.object.address.city.name,
				'state':i.object.address.city.state.name,
				'going':going_list,
				"created_at":i.object.created_at.strftime("%Y-%m-%d %I:%M %p"),
				'going_count':ep.count()
			}

			event_list.append(result_dict)

	return sorted(event_list, key=itemgetter("created_at"), reverse = True)


class Search_Data(APIView):

	permission_classess = [TokenHasReadWriteScope]

	def get(self, request):
		try:
			query = request.GET["query"]
			#if query=="":
			#	return HttpResponse(json.dumps({"success":True, "players":[], "venue":[], "tournament":[], "event":[], "team":[]}),status=200)

			player = player_search(query)
			player[:] = [x for x in player if x.get("account_id") !=request.user.id and x.get('brand_name','goplaybook') == 'adidas']
			venue = venue_search(query)
			tournament = tournament_search(query)
			event = event_search(query)
			team = team_search(query)
			match = match_search(query)

			return HttpResponse(json.dumps({"success":True, "players":player,"match":match, "venue":venue, "tournament":tournament, "event":event, "team":team}),status=200)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"error":str(e), "success":False}), status=400)
