from django.contrib import admin
from businessapp.models import BusinessUser, AccessToken, Application, BasePermissions, ChildPermissions, Options, ContactUs
# Register your models here.
class MyModelAdmin(admin.ModelAdmin):
    readonly_fields=('password',)


# import hashlib
# hash_object = hashlib.sha256(b'Hello World')
# hex_dig = hash_object.hexdigest()
admin.site.register(BusinessUser, MyModelAdmin)
admin.site.register(Application)
admin.site.register(AccessToken)
admin.site.register(BasePermissions)
admin.site.register(ChildPermissions)
admin.site.register(Options)
admin.site.register(ContactUs)

