from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import IntegrityError
from django.utils import timezone
from django.utils.timezone import now, timedelta

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from oauth2_provider.settings import oauth2_settings

from authentication.async_task import send_otp
from .models import BusinessUser, Application, AccessToken, BasePermissions, ChildPermissions, Options, ContactUs
from django.conf import settings
from django.db.models import Q
ADMIN_PERMISSIONS =  settings.ADMIN_PERMISSIONS
MANAGER_PERMISSIONS = settings.MANAGER_PERMISSIONS
pkey = settings.PRIVATE_KEY
key = settings.USER_KEY

from venue.models import Venue, VenueAdminPermissionMap, VenueManagerPermissionMap
from tournament.models import Tournament_Detail, TournamentAdminPermissionMap, TournamentManagerPermissionMap
import time
from datetime import datetime
import json
import random
import traceback
import jwt
import uuid

from businessapp.permissions import customPermission, checkAdminPermission
# Create your views here.

def get_permissions(user):
	p_list=[]
	try:
		admin = VenueAdminPermissionMap.objects.filter(admin = user)
		manager = VenueManagerPermissionMap.objects.filter(manager = user)
		p_list = []
		for a in admin:
			p_dict = {'venue_id':a.venue.id,'venue_name':a.venue.name}
			permissions = a.permission.all()
			for p in permissions:
				p_dict.update({p.slug.replace('-',''):True})
			keys = [key for key, items  in p_dict.items()]
			for i in ADMIN_PERMISSIONS:
				if i.replace('-','') not in keys:
					p_dict.update({i.replace('-',''):False})
			p_list.append(p_dict)

		for m in manager:
			p_dict = {'venue_id':m.venue.id, 'venue_name':m.venue.name}
			permissions = m.permission.all()
			print(permissions)
			for p in permissions:
					p_dict.update({p.slug.replace('-',''):True})
			keys = [key for key, items  in p_dict.items()]
			for i in ADMIN_PERMISSIONS:
				if i.replace('-','') not in keys:
					p_dict.update({i.replace('-',''):False})

			p_list.append(p_dict)

		# if p_list:
		# 	p_list.update({'success':True})
		return p_list
	except Exception as e:
		traceback.print_exc()
		return Response({'error':str(e), 'success':False}, status= 400)


def get_access_token(user, application):
	permissions = get_permissions(user)
	access_payload = {'user_id':user.id,'time': datetime.now().strftime("%Y-%m-%d %I:%M %p"), 'key':key}
	access_token = jwt.encode(access_payload, pkey, algorithm = 'HS256').decode('ascii')
	refresh_payload = {'user_id':user.id, 'access_token':access_token}
	refresh_token = jwt.encode(refresh_payload, pkey, algorithm = 'HS256').decode('ascii')
	print(len(access_token),len(refresh_token))
	client_id = application.client_id
	client_secret = application.client_secret
	expires = timezone.now() + timedelta(seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS)
	print(expires)
#	token = AccessToken.objects.create(
#												user = user,
#												application = application,
#												access_token = access_token,
#												refresh_token = refresh_token,
#												expires = expires
#											)

	return dict(
				access_token = access_token,
				refresh_token = refresh_token,
				client_id = client_id,
				client_secret = client_secret,
				expires_at   = oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
				token_type= 'Bearer',

			)


class RefreshTokenApi(APIView):
	def post(self, request):
		try:
			print(request.data)
			refresh_token = request.data.get('refresh_token')
			client_id = request.data.get('client_id')
			client_secret = request.data.get('client_secret')
			print(refresh_token,'###########################', client_id, '****************************', client_secret)
			app = Application.objects.get(client_id = client_id, client_secret = client_secret)
			user = request.buser
			if app.user != user:
				return Response({'error':'Invalid User', 'success':False}, status = 400)

			#AccessToken.objects.get(refresh_token = refresh_token).delete()
			data = get_access_token(user, app)

			return Response({'token':data,'user_id':user.id, 'mobile':user.mobile, 'email':user.email, 'name':user.name, 'success':True}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class SignUpApi(APIView):

	def post(self, request):
		data = request.data
		name = data.get('name')
		email = data.get('email')
		intrested_in = data.get('intrested')
		mobile = data.get('phone')
		for key, values in data.items():
			if not values:
				return Response({'error':'All Fields Are Required', 'success':False}, status = 400)
			if type(data[key]) == str:
				if data[key].startswith(' ') or len(data[key].replace(" ","")) == 0 :
					return Response({'error':'Provide Valid Data', 'success':False}, status = 400)

		try:
			user = BusinessUser.objects.create(name = name, email = email, mobile = mobile, intrested_in = intrested_in)
			pas = str(uuid.uuid4().hex)[:8]
			user.set_password(pas)
			user.default_password = pas
			user.save()
			Application.objects.create(user = user)
			return Response({'success':True, 'message':'Account Crated'}, status = 200)
		except IntegrityError as e:
			traceback.print_exc()
			if 'email' in str(e):
				return Response({'error':'Email Already Exist', 'success':False}, status = 400)
			elif 'mobile' in str(e):
				return Response({'error':'Mobile Already Exist', 'success':False}, status = 400)
			else :
				return Response({'error':'User Already Exist', 'success':False}, status = 400)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

class LoginApi(APIView):
#   permission_classes= [customPermission,]
	def post(self, request):
		data = request.data
		email = data.get('email')
		mobile = data.get('mobile')
		password = data.get('pass')
		print(data)
		try:
			user = None
			if email:
				user = BusinessUser.objects.get(email = email, is_active = True)
			elif mobile:
				print(mobile,'>>>>>>>>>>>>>>>>>')
				user = BusinessUser.objects.get(mobile = mobile, is_active = True)
			else:
				return Response({'error':'Please Provide Email Or Mobile No', 'success':False}, status = 400)
			now = datetime.now()

			if user.password_sent_time:
				print(now.strftime('%Y/%m/%d %I:%M %p'), user.password_sent_time.strftime('%Y/%m/%d %I:%M %p'))
				check = user.password_sent_time + timedelta(days = 1)
				print(now.strftime('%Y/%m/%d %I:%M %p'), check.strftime('%Y/%m/%d %I:%M %p'))
				print(now > check, '-----------------------------------')
				if now > check:
					return Response({'error':'Password Expired', 'success':False}, status = 400)

			if user.check_password(password):
				application = Application.objects.get(user = user)
				token = get_access_token(user, application)
				print(user.mobile)
				return Response({
								'user_id':user.id,
								'name':user.name,
								'mobile':user.mobile,
								'email':user.email,
								'token':token,
								'success':True}, status = 200)
			else:
				print('in 2 if')
				return Response({'error':'Enter Correct Email/Password'}, status = 400)
		except ObjectDoesNotExist:
			traceback.print_exc()
			return Response({'error':'User Does Not Exists', 'success':False}, status = 400)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)





class ForgotPasswordApi(APIView):
	def post(self, request):
		data = request.data
		mobile = data.get('mobile')
		try:
			acc = BusinessUser.objects.get(mobile = mobile, is_active = True)
			#Code For sending Notification
			pas = str(uuid.uuid4().hex)[:8]
			acc.default_password = pas
			acc.set_password(pas)
			acc.password_sent_time = datetime.now()
			acc.save()
			send_otp(acc.mobile, 'Login using new password: {}. This password will expire after 24hrs so make sure you login within time'.format(pas))
			return Response({'message':'Password Changed', 'pass':acc.default_password}, status = 200)

		except ObjectDoesNotExist:
			return Response({'error':'User Does Not Exist', 'success':False}, status = 400)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class ChangePasswordApi(APIView):
	permission_classes = [customPermission,]
	def post(self, request):
		data = request.data
		user = request.buser
		for key, value in data.items():
			if not value:
				return Response({'error':'All Fields Are Mandatory', 'success':False}, status = 400)
		old_password = data.get('old_password')
		new_password1 = data.get('new_password1')
		new_password2 = data.get('new_password2')
		print(data)
		try:
			print(user.check_password(old_password.strip()))
			if user.check_password(old_password.strip()):
				if new_password1.strip() == new_password2.strip():
					if new_password1.strip() == old_password.strip():
						return Response({'error':"New password can't be same as old password", 'success':False }, status = 400)
					user.set_password(new_password1.strip())
					user.password_sent_time = None
					user.save()
					return Response({'message':'Password Changed SuccessFully', 'success':True}, status = 200)
				else:
					return Response({'error':'Password Does Not Match','success':False}, status = 400)

			else:
				return Response({'error':'Old Password Does Not Match', 'success':False}, status = 400)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


class UserManagementApi(APIView):
	permission_classes = [checkAdminPermission,]
	def get(self, request):
		try:
			buser = request.buser
			user_id = request.GET.get('user_id')
			user = BusinessUser.objects.get(id = user_id, is_active = True)
			admins = VenueAdminPermissionMap.objects.filter(user = buser, admin = user, is_active = True, admin__is_active = True)
			managers = VenueManagerPermissionMap.objects.filter(user = buser, manager = user, is_active = True, manager__is_active = True )
			data = {'user_id':user.id, 'name':user.name, 'email':user.email, 'mobile':user.mobile,'venue_admin':[], 'venue_manager':[], 'tour_admin':[], 'tour_manager':[]}
			if admins:
				admin = []
				for a in admins:
					admin.append(a.venue.id)

				data.update({'venue_admin':admin})


			if managers:
				manager = []
				for m in managers:
					manager.append(m.venue.id)
				data.update({'venue_manager':manager})

			tour_qs= Tournament_Detail.objects.filter(Q(admin= buser) & Q(is_active= True))
			serializer= []
			for obj in tour_qs:
				admin= False
				manager= False
				if user in obj.admin.all():
					data['tour_admin'].append(obj.id)
					admin= True
				if user in obj.manager.all():
					data['tour_manager'].append(obj.id)
					manager= True
				serializer.append({'id':obj.id, 'name':obj.name, 'admin':admin, 'manager':manager})
			data.update({'tournament':serializer})
			print("data-->",data)
			return Response({'data':data, 'success':True}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

	def post(self, request):
		data = request.data
		buser = request.buser
		print(buser)
		name = data.get('name')
		email = data.get('email')
		mobile = data.get('mobile')
		user_type = data.get('user_type')
		vpermission = data.get('vpermission')
		print(vpermission)
		for key,items in data.items():
			if not data['vpermission'] and not data['tpermission']:
				return Response({'error':'Please Provide Venue Or Tounament', 'success':False}, status = 400)
			if not items:
				if data['vpermission'] and not data['tpermission']:
					continue
				return Response({'error':'All Fields Are Required', 'success':False}, status = 400)

		try:
			venues = Venue.objects.filter(id__in = vpermission, owner = buser, is_active = True)
			print(venues)
			#tournaments =

			try:
				user = BusinessUser.objects.create(name = name, email = email, mobile = mobile)
				pas = name+str(uuid.uuid4().hex)[:6]
				user.set_password(pas)
				user.default_password = pas
				user.save()
				Application.objects.create(user = user)
			except IntegrityError:
				traceback.print_exc()
				return Response({'error':'User Already Exist', 'success':False}, status = 400)
			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False}, status = 400)

			if user_type.strip() == 'A':
				print('user type',user_type)
				adminpermission = ChildPermissions.objects.filter(slug__in = ADMIN_PERMISSIONS)
				print('admin permission', adminpermission)
				for v in venues:
					print('venue', v)
					admin = VenueAdminPermissionMap.objects.create(user = buser, venue = v, admin = user)
					admin.permission.add(*adminpermission)
					print('admin',admin)
					admin.save()
				#send_otp(admin.user.mobile, 'You Have Requested To Add New Admin')
				#send_otp('8800365323', '{0} has sent request to add new admin: Name of new admin: {1}, Phone Number: {2}, email: {3}'.format(admin.user.name, admin.admin.name, admin.admin.mobile, admin.admin.email))

			elif user_type.strip() == 'M':
				print('user type',user_type)
				managerpermission = ChildPermissions.objects.filter(slug__in = MANAGER_PERMISSIONS)
				print('permissions',managerpermission)
				for v in venues:
					print('venue',v)
					manager = VenueManagerPermissionMap.objects.create(user = buser, venue = v, manager = user)
					manager.permission.add(*managerpermission)
					print('manager',manager)
					manager.save()
				#send_otp(manager.user.mobile, 'You Have Requested To Add New Manager')
				#send_otp('8800365323', '{0} has sent request to add new admin: Name of new admin: {1}, Phone Number: {2}, email: {3}'.format(manager.user.name, manager.manager.name, manager.manager.mobile, manager.manager.email))
			return Response({'success':True})

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

	def put(self, request):
		data = request.data
		buser = request.buser
		user_id = data.get('user_id')
		name = data.get('name')
		email = data.get('email')
		mobile = data.get('mobile')
		venue_admin = data.get('venue_admin')
		venue_manager = data.get('venue_manager')
		tour_admin= data.get("tour_admin")
		tour_manager= data.get("tour_manager")
		print(data)
	#	vpermission = data.get('vpermission')
	#	for key,items in data.items():
	#		if not data['vpermission'] and not data['tpermission']:
	#			return Response({'error':'Please Provide Venue Or Tounament', 'success':False}, status = 400)
	#		if not items:
	#			if data['vpermission'] and not data['tpermission']:
	#				continue
	#			return Response({'error':'All Fields Are Required', 'success':False}, status = 400)

		try:
			print(buser)
			#venues = Venue.objects.filter(id__in = vpermission, is_active = True)
			#tournaments =
			try:
				user = BusinessUser.objects.get(id = user_id )
				user.name = name
				user.email = email
				user.mobile = mobile
				user.save()

			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False}, status = 400)

			print('112121212121212121',VenueAdminPermissionMap.objects.filter(user = buser, admin = user).delete())
			print('112121212121212121',VenueManagerPermissionMap.objects.filter(user = buser, manager = user).delete())
			if venue_admin:
				adminpermission = ChildPermissions.objects.filter(slug__in = ADMIN_PERMISSIONS)
				venues = Venue.objects.filter(id__in = venue_admin, is_active = True)
				for v in venues:
					VenueAdminPermissionMap.objects.filter(venue = v, admin = user).delete()
					admin = VenueAdminPermissionMap.objects.create(user = v.owner, venue = v, admin = user)
					admin.permission.add(*adminpermission)
					admin.save()

			if venue_manager:
				venue_manager = set(venue_manager) - set(venue_admin)
				managerpermission = ChildPermissions.objects.filter(slug__in = MANAGER_PERMISSIONS)
				venues = Venue.objects.filter(id__in = venue_manager, is_active = True)
				for v in venues:
					VenueManagerPermissionMap.objects.filter(venue = v, manager = user).delete()
					manager = VenueManagerPermissionMap.objects.create(user = v.owner, venue = v, manager = user)
					manager.permission.add(*managerpermission)
					manager.save()

			tour_qs= Tournament_Detail.objects.filter(Q(is_active= True) & Q(admin= buser))
			if tour_admin:
				print("tour_admin-->", tour_admin)
				for obj in tour_qs:
					if obj.id in tour_admin:
						obj.admin.add(user)
						print("inside if",obj, user)
					else:
						obj.admin.remove(user)
						print(obj, user)
					obj.save()
			else:
				for obj in tour_qs:
					obj.admin.remove(user)
					obj.save()

			if tour_manager:
				tour_manager= set(tour_manager) - set(tour_admin)#tour_manager - tour_admin
				print("tour_manager-->",tour_manager)
				for obj in tour_qs:
					if obj.id in tour_manager:
						obj.manager.add(user)
					else:
						obj.manager.remove(user)
					obj.save()
			else:
				for obj in tour_qs:
					obj.manager.remove(user)
					obj.save()

			return Response({'success':True}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

	def delete(self, request):
		try:
			user_id = request.GET.get('user_id')
			buser = request.buser
			user = BusinessUser.objects.get(id = user_id)
			admin = VenueAdminPermissionMap.objects.filter(admin = user, user = buser)
			print(admin)
			manager = VenueManagerPermissionMap.objects.filter(manager = user, user = buser)
			print(manager)
			if admin:
				#user.is_active = False
				#user.save()
				for a in admin:
					a.is_active = False
					a.save()
				send_otp('8822242224', '{0} has sent request to remove admin: Name of admin: {0}, Phone Number: {1}, email: {2}'.format(request.buser.name, user.name, user.mobile, user.email))
			if manager:
				#user.is_active = False
				#user.save()
				for m in manager:
					m.is_active = False
					m.save()
				send_otp('8822242224', '{0} has sent request to remove manager: Name of manager: {0}, Phone Number: {1}, email: {2}'.format(request.buser.name, user.name, user.mobile, user.email))

			return Response({'success':True, 'message':'User Delete SuccessFully'}, status= 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class UserManagementListApi(APIView):
	#permission_classes = [checkAdminPermission,]

	def get(self, request):
		try:
			buser = request.buser
			admins = VenueAdminPermissionMap.objects.filter(user = buser,is_active = True, admin__is_active = True).exclude(admin = buser).distinct('admin__id')
			print(admins)
			managers = VenueManagerPermissionMap.objects.filter(user= buser, is_active = True, manager__is_active = True).distinct('manager__id')
			print(managers)
			users = []
			if admins:
				for i in admins:
					print(i)
					if i:
						users.append({'name':i.admin.name,'email':i.admin.email, 'mobile':i.admin.mobile, 'user_id':i.admin.id})
			value = list(map(lambda x:x['user_id'], users))
			print(value)
			if managers:
				for j in managers:
					print(j)
					if j:
						if j.manager.id not in value:
							users.append({'name':j.manager.name,'email':j.manager.email, 'mobile':j.manager.mobile, 'user_id':j.manager.id})
			return Response({'data':users, 'success':True}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class UserProfileApi(APIView):
	permission_classes = [customPermission,]
	def get(self, request):
		try:
			user = request.buser
			return Response({'name':user.name, 'intrested_in':user.intrested_in, 'user_id':user.id,'email':user.email, 'mobile':user.mobile, 'success':True }, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)



class ContactUsApi(APIView):

	def get(self, request):
		try:
			options = Options.objects.all()
			op_list = []
			for option in options:
				op_list.append({'id':option.id, 'name':option.name})

			return Response({'options':op_list, 'success':True}, status = 201)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


	def post(self, request):
		try:
			data = request.data
			option = data.get('option')
			comment = data.get('comment')
			user = request.buser

			o = Options.objects.get(id = option)
			c = ContactUs.objects.create(option = o, user = user, comment = comment)
			c.save()

			return Response({'success':True, 'message':'Feedback Saved'}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

class UserManagement(APIView):

	def get(self, request):
		try:

			mobile = request.GET.get('mobile')
			obj = BusinessUser.objects.get(mobile = mobile)
			data = []
			if obj:
				return Response({
						'id':obj.id,
						'name':obj.name,
						'mobile':obj.mobile,
						'email':obj.email,
						'success':True
						} , status = 200)



			return Response({'success':False}, status = 400)
		except ObjectDoesNotExist:
			traceback.print_exc()
			print('objectsdoesnotexists')
			return Response({'error':'not exist'}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

	def post(self, request):

		data = request.data
		buser = request.buser
		# print(buser)
		user_id = data.get('user_id')
		name = data.get('name')
		email = data.get('email')
		mobile = data.get('mobile')
		# user_type = data.get('user_type')
		venue_admin = data.get('venue_admin')
		venue_manager = data.get('venue_manager')
		tour_admin = data.get('tour_admin')
		tour_manager = data.get('tour_manager')
		print(data)
		# for key,items in data.items():
		# 	if not data['vpermission'] and not data['tpermission']:
		# 		return Response({'error':'Please Provide Venue Or Tounament', 'success':False}, status = 400)
		# 	if not items:
		# 		if data['vpermission'] and not data['tpermission']:
		# 			continue
		# 		return Response({'error':'All Fields Are Required', 'success':False}, status = 400)

		try:
			user = None
			try:
				if user_id:
					user = BusinessUser.objects.get(id = user_id)
				else:
					user = BusinessUser.objects.create(name = name, email = email, mobile = mobile)
					pas = name+str(uuid.uuid4().hex)[:6]
					user.set_password(pas)
					user.default_password = pas
					user.save()
					Application.objects.create(user = user)
			except IntegrityError:
				traceback.print_exc()
				return Response({'error':'User Already Exist', 'success':False}, status = 400)
			except Exception as e:
				traceback.print_exc()
				return Response({'error':str(e), 'success':False}, status = 400)

			if venue_admin :
				print('admin',venue_admin)
				venues = Venue.objects.filter(id__in = venue_admin, is_active = True)
				adminpermission = ChildPermissions.objects.filter(slug__in = ADMIN_PERMISSIONS)
				print('admin permission', adminpermission)
				print('112121212121212121',VenueAdminPermissionMap.objects.filter(user = buser, admin = user).delete())
				for v in venues:
					print('venue', v)
					admin = VenueAdminPermissionMap.objects.create(user = buser, venue = v, admin = user)
					admin.permission.add(*adminpermission)
					print('admin',admin)
					admin.save()
				#send_otp(admin.user.mobile, 'You Have Requested To Add New Admin')
				#send_otp('8800365323', '{0} has sent request to add new admin: Name of new admin: {1}, Phone Number: {2}, email: {3}'.format(admin.user.name, admin.admin.name, admin.admin.mobile, admin.admin.email))

			if venue_manager:
				venue_manager = set(venue_manager) - set(venue_admin)
				venues = Venue.objects.filter(id__in = venue_manager)
				managerpermission = ChildPermissions.objects.filter(slug__in = MANAGER_PERMISSIONS)
				print('permissions',managerpermission)
				print('112121212121212121',VenueManagerPermissionMap.objects.filter(user = buser, manager = user).delete())
				for v in venues:
					print('venue',v)
					manager = VenueManagerPermissionMap.objects.create(user = buser, venue = v, manager = user)
					manager.permission.add(*managerpermission)
					print('manager',manager)
					manager.save()
				#send_otp(manager.user.mobile, 'You Have Requested To Add New Manager')
				#send_otp('8800365323', '{0} has sent request to add new admin: Name of new admin: {1}, Phone Number: {2}, email: {3}'.format(manager.user.name, manager.manager.name, manager.manager.mobile, manager.manager.email))
			tour_qs= Tournament_Detail.objects.filter(Q(is_active= True) & Q(admin= buser))
			if tour_admin:
				print("tour_admin-->", tour_admin)
				for obj in tour_qs:
					if obj.id in tour_admin:
						obj.admin.add(user)
						print("inside if",obj, user)
					else:
						obj.admin.remove(user)
						print(obj, user)
					obj.save()
			if tour_manager:
				tour_manager= set(tour_manager) - set(tour_admin)#tour_manager - tour_admin
				print("tour_manager-->",tour_manager)
				for obj in tour_qs:
					if obj.id in tour_manager:
						obj.manager.add(user)
					else:
						obj.manager.remove(user)
					obj.save()
			return Response({'success':True})

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)


class VenuePermissionApi(APIView):

	def get(self, request):
		try:
			venue_id = request.GET.get('venue_id')
			buser = request.buser

			admin = VenueAdminPermissionMap.objects.filter(admin = buser, venue__id = venue_id, is_active = True)
			v = Venue.objects.get(id= venue_id)
			p_dict = {'venue_id':v.id, 'venue_name':v.name}
			for a in admin:
				permissions = a.permission.all()
				for p in permissions:
					p_dict.update({p.slug.replace('-',''):True})
				keys = [key for key, items  in p_dict.items()]
				for i in ADMIN_PERMISSIONS:
					if i.replace('-','') not in keys:
						p_dict.update({i.replace('-',''):False})

			manager = VenueManagerPermissionMap.objects.filter(manager  = buser, venue__id = venue_id, is_active = True)
			for m in manager:
				permissions = m.permission.all()
				print(permissions)
				for p in permissions:
					p_dict.update({p.slug.replace('-',''):True})
				keys = [key for key, items  in p_dict.items()]
				for i in ADMIN_PERMISSIONS:
					if i.replace('-','') not in keys:
						p_dict.update({i.replace('-',''):False})
			p_dict.update({'success':True})
			print(p_dict)
			return Response(p_dict, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)
