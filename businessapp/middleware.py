from businessapp.models import BusinessUser, AccessToken
from rest_framework.response import Response
from django.http import HttpResponse
import jwt
import traceback
from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from datetime import datetime

class customMiddleware(MiddlewareMixin):

    def process_request(self, request):
        
        try:
    #        print(request.META['HTTP_AUTHORIZATION'])
            access_token = request.META['HTTP_AUTHORIZATION']
   #         print('1')
            access_token = access_token.split(' ')[1].encode('ascii')
            data = jwt.decode(access_token, settings.PRIVATE_KEY, algorithms = ['HS256'])
            request.buser= BusinessUser.objects.get(pk=data['user_id'])
  #          print(access_token.decode('ascii'))

            try:
                time = data.get('time')
 #               print(time,'>>>>>>>>>>>>>')
                if datetime.strptime(time, "%Y-%m-%d %I:%M %p") > datetime.now():
                    return HttpResponse({'error':'Authorization', 'success':False}, status = 401)                


            except:
                traceback.print_exc()
#            print(request.buser)
        except KeyError:
            print('key error')
            request.buser = None
        except Exception:
            #traceback.print_exc()        
            request.buser= None
            # print exception.message
        return None
