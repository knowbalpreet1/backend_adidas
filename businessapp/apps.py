from django.apps import AppConfig


class BusinessappConfig(AppConfig):
    name = 'businessapp'
    def ready(self):
    	import businessapp.signals