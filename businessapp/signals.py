from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import BusinessUser
from authentication.async_task import send_otp
from venue.models import VenueAdminPermissionMap, VenueManagerPermissionMap
from notification.bviews import send_notification
from datetime import datetime

@receiver(post_save, sender=BusinessUser)
def notify(sender, instance, created, **kwargs):
	print(instance._is_active, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
	if not instance._is_active and instance.is_active:
		#instance.set_password('{0}{1}'.format(instance.name, instance.mobile))
		# instance.default_password = '{0}{1}'.format(instance.name, instance.mobile)
		print('inside 1>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
		send_otp(instance.mobile, 'Congratulations, your GOPLAYBOOK Business account is active, Login using password : {} . This password will expire after 24 hrs so make sure you login within time.'.format(instance.default_password))
		instance.password_sent_time = datetime.now()
		if created:
			instance.save()
		admin = VenueAdminPermissionMap.objects.filter(admin = instance)
		print('instance', admin, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
		if admin:
			for i in admin:
				send_otp(i.user.mobile, 'Dear {0}, new admin: {1} is added for the venue: {2}'.format(i.user.name, i.admin.name, i.venue.name))
				message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, new admin: {1} is added for the venue: {2}'.format(i.user.name, i.admin.name, i.venue.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.UserManagement",
				}
				print(message_body)
				send_notification([i.user], message_body)
		manager = VenueManagerPermissionMap.objects.filter(manager = instance)
		if manager:
			for i in manager:
				send_otp(i.user.mobile, 'Dear {0}, new manager: {1} is added for the venue: {2}'.format(i.user.name, i.manager.name, i.venue.name))
				message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, new manager: {1} is added for the venue: {2}'.format(i.user.name, i.manager.name, i.venue.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.UserManagement",
				}

				send_notification([i.user], message_body)
	
	if instance._is_active and not instance.is_active:
		print('inside 2')
		send_otp('8822242224', '{} User Delete Request'.format(instance.name))
		admin = VenueAdminPermissionMap.objects.filter(admin = instance)
		if admin:
			for i in admin:
				send_otp(i.admin.mobile, 'Dear {0}, you have been removed for the venue: {1}'.format(i.admin.name, i.venue.name))
				message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, you have been removed for the venue: {1}'.format(i.admin.name, i.venue.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.UserManagement",
				}

				send_notification([i.user],message_body)
				send_otp(i.user.mobile, 'Dear {0}, admin: {1} have been removed for the venue: {2}'.format(i.user.name, i.admin.name, i.venue.name))
				message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, admin: {1} have been removed for the venue: {2}'.format(i.user.name, i.admin.name, i.venue.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.UserManagement",
				}

				send_notification([i.user], message_body)
		manager = VenueManagerPermissionMap.objects.filter(manager = instance)
		if manager:
			for i in manager:
				send_otp(i.manager.mobile, 'Dear {0}, you have been removed for the venue: {1}'.format(i.manager.name, i.venue.name))
				message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, you have been removed for the venue: {1}'.format(i.manager.name, i.venue.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.UserManagement",
				}

				send_notification([i.user], message_body)
				send_otp(i.user.mobile, 'Dear {0}, manager: {1} have been removed for the venue: {2}'.format(i.user.name, i.manager.name, i.venue.name))
				message_body = {
				"title":"GoPlayBook",
				"body":'Dear {0}, manager: {1} have been removed for the venue: {2}'.format(i.user.name, i.manager.name, i.venue.name), 
				"click_action":"fcm.ACTION.HELLO",
				"page":"example.UserManagement",
				}

				send_notification([i.user],message_body)

	if not instance.is_active and not instance._is_active:
		if instance.intrested_in == 'V':
			send_otp('8822242224', 'New user has sent request. Name: {0}, Phone Number: {1}, Email: {2}, Intrested_in: {3}'.format(instance.name, instance.mobile, instance.email, 'Venue'))
		elif instance.intrested_in == 'T':
			send_otp('8822242224', 'New user has sent request. Name: {0}, Phone Number: {1}, Email: {2}, Intrested_in: {3}'.format(instance.name, instance.mobile, instance.email, 'Tournament'))
		elif instance.intrested_in == 'B':
			send_otp('8822242224', 'New user has sent request. Name: {0}, Phone Number: {1}, Email: {2}, Intrested_in: {3}'.format(instance.name, instance.mobile, instance.email, 'Venue and Tournament')) 


@receiver(post_save, sender=VenueAdminPermissionMap)
def notifysuperadmin(sender, instance, created, **kwargs):
	if not instance.admin._is_active and not instance.admin.is_active:
		print(1)
		send_otp('8822242224', '{0} has sent request to add new admin: Name of new admin: {1}, Phone Number: {2}, email: {3}'.format(instance.user.name, instance.admin.name, instance.admin.mobile, instance.admin.email))


@receiver(post_save, sender=VenueManagerPermissionMap)
def notifysupermanager(sender, instance, created, **kwargs):
	if not instance.manager._is_active and not instance.manager.is_active:
		print(2)
		send_otp('8822242224', '{0} has sent request to add new manager: Name of new manager: {1}, Phone Number: {2}, email: {3}'.format(instance.user.name, instance.manager.name, instance.manager.mobile, instance.manager.email))
