from django.conf.urls import include, url
from django.contrib import admin
from .views  import SignUpApi, LoginApi, ForgotPasswordApi, ChangePasswordApi, UserManagementApi, UserManagementListApi, UserProfileApi, ContactUsApi, RefreshTokenApi, UserManagement, VenuePermissionApi

urlpatterns = [
	url(r'^signup/$', SignUpApi.as_view(), name = "AdminSignUp"),
	url(r'login/$', LoginApi.as_view(), name = "AdminLogin"),
	url(r'^forgotpassword/$', ForgotPasswordApi.as_view(), name = "AdminForgotPassword"),
	url(r'^changepassword/$', ChangePasswordApi.as_view(), name = "AdminChangePassword"),
	url(r'^usermanagement/$', UserManagementApi.as_view(), name = "UserManagement"),
	url(r'^usermanagementlist/$', UserManagementListApi.as_view(), name = "UserManagementList"),
	url(r'^userprofile/$', UserProfileApi.as_view(), name = "userprofile"),
	url(r'^contactus/$', ContactUsApi.as_view(), name = 'contactus'),
	url(r'^refresh_token/', RefreshTokenApi.as_view(), name = 'refresh_token'),
	url(r'^user/', UserManagement.as_view(), name = 'load_user_data'),
	url(r'venuepermission/', VenuePermissionApi.as_view(), name = "venuepermission"),
]
