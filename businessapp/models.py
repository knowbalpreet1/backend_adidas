from __future__ import unicode_literals
from oauth2_provider.generators import generate_client_id, generate_client_secret
from django.contrib.auth.hashers import get_hasher, make_password
from GoPlayBook.settings import PRIVATE_KEY
from django.db import models
from django.utils import timezone
from datetime import timedelta, datetime

from authentication.async_task import send_otp


# Create your models here.
class BusinessUser(models.Model):
	name= models.CharField(max_length= 100)
	email= models.CharField(max_length= 100, unique = True)
	mobile= models.CharField(max_length= 100, unique = True)
	password= models.CharField(max_length= 256	)
	default_password = models.CharField(max_length = 100, null = True, blank = True)
	password_sent_time = models.DateTimeField(null = True, blank = True)
	INTEREST_CHOICE = (
			('V', 'Venue'),
			('T', 'Tournament'),
			('B', 'Both')
		)
	intrested_in = models.CharField(choices = INTEREST_CHOICE, max_length = 12, null = True, blank = True)
	is_active = models.BooleanField(default = False) 

	def __init__(self, *args, **kwargs):
		super(BusinessUser, self).__init__(*args, **kwargs)
		
		self.__is_active = self.is_active
		self._is_active = self.is_active
		#send_otp('8946984043', 'User Create Request')


	def check_password(self, password):
		hasher = get_hasher()
		salt = PRIVATE_KEY
		encoded = hasher.encode(password, salt)
		print(encoded)
		if encoded == self.password:
			return True
		else:
			return False

	def set_password(self, raw_password):
		self.password = make_password(raw_password, salt = PRIVATE_KEY)
		return self.password

	def __str__(self):
		return self.name 




class Application(models.Model):
	"""
	An Application instance represents a Client on the Authorization server.
	Usually an Application is created manually by client's developers after
	logging in on an Authorization Server.
	Fields:
	* :attr:`client_id` The client identifier issued to the client during the
						registration process as described in :rfc:`2.2`
	* :attr:`BusinessUser` ref to a Django BusinessUser
	* :attr:`client_secret` Confidential secret issued to the client during
							the registration process as described in :rfc:`2.2`
	* :attr:`name` Friendly name for the Application
	"""
	client_id = models.CharField(
		max_length=100, unique=True, default=generate_client_id, db_index=True
	)
	user = models.ForeignKey(
		BusinessUser,
		null=True, blank=True, on_delete=models.CASCADE
	)

	client_secret = models.CharField(
		max_length=1000, blank=True, default=generate_client_secret, db_index=True
	)

	created = models.DateTimeField(auto_now_add=True)


	def __str__(self):
		return self.client_id


class AccessToken(models.Model):
	"""
	An AccessToken instance represents the actual access token to
	access BusinessUser's resources, as in :rfc:`5`.
	Fields:
	* :attr:`BusinessUser` The Django BusinessUser representing resources' owner
	* :attr:`token` Access token
	* :attr:`application` Application instance
	* :attr:`expires` Date and time of token expiration, in DateTime format
	"""
	user = models.ForeignKey(BusinessUser, on_delete=models.CASCADE, blank=True, null=True)
	access_token = models.CharField(max_length=100000, unique=True)
	refresh_token = models.CharField(max_length=100000, unique=True)
	application = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True)
	expires = models.DateTimeField()
	created = models.DateTimeField(auto_now_add=True)

	def is_valid(self, scopes=None):
		"""
		Checks if the access token is valid.
		"""
		return not self.is_expired()

	def is_expired(self):
		"""
		Check token expiration with timezone awareness
		"""
		if not self.expires:
			return True

		return timezone.now() >= self.expires


	def revoke(self):
		"""
		Convenience method to uniform tokens' interface, for now
		simply remove this token from the database in order to revoke it.
		"""
		self.delete()


	def __str__(self):
		return self.access_token



class BasePermissions(models.Model):
	name = models.CharField(max_length = 100)
	is_active = models.BooleanField(default = True)

	def __str__(self):
		return self.name


class ChildPermissions(models.Model):
	name = models.CharField(max_length = 100)
	parent = models.ForeignKey(BasePermissions)
	slug = models.SlugField(max_length = 200)
	is_active = models.BooleanField(default = True)

	def __str__(self):
		return self.name




class Options(models.Model):
	name = models.CharField(max_length = 200)
	is_active = models.BooleanField(default = True)
	
	def __str__(self):
		return self.name


def get_mobile(instance):
	return self.mobile

class ContactUs(models.Model):
	option = models.ForeignKey(Options)
	user = models.ForeignKey(BusinessUser)
	mobile = models.CharField(max_length = 10, null = True, blank = True)
	comment = models.CharField(max_length = 1000)

	def __str__(self):
		return self.user.name+' -----> '+self.option.name
	
	def __init__(self, *args, **kwargs):
		super(ContactUs, self).__init__(*args, **kwargs)
		self.mobile = self.user.mobile
