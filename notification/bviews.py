from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.utils.timezone import now, timedelta
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.timesince import timesince
from authentication.models import Account
from authentication.views import send_OTP
from oauth2_provider.models import Application
from businessapp.models import BusinessUser
from .models import BusinessPushNotifyTokens, BusinessNotification
import traceback
import requests
import json
from datetime import datetime
from threading import Thread
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.conf import settings
import logging
import time

URL = "https://fcm.googleapis.com/fcm/send"
HEADERS = {"Content-Type": "application/json", "Authorization": "Key="+settings.BNOTIFY}

class Business_Register_Device_View(APIView):
	"""This view is used for register a device on server"""

	def post(self, request):
		requested_user = request.buser
		print(request.data, '>>>>>>>>>>>>>>>>>>')
		#playerID = request.data['playerID']
		if not requested_user:
			print('not user', requested_user, '>>>>>>>>>>>>>>>>>>>')
			return Response({'error':'Unknown User', 'success':False}, status = 400)
		data = request.data
		print(data,'>>>>>>>>>>>>>>>>>>')
		device_id = data.get('device_id')
		fcm_token = data.get('fcm_token')
		if not fcm_token:
			print('not fcm_token', fcm_token)
			return HttpResponse(json.dumps({"status":False, "message":"Please provide fcm token"}), status=400)
		device_type = data.get('device_type')
		is_token = False
		try:
			fcm_ = BusinessPushNotifyTokens.objects.get(device_id=device_id)
			print(fcm_, '>>>>>>>>>>>>>>>#################')
			fcm_.fcm_token = fcm_token
			fcm_.is_active = True
			fcm_.save()
			is_token = True
		except Exception as e:
			traceback.print_exc()
			logging.error(e)
			pass
		if not is_token:
			print('1232323????????????????????????????????')
			try:
				b = BusinessPushNotifyTokens.objects.create(user = requested_user, fcm_token=str(fcm_token).strip(), device_type=device_type, device_id=device_id)
				b.save()
				print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
			except Exception as e:
				traceback.print_exc()
				logging.error(e)
				return HttpResponse(json.dumps({"status":False, "message":"something goes wrong. please try again", "err":str(e)}), status=400)
		return HttpResponse(json.dumps({"status":True, "message":"Your device has been successfully register"}))

	def delete(self, request):
		data = request.data
		device_id = request.GET.get('device_id')
		if device_id == None or not device_id:
			fcm_token = request.GET.get('fcm_token')
			try:
				BusinessPushNotifyTokens.objects.get(fcm_token=str(fcm_token).strip()).delete()
				return HttpResponse(json.dumps({"status":True, "message":"Your device has been deleted"}))
			except:
				return HttpResponse(json.dumps({"status":False, "message":"fcm_token does not exist"}), status=400)
		else:
			try:
				BusinessPushNotifyTokens.objects.get(device_id=device_id).delete()
				return HttpResponse(json.dumps({"status":True, "message":"Your device has been deleted"}))
			except:
				return HttpResponse(json.dumps({"status":False, "message":"device_id does not exist"}), status=400)
	def put(self, request):
		return HttpResponse(json.dumps({"status":True}))



def send_notification(userList, message):
	"""This method is send notification on specific player's device"""
	try:

#		message_body={
#			"to":"some_device_token",
#			"content_available": true,
#			"notification": {
#				"title": "hello",
#				"body": "yo",
#				"click_action": "fcm.ACTION.HELLO"
#				},
#			"data": {
#			"extra":"juice"
#			}
#		}
		#message_body = {"title":"GoPlayBook", "body":message, "click_action":"fcm.ACTION.HELLO"}
		user_token_android = BusinessPushNotifyTokens.objects.filter(user__in=userList, device_type='A', is_active=True, is_notify = True)
		user_token_ios = BusinessPushNotifyTokens.objects.filter(user__in=userList, device_type='I', is_active=True, is_notify = True)
		print(user_token_android,">>>>>>>>>>>>>>>>>>>>>>>", user_token_ios, '>>>>>>>>>>>>>>>>>>>')
		if user_token_android.count():
			logging.warning("android device")
			fcm_tokens = [i.fcm_token for i in user_token_android]
			print(message, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
			body={
				"registration_ids": fcm_tokens,
				"priority": "high",
				"content_available": True,
				"notification":message,
				"data":message
				}
			output = requests.post(url=URL, data=json.dumps(body), headers= HEADERS)
			print(output.text, '>>>>>>>>>>>>>>>>>>>>>>')
			logging.warning(output.text)
		if user_token_ios.count():
			logging.warning("ios device")
			message.update({"sound":"default","mutable_content":True,})
			fcm_tokens = [i.fcm_token for i in user_token_ios]
			body = {
				"registration_ids": fcm_tokens,
				"notification":message,
				"priority": "high",
				"data": {}
			}
			#message_body.update(body)
			print(body)
			output = requests.post(url=URL, data=json.dumps(body), headers= HEADERS)
			logging.warning(output.text)
		print("blank")
	except:
		traceback.print_exc()





class NotificationApi(APIView):

	def get(self,request):
		try:
			notifications = None
			if request.buser:
				notifications = BusinessNotification.objects.filter(user=request.buser).order_by('-sent_time')
			elif not request.user.is_anonymous:
				notifications = BusinessNotification.objects.filter(player__user = request.user).order_by('-sent_time')
			result = []
			for n in notifications:
				pic = None
				if n.player and n.player.user.profile_pic:
					pic = n.player.user.profile_pic.url

				result.append({'message':n.message, 'time':timesince(n.sent_time).split(' ', 2)[0].replace(',', '')+' ago', 'id':n.id, 'profile_pic':pic})
			#print(result)
			paginator = Paginator(result, 10)
			page = request.GET.get('page')
			try:
				data = paginator.page(page)
			except PageNotAnInteger:
				data = paginator.page(1)
			except EmptyPage:
				data = paginator.page(paginator.num_pages)
			except Exception as e:
				traceback.print_exc()
				pass
			print(data)
			return Response({'data':data.object_list, 'pages':paginator.num_pages, 'success':True}, status = 200)


		except Exception as  e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status =400 )



class NotificationSwitchApi(APIView):

	def get(self, request):
		try:
			print(request.buser)
			b = BusinessPushNotifyTokens.objects.filter(user = request.buser, is_active = True)
			print(b)
			return Response({'is_notify':b[0].is_notify, 'success':True}, status = 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)

	def post(self, request):
		try:
			notify = request.data.get('notify')
#			print(type(notify) , (not notify))
			BusinessPushNotifyTokens.objects.filter(user = request.buser, is_active = True).update(is_notify = notify )
			return Response({'success':True}, status= 200)
		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)
