from django.conf.urls import url, include
from notification import views, bviews

urlpatterns = [
	#url(r'^userregister/$', bviews.Business_Register_Device_View.as_view(),name="register_token_business"),
	url(r'^register/$', views.Register_Device_View.as_view(),name="register_token"),
	url(r'^userregister/$', bviews.Business_Register_Device_View.as_view(),name="register_token_business"),
	url(r'^notificationlist/', bviews.NotificationApi.as_view(), name = "notification api"),
	url(r'^switch/$', bviews.NotificationSwitchApi.as_view(), name = "notification switch"),
	url(r'^usernotification/', views.UserNotificationApi.as_view(), name = "user notification")
	]
