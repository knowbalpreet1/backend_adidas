from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.utils.timezone import now, timedelta
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.timesince import timesince

from authentication.models import Account
from authentication.views import send_OTP
from oauth2_provider.models import Application
from players.models import Players
from .models import PushNotifyTokens, UserNotification
import traceback
import requests
import json
from datetime import datetime
from threading import Thread
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.conf import settings
import logging
import time

URL = "https://fcm.googleapis.com/fcm/send"
HEADERS = {"Content-Type": "application/json", "Authorization": "Key="+settings.SERVER_KEY}

class Register_Device_View(APIView):
	"""This view is used for register a device on server"""

	def post(self, request):
		requested_user = request.user
		print(request.data, requested_user)
		#playerID = request.data['playerID']
		try:
			player = Players.objects.get(user=requested_user, is_active=True)
		except Exception as e:
			traceback.print_exc()
			return HttpResponse(json.dumps({"status":False,"message":"player does not exist"}), status=400)
		data = request.data
		device_id = data.get('device_id')
		fcm_token = data.get('fcm_token')
		if not fcm_token:
			return HttpResponse(json.dumps({"status":False, "message":"Please provide fcm token"}), status=400)
		device_type = data.get('device_type')
		is_token = False
		try:
			fcm_ = PushNotifyTokens.objects.get(device_id=device_id)
			print(fcm_)
			fcm_.fcm_token = fcm_token
			fcm_.is_active = True
			fcm_.save()
			is_token = True
		except Exception as e:
			traceback.print_exc()
			logging.error(e)
			pass
		if not is_token:
			try:
				p = PushNotifyTokens.objects.create(player=player, fcm_token=str(fcm_token).strip(), device_type=device_type, device_id=device_id)
				print(p,'>>>>>>>>>>>>>>>>>>>>')
			except Exception as e:
				logging.error(e)
				traceback.print_exc()
				return HttpResponse(json.dumps({"status":False, "message":"something goes wrong. please try again", "err":str(e)}), status=400)
		return HttpResponse(json.dumps({"status":True, "message":"Your device has been successfully register"}))

	def delete(self, request):
		data = request.data
		device_id = data.get('device_id')
		if device_id == None:
			fcm_token = data.get('fcm_token')
			try:
				PushNotifyTokens.objects.get(fcm_token=str(fcm_token).strip()).delete()
				return HttpResponse(json.dumps({"status":True, "message":"Your device has been deleted"}))
			except:
				traceback.print_exc()
				return HttpResponse(json.dumps({"status":False, "message":"fcm_token does not exist"}), status=400)
		else:
			try:
				print ("yyyyyy",device_id)
				PushNotifyTokens.objects.get(device_id=device_id).delete()
				return HttpResponse(json.dumps({"status":True, "message":"Your device has been deleted"}))
			except:
				traceback.print_exc()
				return HttpResponse(json.dumps({"status":False, "message":"device_id does not exist"}), status=400)
	def put(self, request):
		return HttpResponse(json.dumps({"status":True}))



def send_notification(input_list):
	"""This method is send notification on specific player's device"""
	try:
		playerList, message_body = input_list
		print(playerList, '------->', message_body, '>>>>>>>>>>>>>>>>>')
		player_token_android = PushNotifyTokens.objects.filter(player__in=playerList, device_type='A', is_active=True)
		player_token_ios = PushNotifyTokens.objects.filter(player__in=playerList, device_type='I', is_active=True)
		print(player_token_ios, player_token_android)
		if player_token_android.count():
			logging.warning("android device")
			fcm_tokens = [i.fcm_token for i in player_token_android]
			body={
				"registration_ids": fcm_tokens,
				"priority": "high",
				"content_available": True,
				"data": message_body
				}
			output = requests.post(url=URL, data=json.dumps(body), headers= HEADERS)
			logging.warning(output)
			print(output.text, '++++++++++++++====================+++')
		if player_token_ios.count():
			logging.warning("ios device")
			message_body.update({"sound":"default","mutable_content":True,})
			fcm_tokens = [i.fcm_token for i in player_token_ios]
			body = {
				"registration_ids": fcm_tokens,
				"notification":message_body,
				"priority": "high",
				"data": {
					"profile_pic":message_body['profile_pic']
				}
			}
			#message_body.update(body)
			print(body)
			output = requests.post(url=URL, data=json.dumps(body), headers= HEADERS)
			print(output.text, '+++++++++=========+++++++++++++++')
			logging.warning(output.text)
		print("blank")
	except:
		traceback.print_exc()


def save_notification(player, instance, notification_type, message, activity_id = None):
	try:
		content_type = ContentType.objects.get_for_model(instance)
		obj_id = instance.id
		print(instance.id,'-------', notification_type, '----------', instance,'-----', message, '------------', activity_id)
		obj = UserNotification(
							content_type = content_type,
							object_id = obj_id,
							player = player,
							message = message,
							notification_type = notification_type,
							activity_id = activity_id
							)
		obj.save()

	except Exception as e:
		traceback.print_exc()
		pass


class UserNotificationApi(APIView):
	permission_classes = [TokenHasReadWriteScope,]
	def get(self, request):
		try:
			user = request.user
			player = Players.objects.get(user = user)
			nots = UserNotification.objects.filter(player = player).order_by('-sent_time')
			result= []
			for no in nots:
				if not no.content_object:
					print('before continue')
					continue
				pic = None
				nt = no.notification_type
				if nt == 'E' or nt == 'P':
					if no.content_object:
						if no.content_object.user.profile_pic:
							pic = no.content_object.user.profile_pic.url
				if nt == 'T':
					if no.content_object:
						if no.content_object.team_pic:
							pic = no.content_object.team_pic.url

				if no.content_object:
					action = None
					if "followed" in no.message:
						action = "following"
					elif "added to the team" in no.message:
						action = "add_player"
					elif "removed as an admin" in no.message:
						action = "remove_admin"
					elif "removed from" in no.message:
						action = "remove_player"
					elif "now the admin" in  no.message:
						action = "add_admin"
					elif "hosting the event" in no.message:
						#print(no.object_id, '+++++++')
						action = "event_host"
					elif "contributor" in no.message :
						print(no.object_id, '>>>>>')
						action = "event_contributor"
					elif "commented" in no.message:
						#print(no.object_id, '------->')
						action = "event_comment"
					obj_id = no.object_id
					if nt == 'E':
						obj_id = no.activity_id
					result.append({
								'message':no.message,
								'time':timesince(no.sent_time).split(' ', 2)[0].replace(',', '')+' ago',
								'id':obj_id,
								'profile_pic':pic,
								'action':action
								})

			paginator = Paginator(result, 10)
			page = request.GET.get('page')
			try:
				data = paginator.page(page)
			except PageNotAnInteger:
				data = paginator.page(1)
			except EmptyPage:
				data = paginator.page(paginator.num_pages)
			except Exception as e:
				traceback.print_exc()
				pass
			print(data.object_list)


			return Response({'data':data.object_list, 'pages':paginator.num_pages, 'success':True}, status = 200)

		except Exception as e:
			traceback.print_exc()
			return Response({'error':str(e), 'success':False}, status = 400)
