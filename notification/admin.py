from django.contrib import admin
from .models import *

admin.site.register(PushNotifyTokens)
admin.site.register(BusinessPushNotifyTokens)
admin.site.register(BusinessNotification)
admin.site.register(UserNotification)
