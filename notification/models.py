from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from players.models import Players
from businessapp.models import BusinessUser
class PushNotifyTokens(models.Model):

	Device_Type = (
		("A", "a"),
		("I", "i"))

	player = models.ForeignKey(Players)
	device_id = models.CharField(max_length=500, blank=True, null=True)
	device_type = models.CharField(max_length=1, choices=Device_Type, default="a")
	fcm_token = models.CharField(max_length=500, unique=True)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return (self.player.user.name)




class BusinessPushNotifyTokens(models.Model):

	Device_Type = (
		("A", "a"),
		("I", "i"))

	user = models.ForeignKey(BusinessUser)
	device_id = models.CharField(max_length=500, blank=True, null=True)
	device_type = models.CharField(max_length=1, choices=Device_Type, default="a")
	fcm_token = models.CharField(max_length=500, unique=True)
	is_active = models.BooleanField(default=True)
	is_notify = models.BooleanField(default = True)

	def __str__(self):
		return (self.user.name)



class BusinessNotification(models.Model):

	user = models.ForeignKey(BusinessUser, null = True, blank = True)
	player = models.ForeignKey(Players, null = True, blank = True)
	message = models.CharField(max_length = 1000)
	sent_time = models.DateTimeField(auto_now_add = True)
	def __str__(self):
		if self.user:
			return self.user.name+ ' -----> ' + self.message
		elif self.player:
			return self.player.user.name+' -----> '+self.message



class UserNotification(models.Model):
	CHOICES = (
			('E', 'Event'),
			('TO', 'Tournament'),
			('T', 'Team'),
			('P', 'Profile'),
			('M', 'Matches')
		)
	player = models.ForeignKey(Players, null = True, blank = True)
	message = models.CharField(max_length = 1000)
	notification_type = models.CharField(max_length = 3, choices = CHOICES, default = None)
	sent_time = models.DateTimeField(auto_now_add = True)
	content_type = models.ForeignKey(ContentType, on_delete = models.CASCADE)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')
	activity_id = models.PositiveIntegerField(null = True, blank = True)

	def __str__(self):
		return self.player.user.name

		
