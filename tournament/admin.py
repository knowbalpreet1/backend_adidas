from django.contrib import admin
from tournament.models import Tournament_Detail,Media, Teamowner ,GameWeek_Detail, GameWeek_Matches ,Tournament_Round, Tournament_Group, Tournament_Round_Team_Map, Tournament_Group_Team_Map,Tournament_Image, Team_Registration, Tournament_Venue_Map, TournamentAdminPermissionMap, TournamentManagerPermissionMap, Tournament_Sponsor
from scorecard.models import Team_Match_Player
from django.db.models import Q
from players.models import Players
# Register your models here.
class Tournament_Round_Team_MapAdmin(admin.ModelAdmin):
    #search_fields = ('round_name',)
    list_display = ('tournament_round','team')

#class Tournament_DetailAdmin(admin.ModelAdmin):
#    def render_change_form(self, request, context, *args, **kwargs):
#        re= str(request)
#        tournament_id= re.split()[2].split('/')[4]
#        print("tournament_id--->", tournament_id)
#        qs= Team_Match_Player.objects.filter(Q(match_stat__tournament_round__isnull= False) & Q(match_stat__tournament_round__tournament__pk= tournament_id))
#        qs1= Team_Match_Player.objects.filter(Q(match_stat__tournament_group__isnull= False) & Q(match_stat__tournament_group__tournament_round__tournament__pk= tournament_id))
#        final_qs= qs | qs1
#        player_qs= Players.objects.filter( id__in= [obj.player.id for obj in final_qs])
#        context['adminform'].form.fields['man_of_the_series'].queryset = player_qs
#        return super(Tournament_DetailAdmin, self).render_change_form(request, context, *args, **kwargs)

admin.site.register(Tournament_Detail)#, Tournament_DetailAdmin)
admin.site.register(Tournament_Round)
admin.site.register(Tournament_Group)
admin.site.register(Tournament_Round_Team_Map, Tournament_Round_Team_MapAdmin)
admin.site.register(Tournament_Group_Team_Map)
admin.site.register(Tournament_Image)
admin.site.register(Team_Registration)
admin.site.register(GameWeek_Detail)
admin.site.register(GameWeek_Matches)
admin.site.register(Media)
admin.site.register(Teamowner)
admin.site.register(Tournament_Venue_Map)
admin.site.register(TournamentAdminPermissionMap)
admin.site.register(TournamentManagerPermissionMap)
admin.site.register(Tournament_Sponsor)
