from rest_framework import serializers
from tournament.models import Tournament_Detail, Tournament_Round, Tournament_Round_Team_Map, Tournament_Group, Tournament_Group_Team_Map, Team_Registration


class Tournament_Serializer(serializers.ModelSerializer):
	logo= serializers.SerializerMethodField()
	#cover_pic= serializers.SerializerMethodField()
	class Meta:
		model = Tournament_Detail
		fields = ('id', 'name', 'tag_line', 'logo', 'registration_start_date', 'registration_end_date', 'is_deactivated')

	def get_logo(self, obj):
		if obj.logo:
			return obj.logo.url
		else:
			return None

	def get_cover_pic(self, obj):
		if obj.cover_pic:
			return obj.cover_pic.url
		else:
			return None

class Tournament_Basic_Serializer(serializers.ModelSerializer):
	#competition_type= serializers.SerializerMethodField()
	sport_type= serializers.SerializerMethodField()
	registration_start_date= serializers.SerializerMethodField()
	registration_end_date= serializers.SerializerMethodField()
	start_date= serializers.SerializerMethodField()
	end_date= serializers.SerializerMethodField()

	class Meta:
		model = Tournament_Detail
		fields = ('id', 'name', 'description', 'registration_start_date', 'registration_end_date', 'start_date', 'end_date', 'tournament_format', 'sport_type')

#	def get_competition_type(self, obj):
#		if obj.competition_type:
#			return {'name': obj.competition_type.name, 'id':obj.competition_type.id}
#		else:
#			return None
#
	def get_sport_type(self, obj):
		if obj.sport_type:
			return {'name': obj.sport_type.name, 'id':obj.sport_type.id}
		else:
			return None
	
	def get_registration_start_date(self, obj):
		return obj.registration_start_date.strftime('%Y/%-m/%-d')

	
	def get_registration_end_date(self, obj):
		return obj.registration_end_date.strftime('%Y/%-m/%-d')

	
	def get_start_date(self, obj):
		return obj.start_date.strftime('%Y/%-m/%-d')

	
	def get_end_date(self, obj):
		return obj.end_date.strftime('%Y/%-m/%-d')

class Tournament_page2_Serializer(serializers.ModelSerializer):
	cover_pic= serializers.SerializerMethodField()
	logo= serializers.SerializerMethodField()
	city= serializers.SerializerMethodField()
	class Meta:
		model = Tournament_Detail
		fields = ('id', 'cover_pic', 'logo', 'email', 'mobile1', 'mobile2', 'city')

	def get_cover_pic(self, obj):
		if obj.cover_pic:
			return obj.cover_pic.url
		else:
			return None

	def get_logo(self, obj):
		if obj.logo:
			return obj.logo.url
		else:
			return None

	def get_city(self, obj):
		if obj.city:
			return {'id': obj.city.id, 'name':obj.city.name}
		else:
			return None

class Tournament_page3_Serializer(serializers.ModelSerializer):
	class Meta:
		model = Tournament_Detail
		fields = ('id', 'tournament_type', 'tournament_level', 'prize', 'reward', 'age_from', 'age_to', 'registration_fee', 'max_no_of_teams')

class Round_Basic_Info_Serializer(serializers.ModelSerializer):
	no_of_teams= serializers.SerializerMethodField()
	group= serializers.SerializerMethodField()
	class Meta:
		model = Tournament_Round
		fields = ('id', 'name', 'round_type', 'is_complete', 'no_of_teams', 'group')
	def get_no_of_teams(self, obj):
		no_of_team_count= Tournament_Round_Team_Map.objects.filter(tournament_round= obj).count()
		return no_of_team_count
	def get_group(self, obj):
		if obj.round_type == "group":
			return Tournament_Group.objects.filter(tournament_round= obj).count()
		else:
			return 0

class Tournament_Group_Serializer(serializers.ModelSerializer):
	team= serializers.SerializerMethodField()
	class Meta:
		model = Tournament_Group
		fields = ('id', 'name', 'team')

	def get_team(self, obj):
		return Tournament_Group_Team_Map.objects.filter(tournament_group= obj).count()

class ResultSerializer(serializers.ModelSerializer):
	image= serializers.SerializerMethodField()
	name= serializers.SerializerMethodField()
	class Meta:
		model = Tournament_Round_Team_Map
		fields = ('name', 'image', 'points', 'match_played', 'won', 'lost', 'ties', 'total_run', 'runs_against')

	def get_name(self, obj):
		return obj.team.name
		
	def get_image(self, obj):
		if obj.team.team_pic:
			return obj.team.team_pic.url
		else:
			return None

class TournamentListSerializer(serializers.ModelSerializer):
	cover_pic = serializers.SerializerMethodField()
	sport_type = serializers.SerializerMethodField()
	city = serializers.SerializerMethodField()
	team = serializers.SerializerMethodField()

	class Meta:
		model = Tournament_Detail
		fields = ('id','name', 'prize', 'sport_type', 'cover_pic', 'city', 'registration_start_date', 'registration_end_date', 'start_date', 'end_date', 'team')

	def get_cover_pic(self, obj):
		if obj.cover_pic:
			return obj.cover_pic.url
		else:
			return None
		
	def get_sport_type(self, obj):
		if obj.sport_type:
			return obj.sport_type.name
		else:
			return ""

	def get_city(self, obj):
		if obj.city:
			return obj.city.name
		else:
			return ""

	def get_team(self, obj):
		team_list=[]
		qs= Team_Registration.objects.filter(tournament=obj,is_approved=True).order_by('-registration_date')[:3]
		for obj1 in qs:
			team_name = obj1.team.name
			if obj1.team.team_pic:
				team_logo = obj1.team.team_pic.url
			else:
				team_logo = None
			team_list.append({'team_name':team_name,'team_logo':team_logo})
		return team_list
