from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from scorecard.models import Team_Match_Player
from players.models import Players, TeamPlayers, TeamAdmin
from tournament.models import Tournament_Detail , Team_Registration
from authentication.async_task import send_otp
import traceback

#@receiver(pre_save, sender=Tournament_Detail)
#def tournament_signal(sender, instance, **kwargs):
	#print("in signals",instance.man_of_the_series.id)
	#player= instance.man_of_the_series.player
	#instance.man_of_the_series= player
	#instance.save() 
	
@receiver(post_save, sender = Team_Registration)
def team_registration_signal(sender, instance, created, **kwargs):
	try:
		if created:
			team = instance.team
			team_players = [t.player.user.mobile for t in TeamAdmin.objects.filter(team = team, is_active = True)]
			print(team_players, '>>>>>>>>>>>>>>>>signals')
			for mobile in team_players:
				send_otp(mobile, "Congratulations, your team {0} is competing in Tournament {1}. Let's Play!".format(team.name, instance.tournament.name))
	except:
		traceback.print_exc()



def create_fixture_notify(match):
	try:
		team1 = match.team1
		team2 = match.team2
		round_name = match.tournament_round.name
		venue = None
		if match.tournament_venue.is_dummy:
			venue = match.tournament_venue.court_name
		else:
			venue = match.tournament_venue.court.venue.name
		team1_admin = [t.player.user.mobile for t in TeamAdmin.objects.filter(team = match.team1, is_active = True)]
		team2_admin = [t.player.user.mobile for t in TeamAdmin.objects.filter(team = match.team2, is_active = True)]
		print("In Signals----------------------")
		print(team1_admin, '-----', team2_admin)

		for t in team1_admin:
			send_otp(t, "Dear Team {0}, your fixture for round {1} has been decided. You will be playing {2} at {3}, {4}".format(team1.name, round_name, team2.name, venue, match.date_time.strftime('%Y-%m-%d %I:%M %p')))

		for t in team2_admin:
			send_otp(t, "Dear Team {0}, your fixture for round {1} has been decided. You will be playing {2} at {3}, {4}".format(team2.name, round_name, team1.name, venue, match.date_time.strftime('%Y-%m-%d %I:%M %p')))			

	except Exception as e:
		print("Except in Signals --------------------")
		traceback.print_exc()




def update_fixture_notify(match):
	try:
		team1 = match.team1
		team2 = match.team2
		round_name = match.tournament_round.name
		venue = None
		if match.tournament_venue.is_dummy:
			venue = match.tournament_venue.court_name
		else:
			venue = match.tournament_venue.court.venue.name
		team1_admin = [t.player.user.mobile for t in TeamAdmin.objects.filter(team = match.team1, is_active = True)]
		team2_admin = [t.player.user.mobile for t in TeamAdmin.objects.filter(team = match.team2, is_active = True)]

		print(team1_admin, '-----', team2_admin)

		for t in team1_admin:
			send_otp(t, "Dear Team {0}, your fixture for round {1} has been rescheduled. You will be playing {2} at {3}, {4}".format(team1.name, round_name, team2.name, venue, match.date_time.strftime('%Y-%m-%d %I:%M %p')))

		for t in team2_admin:
			send_otp(t, "Dear Team {0}, your fixture for round {1} has been rescheduled. You will be playing {2} at {3}, {4}".format(team2.name, round_name, team1.name, venue, match.date_time.strftime('%Y-%m-%d %I:%M %p')))			

	except Exception as e:
		traceback.print_exc()


def shift_team_notify(team_round):
	team = team_round.team
	# round_name = team_round.tournament_round.name
	tournament_name = team_round.tournament_round.tournament.name
	team_admin = [t.player.user.mobile for t in TeamAdmin.objects.filter(team = team, is_active = True)]
	print(team_admin)
	for t in team_admin:
		send_otp(t, "Congratulations, your team {0} has moved to the next round of Tournament {1} . Soon you will be provided with fixture information".format(team.name, tournament_name))

