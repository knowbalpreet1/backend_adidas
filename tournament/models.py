# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from authentication.models import Account
from players.models import Team, CompetitionType, City, Players, SportsType
from venue.models import Court
from businessapp.models import BusinessUser, BasePermissions, ChildPermissions
from geopy import units, distance
from django.utils import timezone
# Create your models here.

class geoManager(models.Manager):
    def nearby(self, latitude, longitude, proximity, min_d=0):
        latitude= float(latitude)
        longitude= float(longitude)
        rough_distance = units.degrees(arcminutes=units.nautical(kilometers=proximity)) * 2
        queryset = self.get_queryset().filter(city__isnull=False,
            city__latitude__range = (latitude - rough_distance, latitude + rough_distance),
            city__longitude__range =(longitude - rough_distance, longitude + rough_distance) ,
            is_deactivated=False,is_certified=True, is_active= True
            )

        locations = []
        for obj in queryset:
            if obj.city.latitude and obj.city.longitude:
                exact_distance = distance.distance(
                    (latitude, longitude),
                    (obj.city.latitude, obj.city.longitude)
                ).kilometers

                if exact_distance <= proximity and exact_distance > min_d:
                    locations.append(obj)
        queryset = queryset.filter(id__in=[l.id for l in locations])
        return queryset

def upload_to_logo(instance, filename):
	return 'tournament/logo/%s' % (filename)

def upload_to_cover_pic(instance, filename):
	return 'tournament/cover_pic/%s' % (filename)
def upload_to_rule(instance, filename):
	return 'tournament/rule/%s' % (filename)

class Tournament_Detail(models.Model):

	name =  models.CharField(max_length= 100)
	tag_line = models.CharField(max_length= 50, null= True, blank= True)
	description = models.TextField(null= True, blank= True)
	sport_type= models.ForeignKey(SportsType, blank= True, null= True)
	competition_type= models.ForeignKey(CompetitionType, blank= True, null= True)
	FORMAT = (('cup','cup'),('league','league'), ('knock-out', 'knock-out'))
	tournament_format = models.CharField(choices= FORMAT, max_length= 10, default= 'cup')
	registration_start_date = models.DateTimeField(null= True, blank= True)
	registration_end_date = models.DateTimeField(null= True, blank= True)
	start_date = models.DateTimeField(null= True, blank= True)
	end_date = models.DateTimeField(null= True, blank= True)

	email= models.CharField(max_length= 100, null= True, blank= True)
	mobile1= models.CharField(max_length= 10, null= True, blank= True)
	mobile2= models.CharField(max_length= 10, null= True, blank= True)
	city= models.ForeignKey(City, null= True, blank= True)

	GENDER = (('male', 'male'),('female', 'female'),('mix', 'mix'),)
	tournament_type = models.CharField(max_length=10, choices=GENDER, blank=True,null=True, default= 'male')
	LEVEL= (('corporate','corporate'),('mix-corporate','mix-corporate'), ('school', 'school'),('college','college'),('academy','academy'),('open-to-all','open-to-all'))#(('A','A'),('B','B'), ('C', 'C'))
	tournament_level= models.CharField(choices= LEVEL, max_length= 20, default= 'A')
	prize = models.CharField(blank= True, null= True, max_length= 500)
	reward= models.CharField(blank= True, null= True, max_length= 500)
	age_from= models.PositiveIntegerField(default=5)
	age_to= models.PositiveIntegerField(default=50)
	registration_fee = models.PositiveIntegerField(default= 0)
	max_no_of_teams = models.PositiveIntegerField(default= 0)

	logo = models.ImageField(upload_to=upload_to_logo, blank= True, null= True)
	cover_pic = models.ImageField(upload_to=upload_to_cover_pic, blank= True, null= True)

	admin = models.ManyToManyField(BusinessUser, blank=True, related_name= 'owner')
	manager = models.ManyToManyField(BusinessUser, blank=True, related_name= 'manager')
	is_active = models.BooleanField(default= True)
	is_published = models.BooleanField(default= False)

	rating = models.PositiveIntegerField(default= 0)
	rule = models.TextField(blank= True, null= True)
#	rule_file = models.FileField(blank= True, null= True)
	rule_file = models.FileField(upload_to= upload_to_rule, blank= True, null= True, )
	faq = models.FileField(upload_to= upload_to_rule, blank= True, null= True)
	is_round_created = models.BooleanField(default= False)
	is_certified = models.BooleanField(default= False)
	is_deactivated = models.BooleanField(default= False)
	created_by= models.ForeignKey(BusinessUser, blank= True, null= True)

	no_of_match= models.PositiveIntegerField(default=1, blank= True, null= True)
	man_of_the_series= models.ForeignKey(Players, blank= True, null= True)

	win_point= models.PositiveIntegerField(default= 3)
	loss_point= models.PositiveIntegerField(default= 0)
	tie_point= models.PositiveIntegerField(default= 1)
	abandoned_point= models.PositiveIntegerField(default= 0)

	gis_manager= geoManager()
	objects = models.Manager()

	class Meta:
		ordering = ['-id']

	def __str__(self):
		return self.name

class Tournament_Venue_Map(models.Model):
	tournament = models.ForeignKey(Tournament_Detail, null= True, blank= True)
	court = models.ForeignKey(Court, null= True, blank= True)
	court_name= models.CharField(blank= True, null= True, max_length= 100)
	is_dummy = models.BooleanField(default= False)
	is_active = models.BooleanField(default= True)
	address= models.CharField(blank= True, null= True, max_length= 200)
	pincode= models.IntegerField(blank= True, null= True, default=0)
	city= models.CharField(blank= True, null= True, max_length= 70)
	state= models.CharField(blank= True, null= True, max_length= 70)
	latitude= models.CharField(blank= True, null= True, max_length= 200)
	longitude= models.CharField(blank= True, null= True, max_length= 200)
	def __str__(self):
		if self.is_dummy:
			return self.tournament.name + ' ---> ' + self.court_name
		return self.tournament.name + ' ---> ' + self.court.venue.name

class Tournament_Image(models.Model):
	tournament = models.ForeignKey(Tournament_Detail, null= True, blank= True)
	image = models.ImageField(blank= True, null= True)
	position = models.PositiveIntegerField(blank= True, null= True)

	def __str__(self):
		return self.tournament.name

class Tournament_Sponsor(models.Model):
	tournament = models.ForeignKey(Tournament_Detail, null= True, blank= True)
	image = models.ImageField(blank= True, null= True)
	name = models.CharField(max_length=100, null= True, blank= True)
	is_active = models.BooleanField(default= True)
	created_by= models.ForeignKey(BusinessUser, blank= True, null= True)

	def __str__(self):
		return self.tournament.name

class Team_Registration(models.Model):

	tournament = models.ForeignKey(Tournament_Detail, null= True, blank= True)
	team = models.ForeignKey(Team, null= True, blank= True)
	paid_amount = models.PositiveIntegerField(default=0)
	PAY_OPTIONS= (('0', '0'), ('25','25'), ('50', '50'), ('100','100'))
	paid= models.CharField(choices= PAY_OPTIONS, max_length=3, default= '0')
	registered_by = models.ForeignKey(Account, blank= True, null= True)
	registration_date = models.DateTimeField(null= True, blank= True)
	is_approved = models.BooleanField(default= False)
	STATUS= (('pending','pending'),('approve','approve'), ('deny','deny'), ('delete', 'delete'))
	status= models.CharField(choices= STATUS, max_length= 10, blank= True, null= True)

	is_paid = models.BooleanField(default= False)
	is_grouped = models.BooleanField(default= False)
	choose_for_fixture = models.BooleanField(default= False)

	class Meta:
		ordering = ['tournament__id']

	def __str__(self):
		return self.tournament.name

class GameWeek_Detail(models.Model):
    tournament = models.ForeignKey(Tournament_Detail, null= True, blank= True)
    begin_date = models.DateTimeField(null= True, blank= True)
    end_date = models.DateTimeField(null= True, blank= True)
    name= models.CharField( max_length=20, null= True )

    class Meta:
        ordering = ['tournament__id']

    def __str__(self):
        return self.name

class Teamowner(models.Model):
	team = models.ForeignKey(Team, null= True,blank= True)
	date = models.DateTimeField(null= True, blank= True)
	name= models.CharField( max_length=20, null= True )
	image_link = models.ImageField(blank= True, null= True)
    

	class Meta:
		ordering = ['team__id']

	def __str__(self):
		return self.name

	
class GameWeek_Matches(models.Model):
    gameweek = models.ForeignKey(GameWeek_Detail, null= True, blank= True)
    team1 = models.ForeignKey(Team, null= True,related_name="gameweekteam1", blank= True)
    team2 = models.ForeignKey(Team, null= False,related_name="gameweekteam2", blank= True)
    date = models.DateTimeField(null= True, blank= True)
    TYPE = (('live','live'),('upcoming','upcoming'),('past','past'))
    status = models.CharField(max_length= 10, blank= True, null= True, choices= TYPE)
    venue = models.CharField( max_length=20, null= True )

    # class Meta:
    #     ordering = ['gameweek__id']

    def __str__(self):
        return self.team1.name + ' VS ' + self.team2.name

class Media(models.Model):
    match = models.ForeignKey(GameWeek_Matches, null= True, blank=True)
    team1 = models.ForeignKey(Team, null= True,related_name="gameweekmediateam1", blank= True)
    team2 = models.ForeignKey(Team, null= True,related_name="gameweekmediateam2", blank= True)
    date = models.DateTimeField(null= True, blank= True, auto_now_add=True)
    disp_feed = models.PositiveIntegerField(default= 1)
    disp_match = models.PositiveIntegerField(default= 1)
    disp_tournament = models.PositiveIntegerField(default= 1)
    title = models.CharField(max_length= 100, null= True, blank= True)
    image_link = models.ImageField(blank= True, null= True)
    video_link = models.CharField(max_length= 500, null= True, blank= True)
    blog_link = models.CharField(max_length= 500, null= True, blank= True)
    blog_introduction = models.CharField(max_length= 100, null= True, blank= True)
    blog_preview_image_link = models.CharField(max_length= 500, null= True, blank= True)


#    class Meta:
#        ordering = ['match__id']

    def __str__(self):
        return str(self.title) #+ ' - ' + str(self.match.id)

class Tournament_Round(models.Model):

	tournament = models.ForeignKey(Tournament_Detail, null= True, blank= True)
	name = models.CharField(max_length= 100)
	TYPE = (('group','group'),('SE','SE'),('DE','DE'), ('knock-out', 'knock-out'), ('league', 'league'))
	round_type = models.CharField(max_length= 10, blank= True, null= True, choices= TYPE)
	round_position = models.PositiveIntegerField(default= 0)

	playoff_teams = models.PositiveIntegerField(default= 0)
	teams_to_play = models.PositiveIntegerField(default= 0)
	number_of_match = models.PositiveIntegerField(default= 1)
	editable= models.BooleanField(default= True)
	is_fixture_created = models.BooleanField(default= False)       ## only used to check wheather this round has fixtures
	is_result_declared = models.BooleanField(default= False)


	is_active = models.BooleanField(default= False)			   ## to check whether the round is created successfully or not
	is_complete= models.BooleanField(default= False)

	class Meta:
		ordering = ['-id']

	def __str__(self):
		return self.name

class Tournament_Round_Team_Map(models.Model):

	tournament_round = models.ForeignKey(Tournament_Round, null= True, blank= True)
	team = models.ForeignKey(Team, null= True, blank= True)
	points = models.FloatField(default= 0)

	match_played = models.PositiveIntegerField(default= 0)
	won = models.PositiveIntegerField(default= 0)
	lost = models.PositiveIntegerField(default= 0)
	ties = models.PositiveIntegerField(default= 0)
	total_run= models.PositiveIntegerField(default= 0)
	runs_against= models.PositiveIntegerField(default= 0)

	is_grouped = models.BooleanField(default= False)
	choose_for_fixture = models.BooleanField(default= False)
	choose_for_next_round = models.BooleanField(default= False)

	class Meta:
		ordering = ['-id']

	def __str__(self):
		return self.tournament_round.name

class Tournament_Group(models.Model):
	tournament_round = models.ForeignKey(Tournament_Round, null= True, blank= True)
	name = models.CharField(max_length= 100, null = True, blank = True)
	playoff_teams = models.PositiveIntegerField(default= 0)
	teams_to_play = models.PositiveIntegerField(default= 0)
	status = models.BooleanField(default= False)

	def __str__(self):
		return self.tournament_round.tournament.name

class Tournament_Group_Team_Map(models.Model):

	tournament_group = models.ForeignKey(Tournament_Group, null= True, blank= True)
	team = models.ForeignKey(Team, null= True, blank= True)

	class Meta:
		ordering = ['-id']

	def __str__(self):
		return self.tournament_group.name

# class Tournament_Fixture_Team_Map(models.Model):
# 	pass



class TournamentAdminPermissionMap(models.Model):
	user = models.ForeignKey(BusinessUser, related_name = 'tournamentusera')
	admin = models.ForeignKey(BusinessUser, related_name = 'tournamentadmin')
	permission = models.ManyToManyField(ChildPermissions, related_name = 'tournamentadminpermission')
	tournament = models.ForeignKey(Tournament_Detail)

	def __str__(self):
		return self.tournament.name

class TournamentManagerPermissionMap(models.Model):
	user = models.ForeignKey(BusinessUser, related_name = 'tournamentuserm')
	manager = models.ForeignKey(BusinessUser, related_name = 'tournamentmanager')
	permission = models.ManyToManyField(ChildPermissions, related_name = 'tournamentmanagerpermission')
	tournament = models.ForeignKey(Tournament_Detail)

	def __str__(self):
		return self.tournament.name
