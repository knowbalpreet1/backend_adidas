from django.shortcuts import render
import traceback, datetime, os
from rest_framework import viewsets, status
from django.conf import settings
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from rest_framework import status
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from tournament.models import Tournament_Detail, Tournament_Round_Team_Map, Tournament_Round, Tournament_Group, Tournament_Group_Team_Map, Team_Registration, Tournament_Venue_Map, Tournament_Sponsor
from tournament.serializers import Round_Basic_Info_Serializer, Tournament_Serializer, Tournament_Group_Serializer, ResultSerializer, Tournament_Basic_Serializer, Tournament_page2_Serializer, Tournament_page3_Serializer, TournamentListSerializer
from scorecard.models import Match_Stat, Match_Stat_Team, Match_Stat_Batting, Match_Stat_Bowling, Match_Stat_Out, Team_Match_Player, Match_Stat_Fall_Of_Wickets, Match_Stat_Live, Match_Stat_Over, Match_Stat_Bowl, Match_Stat_Interval, Match_Stat_Scorer, Match_Stat_Penalty, Match_Stat_Partnership
from venue.models import Court, VenueImg
from players.models import Team, CompetitionType, City, TeamAdmin, SportsType, TeamPlayers
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from rest_framework import status
from GoPlayBook.static import MAX_SIZE_TOURNAMENT_LOGO, MAX_SIZE_TOURNAMENT_COVER_IMAGE
from django.shortcuts import redirect
from businessapp.permissions import customPermission
from django.db.models import Sum
from django.conf import settings
from tournament.signals import create_fixture_notify, update_fixture_notify, shift_team_notify
from notification.models import BusinessNotification
from django.http import HttpResponse
import json
loop = settings.LOOP

def get_date_time_format(date_time):
	try:
		date_time= datetime.datetime.strftime(date_time, '%Y/%m/%d %I:%M %p')
		return [date_time, True]
	except:
		return ["", False]

def checkTournamentPermission(pk, buser):
	try:
		tour_obj= Tournament_Detail.objects.get(Q(pk= pk) & Q(admin= buser))
		return ("admin", tour_obj)
	except:
		try:
			tour_obj= Tournament_Detail.objects.get(Q(pk= pk) & Q(manager= buser))
			return ("manager", tour_obj)
		except:
			return None

class Tournament_DetailViewSet(viewsets.ViewSet):

	permission_classes = [customPermission,]

	def create(self, request):
		try:
			data= request.data
			print('data', data)
			request_user= request.buser
			form_type= data.get('form_type')
			if form_type == "basic":
				name= data.get('name')
	#			tag_line= data.get('tag_line')
				description= data.get('description')
				sport_type= data.get('sport_type')
	#			competition_type= data.get('competition_type')
				tournament_format= data.get('tournament_format')
				registration_start_date= data.get('registration_start_date')
				registration_end_date= data.get('registration_end_date')
				start_date= data.get('start_date')
				end_date= data.get('end_date')
				
				try:
					registration_start_date= datetime.datetime.strptime(registration_start_date, '%Y/%m/%d')
					registration_end_date= datetime.datetime.strptime(registration_end_date, '%Y/%m/%d')
					start_date= datetime.datetime.strptime(start_date, '%Y/%m/%d')
					end_date= datetime.datetime.strptime(end_date, '%Y/%m/%d')
				except Exception:
					traceback.print_exc()
					return Response({'success':False, 'valid_date':False, 'error':"invalid date format"}, status= status.HTTP_400_BAD_REQUEST)
				if not name or name == "" or len(str(name).strip(' ')) == 0:
					return Response({'success':False, 'name':False}, status= status.HTTP_400_BAD_REQUEST)
				if not tournament_format or tournament_format == "" or tournament_format not in ['cup', 'league', 'knock-out']:
					print('tournament_format',tournament_format)
					return Response({'success':False, 'tournament_format':False}, status= status.HTTP_400_BAD_REQUEST)
	#			if not competition_type or competition_type == "" :
	#				return Response({'success':False, 'competition_type':False}, status= status.HTTP_400_BAD_REQUEST)
				if registration_end_date < registration_start_date or start_date < registration_end_date or end_date < start_date: 
					return Response({'success':False, 'valid_date':False}, status= status.HTTP_400_BAD_REQUEST)
				if registration_start_date.date() < datetime.datetime.now().date() or registration_end_date.date() < datetime.datetime.now().date() or start_date.date() < datetime.datetime.now().date() or end_date.date() < datetime.datetime.now().date():
					return Response({'success':False, 'valid_date':False}, status= status.HTTP_400_BAD_REQUEST)
				
	#			comp_obj= CompetitionType.objects.get(Q(id= competition_type) & Q(is_active= True))
				sport_type_obj= SportsType.objects.get(pk= sport_type)

				tour_obj= Tournament_Detail.objects.create(name= name, description= description, sport_type= sport_type_obj, tournament_format= tournament_format,  registration_start_date= registration_start_date, registration_end_date= registration_end_date, start_date= start_date, end_date= end_date, is_active= False, created_by= request_user)
				print(request_user, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
				tour_obj.admin.add(request_user)
				tour_obj.save()
				return Response({'success':True, 'id':tour_obj.id}, status= status.HTTP_201_CREATED)

			if form_type == "page2":
				tournament_pk= data.get('id')
				email= data.get('email')
				mobile1= data.get('mobile1')
				mobile2= data.get('mobile2')
				city= data.get('city')
				
				""" check for user type (admin/manager) """
				user_type= checkTournamentPermission(tournament_pk, request_user)[0]
				if not user_type == "admin":
					return Response({'success':False, 'is_admin':False}, status= status.HTTP_400_BAD_REQUEST)

				if not city or city == "":
					return Response({'success':False, 'city':False}, status= status.HTTP_400_BAD_REQUEST)
				try:
					tournament_obj= Tournament_Detail.objects.get(Q(admin= request_user) & Q(pk= tournament_pk))
				except Exception:
					traceback.print_exc()
					return Response({'success':False, 'error':"Invalid tournament pk"}, status= status.HTTP_400_BAD_REQUEST)
				
				city_obj= City.objects.get(id= city)
				tournament_obj.email = email
				tournament_obj.mobile1 = mobile1
				tournament_obj.mobile2 = mobile2
				tournament_obj.city= city_obj
				tournament_obj.save()
				return Response({'success':True, 'id':tournament_obj.id}, status= status.HTTP_201_CREATED)

			if form_type == "page3":
				tournament_pk= data.get('id')
				tournament_level= data.get('tournament_level')
				max_no_of_teams= data.get('max_no_of_teams')
				tournament_type= data.get('tournament_type')
				age_from= data.get('age_from')
				age_to= data.get('age_to')
				prize= data.get('prize')
				reward= data.get('reward')
				registration_fee= data.get('registration_fee')

				""" check for user type (admin/manager) """
				user_type= checkTournamentPermission(tournament_pk, request_user)[0]
				if not user_type == "admin":
					return Response({'success':False, 'is_admin':False}, status= status.HTTP_400_BAD_REQUEST)
				
				try:
					tournament_obj= Tournament_Detail.objects.get(Q(admin= request_user) & Q(pk= tournament_pk))
				except Exception:
					traceback.print_exc()
					return Response({'success':False, 'error':"Invalid tournament pk"}, status= status.HTTP_400_BAD_REQUEST)
				

				tournament_obj.tournament_level = tournament_level
				tournament_obj.max_no_of_teams = max_no_of_teams
				tournament_obj.tournament_type = tournament_type
				if age_from:
					tournament_obj.age_from = age_from
				if age_to:
					tournament_obj.age_to = age_to
				tournament_obj.prize = prize
				tournament_obj.reward = reward
				tournament_obj.registration_fee = registration_fee
				tournament_obj.is_active = True
				tournament_obj.save()
				return Response({'success':True, 'id':tournament_obj.id}, status= status.HTTP_201_CREATED)

			elif form_type == "logo":
				logo= data.get('logo')
				tour_id= data.get('id')
				# print("here",MAX_SIZE_TOURNAMENT_LOGO)
				""" check for user type (admin/manager) """
				user_type= checkTournamentPermission(tour_id, request_user)[0]
				if not user_type == "admin":
					return Response({'success':False, 'is_admin':False}, status= status.HTTP_400_BAD_REQUEST)

				if len(logo) > MAX_SIZE_TOURNAMENT_LOGO:
					return Response({'success':False, 'file_size':False}, status= status.HTTP_400_BAD_REQUEST)

				tour_obj= Tournament_Detail.objects.get(Q(id= tour_id))
				#if tour_obj.logo:
				#	print(os.path.isfile(instance.file.path))
				#	os.remove(tour_obj.logo.url)
				tour_obj.logo= logo
				tour_obj.save()
				print ('logo------>', tour_obj.logo.url)
				return Response({'success':True, 'id':tour_obj.id, 'logo':tour_obj.logo.url}, status= status.HTTP_201_CREATED)
			elif form_type == "cover_pic":
				cover_pic= data.get('cover_pic')
				tour_id= data.get('id')
				""" check for user type (admin/manager) """
				user_type= checkTournamentPermission(tour_id, request_user)[0]
				if not user_type == "admin":
					return Response({'success':False, 'is_admin':False}, status= status.HTTP_400_BAD_REQUEST)

				if len(cover_pic) > MAX_SIZE_TOURNAMENT_COVER_IMAGE:
					return Response({'success':False, 'file_size':False}, status= status.HTTP_400_BAD_REQUEST)
				tour_obj= Tournament_Detail.objects.get(Q(id= tour_id))
				tour_obj.cover_pic= cover_pic
				tour_obj.save()

				return Response({'success':True, 'id':tour_obj.id, 'cover_pic':tour_obj.cover_pic.url}, status= status.HTTP_201_CREATED)
			else:
				return Response({'success':False}, status= status.HTTP_400_BAD_REQUEST)
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def update(self, request, pk):
		try:
			data= request.data
			form_type= data.get('form_type')
			request_user= request.buser
			tournament_pk= pk
			""" check for user type (admin/manager) """
			user_type= checkTournamentPermission(tournament_pk, request_user)[0]
			if not user_type == "admin":
				return Response({'success':False, 'is_admin':False}, status= status.HTTP_400_BAD_REQUEST)

			print(data, '.....................')
			if form_type == "basic":
				name= data.get('name')
#				tag_line= data.get('tag_line')
				description= data.get('description')
				registration_start_date= data.get('registration_start_date')
				registration_end_date= data.get('registration_end_date')
				start_date= data.get('start_date')
				end_date= data.get('end_date')

				try:
					registration_start_date= datetime.datetime.strptime(registration_start_date, '%Y/%m/%d')
					registration_end_date= datetime.datetime.strptime(registration_end_date, '%Y/%m/%d')
					start_date= datetime.datetime.strptime(start_date, '%Y/%m/%d')
					end_date= datetime.datetime.strptime(end_date, '%Y/%m/%d')
				except Exception:
					traceback.print_exc()
					return Response({'success':False, 'valid_date':False, 'error':"invalid date format"}, status= status.HTTP_400_BAD_REQUEST)

				if not name or name == "":
					return Response({'success':False, 'name':False}, status= status.HTTP_400_BAD_REQUEST)
				if registration_end_date < registration_start_date or start_date < registration_end_date or end_date < start_date: 
					print('11111')
					return Response({'success':False, 'valid_date':False}, status= status.HTTP_400_BAD_REQUEST)
				#if registration_start_date.date() < datetime.datetime.now().date() or registration_end_date.date() < datetime.datetime.now().date() or start_date.date() < datetime.datetime.now().date() or end_date.date() < datetime.datetime.now().date():
				#	print('22222')
				#	return Response({'success':False, 'valid_date':False}, status= status.HTTP_400_BAD_REQUEST)

				tour_obj= Tournament_Detail.objects.get(Q(admin= request_user) & Q(pk= tournament_pk))
				tour_obj.name= name
				#tour_obj.tag_line= tag_line
				tour_obj.description= description
				tour_obj.registration_start_date= registration_start_date
				tour_obj.registration_end_date= registration_end_date
				tour_obj.start_date= start_date
				tour_obj.end_date= end_date
				tour_obj.save()

				return Response({'success':True}, status= status.HTTP_200_OK)

			elif form_type == "page2":

				tournament_pk= pk
				email= data.get('email')
				mobile1= data.get('mobile1')
				mobile2= data.get('mobile2')
				city= data.get('city')
				
				if not city or city == "":
					return Response({'success':False, 'city':False}, status= status.HTTP_400_BAD_REQUEST)
				try:
					tournament_obj= Tournament_Detail.objects.get(Q(admin= request_user) & Q(pk= tournament_pk))
				except Exception:
					traceback.print_exc()
					return Response({'success':False, 'error':"Invalid tournament pk"}, status= status.HTTP_400_BAD_REQUEST)
				
				city_obj= City.objects.get(id= city)
				tournament_obj.email = email
				tournament_obj.mobile1 = mobile1
				tournament_obj.mobile2 = mobile2
				tournament_obj.city= city_obj
				tournament_obj.save()
				return Response({'success':True, 'id':tournament_obj.id}, status= status.HTTP_201_CREATED)

			if form_type == "page3":
				tournament_pk= pk
				tournament_level= data.get('tournament_level')
				tournament_type= data.get('tournament_type')
				age_from= data.get('age_from')
				age_to= data.get('age_to')
				prize= data.get('prize')
				reward= data.get('reward')
				registration_fee= data.get('registration_fee')
				
				try:
					tournament_obj= Tournament_Detail.objects.get(Q(admin= request_user) & Q(pk= tournament_pk))
				except Exception:
					traceback.print_exc()
					return Response({'success':False, 'error':"Invalid tournament pk"}, status= status.HTTP_400_BAD_REQUEST)
				

				tournament_obj.tournament_level = tournament_level
				if age_from:
					tournament_obj.age_from = age_from
				if age_to:
					tournament_obj.age_to = age_to
				tournament_obj.prize = prize
				tournament_obj.reward = reward
				tournament_obj.tournament_type= tournament_type
				tournament_obj.registration_fee = registration_fee
				tournament_obj.save()
				return Response({'success':True, 'id':tournament_obj.id}, status= status.HTTP_201_CREATED)

			else:
				traceback.print_exc()
				return Response({'success':False}, status= status.HTTP_400_BAD_REQUEST)
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def list(self, request):
		try:
			data= request.GET
			request_user= request.buser
			page= data.get('page')
			form_type= data.get('form_type')
			if form_type == "my_tour":

				qs= Tournament_Detail.objects.filter(Q(Q(admin= request_user) | Q(manager= request_user)) & Q(is_active= True)).distinct('id')

				p = Paginator(qs, 6)
				total_pages = p.num_pages
				try:
					tour_queryset = p.page(page)
				except PageNotAnInteger:
					tour_queryset = p.page(1)
				except EmptyPage:
					tour_queryset = p.page(1)                            

				serializer= Tournament_Serializer(tour_queryset, many= True)
#				print("dta----->", serializer.data)
				return Response({'data':serializer.data, 'total_pages':total_pages, 'success':True})
			elif form_type == "register":
				
				qs= Tournament_Detail.objects.filter(Q(Q(admin= request_user) | Q(manager= request_user)) & Q(registration_end_date__gte= datetime.datetime.now()) & Q(is_active= True))
				p = Paginator(qs, 6)
				total_pages = p.num_pages
				try:
					tour_queryset = p.page(page)
				except PageNotAnInteger:
					tour_queryset = p.page(1)
				except EmptyPage:
					tour_queryset = p.page(1)                            

				serializer= Tournament_Serializer(tour_queryset, many= True)
				return Response({'data':serializer.data, 'total_pages':total_pages, 'success':True})
			return Response({'success':True}, status= status.HTTP_200_OK)
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def retrieve(self, request, pk):
		try:
			request_user= request.buser
			data= request.GET
			form_type= data.get('form_type')
			tournament_pk= pk
			obj= None
			try:
				obj= Tournament_Detail.objects.get(Q(admin= request_user) & Q(is_active= True) & Q(pk= tournament_pk))
			except:
				obj= Tournament_Detail.objects.get(Q(manager= request_user) & Q(is_active= True) & Q(pk= tournament_pk))
			if form_type == "basic":

				serializer= Tournament_Basic_Serializer(obj)
				print(serializer.data)
				return Response(serializer.data)

			elif form_type == "page2":

				serializer= Tournament_page2_Serializer(obj)
				return Response(serializer.data)

			elif form_type == "page3":

				serializer= Tournament_page3_Serializer(obj)
				print(serializer.data)
				return Response(serializer.data)

			else:
				return Response({'error':'wrong option', 'success':False})
			
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def delete(self, request):
		try:
			data= request.GET
			print(data)
			tournament_id= data.get('tournament_id')
			form_type= data['form_type']
			print(tournament_id, form_type)
			request_user= request.buser
			tour_obj= None
			try:
				tour_obj= Tournament_Detail.objects.get(Q(id= tournament_id) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj= Tournament_Detail.objects.get(Q(id= tournament_id) & Q(manager= request_user) & Q(is_active= True))
			if form_type == "deactivate" :
				tour_obj.is_deactivated= True
				tour_obj.save()
			elif form_type == "activate" :
				tour_obj.is_deactivated= False
				tour_obj.save()
			elif form_type == "delete":
				tour_obj.is_active= False
				tour_obj.save()
			else:
				return Response({'success':False, 'error':'Invalid Option'}, status= status.HTTP_400_BAD_REQUEST)
			
			
			return Response({'success':True, 'message':'Tournament deleted/Deactivated successfully'}, status= status.HTTP_200_OK)            
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

class Tournament_Venue_MapViewset(viewsets.ViewSet):

	permission_classes = [customPermission,]
	
	def create(self, request):
		try:
			data=request.data
			request_user = request.buser

			tournament_pk = data.get('tournament_id')
			court_id = data.get('court_id')
			is_dummy= data.get('is_dummy')
			court_name= data.get('court_name')
			address= data.get('address')
			pincode= data.get('pincode')
			city= data.get('city')
			state= data.get('state')
			latitude= data.get('latitude')
			longitude= data.get('longitude')
			print(data)
			if not is_dummy:
				print(not is_dummy)	
				if not tournament_pk or not court_id :
					return Response("Invalid Information", status= status.HTTP_400_BAD_REQUEST)
			tournament_obj= None
			try:
				try:
					tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(admin= request_user) )
				except:
					tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(manager= request_user))
				# court_obj= Court.objects.get(Q(pk= court_id) & Q(is_active= True))
			except ObjectDoesNotExist:
				
				return Response({"error":"Team doesn't Exist", 'success':False}, status= status.HTTP_400_BAD_REQUEST)
			
			if not is_dummy:		
				court_obj= Court.objects.get(Q(pk= court_id) & Q(is_active= True))	
				obj, created= Tournament_Venue_Map.objects.get_or_create(tournament= tournament_obj, court= court_obj)
				obj.is_active= True
				obj.save()
			else:
				obj, created= Tournament_Venue_Map.objects.get_or_create(tournament= tournament_obj, court_name= court_name)
				obj.address= address
				#obj.pincode= pincode
				obj.city= city
				obj.state= state
				obj.latitude= latitude
				obj.longitude= longitude
				obj.is_dummy= True
				obj.is_active= True
				obj.save()

			return Response({'success':True, 'message':'Court Successfully added'}, status= status.HTTP_201_CREATED)
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def list(self, request):
		try:
			request_user= request.buser
			data= request.GET
			tournament_pk= data.get('tournament_id')
			page= data.get('page')
			qs= Tournament_Venue_Map.objects.filter(Q(tournament__id= tournament_pk) & Q(Q(tournament__admin= request_user) | Q(tournament__manager= request_user)) & Q(is_active= True)).distinct('id')
			p = Paginator(qs, 6)
			total_pages = p.num_pages
			try:
				qs = p.page(page)
			except PageNotAnInteger:
				qs = p.page(1)
			except EmptyPage:
				qs = p.page(1)                            

			serializer= []
			for obj in qs:
				court_id= None
				if obj.court:
					court_id= obj.court
				# else:

				court_name= ""
				venue_name= ""
				if obj.court:
					court_name= obj.court.name
					venue_name= obj.court.venue.name
				else:
					venue_name= obj.court_name
				image= None
				img_qs= None
				if obj.court:
					img_qs= VenueImg.objects.filter(venue= obj.court.venue)
					if img_qs.exists():
						image= img_qs[0].url.url
				serializer.append({'id':obj.id, 'court_name':court_name, 'venue_name':venue_name, 'venue_img':image})
			return Response({'success':True, 'data':serializer, 'total_pages':total_pages})
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)	

	def delete(self, request):
		try:
			data= request.GET
			obj_id= data.get('id')
			request_user= request.buser
			print(request_user)
			venue_map_obj= None
			try:
				venue_map_obj = Tournament_Venue_Map.objects.get(Q(pk= obj_id) & Q(tournament__admin= request_user) )#| Q(tournament__manager= request_user)) & Q(is_active= True))
			except:
				venue_map_obj = Tournament_Venue_Map.objects.get(Q(pk= obj_id) & Q(tournament__manager= request_user))
			venue_map_obj.is_active= False
			venue_map_obj.save()
			return Response({'success':True, 'message':'Court deleted successfully'}, status= status.HTTP_200_OK)            
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

class Tournament_SponsorViewset(viewsets.ViewSet):

	permission_classes = [customPermission,]
	
	def create(self, request):
		try:
			data=request.data
			request_user = request.buser

			tournament_pk = data.get('tournament_id')
			name= data.get('name')
			image= data.get('image')
			tournament_obj= None
			try:
				tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(admin= request_user) & Q(is_active= True))
			except:
				tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(manager= request_user) & Q(is_active= True))
			sponsor_obj= Tournament_Sponsor.objects.create(tournament= tournament_obj, name= name, created_by= request_user)
			sponsor_obj.image= image
			sponsor_obj.save()

			return Response({'success':True, 'message':'Sponsor Successfully added'}, status= status.HTTP_201_CREATED)
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

#	def update(self, request, pk):
#		try:
#			data = request.data
#			request_user = request.buser
#			form_type = data.get('form_type')
#			sponsor_obj_pk = pk
#
#			sponsor_obj = Tournament_Sponsor.objects.get(Q(pk= sponsor_obj_pk) & Q(tournament__admin= request_user) & Q(is_active= True))
#			#if form_type == "image":
#			image= data.get('image')
#			sponsor_obj.image= image
#			#elif form_type == "name":
#			name= data['name']
#			sponsor_obj.name= name
#			sponsor_obj.save()
#			return Response({'success':True}, status= status.HTTP_200_OK)
#		except :
#			traceback.print_exc()
#			return Response({'success':False}, status= 400)
#

	def retrieve(self, request, pk):
		try:
			request_user= request.buser
			data= request.GET
			tournament_pk= pk
			page= data.get('page')
			qs= Tournament_Sponsor.objects.filter(Q(tournament__id= tournament_pk) & Q(is_active= True) & Q(Q(tournament__admin= request_user) | Q(tournament__manager= request_user))).distinct('id')
			p = Paginator(qs, 6)
			total_pages = p.num_pages
			try:
				qs = p.page(page)
			except PageNotAnInteger:
				qs = p.page(1)
			except EmptyPage:
				qs = p.page(1)                            

			serializer= []
			for obj in qs:
				image= None
				if obj.image:
					image= obj.image.url

				serializer.append({'id':obj.id, 'image':image, 'name':obj.name})
			return Response({'success':True, 'data':serializer})
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)	

	def delete(self, request):
		try:
			data= request.GET
			obj_id= data.get('id')
			request_user= request.buser
			sponsor_obj= None
			try:
				sponsor_obj = Tournament_Sponsor.objects.get(Q(pk= obj_id) & Q(tournament__admin= request_user) & Q(is_active= True))
			except:
				sponsor_obj = Tournament_Sponsor.objects.get(Q(pk= obj_id) & Q(tournament__manager= request_user) & Q(is_active= True))
			sponsor_obj.is_active= False
			sponsor_obj.save()
			return Response({'success':True, 'message':'Court deleted successfully'}, status= status.HTTP_200_OK)            
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

class EditSponsor(viewsets.ViewSet):
	def create(self, request):
                try:
                        data = request.data
                        request_user = request.buser
                        form_type = data.get('form_type')
                        sponsor_obj_pk = data.get('sponsor_id')
#                        print(data)
                        sponsor_obj= None
                        try:
	                        sponsor_obj = Tournament_Sponsor.objects.get(Q(pk= sponsor_obj_pk) & Q(tournament__admin= request_user)  & Q(is_active= True))
                        except:
                                sponsor_obj = Tournament_Sponsor.objects.get(Q(pk= sponsor_obj_pk) & Q(tournament__manager= request_user)  & Q(is_active= True))
                        #if form_type == "image":
                        image= data.get('image')
                        sponsor_obj.image= image
                        #elif form_type == "name":
                        name= data['name']
                        sponsor_obj.name= name
                        sponsor_obj.save()
                        return Response({'success':True}, status= status.HTTP_200_OK)
                except :
                        traceback.print_exc()
                        return Response({'success':False}, status= 400)


class Team_RegistrationViewSet(viewsets.ViewSet):

	permission_classes = [TokenHasReadWriteScope,]
	
	def create(self, request):
		try:

			data=request.data
			request_user = request.user

			tournament_pk = data.get('tournament_id')
			team_pk = data.get('team_id')
			# paid= data.get('paid')
			
			if not tournament_pk or not team_pk :
				return Response({'success':False, 'error':'Invalid tournament id'}, status= status.HTTP_400_BAD_REQUEST)

			# if not paid in ['0', '25', '50', '100']:
			# 	return Response({'success':False, 'paid':False}, status= status.HTTP_400_BAD_REQUEST)

			try:
				tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True))
				team_obj= Team.objects.get(Q(pk= team_pk) & Q(is_active= True) & Q(created_by__user= request_user))

			except ObjectDoesNotExist:
				
				return Response({"error":"Team doesn't Exist", 'success':False}, status= status.HTTP_400_BAD_REQUEST)
			
			if tournament_obj.registration_end_date < datetime.datetime.now() and Team_Registration.objects.filter(Q(tournament= tournament_obj) & Q(status="approve")).count() < tournament_obj.max_no_of_teams:
				return Response({'success':False, 'error':'Registrations closed'}, status= status.HTTP_400_BAD_REQUEST)
			team_registration_obj, created= Team_Registration.objects.get_or_create(tournament= tournament_obj, team= team_obj)
			if not created:
				return Response({'success':False, 'already_sent':True, 'error':'Request already sent'}, status= status.HTTP_400_BAD_REQUEST)
			
			team_registration_obj.registered_by= request_user
			team_registration_obj.registration_date= datetime.datetime.now()
			team_registration_obj.is_approved= False
			team_registration_obj.status= 'pending'
			# team_registration_obj.paid= paid
			team_registration_obj.is_paid= False
			team_registration_obj.save()

			return Response({'success':True, 'message':'Request sent Successfully', 'registration_id':team_registration_obj.id})
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)


class Admin_Team_RegistrationViewSet(viewsets.ViewSet):

	permission_classes = [customPermission,]
	
	def create(self, request):
		try:
			data=request.data
			request_user = request.buser
			print("data",data)
			tournament_pk = data.get('tournament_id')
			team_pk = data.get('team_id')
			paid= data.get('paid')
			admin= data.get('admin')
			
			if not tournament_pk or not team_pk :
				return Response({'success':False, 'error':'Invalid tournament id'}, status= status.HTTP_400_BAD_REQUEST)

			if not paid in ['0', '25', '50', '100']:
				return Response({'success':False, 'paid':False}, status= status.HTTP_400_BAD_REQUEST)
			tournament_obj=  None
			try:
				try:
					tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True) & Q(admin= request_user))
				except:
					tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True) & Q(manager= request_user))
				team_obj= Team.objects.get(Q(pk= team_pk) & Q(is_active= True))

			except ObjectDoesNotExist:
				
				return Response({"error":"Team doesn't Exist", 'success':False}, status= status.HTTP_400_BAD_REQUEST)
			
			if tournament_obj.registration_end_date < datetime.datetime.now() or Team_Registration.objects.filter(Q(tournament= tournament_obj) & Q(status="approve")).count() >= tournament_obj.max_no_of_teams:
				return Response({'success':False, 'error':'Registrations closed'}, status= status.HTTP_400_BAD_REQUEST)
			if tournament_obj.is_round_created:
				return Response({'success':False, 'error':'Can\'t add teams as you already created the round'}, status= status.HTTP_400_BAD_REQUEST)
			team_registration_obj, created= Team_Registration.objects.get_or_create(tournament= tournament_obj, team= team_obj)
			#if not created:
			#	return Response({'success':False, 'already_sent':True, 'error':'Request already sent'}, status= status.HTTP_400_BAD_REQUEST)
			registered_by= None
			if admin and not admin == "null" :
				 team_registration_obj.registered_by= TeamAdmin.objects.get(player__user__pk= admin, is_active= True, team__pk= team_pk).player.user
#			team_registration_obj.registered_by= registered_by#request_user
			team_registration_obj.registration_date= datetime.datetime.now()
			team_registration_obj.is_approved= True
			team_registration_obj.status= 'approve'
			team_registration_obj.paid= paid
			team_registration_obj.is_paid= True
			team_registration_obj.save()

			return Response({'success':True, 'message':'Request sent Successfully', 'registration_id':team_registration_obj.id})
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def update(self, request, pk):
		try:
			data = request.data
			request_user = request.buser
			action = data.get('action')
			registration_pk = pk
			registration_obj= None
			try:
				registration_obj = Team_Registration.objects.get(Q(pk= registration_pk) & Q(tournament__admin= request_user))
			except:
				registration_obj = Team_Registration.objects.get(Q(pk= registration_pk) & Q(tournament__manager= request_user))
			if action == "approve":
				if Team_Registration.objects.filter(Q(tournament= registration_obj.tournament) & Q(is_approved= True)).count() < registration_obj.tournament.max_no_of_teams:
					if not registration_obj.status == 'pending':
						return Response({'success':False, 'error':'Invalid Option'}, status= status.HTTP_400_BAD_REQUEST)	
					registration_obj.is_approved = True
					registration_obj.status= 'approve'
					registration_obj.save()
					return Response({'success':True}, status= status.HTTP_200_OK)
				else:
					return Response({'success':True, 'message':'Registrations are closed', 'number_of_teams':True}, status= status.HTTP_200_OK)
			elif action == "deny":
				if registration_obj.tournament.is_round_created:
					return Response({'success':False, 'error':'Registrations closed'}, status= status.HTTP_400_BAD_REQUEST)
				if registration_obj.is_approved:
					return Response({'success':False, 'error':'Invalid Operation'}, status= status.HTTP_400_BAD_REQUEST)	
				if not registration_obj.status == 'pending':
					return Response({'success':False, 'error':'Invalid Operation'}, status= status.HTTP_400_BAD_REQUEST)	
				registration_obj.status= 'deny'
				registration_obj.save()
				# registration_obj.delete()
				return Response({'success':True, 'message':'Request Denied successfully'}, status= status.HTTP_200_OK)            
			else:
				return Response({'success':False, 'error':'Error'}, status= status.HTTP_400_BAD_REQUEST)
		except Exception:
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def delete(self, request):
		try:
			data= request.GET
			registration_id= data.get('registration_id')
			request_user= request.buser
			registration_obj= None
			try:
				registration_obj = Team_Registration.objects.get(Q(pk= registration_id) & Q(tournament__admin= request_user))
			except:
				registration_obj = Team_Registration.objects.get(Q(pk= registration_id) & Q(tournament__manager= request_user))
			if registration_obj.tournament.is_round_created:
				return Response({'success':False, 'error':'You can\'t delete team once you created a round'}, status= status.HTTP_400_BAD_REQUEST)
			registration_obj.delete()
			return Response({'success':True, 'message':'Request deleted successfully'}, status= status.HTTP_200_OK)            
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

	def retrieve(self, request, pk):
		try:
			data= request.GET
			form_type= data.get('form_type')
			tournament_pk= pk
			request_user= request.buser
			tour_obj= Tournament_Detail.objects.get(Q(pk= pk) & Q(is_active= True) & Q(is_deactivated= False))
			if form_type == "pending":
				registration_qs = Team_Registration.objects.filter(Q(tournament__pk= tournament_pk) & (Q(tournament__admin= request_user) | Q(tournament__manager= request_user)) & Q(tournament__is_active= True) & Q(status= 'pending') )
			elif form_type == "approve":
				registration_qs = Team_Registration.objects.filter(Q(tournament__pk= tournament_pk) & (Q(tournament__admin= request_user) | Q(tournament__manager= request_user)) & Q(tournament__is_active= True) & Q(status= 'approve'))

			elif form_type == "deny":
				registration_qs = Team_Registration.objects.filter(Q(tournament__pk= tournament_pk) & (Q(tournament__admin= request_user) | Q(tournament__manager= request_user)) & Q(tournament__is_active= True) & Q(status= 'deny'))
			serializer= []
			for obj in registration_qs:
				name= ""
				mobile = ""
				if obj.registered_by:
					name= obj.registered_by.name
					mobile= obj.registered_by.mobile
				serializer.append({'id':obj.id, 'team_name':obj.team.name, 'paid':obj.paid, 'name':name, 'mobile':mobile, 'team_id':obj.team.id})
			return Response({'success':True, 'data':serializer, 'max_no_of_teams':tour_obj.max_no_of_teams})
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

class Tournament_RoundViewSet(viewsets.ViewSet):
	permission_classes = [customPermission]

	def create(self, request):
		try:
			data=request.data
			request_user = request.buser
			print(data)
			tournament_pk = data.get('tournament')
			round_name = data.get('round_name')
			round_type = data.get('round_type')
			total_teams = 0
			# print('tournament_pk', tournament_pk)			
			tournament_obj= None
			try:
				tournament_obj = Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True) & Q(admin= request_user))
			except:
				tournament_obj = Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True) & Q(manager= request_user))
			if not tournament_pk or not round_type or round_type == "" or not round_name or round_name == "" or len(str(round_name).strip(' ')) == 0 or len(str(round_type).strip(' ')) == 0 :
				return Response({'success':False, 'error':'Provided information is incorrect'}, status= status.HTTP_400_BAD_REQUEST)

			if tournament_obj.tournament_format == "league":
				if tournament_obj.is_round_created:
					return Response({'success':False, 'error':'You can\'t create 2nd round in a League'}, status= status.HTTP_400_BAD_REQUEST)
				total_teams = Team_Registration.objects.filter(Q(tournament= tournament_obj) & Q(status= 'approve'))
				tournament_round_obj= None
#				if tournament_obj.no_of_match == 1:
				tournament_round_obj= Tournament_Round.objects.create(name= round_name, tournament= tournament_obj, round_type= "league", round_position= 1, is_complete= True, is_active= True)
				tournament_obj.is_round_created= True
				tournament_obj.save()
				for obj in total_teams:
					Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= obj.team)
#				elif tournament_obj.no_of_match == 2:
#					tournament_round_obj= Tournament_Round.objects.create(name= round_name, tournament= tournament_obj, round_type= "DE", round_position= 1, is_complete= True, is_active= True)
#					tournament_obj.is_round_created= True
#					tournament_obj.save()
#					for obj in total_teams:
#						Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= obj.team)

				tournament_obj.teams_to_play= total_teams.count()
				tournament_obj.save()
				print("inside league type tournament", tournament_obj.no_of_match)
				response_json = {'round_id': tournament_round_obj.id, 'success':True}
				return Response(response_json, status= status.HTTP_201_CREATED)
			if not tournament_obj.tournament_format == "knock-out" and tournament_obj.is_round_created:

				max_round = Tournament_Round.objects.filter(Q(tournament= tournament_obj) & Q(is_active= True)).latest('round_position')
				print("max_round",max_round.pk)
				if max_round.round_type == "group":
					team1_qs= Tournament_Round_Team_Map.objects.filter(Q(tournament_round= max_round))
					if team1_qs.exists() and not team1_qs.filter(is_grouped= False).exists():
						max_round.is_complete= True
						max_round.save()
					else:
						return Response({'success':False, 'error':'first complete the previous round or delete it', "error_code":1}, status= status.HTTP_400_BAD_REQUEST)
			elif not tournament_obj.tournament_format == "knock-out":
				registered_teams = Team_Registration.objects.filter(Q(tournament= tournament_obj) & Q(status= 'approve')).count()
				if registered_teams < 2 :
					return Response({'success':False, 'error':'minimum two teams are required'}, status= status.HTTP_400_BAD_REQUEST)
				if not round_type == "group" and not registered_teams % 2 == 0:
					return Response({'success':False, 'error':'number of registered teams is not appropriate'}, status= status.HTTP_400_BAD_REQUEST)
			
			if tournament_obj.tournament_format == "knock-out":
				tournament_round_obj = Tournament_Round.objects.create(name= round_name, tournament= tournament_obj, round_type= "knock-out", is_active= True)
			elif tournament_obj.tournament_format == "cup":
				tournament_round_obj = Tournament_Round.objects.create(name= round_name, tournament= tournament_obj, round_type= round_type, is_active= True)
			if tournament_obj.is_round_created:
				
				# print ("other than first round")
				max_round_position = Tournament_Round.objects.filter(Q(tournament= tournament_obj) & Q(is_active= True)).latest('round_position')
				if round_type == "group" and not max_round_position.round_type == "group":
					return Response({'success':False, 'error':'You can\'t create group type round'}, status= status.HTTP_400_BAD_REQUEST)
				round_position = max_round_position.round_position + 1
				max_round_position.editable = False
				# max_round_position.save()
				unActive_tournament_rounds_qs = Tournament_Round.objects.filter(Q(tournament= tournament_obj) & Q(tournament__created_by= request_user) & Q(is_active= False) & Q(round_position= round_position-1))
				unActive_tournament_rounds_qs.delete()
				total_team= 50
				if tournament_obj.tournament_format == "knock-out":
					tournament_round_obj.is_complete= True					

			else:
				round_position = 1
				total_teams = Team_Registration.objects.filter(Q(tournament= tournament_obj) & Q(status= 'approve'))
				for obj in total_teams:
					Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= obj.team)
				total_teams = total_teams.count()
				if tournament_obj.tournament_format == "knock-out":
					tournament_round_obj.is_complete= True
				else:
					if round_type == "SE" or round_type == "DE":
						tournament_round_obj.playoff_teams = total_teams/2
						tournament_round_obj.is_complete= True
				tournament_round_obj.teams_to_play = total_teams
				tournament_obj.save()

			tournament_round_obj.round_position= round_position
			tournament_obj.is_round_created = True
			tournament_obj.save()

			if total_teams/2 == 1 and round_type in ["DE","SE"] :

				tournament_round_obj.editable= False

			tournament_round_obj.save()
			
			response_json = {'round_id': tournament_round_obj.id, 'success':True}
			
			return Response(response_json, status= status.HTTP_201_CREATED)
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

	def list(self, request):
		try:

			data = request.GET
			request_user = request.buser

			tournament_pk = data.get('tournament_id')
			form_type = data.get('form_type')

			tournament_obj = Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True))

			if form_type and form_type == "simple":
				tournament_round_queryset = Tournament_Round.objects.filter(Q(tournament= tournament_obj) & Q(is_active= True) & Q(is_complete= True)).order_by('round_position')
				serializer= Round_Basic_Info_Serializer(tournament_round_queryset, many= True)
				response_json = {'data':serializer.data, 'success':True}
				return Response(response_json)
			elif form_type and form_type == "admin":
				if not request_user in tournament_obj.admin.all() and not request_user in tournament_obj.manager.all():
					return Response({'success':False, 'error':'Unauthorized'}, status= status.HTTP_401_UNAUTHORIZED)
				tournament_round_queryset = Tournament_Round.objects.filter(Q(tournament= tournament_obj) & Q(is_active= True)).order_by('round_position')
				serializer= Round_Basic_Info_Serializer(tournament_round_queryset, many= True)
				response_json = {'data':serializer.data, 'success':True}
				return Response(response_json)
			else:
				return Response({'success':False, 'error':'wrong option'}, status= status.HTTP_400_BAD_REQUEST)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

	def update(self, request, pk):
		try:

			data = request.data
			request_user = request.buser
			form_type = data.get('form_type')
			print('data',data)
			tournament_round_obj= None
			try:
				tournament_round_obj = Tournament_Round.objects.get(Q(tournament__admin= request_user) & Q(is_active= True) & Q(pk= pk))
			except:
				tournament_round_obj = Tournament_Round.objects.get(Q(tournament__manager= request_user) & Q(is_active= True) & Q(pk= pk))
			if form_type == "basic":
				name= data.get('name')

				if not name or name == "" or len(str(name).strip(' ')) == 0 :
					return Response({'success':False, 'error':'Name can\'t be empty'}, status= status.HTTP_400_BAD_REQUEST)
				
	#			tournament_round_obj = Tournament_Round.objects.get(Q(Q(tournament__admin= request_user) | Q(tournament__manager= request_user)) & Q(is_active= True) & Q(pk= pk))

				tournament_round_obj.name= name
				tournament_round_obj.save()

			elif form_type == "complete":
	#			tournament_round_obj = Tournament_Round.objects.get(Q(Q(tournament__admin= request_user) | Q(tournament__manager= request_user)) & Q(is_active= True) & Q(pk= pk))
				tournament_round_obj.is_complete= True
				tournament_round_obj.save()
			response_json = {'success':True}
			return Response(response_json)
			
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		
	
	def delete(self, request):
		try:

			data = request.GET
			request_user = request.buser
			round_pk= data.get('round')
			tournament_round_obj= None
			try:
				tournament_round_obj = Tournament_Round.objects.get(Q(tournament__admin= request_user) & Q(is_active= True) & Q(pk= round_pk))
			except:
				tournament_round_obj = Tournament_Round.objects.get(Q(tournament__manager= request_user) & Q(is_active= True) & Q(pk= round_pk))
			round_qs= Tournament_Round.objects.filter(Q(tournament= tournament_round_obj.tournament) & Q(pk__gt= round_pk))
			if round_qs.exists():
				round_qs.delete()
			if tournament_round_obj.round_position == 1:
				tournament_round_obj.tournament.is_round_created = False
				tournament_round_obj.tournament.save()
			else:
	#			round_qs= Tournament_Round.objects.filter(Q(tournament= tournament_round_obj.tournament) & Q(pk__gt= round_pk)).delete()
				Tournament_Round_Team_Map.objects.filter(tournament_round__round_position= tournament_round_obj.round_position-1).update(is_grouped= False, choose_for_fixture= False, choose_for_next_round= False)
			tournament_round_obj.delete()

			return Response({'success':True})
			
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

class Tournament_Round_Team_MapViewSet(viewsets.ViewSet):

	permission_classes = [customPermission]
	def list(self, request):
		try:

			data = request.GET
			request_user = request.buser

			round_pk = data.get('round')
			page= data.get('page')
			group_serializer= []
			round_obj= None
			try:
				round_obj= Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__admin= request_user))# | Q(tournament__manager= request_user))
			except:
				round_obj= Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__manager= request_user))

			if not round_obj.round_type == "group":
				team_queryset = Tournament_Round_Team_Map.objects.filter(Q(tournament_round= round_obj)).order_by('id')
#				p = Paginator(team_queryset, 6)
#				total_pages = p.num_pages
#				try:
#					team_queryset = p.page(page)
#				except PageNotAnInteger:
#					team_queryset = p.page(1)
#				except EmptyPage:
#					team_queryset = p.page(1)                            
#
				serializer= []
				for obj in team_queryset:
					image= None
					if obj.team.team_pic:
						image= obj.team.team_pic.url
					serializer.append({'id':obj.team.id, 'name':obj.team.name, 'image':image, 'choose_for_next_round':obj.choose_for_next_round})
			else:
				group_qs= Tournament_Group.objects.filter(Q(tournament_round= round_obj)).order_by('id')
#				
				if not group_qs.exists():
					response_json = {'data':[], 'success':True, 'group_data':[]}
					return Response(response_json)
				for obj in group_qs:
					group_serializer.append({'id':obj.id, 'name':obj.name})
				
				
				group_pk= data.get('group')
					
				team_queryset= None		
				if not group_pk:
	#				team_queryset = Tournament_Group_Team_Map.objects.filter(Q(tournament_group__tournament_round= round_obj)).order_by('id')
					group_pk= group_qs.first().pk
#				else:
				
					print('group_pk-->',group_pk)
				
				team_queryset = Tournament_Group_Team_Map.objects.filter(Q(tournament_group__pk= group_pk)).order_by('id')
				#print("team ciounr", team_queryset.count())
#				p = Paginator(team_queryset, 6)
#				total_pages = p.num_pages
#				try:
#					team_queryset = p.page(page)
#				except PageNotAnInteger:
#					team_queryset = p.page(1)
#				except EmptyPage:
#					team_queryset = p.page(1)                            

				serializer= []
				for obj in team_queryset:
					image= None
					if obj.team.team_pic:
						image= obj.team.team_pic.url
					serializer.append({'id':obj.team.id, 'name':obj.team.name, 'image':image, 'choose_for_next_round':Tournament_Round_Team_Map.objects.get(Q(team=obj.team) & Q(tournament_round= obj.tournament_group.tournament_round)).choose_for_next_round})
				print("serializer-->",serializer)

			response_json = {'data':serializer, 'success':True, 'group_data':group_serializer}
			return Response(response_json)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def retrieve(self, request, pk):
		try:

			data = request.GET
			request_user = request.buser

			round_pk = pk
			page= data.get('page')

			team_queryset = Tournament_Round_Team_Map.objects.filter(Q(tournament_round__pk= round_pk) & Q(choose_for_fixture= False)).order_by('id')
#			p = Paginator(team_queryset, 6)
#			total_pages = p.num_pages
#			try:
#				team_queryset = p.page(page)
#			except PageNotAnInteger:
#				team_queryset = p.page(1)
#			except EmptyPage:
#				team_queryset = p.page(1)                            

			serializer= []
			for obj in team_queryset:
				image= None
				if obj.team.team_pic:
					image= obj.team.team_pic.url
				serializer.append({'id':obj.team.id, 'name':obj.team.name, 'image':image})

			response_json = {'data':serializer, 'success':True, 'total_pages':1}
			return Response(response_json)

		except Exception :
			traceback.print_exc()        	

class GetRoundTeam(viewsets.ViewSet):
	def retrieve(self, request, pk):
                try:

                        data = request.GET
                        request_user = request.buser

                        round_pk = pk
                        page= data.get('page')

                        team_queryset = Tournament_Round_Team_Map.objects.filter(Q(tournament_round__pk= round_pk) & Q(is_grouped= False)).order_by('id')
                        p = Paginator(team_queryset, 12)
                        total_pages = p.num_pages
                        try:
                                team_queryset = p.page(page)
                        except PageNotAnInteger:
                                team_queryset = p.page(1)
                        except EmptyPage:
                                team_queryset = p.page(1)

                        serializer= []
                        for obj in team_queryset:
                                image= None
                                if obj.team.team_pic:
                                        image= obj.team.team_pic.url
				
                                serializer.append({'id':obj.team.id, 'name':obj.team.name, 'image':image})

                        response_json = {'data':serializer, 'success':True, 'total_pages':total_pages}
                        return Response(response_json)

                except Exception :
                        traceback.print_exc()

                        return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)


class Tournament_GroupViewSet(viewsets.ViewSet):

	permission_classes = [customPermission]
	def create(self, request):
		try:
			data=request.data
			request_user = request.buser
			round_pk = data.get('round')
			group_name = data.get('name')
			print(data)	
			if not round_pk or not group_name or len(str(group_name).strip(' ')) == 0 :
				return Response({"success":False, "error":"Incomplete Information"}, status= status.HTTP_400_BAD_REQUEST)
			#-----Added By Gaurav---------
			tournament_round_obj= None
			try:
				try:
					tournament_round_obj = Tournament_Round.objects.get(Q(tournament__admin= request_user) & Q(is_active= True) & Q(pk= round_pk))
				except:
					tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__manager= request_user) & Q(is_active= True))
			except:
				return Response({"success":False, "error":"Tournament round does not exist"}, status= status.HTTP_400_BAD_REQUEST)
				

			if not tournament_round_obj.round_type == "group":
				return Response({"success":False, "error":"Not a Group Type Round"}, status= status.HTTP_400_BAD_REQUEST)
			elif tournament_round_obj.is_complete:
				return Response({"success":False, "error":"Round is complete"}, status= status.HTTP_400_BAD_REQUEST)

			tournament_group_obj= Tournament_Group.objects.create(tournament_round= tournament_round_obj, name= group_name)
			tournament_group_obj.save()

			response_json = {'group_id': tournament_group_obj.id, 'round_id': tournament_round_obj.id, 'success':True}

			return Response(response_json, status= status.HTTP_201_CREATED)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def list(self, request):
		try:

			data = request.GET
			request_user = request.buser

			round_pk = data.get('round')
			tournament_group_queryset= None

			round_obj= Tournament_Round.objects.get(pk= round_pk)
			if request_user in round_obj.tournament.admin.all():
#			try:
				tournament_group_queryset = Tournament_Group.objects.filter(Q(tournament_round__pk= round_pk) & Q(tournament_round__tournament__admin= request_user)).order_by('id')
			else:
#			except:
				tournament_group_queryset = Tournament_Group.objects.filter(Q(tournament_round__pk= round_pk) & Q(tournament_round__tournament__manager= request_user)).order_by('id')
#				print(tournament_goup_serializer)

			serializer = Tournament_Group_Serializer(tournament_group_queryset, many= True)
			print("group-->", serializer.data)
			return Response({'data':serializer.data, 'success':True})

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def retrieve(self, request, pk):
		try:

			data = request.GET
			request_user = request.buser
			page= data.get('page')
#			tour_obj= checkTournamentPermission()
			tournament_group_obj = Tournament_Group.objects.get(pk=pk)#Q(tournament_round__tournament__manager= request_user) | Q(tournament_round__tournament__admin= request_user) , Q(pk= pk))
#a			tournament_group_obj = Tournament_Group.objects.get(pk= pk, tournament_round__tournament__admin= request_user, tournament_round__tournament__manager= request_user)
			# tournament_round_obj = tournament_group_obj.tournament_round
			if not request_user in tournament_group_obj.tournament_round.tournament.admin.all() and not request_user in tournament_group_obj.tournament_round.tournament.manager.all():
				print("not a valid user", request_user)
				return Response({'success':False, 'error':'Not a valid User'}, status= status.HTTP_400_BAD_REQUEST)

			tournament_group_team_map_queryset = Tournament_Group_Team_Map.objects.filter(tournament_group= tournament_group_obj)
			
			p = Paginator(tournament_group_team_map_queryset, 6)
			total_pages = p.num_pages
			try:
				tournament_group_team_map_queryset = p.page(page)
			except PageNotAnInteger:
				tournament_group_team_map_queryset = p.page(1)
			except EmptyPage:
				tournament_group_team_map_queryset = p.page(1)                            


			serializer= []
			for obj in tournament_group_team_map_queryset:
				image= None
				if obj.team.team_pic:
					image= obj.team.team_pic.url
				serializer.append({'id':obj.team.id, 'name':obj.team.name, 'image':image})

			
			response_json = {'data':serializer, 'success':True, 'total_pages':total_pages}
			return Response(response_json)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def update(self, request, pk):
		try:
			data = request.data
			request_user = request.buser
			print( "data-->",data)
			group_name = data.get('name')

			if not group_name or group_name == "null" or len(str(group_name).strip(' ')) == 0 :
				return Response({'success':False, 'error':'Invalid Data', 'group_name':False}, status= status.HTTP_400_BAD_REQUEST)				
#			tournament_group_obj.name = group_name
			tournament_group_obj= None
			try:
				tournament_group_obj = Tournament_Group.objects.get(Q(pk= pk) & Q(tournament_round__tournament__manager= request_user))
			except:
				tournament_group_obj = Tournament_Group.objects.get(Q(pk= pk) & Q(tournament_round__tournament__admin= request_user))
			tournament_group_obj.name = group_name
			tournament_group_obj.save()

			return Response({'success':True})

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def delete(self, request):
		try:
			data = request.GET
			request_user = request.buser
			print( "data-->",data)
			group_pk = data.get('group')
			tournament_group_obj= None
			try:
				tournament_group_obj = Tournament_Group.objects.get(Q(pk= group_pk) & Q(tournament_round__tournament__admin= request_user))          
			except:
				tournament_group_obj = Tournament_Group.objects.get(Q(pk= group_pk) & Q(tournament_round__tournament__manager= request_user))
			if Tournament_Group_Team_Map.objects.filter(tournament_group= tournament_group_obj).exists():
				return Response({'success':False, 'error':'First shift the teams'}, status= status.HTTP_400_BAD_REQUEST)
			
			tournament_group_obj.delete()
			return Response({'success':True})

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class Tournament_Group_Team_MapViewSet(viewsets.ViewSet):

	permission_classes = [customPermission]
	def create(self, request):
		try:
			data=request.data
			request_user = request.buser
			# print "data--->",data
			tournament_group_pk = data.get('group')
			form_type= data['form_type']
			team_pk = data.get('team')

			if not tournament_group_pk :
				return Response({"success":False, "error":"Información incompleta"}, status= status.HTTP_400_BAD_REQUEST)
			tournament_group_obj= None
			try:
				tournament_group_obj = Tournament_Group.objects.get(Q(pk= tournament_group_pk) & Q(tournament_round__tournament__admin= request_user))
			except:
				tournament_group_obj = Tournament_Group.objects.get(Q(pk= tournament_group_pk) & Q(tournament_round__tournament__manager= request_user))
			tournament_round_obj = tournament_group_obj.tournament_round

			if not tournament_round_obj.editable or tournament_round_obj.is_fixture_created or tournament_round_obj.is_complete: 
				return Response({"success":False, "error":"can't edit the group"}, status= status.HTTP_400_BAD_REQUEST)

			if form_type == "add":
				print(tournament_round_obj, team_pk, tournament_round_obj.is_complete)
				team_obj = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team_pk) & Q(tournament_round__is_complete= False))
				tournament_group_team_map_obj, created = Tournament_Group_Team_Map.objects.get_or_create(tournament_group= tournament_group_obj, team= team_obj.team)
				# tournament_group_team_map_obj.save()
				team_obj.is_grouped= True
				team_obj.save()
			elif form_type == "remove":
				tournament_group_team_map_qs= Tournament_Group_Team_Map.objects.get(Q(tournament_group= tournament_group_obj) & Q(team__pk= team_pk))
				team_obj = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(is_grouped= True) & Q(team= tournament_group_team_map_qs.team))									
				team_obj.is_grouped= False
				team_obj.save()
				tournament_group_team_map_qs.delete()
						
			return Response({'success':True}, status= status.HTTP_200_OK)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class FixtureViewSet(viewsets.ViewSet):

	permission_classes = [customPermission, ]

	def get_venue(self,fixture_obj):
		venue= None
		if fixture_obj.tournament_venue:
			venue_obj= fixture_obj.tournament_venue
			if venue_obj.is_dummy:
				venue= {'id': venue_obj.id, 'name':venue_obj.court_name}
			else:
				venue= {'id': venue_obj.id, 'name':venue_obj.court.venue.name + str(" ( ") + venue_obj.court.name + str(" )")}

		return venue

	def get_number(self, fixture_obj, tournament_id):
		number= fixture_obj.id
		if number < 10000:
			multiplier= 0
			if number<10:
				multiplier= 3
			elif number <100:
				multiplier= 2
			elif number<1000:
				multiplier=1
			req= '0'*multiplier + str(number)
			print("inside if")
		else:
			req= str(number[-4:-1])
		print("req-->",req)
		if tournament_id < 100:
			if tournament_id <10:
				tournament_id= '0'+ str(tournament_id)
			pass
		else:
			tournament_id= str(tournament_id)[-3:-1]
			print("tour_id--->", tournament_id)
		
		random_number= req + str(tournament_id)
		return random_number

	def post(self, request):
		try:
			data=request.data
			request_user = request.buser
			round_pk = data.get('round')
			team1_pk = data.get('team1')
			team2_pk = data.get('team2')
			date_time= data.get('date_time')
			venue_pk = data.get('venue')
			print('round', round_pk, data)
			if not team1_pk or not team2_pk :
				return Response({"error":"Team detail is mendatory", "success":False}, status= status.HTTP_400_BAD_REQUEST)

			if not venue_pk :
				return Response({'success':False, 'error': 'Ground can\'t be null'}, status= status.HTTP_400_BAD_REQUEST)

			if venue_pk and not venue_pk == "":
				try:
					venue_obj= Tournament_Venue_Map.objects.get(Q(pk=venue_pk) & Q(is_active= True))
				except Exception as e:
					return Response({'success':False, 'error': 'Invalid Ground Id'}, status= status.HTTP_400_BAD_REQUEST)
			else:
				venue_obj= None

			try:
				date_time= datetime.datetime.strptime(date_time, "%Y/%m/%d %I:%M %p")
			except Exception:
				return Response({'success':False, 'error': 'invalid date time format'}, status= status.HTTP_400_BAD_REQUEST)
			tournament_round_obj= None
			try:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__admin= request_user) & Q(is_active= True))
			except:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__manager= request_user) & Q(is_active= True))
	      	
			if tournament_round_obj.round_type== "SE" or tournament_round_obj.round_type== "knock-out":
				team_obj1 = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team1_pk))
				team_obj2 = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team2_pk))
				fixture_obj = Match_Stat.objects.create(tournament_round= tournament_round_obj, team1= team_obj1.team, team2= team_obj2.team, date_time= date_time)
				fixture_obj.tournament_venue= venue_obj
				random_number= self.get_number(fixture_obj, tournament_round_obj.tournament.id)
				print("rabdom_id-->",random_number)
				fixture_obj.match_id= random_number
				fixture_obj.save()
				loop.run_in_executor(None, create_fixture_notify, fixture_obj)
				try:
					if request.buser in team_obj1.tournament_round.tournament.manager.all():
						admins = team_obj1.tournament_round.tournament.admin.all()
						print(admins, '------>>>>>>>>>>>>')
						for a in admins:
							BusinessNotification.objects.create(user = a, message = "{0} chsanged a fixture schedule.".format(request.buser.name))	
				except :
					traceback.print_exc()

				Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= team_obj1.team)
				Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= team_obj2.team)
				team_obj1.choose_for_fixture = True
				team_obj1.save()
				team_obj2.choose_for_fixture = True
				team_obj2.save()
				tournament_round_obj.is_fixture_created = True
				# tournament_round_obj.save()

				tournament_round_obj.editable= False
				tournament_round_obj.save()
				return Response({"success":True}, status= status.HTTP_201_CREATED)

			elif tournament_round_obj.round_type== "DE":
				team_obj1 = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team1_pk))
				team_obj2 = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team2_pk))
				fixture_obj1 = Match_Stat.objects.create(tournament_round= tournament_round_obj, team1= team_obj1.team, team2= team_obj2.team, date_time= date_time)
				fixture_obj1.tournament_venue= venue_obj
				random_number= self.get_number(fixture_obj1, tournament_round_obj.tournament.id)
				fixture_obj1.match_id= random_number
				fixture_obj1.save()
				loop.run_in_executor(None, create_fixture_notify, fixture_obj1)
				Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj1, team= team_obj1.team)
				Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj1, team= team_obj2.team)
				team_obj1.choose_for_fixture = True
				team_obj1.save()
				team_obj2.choose_for_fixture = True
				team_obj2.save()

				fixture_obj2 = Match_Stat.objects.create(tournament_round= tournament_round_obj, team1= team_obj2.team, team2= team_obj1.team, date_time= date_time)
				fixture_obj2.tournament_venue= venue_obj
				random_number= self.get_number(fixture_obj2, tournament_round_obj.tournament.id)
				fixture_obj2.match_id= random_number
				fixture_obj2.save()
				loop.run_in_executor(None, create_fixture_notify, fixture_obj2)
				Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj2, team= team_obj1.team)
				Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj2, team= team_obj2.team)
	      		
				tournament_round_obj.is_fixture_created = True
				tournament_round_obj.editable= False
				tournament_round_obj.save()

				return Response({"success":True}, status= status.HTTP_201_CREATED)
			else:
				return Response({"success":False, "error":"Ronda es del tipo de grupo"}, status= status.HTTP_400_BAD_REQUEST)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def list(self, request):
		try:
			data = request.GET
			request_user= request.buser

			round_pk = data.get('round')
			page = data.get('page')
			tournament_round_obj= None
			try:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__admin= request_user) )
			except:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__manager= request_user) )
			tournament_obj = tournament_round_obj.tournament

			if not tournament_round_obj.is_active: 
				return Response({"error":"Round detail invalid", "success":False}, status= status.HTTP_400_BAD_REQUEST)
			response_data = []
			total_pages = 1
			if tournament_round_obj.round_type== "group":
				group_pk = data.get('group')
				if not group_pk or group_pk == "null":
					if tournament_round_obj.is_fixture_created:
						group_pk = Tournament_Group.objects.filter(tournament_round= tournament_round_obj).order_by('id')[0].pk
					else:
						response_data = {}
						response_data.update({'round_name':tournament_round_obj.name, 'tournament_id':tournament_round_obj.tournament.id, 'data': [], 'is_fixture_created':tournament_round_obj.is_fixture_created})
						return Response(response_data)
				response_data = {}
				print("group_pk--->", group_pk)
				tournament_group_obj = Tournament_Group.objects.get(Q(tournament_round= tournament_round_obj) & Q(pk= group_pk))
				fixture_queryset = Match_Stat.objects.filter(Q(tournament_round= tournament_round_obj) & Q(tournament_group= tournament_group_obj)).order_by('id')
				fixture_data = []
				if fixture_queryset.exists():
					p = Paginator(fixture_queryset, 6)
					total_pages = p.num_pages
					try:
						fixture_queryset = p.page(page)
					except PageNotAnInteger:
						fixture_queryset = p.page(1)
					except EmptyPage:
						fixture_queryset = p.page(1)                                                        
					for fixture_obj in fixture_queryset:
						team1_name = fixture_obj.team1.name
						team2_name = fixture_obj.team2.name
						date_time = None
						if fixture_obj.date_time:
							date_time = fixture_obj.date_time.strftime("%Y/%-m/%d %I:%M %p")
						venue= self.get_venue(fixture_obj)
						fixture_data.append({'fixture_id':fixture_obj.id, 'team1':team1_name, 'team2':team2_name, 'date_time':date_time, 'venue':venue, 'match_id':fixture_obj.match_id, 'is_result_declared':fixture_obj.is_result_declared})
				else:
					fixture_data = []
				group_data= Tournament_Group_Serializer(Tournament_Group.objects.filter(tournament_round= tournament_round_obj).order_by('id'), many= True)
				response_data.update({'round_name':tournament_round_obj.name, 'tournament_id':tournament_round_obj.tournament.id, 'data': fixture_data, 'number_of_match':tournament_round_obj.number_of_match, 'is_fixture_created':tournament_round_obj.is_fixture_created, 'group_name': tournament_group_obj.name, 'total_pages':total_pages, 'group_data':group_data.data, 'group_id':tournament_group_obj.id})
				return Response(response_data)

			else:
				page = data.get('page')
				page_length = 6
			
				fixture_queryset = Match_Stat.objects.filter(tournament_round= tournament_round_obj).order_by('id')
				fixture_data = []
				if fixture_queryset.exists():
					p = Paginator(fixture_queryset, 6)
					total_pages = p.num_pages
					try:
						fixture_queryset = p.page(page)
					except PageNotAnInteger:
						fixture_queryset = p.page(1)
					except EmptyPage:
						fixture_queryset = p.page(1)                                

					for fixture_obj in fixture_queryset:
						team1_name = fixture_obj.team1.name

						team2_name = fixture_obj.team2.name
						# # print "team2_name-->",fixture_team_map_queryset.last().team.team_name
						date_time = None
						if fixture_obj.date_time:
							date_time= datetime.datetime.strftime(fixture_obj.date_time, "%Y/%-m/%d %I:%M %p")

						venue= self.get_venue(fixture_obj)
#
						fixture_data.append({'fixture_id':fixture_obj.id, 'team1':team1_name, 'team2':team2_name, 'date_time':date_time, 'venue':venue, 'match_id':fixture_obj.match_id, 'is_result_declared':fixture_obj.is_result_declared})

				fixture_data = {'round_name':tournament_round_obj.name, 'tournament_id':tournament_round_obj.tournament.id, 'data': fixture_data, 'number_of_match':tournament_round_obj.number_of_match, 'is_fixture_created':tournament_round_obj.is_fixture_created, "total_pages":total_pages, 'group_data':None, 'round_type':tournament_round_obj.round_type}
				return Response(fixture_data)


		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

	def update(self, request, pk):
		try:
			data=request.data
			request_user = request.buser

			fixture_pk = pk
			date_time = data.get('date_time')
			venue_pk = data.get('venue')

			print("request_user--->", request_user, data)
#			date_time= None

			new_date_time= None
			try:
				new_date_time = datetime.datetime.strptime(date_time, "%Y/%m/%d %I:%M %p")
			except :
				#print("inside not date")
				traceback.print_exc()
				return Response({'success':False, 'date_format':False, 'error':'Please neter a valid date format'}, status= status.HTTP_400_BAD_REQUEST)

			venue_obj= None
			try:
				venue_obj = Tournament_Venue_Map.objects.get(Q(pk= venue_pk) & Q(is_active= True))
			except :
				traceback.print_exc()
				#pass
				return Response({'success':False, 'error':'Ground can\'t be null'}, status= status.HTTP_400_BAD_REQUEST)
			fixture_obj= None
			try:
				fixture_obj = Match_Stat.objects.get(Q(pk= fixture_pk) & Q(tournament_round__tournament__admin= request_user))
			except:
				fixture_obj = Match_Stat.objects.get(Q(pk= fixture_pk) & Q(tournament_round__tournament__manager= request_user))
			fixture_obj.date_time = new_date_time 
			fixture_obj.tournament_venue = venue_obj	
			fixture_obj.save()          
			fixture_obj.save()                  
			loop.run_in_executor(None, update_fixture_notify, fixture_obj)
			try:
				
				if request.buser in fixture_obj.tournament_round.tournament.manager.all():
					print('inside >>>>>>>>>>>')
					admins = fixture_obj.tournament_round.tournament.admin.all()
					for a in admins:
						print(a)
						BusinessNotification.objects.create(user = a, message = "{0} changed a fixture schedule.".format(request.buser.name))
			except :
				traceback.print_exc()
				pass
			return Response({'success':True})

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)
	
	def delete(self, request):
		try:
			data= request.GET
			round_id= data.get('round_id')
			request_user= request.buser
			print(request_user)
			round_obj= None
			try:
				round_obj= Tournament_Round.objects.get(Q(pk= round_id) & Q(tournament__admin= request_user) & Q(tournament__is_active= True))
			except:
				round_obj= Tournament_Round.objects.get(Q(pk= round_id) & Q(tournament__manager= request_user) & Q(tournament__is_active= True))
			if round_obj.is_fixture_created:
				round_team_map_qs= Tournament_Round_Team_Map.objects.filter(tournament_round= round_obj).update(choose_for_fixture= False)
				match_qs = Match_Stat.objects.filter(Q(tournament_round= round_obj))# & Q(tournament__admin= request_user))# & Q(is_active= True))
				match_qs.delete()

			return Response({'success':True, 'message':'Fixtures deleted successfully'}, status= status.HTTP_200_OK)            
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

class createMatch(viewsets.ViewSet):
	permission_classes= [customPermission]

	def get_number(self, fixture_obj, tournament_id):
		number= fixture_obj.id
		if number < 10000:
			multiplier= 0
			if number<10:
				multiplier= 3
			elif number <100:
				multiplier= 2
			elif number<1000:
				multiplier=1
			req= '0'*multiplier + str(number)
		else:
			req= str(number[-4:-1])
		print("number-->", req)
		if tournament_id < 100:
			if tournament_id < 10:
				tournament_id= "0"+ str(tournament_id)
			pass
		else:
			tournament_id= str(tournament_id)[-3:-1]
		print("tour_id-->", tournament_id)
		
		random_number= req + str(tournament_id)
		return random_number
	
	def create(self, request):
		
		data= request.data
		request_user= request.buser
		tournament_pk= data.get('tournament_id')
		number_of_match = data.get('no_of_match')
		round_pk = data.get('round')

		print('number-->',number_of_match)
		tournament_obj= None
		try:
			tournament_obj = Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True) & Q(admin= request_user))
		except:
			tournament_obj = Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True) & Q(manager= request_user))
		
		tournament_round_obj = Tournament_Round.objects.get(Q(tournament= tournament_obj) & Q(pk= round_pk))# & Q(tournament__created_by= request_user))
		tournament_obj = tournament_round_obj.tournament
		print("fixture_created-->", tournament_round_obj.is_fixture_created)
		if tournament_round_obj.is_fixture_created :

			fixture_queryset = Match_Stat.objects.filter(tournament_round= tournament_round_obj)
			fixture_queryset.delete()
		if tournament_obj.tournament_format == "knock-out" :
			round_team_obj= Tournament_Round_Team_Map.objects.filter(tournament_round= tournament_round_obj)
			i = 0
			for obj in round_team_obj:
				if i >= round_team_obj.count():
					break
				obj1= round_team_obj[i].team
				i+= 1
				try:
					obj2= round_team_obj[i].team
				except:
					break
				match_stat_obj, created= Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, team1= obj1, team2= obj2)
				Match_Stat_Team.objects.get_or_create(match_stat= match_stat_obj, team= obj1)
				Match_Stat_Team.objects.get_or_create(match_stat= match_stat_obj, team= obj2)
				i+= 1

			tournament_round_obj.is_fixture_created = True
			tournament_round_obj.editable = False
			tournament_round_obj.number_of_match = 1
			tournament_round_obj.save()

		elif tournament_obj.tournament_format == "league" and tournament_round_obj.round_type== "league":

			round_team_map_qs = Tournament_Round_Team_Map.objects.filter(tournament_round= tournament_round_obj)
			if int(number_of_match) == 1:
				for obj in round_team_map_qs:
					round_team_map_qs= round_team_map_qs.exclude(pk= obj.id)
					if not round_team_map_qs.exists():
						break
					for obj1 in round_team_map_qs:
						fixture_obj, created = Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, team1= obj.team, team2= obj1.team)

						fixture_obj.match_id = self.get_number(fixture_obj, tournament_pk)
						fixture_obj.save()
						Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj.team)
						Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj1.team)

			elif int(number_of_match) == 2:
				for obj in round_team_map_qs:
					round_team_map_qs= round_team_map_qs.exclude(pk= obj.id)
					if not round_team_map_qs.exists():
						break
					print("demo_qs-->",round_team_map_qs.count())
					for obj1 in round_team_map_qs:
										  
						fixture_obj, created = Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, team1= obj.team, team2= obj1.team)

						fixture_obj.match_id = self.get_number(fixture_obj, tournament_pk)
						fixture_obj.save()
						Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj.team)
						Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj1.team)
						
						fixture_obj1, created1 = Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, team1= obj1.team, team2= obj.team)

						fixture_obj1.match_id = self.get_number(fixture_obj1, tournament_pk)
						fixture_obj1.save()
						Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj1, team= obj.team)
						Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj1, team= obj1.team)

			tournament_round_obj.is_fixture_created = True
			tournament_round_obj.editable = False
			# tournament_round_obj.number_of_match = 1
			tournament_round_obj.save()


		elif tournament_obj.tournament_format == "cup" and tournament_round_obj.round_type== "group":
			print("number_of_match",number_of_match, "inside cup and group")
			team1_qs= Tournament_Round_Team_Map.objects.filter(Q(tournament_round= tournament_round_obj))
			if team1_qs.exists() and not team1_qs.filter(is_grouped= False).exists():
				tournament_round_obj.is_complete= True
				tournament_round_obj.save()
			else:
				return Response({'success':False, 'error':'Either you have no teams or some ungrouped teams'}, status= status.HTTP_400_BAD_REQUEST)
			tournament_group_queryset = Tournament_Group.objects.filter(tournament_round= tournament_round_obj)
			if int(number_of_match) == 1:
				print("inside 1", tournament_group_queryset.count())
				# tournament_round_obj.editable = False
				for group_obj in tournament_group_queryset:

					tournament_group_team_queryset = Tournament_Group_Team_Map.objects.filter(tournament_group= group_obj) 
					for obj in tournament_group_team_queryset:
						tournament_group_team_queryset= tournament_group_team_queryset.exclude(pk= obj.id)
						if not tournament_group_team_queryset.exists():
							break
						# print("demo_qs-->",tournament_group_team_queryset.count())
						for obj1 in tournament_group_team_queryset:
							fixture_obj, created = Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, tournament_group= group_obj, team1= obj.team, team2= obj1.team)
							fixture_obj.match_id = self.get_number(fixture_obj, tournament_pk)
							fixture_obj.save()
							print("match_id--->", fixture_obj.match_id)
							Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj.team)
							Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj1.team)

		
			elif int(number_of_match) == 2:
				for group_obj in tournament_group_queryset:
					tournament_group_team_queryset = Tournament_Group_Team_Map.objects.filter(tournament_group= group_obj) 
					for obj in tournament_group_team_queryset:
						tournament_group_team_queryset= tournament_group_team_queryset.exclude(pk= obj.id)
						if not tournament_group_team_queryset.exists():
							break
						print("demo_qs-->",tournament_group_team_queryset.count())
						for obj1 in tournament_group_team_queryset:
											  
							fixture_obj, created = Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, tournament_group= group_obj, team1= obj.team, team2= obj1.team)
							fixture_obj.match_id = self.get_number(fixture_obj, tournament_pk)
							fixture_obj.save()
							Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj.team)
							Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj, team= obj1.team)
							fixture_obj1, created1 = Match_Stat.objects.get_or_create(tournament_round= tournament_round_obj, tournament_group= group_obj, team1= obj1.team, team2= obj.team)
							fixture_obj1.match_id = self.get_number(fixture_obj1, tournament_pk)
							fixture_obj1.save()
							Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj1, team= obj.team)
							Match_Stat_Team.objects.get_or_create(match_stat= fixture_obj1, team= obj1.team)

			else:
				return Response({'success':False, 'error':'invalid option'}, status= status.HTTP_400_BAD_REQUEST)
			tournament_round_obj.is_fixture_created = True
			tournament_round_obj.editable = False
			tournament_round_obj.number_of_match = number_of_match
			tournament_round_obj.save()
		else:
			return Response({'success':False, 'error':'invalid round id'}, status= status.HTTP_400_BAD_REQUEST)
		return Response({'success':True}, status= status.HTTP_201_CREATED)

	def delete(self, request):
		try:
			data= request.GET
			match_pk= data.get('match_id')
			request_user= request.buser
			print(request_user)
			
			match_obj = Match_Stat.objects.get(Q(pk= match_pk))
			if match_obj.tournament_round and not match_obj.tournament_round.round_type == "group" and (request_user in match_obj.tournament_round.tournament.admin.all() or request_user in match_obj.tournament_round.tournament.manager.all()):
				round_team_map_qs= Tournament_Round_Team_Map.objects.filter(Q(tournament_round= match_obj.tournament_round) & (Q(team__pk= match_obj.team1.pk) | Q(team__pk= match_obj.team2.pk))).update(choose_for_fixture= False)
				match_obj.delete()

			return Response({'success':True, 'message':'Fixtures deleted successfully'}, status= status.HTTP_200_OK)            
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)    		

#class Tournament_Round_CompleteViewSet(viewsets.ViewSet):
#
#	def create(self, request):
#		try:
#
#			data = request.data
#			request_user = request.buser
#			round_pk = data.get('round')
#			form_type = data.get('form_type')
#
#			tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk) & Q(tournament__admin= request_user))
#			
#			if form_type == "publish":
#				Tournament_Round_Team_Map.objects.filter(tournament_round= tournament_round_obj).update(match_played= 0, won= 0, lost= 0, ties= 0, points= 0)
#				fixture_qs = Match_Stat.objects.filter(Q(tournament_round= tournament_round_obj) & Q(is_result_declared= True))
#				print("fixture_qs-->",tournament_round_obj.tournament.is_round_created)
#				for fixture_obj in fixture_qs :
#					match_team_goal_qs = Match_Stat_Team.objects.filter(Q(match_stat= fixture_obj) & Q(is_submit= True)).order_by('-total_run')
#					print("match_team_goal_qs-->",match_team_goal_qs)
#					if not match_team_goal_qs.exists():
#						continue
#					if not match_team_goal_qs.first().total_run == match_team_goal_qs.last().total_run:
#
#							tournament_round_team_map_obj = Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= match_team_goal_qs.first().team)[0]
#							tournament_round_team_map_obj.points += 3
#							tournament_round_team_map_obj.won += 1
#							tournament_round_team_map_obj.save()
#					
#					else:
#						
#						for match_team_goal_obj in match_team_goal_qs:
#							# print "here-->",match_team_goal_obj.match.team
#							tournament_round_team_map_obj = Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= match_team_goal_obj.team)[0]
#							tournament_round_team_map_obj.points += 1
#							tournament_round_team_map_obj.ties += 1
#							tournament_round_team_map_obj.save()
#
#					tournament_round_team_map_obj1 = Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= match_team_goal_qs[0].team)[0]
#					tournament_round_team_map_obj2 = Tournament_Round_Team_Map.objects.get_or_create(tournament_round= tournament_round_obj, team= match_team_goal_qs[1].team)[0]
#					
#					tournament_round_team_map_obj1.total_run += match_team_goal_qs.first().total_run
#					tournament_round_team_map_obj1.runs_against += match_team_goal_qs.first().run_against
#
#					tournament_round_team_map_obj2.total_run += match_team_goal_qs.last().total_run
#					tournament_round_team_map_obj2.runs_against += match_team_goal_qs.last().run_against
#
#					tournament_round_team_map_obj1.match_played += 1
#					tournament_round_team_map_obj2.match_played += 1
#
#					fixture_obj.save()
#					tournament_round_team_map_obj1.save()
#					tournament_round_team_map_obj2.save()
#
#				tournament_round_obj.is_result_declared = True
#				tournament_round_obj.save()
#				return Response({"success":True}, status= status.HTTP_200_OK)
#			else:
#				# print "Invalid Option"
#				return Response({'error':"Invalid Option", "success":False}, status= status.HTTP_400_BAD_REQUEST)
#			
#			return Response("updated", status= status.HTTP_205_RESET_CONTENT)
#
#		except Exception :
#			traceback.print_exc()        	
#			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

#class ResultViewSet(viewsets.ViewSet):
#
#	def retrieve(self, request, pk):
#		try:
#			tournament_pk = pk
#			round_serializer = []
#			tournament_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_active= True))
#			print('tournament_obj',tournament_obj)
#			if tournament_obj.tournament_format == 'league':
#				# print('tournament_obj.tournament_format', tournament_obj.tournament_format)
#				tournament_round_obj = Tournament_Round.objects.get(Q(tournament= tournament_obj) & Q(is_result_declared= True))
#				round_team_map_qs = Tournament_Round_Team_Map.objects.filter(Q(tournament_round= tournament_round_obj))
#
#				team_serializer = ResultSerializer(round_team_map_qs, many= True)
#				team_serializer = team_serializer.data
#				team_serializer.sort(key=lambda x: (x['points'], x['total_run']-x['runs_against'], x['won']))
#				team_serializer = list(reversed(team_serializer))
#				# print('team_serializer',team_serializer)
#				round_serializer.append({'round_name':tournament_round_obj.name, 'group': team_serializer})
#
#			elif tournament_obj.tournament_format == 'cup':
#				tournament_round_qs = Tournament_Round.objects.filter(Q(tournament__pk= tournament_pk) & Q(is_result_declared= True) & Q(round_type= "group"))
#				for round_obj in tournament_round_qs:
#					group_serializer = []
#					group_qs = Tournament_Group.objects.filter(tournament_round= round_obj).order_by('id')
#					for group_obj in group_qs:
#						group_team_map_qs = Tournament_Group_Team_Map.objects.filter(tournament_group= group_obj)
#						round_team_map_qs = Tournament_Round_Team_Map.objects.filter(Q(team__in= [obj.team for obj in group_team_map_qs]) & Q(tournament_round= group_obj.tournament_round))
#
#						team_serializer = ResultSerializer(round_team_map_qs, many= True)
#						team_serializer = team_serializer.data
#						team_serializer.sort(key=lambda x: (x['points'], x['total_run']-x['runs_against'], x['won']))
#						team_serializer = list(reversed(team_serializer))
#						group_serializer.append({'group_name': group_obj.name, 'team': team_serializer})
#
#					round_serializer.append({'round_name':round_obj.name, 'group': group_serializer})
#
#			return Response(round_serializer)
#			
#		except Exception :
#			traceback.print_exc()        	
#			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class ShiftTeamViewSet(viewsets.ViewSet):

	permission_classes = [customPermission]
	def create(self,request):
		try:
			request_user=request.buser
			data=request.data
			round_pk=data.get('round')
			team_pk = data.get('team')
			tournament_round_obj= None
			try:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk)  & Q(tournament__admin= request_user) & Q(is_active= True))
			except:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk)  & Q(tournament__manager= request_user) & Q(is_active= True))
			next_round_obj = Tournament_Round.objects.get(Q(tournament= tournament_round_obj.tournament) & Q(round_position= tournament_round_obj.round_position+1))
			if not next_round_obj.editable:
				return Response({'success':False, 'error':'Next Round is not editable'}, status= status.HTTP_400_BAD_REQUEST)
			team_obj = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team_pk))
			tournament_round_team_map_obj = Tournament_Round_Team_Map.objects.get_or_create(tournament_round= next_round_obj, team= team_obj.team)
			team_obj.choose_for_next_round= True
			team_obj.save()
			loop.run_in_executor(None, shift_team_notify, tournament_round_team_map_obj[0])
			return Response({'success':True},status= status.HTTP_201_CREATED)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Please create next Round First'}, status= status.HTTP_400_BAD_REQUEST)


	def update(self, request, pk):
		try:
			request_user=request.buser
			data=request.data
			round_pk= pk
			team_pk = data.get('team')
			tournament_round_obj= None
			try:

				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk)  & Q(admin= request_user) & Q(is_active= True))
			except:
				tournament_round_obj = Tournament_Round.objects.get(Q(pk= round_pk)  & Q(manager= request_user) & Q(is_active= True))
#			if tournament_round_obj.round:a
			next_round_obj = Tournament_Round.objects.get(Q(tournament= tournament_round_obj.tournament) & Q(round_position= tournament_round_obj.round_position+1))
			if not next_round_obj.editable:
				return Response({'success':False, 'error':'Next Round is not editable'}, status= status.HTTP_400_BAD_REQUEST)
			# Tournament_Round_Team_Map.objects.filter(~Q(pk__in= team_list) & Q(tournament_round = next_round_obj)).delete()
			#for team_pk in team_list:
			team_obj = Tournament_Round_Team_Map.objects.get(Q(tournament_round= tournament_round_obj) & Q(team__pk= team_pk))
			tournament_round_team_map_obj = Tournament_Round_Team_Map.objects.get_or_create(tournament_round= next_round_obj, team= team_obj.team)
			team_obj.choose_for_next_round= True
			team_obj.save()
			return Response({'success':True},status= status.HTTP_201_CREATED)

		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Please create next Round First'}, status= status.HTTP_400_BAD_REQUEST)

class RedirectDemo(viewsets.ViewSet):

	# permission_classes = [TokenHasReadWriteScope]
	def create(self,request):
		try:
			print(request)
			return redirect('https://www.google.co.in/?gfe_rd=cr&dcr=0&ei=4L4bWuOBNIeH2QTn76h4')			
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class RedirectDemo(viewsets.ViewSet):

	# permission_classes = [TokenHasReadWriteScope]
	def create(self,request):
		try:
			print(request)
			return redirect('https://www.google.co.in/?gfe_rd=cr&dcr=0&ei=4L4bWuOBNIeH2QTn76h4')			
		except Exception :
			traceback.print_exc()        	
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class ListTournament(viewsets.ViewSet):
	def create(self, request):
		try:
			data = request.data
			request_user = request.user
			print("data-->", data)
			page= data.get('page')
			latitude= data.get('latitude')
			longitude= data.get('longitude')
			min_distance= int(data.get('min_distance'))
			max_distance= int(data.get('max_distance'))
			min_prize= int(data.get('min_prize'))
			max_prize= int(data.get('max_prize'))
			min_entry= int(data.get('min_entry'))
			max_entry= int(data.get('max_entry'))
			type_of_team= data.get('type_of_team')
			type_of_tournament= data.get('type_of_tournament')
			

			tournament_qs = Tournament_Detail.objects.filter(Q(is_active= True) & Q(is_certified= True))
			print("before",tournament_qs, min_prize, max_prize)
			tournament_qs= tournament_qs.filter(Q(registration_fee__gte= min_entry) & Q(registration_fee__lte= max_entry) & Q(tournament_level__in= type_of_team) & Q(tournament_format__in= type_of_tournament))
#			tournament_qs= tournament_qs.gis_manager.nearby(latitude, longitude, max_distance, min_distance)
			print("tournament_qs-->",tournament_qs)
			p = Paginator(tournament_qs, 6)
			total_pages = p.num_pages
			try:
				tour_queryset = p.page(page)
			except PageNotAnInteger:
				tour_queryset = p.page(1)
			except EmptyPage:
				tour_queryset = p.page(1)                            
			serializer= TournamentListSerializer(tour_queryset, many= True)
			return Response({'data':serializer.data, 'total_pages':total_pages, 'success':True})
		except :
			traceback.print_exc()
			return Response({'success':False, 'error':'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class ProfileViewSet(viewsets.ViewSet):

	def get_venue(self,fixture_obj):
		venue= None
		if fixture_obj.tournament_venue:
			venue_obj= fixture_obj.tournament_venue
			if venue_obj.is_dummy:
				venue= {'id': venue_obj.id, 'name':venue_obj.court_name}
			else:
				venue= {'id': venue_obj.id, 'name':venue_obj.court.venue.name + venue_obj.court.name}

		return venue

	def get_fixture_detail(self, fixture_obj):
		team1_logo= None
		team1_name = fixture_obj.team1.name
		if fixture_obj.team1.team_pic:
			team1_logo= fixture_obj.team1.team_pic.url
		team2_logo= None
		team2_name = fixture_obj.team2.name
		if fixture_obj.team2.team_pic:
			team2_logo= fixture_obj.team2.team_pic.url
		date_time = fixture_obj.date_time.strftime("%d %b, %I:%M %p")
		match_id= fixture_obj.id
		match_stat_team1_id= Match_Stat_Team.objects.get(match_stat= fixture_obj, team= fixture_obj.team1).id
		match_stat_team2_id= Match_Stat_Team.objects.get(match_stat= fixture_obj, team= fixture_obj.team2).id
		a= {'id':fixture_obj.id,'team1_logo':team1_logo, 'team1':team1_name, 'team2_logo':team2_logo, 'team2':team2_name, 'date_time':date_time, 'venue':self.get_venue(fixture_obj), 'won_by':fixture_obj.won_by, 'match_stat_team1_id':match_stat_team1_id, 'match_stat_team2_id':match_stat_team2_id, 'is_live':fixture_obj.is_live}
		return a

	def match_serializer(self, fixture_queryset):
		fixture_data = []
		for fixture_obj in fixture_queryset:
			fixture_data.append(self.get_fixture_detail(fixture_obj))#{'id':fixture_obj.id, 'team1':team1_name, 'team2':team2_name, 'date_time':date_time})
		return fixture_data

	def list(self, request):
		try:
			request_user= request.user
			data= request.GET
			form_type= data.get('form_type')
			tournament_pk= data['tournament_id']
			tour_obj= Tournament_Detail.objects.get(Q(is_active= True) & Q(pk= tournament_pk))
			profile_pic= None
			cover_pic= None
			city= None
			if tour_obj.city:
				city= tour_obj.city.name
			if tour_obj.logo:
				profile_pic= tour_obj.logo.url
			if tour_obj.cover_pic:
				cover_pic= tour_obj.cover_pic.url

			if form_type == "basic":

#				profile_pic= None
#				cover_pic= None
#				city= None

#				if tour_obj.logo:
#					profile_pic= tour_obj.logo.url
#				if tour_obj.cover_pic:
#					cover_pic= tour_obj.cover_pic.url
#				if tour_obj.city:
#					city= tour_obj.city.name

				#team_registred_count= Team_Registration.objects.filter(Q(tournament= tour_obj) & Q(status= 'approve')).count()

				sponsor_qs= Tournament_Sponsor.objects.filter(Q(tournament= tour_obj) & Q(is_active= True))
				sponsor_serializer= []
				for obj in sponsor_qs:
					img= None
					if obj.image:
						img= obj.image.url
#					print ('spo_img---',img, obj.name)
					sponsor_serializer.append({'name':obj.name, 'image':img})

				team_list=[]
				team_qs= Team_Registration.objects.filter(Q(tournament=tour_obj) & Q(status='approve')).order_by('-registration_date')
				team_registred_count= team_qs.count()
				team_qs= team_qs[:3]
				for obj1 in team_qs:
					team_name = obj1.team.name
					if obj1.team.team_pic:
						team_logo = obj1.team.team_pic.url
					else:
						team_logo = None
					team_list.append({'name':team_name,'image':team_logo})
	#			print('111111111111111111111111')
				venue_list= []
				qs= Tournament_Venue_Map.objects.filter(Q(is_active= True) & Q(tournament= tour_obj))
				for obj in qs:
					img= None
					venue_id= None
					city1= None
					if obj.is_dummy:
						name= obj.court_name
						city1= obj.city
						venue_id= obj.id
					else:
						img_qs= VenueImg.objects.filter(venue= obj.court.venue)
						if img_qs.exists():
							img= img_qs.first().url.url
	#					print('22222222222',img)
						name= obj.court.venue.name + obj.court.name
						city1= obj.court.venue.address.name
						venue_id= obj.court.venue.id

					venue_list.append({'name':name, 'image':img, 'city':city1, 'id':venue_id, 'is_dummy':obj.is_dummy})

				serializer_data= {
								'id':tour_obj.id,
								'name':tour_obj.name,
								'city':tour_obj.city.name,
								'prize':tour_obj.prize,
								'registration_fee':tour_obj.registration_fee,
								'team_registered':team_registred_count,
								'max_no_of_teams':tour_obj.max_no_of_teams,
								'description':tour_obj.description,
								'sport_type': tour_obj.sport_type.name,
								'tournament_type':tour_obj.tournament_format,
								'tournament_level':tour_obj.tournament_level,
								'registration_start_date':tour_obj.registration_start_date.strftime("%Y-%m-%d"),
								'registration_end_date':tour_obj.registration_end_date.strftime("%Y-%m-%d"),
								'start_date':tour_obj.start_date.strftime("%Y-%m-%d"),
								'end_date':tour_obj.end_date.strftime("%Y-%m-%d"),
								'sponsor':sponsor_serializer,
								'registered_teams':team_list,
								'venue':venue_list,
								'success':True,
								'logo':profile_pic,
								'cover_pic':cover_pic,
								'city':city								
					}

				return Response(serializer_data)

			elif form_type == "teams":
				team_list=[]
				team_qs= Team_Registration.objects.filter(Q(tournament=tour_obj) & Q(status='approve')).order_by('-registration_date')
				for obj1 in team_qs:
					team_name = obj1.team.name
					if obj1.team.team_pic:
						team_logo = obj1.team.team_pic.url
					else:
						team_logo = None
					team_list.append({'id':obj1.team.id, 'name':team_name,'image':team_logo})				

				return Response({'data':team_list, 'success':True})

			elif form_type == "fixture":
				
				round_qs= Tournament_Round.objects.filter(Q(tournament= tour_obj) & Q(is_active= True) & Q(is_fixture_created= True)).order_by('round_position')
				final_json= []
				for round_obj in round_qs:

					if round_obj.round_type== "group":
						tournament_group_qs = Tournament_Group.objects.filter(Q(tournament_round= round_obj)).order_by('id')
						fixture_group_serializer= []
						for group_obj in tournament_group_qs:
							#print("group1",group_obj)
							fixture_queryset = Match_Stat.objects.filter(Q(tournament_round= round_obj) & Q(tournament_group= group_obj) & Q(date_time__isnull= False)).order_by('-date_time')[:2]
							print("fixture_qs-->", fixture_queryset)
							fixture_data = []
							#fixture_group_serializer= []
#							for fixture_obj in fixture_queryset:
#								team1_name = fixture_obj.team1.name
#								team2_name = fixture_obj.team2.name
#								date_time = fixture_obj.date_time.strftime("%d %b, %I:%M %p")
##								team1_logo, team1_name, team2_logo, team2_logo, date_time= self.get_fixture_detail(fixture_obj)
#								a=self.get_fixture_detail(fixture_obj)
#								print("a--->",a)
#								fixture_data.append(a)#{'id':fixture_obj.id, 'team1':team1_name, 'team2':team2_name, 'date_time':date_time})
#							print("fixture--->", fixture_data)
							fixture_data= self.match_serializer(fixture_queryset)
							fixture_group_serializer.append({'group_name':group_obj.name, 'match':fixture_data, "group_id":group_obj.id})
							
						#group_data= Tournament_Group_Serializer(Tournament_Group.objects.filter(tournament_round= round_obj).order_by('id'), many= True)
						final_json.append({'round_name':round_obj.name, 'data': fixture_group_serializer, 'group_name': group_obj.name, 'round_type':round_obj.round_type, 'round_id':round_obj.id})
						# return Response(response_data)

					else:
					
						fixture_queryset = Match_Stat.objects.filter(Q(tournament_round= round_obj) & ~Q(date_time__isnull= True)).order_by('-date_time')[:2]
						# fixture_data = []
						print("inside fixture ", fixture_queryset)
						if fixture_queryset.exists():
							# if tournament_round_obj.round_type == "DE":
								# no_of_de_match = 0
								# res = []
							fixture_data = []
#							for fixture_obj in fixture_queryset:
#								team1 = fixture_obj.team1
#								team2 = fixture_obj.team2
#								date_time= fixture_obj.date_time.strftime("%d %b, %I:%M %p")#datetime.datetime.strftime(fixture_obj.date_time, "%d %b, %I:%M %p")
#
#								venue= self.get_venue(fixture_obj)
#
#								fixture_data.append({'id':fixture_obj.id, 'team1':team1.name, 'team2':team2.name, 'date_time':date_time,'venue':venue })
							fixture_data= self.match_serializer(fixture_queryset)
							final_json.append({'data': fixture_data, 'round_name':round_obj.name, 'round_type': round_obj.round_type, 'group_name':"", 'round_id':round_obj.id})
				return Response({'data':final_json, 'success':True, 'logo':profile_pic,	'cover_pic':cover_pic, 'name':tour_obj.name, 'city':city})

			elif form_type == "seeAll":
				round_id= data.get('round_id')	
				page= data.get('page')

				round_obj= Tournament_Round.objects.get(Q(tournament= tour_obj) & Q(is_active= True) & Q(is_fixture_created= True) & Q(pk= round_id))
				final_json= {}
				# for round_obj in round_qs:

				if round_obj.round_type== "group":
					group_id= data.get('group_id')
					fixture_group_serializer= []
					group_obj = Tournament_Group.objects.get(Q(tournament_round= round_obj) & Q(pk= group_id))
					# for group_obj in tournament_group_qs:
					fixture_queryset = Match_Stat.objects.filter(Q(tournament_round= round_obj) & Q(tournament_group= group_obj) & Q(date_time__isnull= False)).order_by('date_time')

					p = Paginator(fixture_queryset, 6)
					total_pages = p.num_pages
					try:
						fixture_queryset = p.page(page)
					except PageNotAnInteger:
						fixture_queryset = p.page(1)
					except EmptyPage:
						fixture_queryset = p.page(1)                            
					fixture_data = []

					p = Paginator(fixture_queryset, 6)
					total_pages = p.num_pages
					try:
						fixture_queryset = p.page(page)
					except PageNotAnInteger:
						fixture_queryset = p.page(1)
					except EmptyPage:
						fixture_queryset = p.page(1)                            

					fixture_data= self.match_serializer(fixture_queryset)
					fixture_group_serializer.append({'group_name':group_obj.name, 'match':fixture_data})
						
					group_data= Tournament_Group_Serializer(Tournament_Group.objects.filter(tournament_round= round_obj).order_by('id'), many= True)
					final_json.update({'round_name':round_obj.name, 'data': fixture_group_serializer, 'group_name': group_obj.name, 'group_data':group_data.data, 'round_type':round_obj.round_type, 'success':True, 'total_pages':total_pages})

				else:
				
					fixture_queryset = Match_Stat.objects.filter(Q(tournament_round= round_obj) & ~Q(date_time__isnull= True)).order_by('id')[:2]
					if fixture_queryset.exists():
						fixture_data = []
#						for fixture_obj in fixture_queryset:
#							team1 = fixture_obj.team1.name
#							team2 = fixture_obj.team2.name
#							date_time = fixture_obj.strftime("%d %b, %I:%M %p")
#
#							venue= self.get_venue(fixture_obj)

						p = Paginator(fixture_queryset, 6)
						total_pages = p.num_pages
						try:
							fixture_queryset = p.page(page)
						except PageNotAnInteger:
							fixture_queryset = p.page(1)
						except EmptyPage:
							fixture_queryset = p.page(1)                            
						fixture_data= self.match_serializer(fixture_queryset)#.append({'id':fixture_obj.id, 'team1':team1, 'team2':team2, 'date_time':date_time,'venue':venue })

						final_json.update({'data': fixture_data, 'round_name':round_obj.name, 'group_data':None, 'round_type': round_obj.round_type, 'group_name':"", 'success':True, 'total_pages':total_pages})
				final_json.update({'logo':profile_pic, 'cover_pic':cover_pic,'name':tour_obj.name})
				return Response(final_json)

			elif form_type == "statics":
				print("inside stats")
				round_serializer= []
				batsman_serializer= []
				bowler_serializer= []
				team_serializer= []
				if tour_obj.tournament_format == 'league':
					try:
						tournament_round_obj = Tournament_Round.objects.get(Q(tournament= tour_obj) & Q(is_result_declared= True))
						round_team_map_qs = Tournament_Round_Team_Map.objects.filter(Q(tournament_round= tournament_round_obj))
						match_stat_qs= Match_Stat.objects.filter(Q(tournament_round= tournament_round_obj) & Q(is_result_declared= True) & Q(date_time__isnull= False))
						for team in round_team_map_qs:
							run_rate = 0
							""" calculate number of win, tie, total_run, total_overs, against)run, against_overs if team1 == team """
							first_ining_match_stat_qs= match_stat_qs.filter(team1= team.team)
							total_matches= first_ining_match_stat_qs.count()
							win= first_ining_match_stat_qs.filter(winner= team.team.id).count()
							tie= first_ining_match_stat_qs.filter(is_draw= True).count()
							run_overs= Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & Q(team=team.team)).values('total_run', 'overs', 'run_against', 'wickets')
							if run_rate.exists():
								d = list(map(lambda x:  x['total_run']/ x['overs'] if not x['run_against'] else x['total_run']/ (x['overs']+ (x['run_against']/6)), run_overs))
								run_rate = sum(d)/ len(d)
								print("run_rate 1-->", run_rate)
							against_run_overs= Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & ~Q(team= team.team)).values('total_run', 'overs', 'run_against')
							if against_run_overs.exists():
								d = list(map(lambda x:  x['total_run']/ x['overs'] if not x['run_against'] else x['total_run']/ (x['overs']+ (x['run_against']/6)), against_run_overs))								
								against_run_rate = sum(d)/ len(d)
							print("against_run_rate 1 ---->",against_run_rate)
							""" calculate number of win, tie, total_run, total_overs, against)run, against_overs if team2 == team """
							if not total_matches:
								total_matches= 0
							if not win:
								win= 0
							if not tie:
								tie= 0
							if not run_rate:
								run_rate= 0
							if not against_run_rate:
								against_run_rate= 0
							first_ining_match_stat_qs= match_stat_qs.filter(team2= team.team)
							total_matches += first_ining_match_stat_qs.count()
							win += first_ining_match_stat_qs.filter(winner= team.team.id).count()
							tie += first_ining_match_stat_qs.filter(is_draw= True).count()
							run_overs = Match_Stat_Team.objects.filter(Q(match_stat__in= fiwrst_ining_match_stat_qs) & Q(team=team.team)).values('total_run', 'overs', 'run_against')
							if run_overs.exists():
								d = list(map(lambda x:  x['total_run']/ x['overs'] if not x['run_against'] else x['total_run']/ (x['overs']+ (x['run_against']/6)), run_overs))
								run_rate += (sum(d) / len(d))
							print("run_rate 2-->", run_rate)
							against_run_overs= Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & ~Q(team= team.team)).values('total_run', 'overs', 'run_against')
							if against_run_overs.exists():
								d1 = list(map(lambda x:  x['total_run']/ x['overs'] if not x['run_against'] else x['total_run']/ (x['overs']+ (x['run_against']/6)), against_run_overs))
								against_run_rate += (sum(d1)/ len(d1))
							print("against_run_rate 2-->", against_run_rate)
							if not total_matches:
								total_matches= 0
							if not win:
								win= 0
							if not tie:
								tie= 0

							loss= total_matches - (win + tie)
							points= (win * tour_obj.win_point) + tie * tour_obj.tie_point + loss * tour_obj.loss_point
							#print(total_run, total_overs, against_run, against_overs)
							run_rate= round((run_rate - against_run_rate), 2)

							team_serializer.append({'id':team.team.id, 'name':team.team.name, 'win':win, 'loss':loss, 'tie':tie, 'run_rate':run_rate, 'points':points})
	#									print("team_serializer-->", team_serializer)
						team_serializer.sort(key=lambda x: (x['points'], x['run_rate'], x['win'], x['loss']))
						team_serializer = list(reversed(team_serializer))
						print("team---------------->", team_serializer)
						round_serializer.append({'round_name':tournament_round_obj.name, 'data': team_serializer, 'round_type':tournament_round_obj.round_type})
						
					except:
						traceback.print_exc()
						round_serializer.append({'round_name':"", 'data': [], 'round_type':""})

				elif tour_obj.tournament_format == "knock-out":
					print("inside knock-out----->")
					tournament_round_qs = Tournament_Round.objects.filter(Q(tournament= tour_obj) & Q(is_result_declared= True))
					
					for round_obj in tournament_round_qs:
						serializer = []

						fixture_queryset = Match_Stat.objects.filter(Q(tournament_round= round_obj) & Q(date_time__isnull= False)).order_by('date_time')[:2]
						if fixture_queryset.exists():
							fixture_data = []
							serializer= self.match_serializer(fixture_queryset)
						round_serializer.append({'round_name':round_obj.name, 'data': serializer, 'round_type':round_obj.round_type, 'round_id':round_obj.id})

				elif tour_obj.tournament_format == 'cup':
					print("inside cup")
					tournament_round_qs = Tournament_Round.objects.filter(Q(tournament= tour_obj) & Q(is_result_declared= True))
					for round_obj in tournament_round_qs:
						serializer = []
						if round_obj.round_type == "group":
							group_qs = Tournament_Group.objects.filter(tournament_round= round_obj).order_by('id')
#							print("1----------",group_qs)
							for group_obj in group_qs:
								team_serializer= []
								group_team_map_qs = Tournament_Group_Team_Map.objects.filter(tournament_group= group_obj)
#								print("2",  group_team_map_qs)
								for team in group_team_map_qs:
									
									match_stat_qs= Match_Stat.objects.filter(Q(tournament_group= group_obj) & Q(is_result_declared= True))
									print("team-->",team.team)

									""" calculate number of win, tie, total_run, total_overs, against)run, against_overs if team1 == team """
									first_ining_match_stat_qs= match_stat_qs.filter(team1= team.team)
									total_matches= first_ining_match_stat_qs.count()
									win= first_ining_match_stat_qs.filter(winner= team.team.id).count()
									tie= first_ining_match_stat_qs.filter(is_draw= True).count()
									run_overs= Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & Q(team=team.team)).values('total_run', 'overs', 'run_against', 'wickets', 'match_stat_id')
									total_run = 0
									total_overs = 0
									if run_overs.exists():
										""" changes made by nikhil """
										total_run = sum(list(map(lambda x: x["total_run"], run_overs)))
										total_overs = sum(list(map(lambda x : int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) if x["wickets"]==10 else int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) + round(x["run_against"]/6, 1),run_overs)))
										
										"""code bby sumit sir commented by nikhil """
										d = list(map(lambda x:  x['total_run']/int(Match_Stat.objects.get(pk= x['match_stat_id']).no_of_overs) if x['wickets'] == 10 else (x['total_run']/x['overs'] if not x['run_against'] else x['total_run']/ (x['overs']+ (x['run_against']/6)) ), run_overs))
										run_rate = sum(d)/len(d)
										# print("1-->", run_rate)

									against_run_overs= Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & ~Q(team= team.team)).values('total_run', 'overs', 'run_against', 'wickets', 'match_stat_id')
									print(against_run_overs)
									against_total_run = 0
									against_total_over = 0
									if against_run_overs.exists():
										""" new code by nikhil """
										against_total_run = sum(list(map(lambda x: x["total_run"], against_run_overs)))
										against_total_over = sum(list(map(lambda x : int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) if x["wickets"]==10 else int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) + round(x["run_against"]/6, 1),against_run_overs)))
										
										""" code by sumit sir commented by nikhil """
										d = list(map(lambda x:  x['total_run']/int(Match_Stat.objects.get(pk= x['match_stat_id']).no_of_overs) if x['wickets'] == 10 else (x['total_run']/x['overs']) if not x['run_against'] else  x['total_run']/ (x['overs']+ (x['run_against']/6)), against_run_overs))
										against_run_rate = sum(d)/len(d)
										# print("2-->",against_run_rate)
									""" calculate number of win, tie, total_run, total_overs, against)run, against_overs if team2 == team """
									
									if not total_matches:
										total_matches= 0
									if not win:
										win= 0
									if not tie:
										tie= 0

									if not run_rate:
										run_rate= 0
									if not against_run_rate:
										against_run_rate= 0
									first_ining_match_stat_qs= match_stat_qs.filter(team2= team.team)
									total_matches += first_ining_match_stat_qs.count()
									win += first_ining_match_stat_qs.filter(winner= team.team.id).count()
									tie += first_ining_match_stat_qs.filter(is_draw= True).count()
									run_overs = Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & Q(team=team.team)).values('total_run', 'overs', 'run_against', 'wickets', 'match_stat_id')
	#								print(run_overs)
									team2_total_run = 0
									team2_total_overs = 0
									if run_overs.exists():
										"""code by nikhil """
										team2_total_run = sum(list(map(lambda x: x["total_run"], run_overs)))
										team2_total_overs = sum(list(map(lambda x : int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) if x["wickets"]==10 else int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) + round(x["run_against"]/6, 1),run_overs)))
										""" commented by nikhil"""
										d = list(map(lambda x:  x['total_run']/int(Match_Stat.objects.get(pk= x['match_stat_id']).no_of_overs) if x['wickets'] == 10 else (x['total_run']/x['overs']) if not x['run_against'] else  x['total_run']/ (x['overs']+ (x['run_against']/6)), run_overs))
										run_rate += (sum(d) / len(d))
										# print("run_rate 3-->", run_rate)
									against_run_overs= Match_Stat_Team.objects.filter(Q(match_stat__in= first_ining_match_stat_qs) & ~Q(team= team.team)).values('total_run', 'overs', 'run_against', 'wickets', 'match_stat_id')
									
									against2_total_run = 0
									against2_total_over = 0
									if against_run_overs.exists():
										""" code by nikhil """

										against2_total_run = sum(list(map(lambda x: x["total_run"], run_overs)))
										against2_total_over = sum(list(map(lambda x : int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) if x["wickets"]==10 else int(Match_Stat.objects.get(id=x["match_stat_id"]).no_of_overs) + round(x["run_against"]/6, 1),against_run_overs)))

										d = list(map(lambda x:  x['total_run']/int(Match_Stat.objects.get(pk= x['match_stat_id']).no_of_overs) if x['wickets'] == 10 else (x['total_run']/x['overs']) if not x['run_against'] else x['total_run']/ (x['overs']+ (x['run_against']/6)), against_run_overs))
										against_run_rate += (sum(d) / len(d))
										# print("against_run_rate 4-->",against_run_rate)
									if not total_matches:
										total_matches= 0
									if not win:
										win= 0
									if not tie:
										tie= 0
									loss= total_matches - (win + tie)
									make_run_rate = (total_run + team2_total_run) / (total_overs + team2_total_overs)
									#make_run_rate = (total_run / total_overs) + (team2_total_run / team2_total_overs)
								
									conceded_run_rate = (against_total_run+against2_total_run) / (against_total_over+against2_total_over)
									#conceded_run_rate = (against_total_run / against_total_over) + (against2_total_run / against2_total_over)
									print(make_run_rate, ">>>>>>>>>>>>>>>>>", conceded_run_rate)
									points= (win * tour_obj.win_point) + tie * tour_obj.tie_point + loss * tour_obj.loss_point
									# run_rate= round((run_rate - against_run_rate), 2)
									run_rate = round((make_run_rate - conceded_run_rate), 2)
									print(run_rate, "hjhjhjhjhjjhjhjhj")

									team_serializer.append({'id':team.team.id, 'name':team.team.name, 'win':win, 'loss':loss, 'tie':tie, 'run_rate':run_rate, 'points':points})
								team_serializer.sort(key=lambda x: (x['points'], x['run_rate'], x['win'], x['loss']))
								serializer.append({'group_id':group_obj.id, 'group_name': group_obj.name, 'team': team_serializer[::-1]})
						else:
							fixture_queryset = Match_Stat.objects.filter(tournament_round= round_obj).order_by('id')[:2]
		#					print("inside cup with round type other than Group")
							if fixture_queryset.exists():
								fixture_data = []
								serializer= self.match_serializer(fixture_queryset)


						round_serializer.append({'round_name':round_obj.name, 'data': serializer, 'round_type':round_obj.round_type, 'round_id':round_obj.id})
				""" Best batsman queryset """
				best_batsman_qs= Match_Stat_Batting.objects.filter(Q(Q(match_stat_team__match_stat__tournament_round__isnull= False) & Q(match_stat_team__match_stat__tournament_round__tournament= tour_obj)) | Q(Q(match_stat_team__match_stat__tournament_group__isnull= False) & Q(match_stat_team__match_stat__tournament_group__tournament_round__tournament= tour_obj))).order_by('-run')
				prev= None
				player_list= []
				n= 0
				batsman_serializer= []
				for obj in best_batsman_qs:
					if not prev == obj.player and not obj.player in player_list and n < 15:
						player_list.append(obj.player)
						prev= obj.player
						n += 1
						img= None
						if obj.player.user.profile_pic:
							img= obj.player.user.profile_pic.url
						run= best_batsman_qs.filter(player= obj.player).aggregate(sum= Sum('run'))['sum']
						batsman_serializer.append({'id':obj.player.id, 'run':run, 'name':obj.player.user.name, 'profile_pic':img, 'team_name':obj.match_stat_team.team.name})
				batsman_serializer.sort(key=lambda x: (x['run']))
				""" Best Bowler queryset """
				best_bowler_qs= Match_Stat_Bowling.objects.filter(Q(Q(match_stat_team__match_stat__tournament_round__isnull= False) & Q(match_stat_team__match_stat__tournament_round__tournament= tour_obj)) | Q(Q(match_stat_team__match_stat__tournament_group__isnull= False) & Q(match_stat_team__match_stat__tournament_group__tournament_round__tournament= tour_obj))).order_by('-wickets')	
#				print("bowler-->",best_bowler_qs)
				prev= None
				player_list= []
				n= 0
				bowler_serializer= []
				for obj in best_bowler_qs:
					if not prev == obj.player and not obj.player in player_list and n < 15:
						player_list.append(obj.player)
						prev= obj.player
						n += 1
						img= None
						if obj.player.user.profile_pic:
							img= obj.player.user.profile_pic.url
						run= best_bowler_qs.filter(player= obj.player).aggregate(sum= Sum('wickets'))['sum']
						bowler_serializer.append({'id':obj.player.id, 'run':run, 'name':obj.player.user.name, 'profile_pic':img, 'team_name':obj.match_stat_team.team.name})
				bowler_serializer.sort(key=lambda x: (x['run']))
#					print("best+_batsman---->",best_batsman_qs)

				man_of_the_series= tour_obj.man_of_the_series
				if man_of_the_series:
					print("man_of_the_series-->",man_of_the_series)
					img=None
					if man_of_the_series.user.profile_pic:
						img= man_of_the_series.user.profile_pic.url
					batting= {}
					bowling= {}
					fielding= {}
					total= Match_Stat_Batting.objects.filter(Q(Q(match_stat_team__match_stat__tournament_round__isnull= False) & Q(match_stat_team__match_stat__tournament_round__tournament= tour_obj)) | Q(Q(match_stat_team__match_stat__tournament_group__isnull= False) & Q(match_stat_team__match_stat__tournament_group__tournament_round__tournament= tour_obj)))
					total= total.filter(player= man_of_the_series)
					total= total.aggregate(total_run= Sum('run'), total_bowl= Sum('bowl'), total_fours= Sum('fours'), total_six= Sum('six'))
					print("run---->",total['total_run'])
					batting.update({'run':total['total_run'], 'bowl':total['total_bowl'], 'fours':total['total_fours'], 'six':total['total_six'], 'run_rate':4})
					
					total_bowl= Match_Stat_Bowling.objects.filter(Q(Q(match_stat_team__match_stat__tournament_round__isnull= False) & Q(match_stat_team__match_stat__tournament_round__tournament= tour_obj)) | Q(Q(match_stat_team__match_stat__tournament_group__isnull= False) & Q(match_stat_team__match_stat__tournament_group__tournament_round__tournament= tour_obj)))
					total_bowl= total_bowl.filter(player= man_of_the_series)
					total_bowl= total_bowl.aggregate(total_run= Sum('run'), total_overs= Sum('overs'), total_maiden= Sum('maiden'), total_wickets= Sum('wickets'))
					bowling.update({'run':total_bowl['total_run'], 'overs':total_bowl['total_overs'], 'maiden':total_bowl['total_maiden'], 'wickets':total_bowl['total_wickets'], 'economy':3.2})

					all_fixture_id= [obj.id for obj in Match_Stat.objects.filter(Q(Q(tournament_round__isnull= False) & Q(tournament_round__tournament= tour_obj)) | Q(Q(tournament_group__isnull= False) & Q(tournament_group__tournament_round__tournament= tour_obj)))]
					catch_count= Match_Stat_Out.objects.filter(Q(pk__in= all_fixture_id) & Q(whom= man_of_the_series) & Q(out_type__in= ['caught', 'caught&bowled', 'caughtbehind'])).count()
					run_out= Match_Stat_Out.objects.filter(Q(pk__in= all_fixture_id) & Q(whom= man_of_the_series) & Q(out_type= 'runout')).count()
					stump= Match_Stat_Out.objects.filter(Q(pk__in= all_fixture_id) & Q(whom= man_of_the_series) & Q(out_type= 'stumped')).count()
					fielding.update({'catch':catch_count, 'run_out':run_out, 'stump':stump})
					man_of_the_series= {'batting':batting, 'bowling':bowling, 'fielding':fielding, 'profile_pic':img, 'name':man_of_the_series.user.name}
				return Response({'data':round_serializer, 'success':True, 'logo':profile_pic, 'cover_pic':cover_pic,'name':tour_obj.name, 'batsman':batsman_serializer[::-1][:10], 'bowler':bowler_serializer[::-1][:10], 'man_of_the_series':man_of_the_series, 'city':city})

			elif form_type == "page2":

				serializer= Tournament_page2_Serializer(obj)
				return Response(serializer.data)

			elif form_type == "page3":

				serializer= Tournament_page3_Serializer(obj)
				return Response(serializer.data)

			else:
				return Response({'error':'wrong option', 'success':False})
			
		except Exception:
			traceback.print_exc()
			return Response({'success':False, 'error': 'Exception'}, status= status.HTTP_400_BAD_REQUEST)

class RulesFaqViewSet(viewsets.ViewSet):
	permission_classes= [customPermission,]
	def create(self, request):
		try:
			request_user = request.buser
			data = request.data
			tournament_id = data.get('tournament_id')
			form_type = data.get('form_type')
			url = None
			file = data.get('file')
			if len(file) > MAX_SIZE_TOURNAMENT_LOGO:
					return Response({'success':False, 'file_size':False}, status= status.HTTP_400_BAD_REQUEST)	
			tour_obj= None
			try:
				tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(manager= request_user) & Q(is_active= True))
		
			if form_type == "rule":
	#			tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(Q(admin= request_user) | Q(manager= request_user)) & Q(is_active= True))
				if tour_obj.rule:
					os.remove(settings.BASE_DIR+ str(tour_obj.rule_file.url))
				tour_obj.rule_file = file
				tour_obj.save()
				url = tour_obj.rule_file.url
			elif form_type == "faq":
				file = data.get('file')
	#			tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(Q(admin= request_user) | Q(manager= request_user)) & Q(is_active= True))
				if tour_obj.faq:
					os.remove(settings.BASE_DIR+ str(tour_obj.faq.url))
				tour_obj.faq = file
				tour_obj.save()
				url= tour_obj.faq.url
			else:
				return Response({"status":False,"message":"Invalid Option"}, status= status.HTTP_400_BAD_REQUEST)				
			return Response({"status":True, 'url':url},status= status.HTTP_200_OK)
		except Exception as e:
			traceback.print_exc()
			return Response({"status":False,"message":str(e)},status=400)		

	def retrieve(self, request, pk):
		try:
			request_user = request.buser
			tour_obj= None
			try:
				tour_obj= Tournament_Detail.objects.get(Q(pk= pk) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj= Tournament_Detail.objects.get(Q(pk= pk) & Q(manager= request_user) & Q(is_active= True))

#			tour_obj = Tournament_Detail.objects.get(Q(pk= pk) & Q(Q(admin= request_user) | Q(manager= request_user)) & Q(is_active= True))
			rule_file= None
			faq_file= None
			if tour_obj.rule_file:
				rule_file= tour_obj.rule_file.url
			if tour_obj.faq:
				faq_file= tour_obj.faq.url

			serializer= {'rule_file':rule_file, 'faq':faq_file, 'success': True}
			return Response(serializer,status= status.HTTP_200_OK)
		except Exception as e:
			traceback.print_exc()
			return Response({"status":False,"message":str(e)},status=400)					
		
class PointViewSet(viewsets.ViewSet):
	permission_classes= [customPermission,]

	def create(self, request):
		try:
			request_user = request.buser
			data = request.data
			tournament_id = data.get('tournament_id')
			win_point = data.get('win_point')
			loss_point = data.get('loss_point')
			tie_point = data.get('tie_point')
			abandoned_point = data.get('abandoned_point')
			tour_obj= None
			try:
				tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(manager= request_user) & Q(is_active= True))

			if not tour_obj.is_round_created:
				
				tour_obj.win_point= win_point
				tour_obj.loss_point= loss_point
				tour_obj.tie_point= tie_point
				tour_obj.abandoned_point= abandoned_point
				tour_obj.save()
				return Response({"status":True,"message":"Points updated successfully"}, status= status.HTTP_200_OK)
			else:
				return Response({"status":False,"error":"You can\'t update points once you created rounds"}, status= status.HTTP_400_BAD_REQUEST)			
		except Exception:
			traceback.print_exc()
			return Response({"status":False,"error":"Exception"},status= status.HTTP_400_BAD_REQUEST)		

	def retrieve(self, request, pk):
		try:
			request_user = request.buser
			tour_obj= None
			try:
				tour_obj = Tournament_Detail.objects.get(Q(pk= pk) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj = Tournament_Detail.objects.get(Q(pk= pk) & Q(manager= request_user) & Q(is_active= True))
			
			serializer= {'win_point':tour_obj.win_point, 'loss_point':tour_obj.loss_point, 'tie_point':tour_obj.tie_point, 'abandoned_point':tour_obj.abandoned_point, 'success': True}
			return Response(serializer,status= status.HTTP_200_OK)
		except Exception :
			traceback.print_exc()
			return Response({"status":False,"error":"Exception"},status= status.HTTP_400_BAD_REQUEST)

	def update(self, request, pk):
		try:
			request_user = request.buser
			tour_obj= None
			try:
				tour_obj = Tournament_Detail.objects.get(Q(pk= pk) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj = Tournament_Detail.objects.get(Q(pk= pk) & Q(manager= request_user) & Q(is_active= True))

			tour_obj.win_point= 3
			tour_obj.loss_point= 0
			tour_obj.tie_point= 1
			tour_obj.abandoned_point= 0
			tour_obj.save()
			return Response({"status":True,"message":"Points updated successfully"}, status= status.HTTP_200_OK)
		except Exception :
			traceback.print_exc()
			return Response({"status":False,"error":"Exception"},status= status.HTTP_400_BAD_REQUEST)

class playerTeamViewSet(viewsets.ViewSet):
	permission_classes= [customPermission,]

	def retrieve(self, request, pk):
		try:
			request_user = request.buser
			tournament_id= request.GET.get('tournament_id')
			print("team--->",pk)
#			team_player_map = Team_Match_Player.objects.filter(Q(team__id= pk) & Q(Q(Q(match_stat__tournament_round__isnull= False) & Q(Q(match_stat__tournament_group__tournament_round__tournament__admin= request_user) | Q(match_stat__tournament_group__tournament_round__tournament__manager= request_user)) & Q(match_stat__tournament_round__tournament__id= tournament_id)) | Q(Q(match_stat__tournament_group__isnull= False)& Q(Q(match_stat__tournament_group__tournament_round__tournament__admin= request_user) | Q(match_stat__tournament_group__tournament_round__tournament__manager= request_user))  & Q(match_stat__tournament_group__tournament_round__tournament__id= tournament_id)))).distinct('player')
			team_player_map = Team_Match_Player.objects.filter(Q(team__id= pk) & Q(Q(Q(match_stat__tournament_round__isnull= False) & Q(Q(match_stat__tournament_round__tournament__admin= request_user) | Q(match_stat__tournament_group__tournament_round__tournament__manager= request_user)) & Q(match_stat__tournament_round__tournament__id= tournament_id)) | Q(Q(match_stat__tournament_group__isnull= False)& Q(Q(match_stat__tournament_group__tournament_round__tournament__admin= request_user) | Q(match_stat__tournament_group__tournament_round__tournament__manager= request_user))  & Q(match_stat__tournament_group__tournament_round__tournament__id= tournament_id)))).distinct('player')
			print("team+_players-->",team_player_map)
			serializer= []			
			for obj in team_player_map:
				img= None
				# if obj.player.user.
				serializer.append({'team_id':obj.team.id, 'player_id':obj.player.id, 'name':obj.player.user.name})
#			print("serializer-->",serializer)
			return Response(serializer, status= status.HTTP_200_OK)
	#		return Response(team_player_map, status= status.HTTP_200_OK)
		except Exception :
			traceback.print_exc()
			return Response({"status":False,"message":"Exception"},status= status.HTTP_400_BAD_REQUEST)

	def create(self, request):
		try:
			request_user = request.buser
			data = request.data
			tournament_id= data.get('tournament')
			team_id = data.get('team')
			player_id = data.get('player')
			
			tour_obj= None
			try:
				tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(admin= request_user) & Q(is_active= True))
			except:
				tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & Q(manager= request_user) & Q(is_active= True))
	
			player_obj= Team_Match_Player.objects.filter(Q(team__id= team_id) & Q(Q(Q(match_stat__tournament_round__isnull= False) & Q(match_stat__tournament_round__tournament= tour_obj) & Q(match_stat__tournament_round__tournament= tour_obj)) | Q(Q(match_stat__tournament_group__isnull= False) & Q(match_stat__tournament_group__tournament_round__tournament= tour_obj) & Q(match_stat__tournament_group__tournament_round__tournament= tour_obj))) & Q(player__id= player_id))
			if player_obj.exists():
				tour_obj.man_of_the_series= player_obj.first().player
				tour_obj.save()
			return Response({"status":True,"message":"Done"}, status= status.HTTP_200_OK)			
		except Exception:
			traceback.print_exc()
			return Response({"status":False,"message":"Exception"},status= status.HTTP_400_BAD_REQUEST)		
	def list(self, request):
		try:
			request_user = request.buser
			tournament_id= request.GET.get('tournament_id')
			tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & (Q(admin= request_user) | Q(manager= request_user)) & Q(is_active= True))
			man_of_the_series= tour_obj.man_of_the_series
			serializer= {'declared':False}
			if man_of_the_series:
				match_team_player_obj= Team_Match_Player.objects.filter((Q(Q(match_stat__tournament_round__isnull= False) & Q(match_stat__tournament_round__tournament= tour_obj)) | (Q(match_stat__tournament_group__isnull= False) & Q(match_stat__tournament_group__tournament_round__tournament= tour_obj))) & Q(player= man_of_the_series))
				print(match_team_player_obj.values('team__id', 'team__name'), man_of_the_series.id)
				match_team_player_obj= match_team_player_obj.first()
				team_obj= match_team_player_obj.team
				team_json= {'team_id':team_obj.id, 'team_name':team_obj.name}
				man_json= {'player_id':man_of_the_series.id, 'name':man_of_the_series.user.name}
				serializer.update({'team':team_json, 'player':man_json, 'declared':True})
			return Response(serializer, status= status.HTTP_200_OK)
		except Exception :
			traceback.print_exc()
			return Response({"status":False,"message":"Exception"},status= status.HTTP_400_BAD_REQUEST)

class OnLoadTournament(viewsets.ViewSet):
	permission_classes= [customPermission,]

	def retrieve(self, request, pk):
		try:
			request_user = request.buser
			user_type, tour_obj= checkTournamentPermission(pk, request_user)
			print("user_type-->", user_type, pk, request_user)
			admin= False
			if user_type == "admin":
				admin= True			
			elif user_type == "manager":
				admin= False			
			
			return Response({'is_admin':admin, 'success':True, 'type':tour_obj.tournament_format}, status= status.HTTP_200_OK)
		except Exception :
			traceback.print_exc()
			return Response({"status":False,"error":"Exception"},status= status.HTTP_400_BAD_REQUEST)

class MyTournament(viewsets.ViewSet):
	permission_classes= [TokenHasReadWriteScope,]

	def list(self, request):
		try:
			request_user = request.user
			data= request.GET
			#tournament_pk= pk
			page= data.get('page')
			form_type= data.get('form_type')

			tournament_serializer= []
			tournament_qs= None
			#tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_pk) & Q(is_certified= True) & Q(is_active= True) & Q(is_deactivated= False))
			team_player_qs= TeamPlayers.objects.filter(Q(player__user= request_user) & Q(is_active= True))
			team_admin_qs= TeamAdmin.objects.filter(player__user= request_user, is_active= True)
#			print("team_admin-->", team_admin_qs.values('team__name'))
			team_qs=  [obj.team for obj in team_player_qs] + [obj.team for obj in team_admin_qs]
			if form_type == "upcoming":
#				team_qs= TeamPlayers.objects.filter(Q(player__user= request_user) & Q(is_active= True))
		#		print("team_Qs====>", team_qs.values('team__name'))
				tournament_qs= Team_Registration.objects.filter(Q(team__in= team_qs) & Q(status= 'approve') & Q(tournament__end_date__gte= datetime.datetime.now().date()) & Q(tournament__is_active= True) & Q(tournament__is_deactivated= False)).distinct('tournament__id').order_by('-tournament__id')

			elif form_type == "previous":
#				team_qs= TeamPlayers.objects.filter(Q(player__user= request_user) & Q(is_active= True))
				tournament_qs= Team_Registration.objects.filter(Q(team__in= team_qs) & Q(status= 'approve') & Q(tournament__end_date__lt= datetime.datetime.now().date()) & Q(tournament__is_active= True) & Q(tournament__is_deactivated= False)).distinct('tournament__id').order_by('-tournament__id')

			p = Paginator(tournament_qs, 6)
			total_pages = p.num_pages
			try:
				tournament_qs = p.page(page)
			except PageNotAnInteger:
				tournament_qs = p.page(1)
			except EmptyPage:
				tournament_qs = p.page(1)                            

			for obj in tournament_qs:
				serializer= TournamentListSerializer(obj.tournament)
				tournament_serializer.append(serializer.data)
			
			return Response({'data':tournament_serializer, 'total_pages':total_pages, 'success':True})
		except Exception :
			traceback.print_exc()
			return Response({"status":False,"error":"Exception"},status= status.HTTP_400_BAD_REQUEST)

def reset_match(request):
	try:
		print(request.GET)
		match_id= request.GET.get('match_id')
		match_obj= Match_Stat.objects.get(match_id= match_id)
		player_qs= Team_Match_Player.objects.filter(match_stat= match_obj)
		team_qs= Match_Stat_Team.objects.filter(match_stat= match_obj)
		bat= Match_Stat_Batting.objects.filter(match_stat_team__match_stat= match_obj)
		bowl= Match_Stat_Bowling.objects.filter(match_stat_team__match_stat= match_obj)
		wicket= Match_Stat_Fall_Of_Wickets.objects.filter(match_stat_team__match_stat= match_obj)
		live= Match_Stat_Live.objects.filter(match_stat= match_obj)
		over= Match_Stat_Over.objects.filter(match_stat_team__match_stat= match_obj)
		out= Match_Stat_Out.objects.filter(match_stat= match_obj.id)
		bowling= Match_Stat_Bowl.objects.filter(match_stat_over__in= over)
		interval= Match_Stat_Interval.objects.filter(match_stat= match_obj)
		scorer= Match_Stat_Scorer.objects.filter(match_stat= match_obj)
		penalty= Match_Stat_Penalty.objects.filter(match_stat_team__match_stat= match_obj)
		partnership= Match_Stat_Partnership.objects.filter(match_stat_team__match_stat= match_obj)
		partnership.delete()
		penalty.delete()
		scorer.delete()
		interval.delete()
		bowling.delete()
		out.delete()
		over.delete()
		live.delete()
		wicket.delete()
		bowl.delete()
		bat.delete()
		player_qs.delete()
		team_qs.update(run_against=0, penalty_run=0, wickets=0, overs=0, bowl=0, byes=0, leg_byes=0, wide=0, no_ball=0, extras=0, run_rate=0, is_submit= False, total_run= 0)
		match_obj.no_of_overs= None
		match_obj.no_of_players= None
		match_obj.end_datetime= None
		match_obj.type_of_bowl= ""
		match_obj.toss_win= None
		match_obj.umpire1= None
		match_obj.umpire2= None
		match_obj.umpire3= None
		match_obj.umpire4= None
		match_obj.scorer1= None
		match_obj.scorer2= None
		match_obj.commentator= None
		match_obj.match_refree= None
		match_obj.first_ining= None
		match_obj.winner= None
		match_obj.is_live= False
		match_obj.is_canceled= False
		match_obj.is_draw= False
		match_obj.is_1st_ining_over= False
		match_obj.is_2nd_ining_over= False
		match_obj.is_break= False
		match_obj.is_result_declared= False
		match_obj.man_of_the_match= None
		match_obj.m_type= ""
		match_obj.m_format= ""
		match_obj.won_by= ""
		match_obj.save()
		return HttpResponse(json.dumps({'success':True}))
	except:
		traceback.print_exc()
		return HttpResponse(json.dumps({'success':False}), status= status.HTTP_400_BAD_REQUEST)
	
class ResetFixtureViewSet(viewsets.ViewSet):
	permission_classes = [customPermission, ]
	
	def create(self, request):
	
		try:
			print(request.data)
			match_id= request.data.get('match_id')
			tournament_id= request.data.get('tournament_id')
			tour_obj= Tournament_Detail.objects.get(Q(pk= tournament_id) & (Q(admin= request.buser) | Q(manager= request.buser)))
			match_obj= Match_Stat.objects.get(Q(match_id= match_id) & (Q(tournament_group__tournament_round__tournament= tour_obj) | Q(tournament_round__tournament= tour_obj)))
			player_qs= Team_Match_Player.objects.filter(match_stat= match_obj)
			team_qs= Match_Stat_Team.objects.filter(match_stat= match_obj)
			bat= Match_Stat_Batting.objects.filter(match_stat_team__match_stat= match_obj)
			bowl= Match_Stat_Bowling.objects.filter(match_stat_team__match_stat= match_obj)
			wicket= Match_Stat_Fall_Of_Wickets.objects.filter(match_stat_team__match_stat= match_obj)
			live= Match_Stat_Live.objects.filter(match_stat= match_obj)
			over= Match_Stat_Over.objects.filter(match_stat_team__match_stat= match_obj)
			out= Match_Stat_Out.objects.filter(match_stat= match_obj.id)
			bowling= Match_Stat_Bowl.objects.filter(match_stat_over__in= over)
			interval= Match_Stat_Interval.objects.filter(match_stat= match_obj)
			scorer= Match_Stat_Scorer.objects.filter(match_stat= match_obj)
			penalty= Match_Stat_Penalty.objects.filter(match_stat_team__match_stat= match_obj)
			partnership= Match_Stat_Partnership.objects.filter(match_stat_team__match_stat= match_obj)
			partnership.delete()
			penalty.delete()
			scorer.delete()
			interval.delete()
			bowling.delete()
			out.delete()
			over.delete()
			live.delete()
			wicket.delete()
			bowl.delete()
			bat.delete()
			player_qs.delete()
			team_qs.update(run_against=0, penalty_run=0, wickets=0, overs=0, bowl=0, byes=0, leg_byes=0, wide=0, no_ball=0, extras=0, run_rate=0, is_submit= False, total_run= 0)
			match_obj.no_of_overs= None
			match_obj.no_of_players= None
			match_obj.end_datetime= None
			match_obj.type_of_bowl= ""
			match_obj.toss_win= None
			match_obj.umpire1= None
			match_obj.umpire2= None
			match_obj.umpire3= None
			match_obj.umpire4= None
			match_obj.scorer1= None
			match_obj.scorer2= None
			match_obj.commentator= None
			match_obj.match_refree= None
			match_obj.first_ining= None
			match_obj.winner= None
			match_obj.is_live= False
			match_obj.is_canceled= False
			match_obj.is_draw= False
			match_obj.is_1st_ining_over= False
			match_obj.is_2nd_ining_over= False
			match_obj.is_break= False
			match_obj.is_result_declared= False
			match_obj.man_of_the_match= None
			match_obj.m_type= ""
			match_obj.m_format= ""
			match_obj.won_by= ""
			match_obj.save()
			return HttpResponse(json.dumps({'success':True}))
		except:
			traceback.print_exc()
			return HttpResponse(json.dumps({'success':False}), status= status.HTTP_400_BAD_REQUEST)
		



