from django.conf.urls import include, url
from tournament import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(r'tournamentDetail', views.Tournament_DetailViewSet, base_name= 'tournament_detail')
router.register(r'tourCourt', views.Tournament_Venue_MapViewset, base_name= 'Tournament_Venue_MapViewset')
router.register(r'teamRegisteration', views.Admin_Team_RegistrationViewSet, base_name= 'Team_Registration')
router.register(r'round', views.Tournament_RoundViewSet, base_name= 'Tournament_Round')
router.register(r'group', views.Tournament_GroupViewSet, base_name= 'Tournament_Group')
router.register(r'roundTeam', views.Tournament_Round_Team_MapViewSet, base_name= 'Tournament_Round_Team_MapViewSet')
router.register(r'groupTeam', views.Tournament_Group_Team_MapViewSet, base_name= 'Tournament_Group_Team_MapViewSet')
router.register(r'createMatch', views.createMatch, base_name= 'createMatch')
router.register(r'match', views.FixtureViewSet, base_name= 'FixtureViewSet')
# router.register(r'completeRound', views.Tournament_Round_CompleteViewSet, base_name= 'Tournament_Round_CompleteViewSet')
# router.register(r'result', views.ResultViewSet, base_name= 'ResultViewSet')
router.register(r'shiftTeam', views.ShiftTeamViewSet, base_name= 'ShiftTeamViewSet')
router.register(r'redirectDemo', views.RedirectDemo, base_name= 'redirectDemo')
router.register(r'tournamentSponsor', views.Tournament_SponsorViewset, base_name= 'Tournament_Sponsor')
router.register(r'editSponsor', views.EditSponsor, base_name= 'EditTournament_Sponsor')
router.register(r'getRoundTeam', views.GetRoundTeam, base_name= 'getRoundTeam')
router.register(r'rule&Faq', views.RulesFaqViewSet, base_name= 'RulesFaqViewSet')
router.register(r'points', views.PointViewSet, base_name= 'PointViewSet')
router.register(r'player', views.playerTeamViewSet, base_name= 'playerTeamViewSet')
router.register(r'onLoad', views.OnLoadTournament, base_name= 'OnLOadTournament')
router.register(r'listTournament', views.ListTournament, base_name= 'ListTournament')
router.register(r'profile', views.ProfileViewSet, base_name= 'ProfileViewSet')
router.register(r'myTournament', views.MyTournament, base_name= 'MyTournament')
router.register(r'gameweek', views.GameWeek, base_name= 'gameweek')
router.register(r'matches', views.Match, base_name= 'matches')
router.register(r'media', views.Media_Details, base_name= 'media')
router.register(r'resetFixture', views.ResetFixtureViewSet, base_name= 'ResetFixtureViewSet')
urlpatterns = [

    url(r'^',include(router.urls)),
    url(r'^reset_match', views.reset_match),

]
